
![Des livres en communs, vers de nouveaux imaginaires](DLEC-logo.png)

Se démarquant de l’édition classique, *Des livres en communs* propose des « livres libres » parce qu’ils sont placés sous une licence qui permet au lecteur de disposer des mêmes libertés qu’un utilisateur de logiciels libres. Ces ouvrages s’inscrivent dans cette culture des biens communs qui favorise la création, le partage, la diffusion et l’appropriation collective de la connaissance.

Pour plus d’informations sur le projet *Des livres en communs*, consultez [https://deslivresencommuns.org/](https://deslivresencommuns.org/)

Framasoft est une association d’éducation populaire loi 1901 fondée en 2004, financée par vos dons, qui tente de contribuer à une société empreinte de justice sociale où le numérique permet aux humain·es de s’émanciper, à contre-courant des imaginaires du capitalisme de surveillance.

Pour plus d’informations sur Framasoft, consultez [http://www.framasoft.org](http://www.framasoft.org)

ISBN : 979-10-92674-36-1\
Dépôt légal : Octobre 2024\

Copyright 2024 : Margaux Lallemant, Timothé Bodo, Framasoft (coll. Des livres en communs)

*L’amour en commun* est placé sous [Licence Creative Commons By-Sa](https://creativecommons.org/licenses/by-sa/3.0/fr/).


À tous·tes celleux que nous aimons, \
À tous·tes celleux qui tentent de construire des nous sans créer des eux.

# Préface
« Les espèces non sociables (…) sont condamnées à dépérir. »  
Pierre Kropotkine, *L'Entraide, un facteur de l'évolution*,  éd. Hachette, 1906, p. 319

Lorsque la proposition de Margaux et Timothé nous est parvenue, elle proposait de se pencher sur l’amour en tant que « ciment, […] force motrice, […] moyen d’organisation et […] éthique pour construire une société désirable ». Fort prudemment, la forme que cela devait revêtir était majoritairement définie par ce que cela ne serait pas, sans s’aventurer à en préciser la portée exacte, précaution utile et modeste face à l’ampleur de la tâche.

Avant de s’attaquer à la rédaction, ils se sont accordé le temps de parcourir de nouveaux textes, de confronter des expériences de vie à leur hypothèse première et peu à peu s’est dessiné un ouvrage déroutant, bien plus général et ambitieux. Le sujet qui devait être suivi en son déploiement est devenu objet d’étude préalable, questionné dans ses implications et narratifs, histoire d’en mieux comprendre la réalité et les notions sous-jacentes. Immanquablement, cette nouvelle approche questionnait plus globalement la façon dont tout cela se déployait de façon générale dans nos sociétés. Il s’est donc imposé d’en tracer au moins quelques développements, préalables nécessaires pour proposer des voies où essayer d’en infléchir le cours là où ses ressorts leur paraissaient le plus suspect voire toxique. Et il fallut marquer une limite malgré l’engagement dont iels faisaient preuve, car le simple essai allait finir encyclopédie. Il subsiste donc certainement autant de questions qu’au départ, mais en d’autres points. La surface de contact avec l’inconnu n’a guère été réduite, simplement déplacée.

Il n’est qu’à voir les insectes eusociaux pour prendre conscience du formidable atout évolutif qu’est la sociabilité. À celle-ci le primate humain ajoute la culture, terme valise qui peut englober toutes les spécificités des différents groupes sociaux dans la façon dont se déploient, entre autres, les stratégies et outils relationnels. Et si Adam Smith, et libéraux et né(cr)o libéraux à sa suite, identifient l’intérêt personnel comme unique moteur d’engagement, Margaux et Timothé proposent une motivation autrement plus enthousiasmante : l’amour.

Bien loin d’une vision naïve, romantique et hétérosexuelle du terme, ils envisagent même de l’étendre à du non humain. Puiser dans une pratique sociale qui nous a mené là où nous en sommes les moyens de dépasser nos problèmes actuels. Redéfinir les limites du monde que nous avons jusque là envisagées, et dont un grand nombre semblent atteintes, non pas par une nouvelle ruée vers l’ouest, vers de nouvelles étendues à conquérir sans égard pour ce qui pourrait s’y trouver, mais en changeant de regard sur nos territoires si familiers. C’est là une vision très politique de l’amour. Très politique également de toujours imbriquer l’individu dans un collectif, subi ou choisi, et de questionner les pratiques à l’aune de leur passage à l’échelle ou des répercussions au-delà de l’humain. 

Il leur a semblé logique qu’en guise de conclusion se déploie un texte fictionnel, une proposition littéraire et poétique qui n’a rien de définitif. Avec l’espoir que le résultat saura germer en de nouvelles œuvres, nourries du terreau de leurs propositions préalables.

Tout cela fut un travail ardu, qui les a mené·es en des chemins imprévus, et qui demeure empli de bifurcations à reconnaître, de paysages à arpenter, d’horizons à dévoiler. Mais le format de l’ouvrage, l’accessibilité de ses sources ainsi que le choix qu’iels ont fait de le verser dans les communs permettra à toutes celles et ceux qui le souhaitent de le reprendre, l’amender, l’enrichir, le questionner, le transformer, se l’approprier. Une magnifique déclaration d’amour de leur part, envers nous tous·tes.

Yann Kervran, éditeur pour *Des livres en communs*

# Introduction

## D’où vient l’essai « L’amour en commun » ?

L’amour en commun est le fruit de plusieurs rencontres. D’abord, celle
de Timothé et Margaux, colocataires depuis plus de deux ans,
passablement éméchés dans la cuisine. Nous revenons de nos soirées
respectives et de notre discussion naît l’envie d’écrire les histoires
d’amour de nos ami·es. Le constat : il y a de quoi faire un livre de
toutes ces modalités amoureuses.

Quelques jours plus tard, nous découvrons l’appel à projet « Vers un
monde plus contributif, plus solidaire, plus éthique et plus libre :
comment s’outiller et s’organiser ensemble ? » de la maison d’édition
Des livres en communs, gérée par l’association Framasoft. Une bourse
d’écriture est dédiée au travail de création des auteur·ices
sélectionné·es, pour assurer leur rémunération et pouvoir ensuite verser
le livre dans les communs, soit diffuser l’ouvrage en ligne
gratuitement, sans barrière à l’achat.

Et nous pensons que ça peut coller avec notre envie d’écrire. Nous
intitulons notre projet : « L’amour en commun. *Moyen d’organisation et
moteur d’engagement pour une société alternative au capitalisme* ».
Voici notre proposition initiale.

Pour nous, l’amour constitue un ciment, une force motrice, un moyen
d’organisation et une éthique pour construire une société désirable.
Galvaudé, mal et sur-employé, ce concept a été vidé de son sens. Pour
contrer cela, nous voulons revenir à sa racine. La définition grecque de
l’amour propose quatre nuances : *éros* (l’amour physique), *agape*
(l’amour spirituel), *storgê* (l’amour familial) et *philia*
(l’amitié, le lien social). Ces piliers recouvrent de multiples rapports
sociaux dans lesquels les individus sont imbriqués. En suivant cette
définition, l’amour peut englober les pratiques du soin, de l’empathie,
du désir…

Comment l’amour peut-il recouvrir ces notions aux définitions
différentes et parfois opposées, sans devenir un contenant informe et
sans substance ? Nous prenons le parti d’affirmer que ces notions se
rejoignent, en ce qu’elles portent une attention à l’autre et tournent
la focale vers l’extérieur de l’individu. L’amour parle de notre rapport
à l’altérité. En ceci, le concept d’amour, redéfini, peut constituer la
base d’une société du commun.

Parler d’amour nécessite de reconnaître ses points aveugles :
entrelacement historique de l’amour romantique, du modèle du mariage et
du patriarcat, motif de légitimation de violences conjugales, voire
d’homicides, rapports de domination, misère sexuelle et affective des
personnes exclues du marché de l’amour…

L’amour est un terme parapluie, qui abrite de nombreux sens et registres
justificatifs. Nous souhaitons retirer ce parapluie des mains qui
l’utilisent à mauvais escient, pour qu’il redevienne un abri pour nous
tous·tes. Accepter les définitions imposées socialement sans les
questionner, c’est se faire voler le débat. Redéfinir l’amour bâtit des
alternatives. Nous faisons le choix de nous focaliser sur son potentiel
positif, afin de pousser à l’action et la mise en commun.

Redéfinir l’amour pour cimenter notre société ne relève pas de la
mièvrerie. Cela ne signifie pas qu’il faut toujours aimer, supprimer le
conflit et la lutte, car aimer pousse à lutter. Cela revient à opérer
une bascule pour que le « prendre soin » devienne la norme. C’est
repartir de l’éthique au sens de Davreux, et se poser continuellement la
question « comment être le moins salaud possible ? ». Cela permet de
reconnaître les espaces et les configurations où l’amour n’est pas
possible.

Dans ce livre, nous souhaitons interroger comment le commun de l’amour,
en tant que moyen d’organisation et moteur d’engagement, permet de
construire une alternative à la société capitaliste.

## Comment notre pensée a évolué ?

Les idées qui sous-tendaient notre propos ne se retrouvent pas toutes
dans le livre. Il nous semble cependant important de les énoncer, pour
comprendre comment notre pensée a cheminé sur le sujet.

Pour faire de l’amour un « commun », c’est-à-dire un concept qui parle
de notre rapport à l’autre plutôt que de nos relations amoureuses ou
familiales, nous voulions penser l’amour hors des structures sociales
préétablies (couple hétérosexuel et monogame), pour choisir nos contrats
et nos croyances (notamment via la spiritualité), et explorer de
nouvelles manières de vivre ensemble (coloc-famille). Si notre réflexion
s’appuie beaucoup sur la critique de l’amour romantique et l’ouverture
de l’éventail relationnel pour décentrer le couple monogame, ainsi que
sur la multiplicité des familles existantes, nous avons abandonné la
spiritualité sur le chemin. Nous trouvions cette voie trop dangereuse,
politiquement critiquable, pas assez ancrée dans le réel.

La deuxième partie du livre souhaitait montrer comment l’amour peut
devenir un commun suscitant l’engagement et l’action, notamment dans la
défense d’un espace collectif, ou la création de solidarités dans les
luttes. Ici, nous souhaitions nous appuyer sur des expériences concrètes
comme celle de la ZAD de Notre-Dame-Des-Landes, ou l’ouverture de
squats. Nous avons abandonné le traitement des squats car nous
craignions que notre méconnaissance du sujet nous mène sur des fausses
pistes. Si nous parlons toujours de la ZAD dans la version actuelle du
livre, l’épreuve des faits nous a poussé à adopter une vision plus
critique de ce qui s’est passé après l’abandon du projet d’aéroport à
Notre-Dame-Des-Landes.

Finalement, dans ses développements, le livre final n’a pas grand-chose
à voir avec le projet initial. Et l’évolution de notre pensée est sans
aucun doute ce que nous retirons de plus riche de notre processus
d’écriture.

## Comment écrire à deux ?

Le processus d’écriture à quatre mains a aussi influencé l’essai tel
qu’il est aujourd’hui. Pour tenir compte de nos temporalités
différentes et des importantes recherches nécessaires à l’écriture, nous
avons décidé de nous répartir les chapitres qui nous tenaient le plus à
cœur. Il fallait aussi tenir compte de nos ressources différentes :
Timothé a un travail salarié, Margaux est intermittente du spectacle et
consacre plus de la moitié de son temps de travail à l’écriture. Si nous
avons pour des questions organisationnelles chacun·e écrit nos
parties, nous avons beaucoup échangé sur le contenu. Avec le recul, nous
pensons que cela offre à l’essai plusieurs niveaux de lecture, puisque
nous traitons les sujets avec nos propres perspectives. Cela se ressent
dans la manière dont nous parlons de notre essai, qui est très
différente. Quand nos connaissances nous demandent de quoi parle notre
livre, Margaux répond qu’il parle d’amour, Timothé de relations.

Cela rend visibles nos points de départ respectifs et ce qu’ils
induisent dans la construction du livre. Pour Margaux, l’écriture part
d’un questionnement amoureux : pourquoi est-il si difficile de vivre la
non-exclusivité aujourd’hui ? Pourquoi le couple et l’amour romantique
ne sont pas satisfaisants non plus ? L’idée de base est de montrer que
ce sentiment n’est pas une question individuelle, mais un fait social
qui appelle à questionner les structures de la société. Autrement dit,
concevoir nos relations amoureuses autrement ne peut pas faire l’objet
de tentatives individuelles.

Pour Tim, l’écriture part de la recherche d’un projet de vie : comment
faire famille hors des schémas hétéros mais sans être Queer ? Pour cela
il faut interroger ce qu’est la famille et ce que nous pourrions en
faire pour ouvrir des pistes sur la parentalité et les foyers. Dans les
familles, il n’y a pas que de l’amour, tout comme dans nos relations
vis-à-vis de nos besoins ou des vivants. C’est pour cela qu’au fil de
l’écriture « relations » lui a semblé plus adapté « qu’amour » pour
décrire le contenu de cet essai.

Ces interrogations nécessitent de réinterroger l’ensemble de la
société : le rapport binaire au genre, au vivant, la place du soin et du
travail dans nos sociétés… L’essai part de « l’amour » amoureux pour
questionner le rapport à l’autre et les structures qui nous poussent à
créer de l’altérité.

Les chapitres rédigés par Margaux s’ouvrent avec les sinogrammes 荣霞燕

Les chapitres de Tim avec s’ouvrent avec la suite de symbole (éçè)

Concrètement, il est important de dire ce que ce livre n’est pas : ce
livre n’est pas scientifique. Ce livre est militant, ce livre est
poétique, ce livre est témoignage, ce livre est boîte à outil, ce livre
est chemins de traverses, questionnements personnels et sociaux. Et
c’est ce qui nous plaît : s’inspirer de la vie réelle et de témoignages
d’individus sur leurs pratiques, exposer nos idées communes en retraçant
le fil de nos lectures et de nos rencontres, explorer poétiquement les
possibles et donner une assise imaginaire à nos tentatives de
réinvention. Ce livre est humble, ce livre est aimant, il accueille donc
toutes nos velléités de recherche-création, en s’adressant à toutes
celles et ceux qui souhaitent contribuer à penser une société du commun.

## De quoi parle « L’amour en commun » aujourd’hui ?

Au départ, l’essai souhaitait interroger comment le commun de l’amour,
en tant que moyen d’organisation et moteur d’engagement, permettait de
construire une alternative à la société capitaliste. Finalement, cet
essai parle plutôt des structures sociales qui entravent le fait de
pouvoir nouer des relations épanouies, en nous poussant à construire de
l’altérité. Il propose tour à tour de les subvertir, de les contourner,
de les détruire, au cas par cas. Le livre s’articule comme suit :

La première partie vise à sortir l’amour du prisme romantique et
familial. Elle étudie comment l’amour au sens grec *eros* peut être
investi de multiples manières, pour ouvrir le spectre relationnel et
questionner l’hégémonie hétérosexuelle et monogame. Elle réinvestit
ensuite l’amitié (*filia*) comme point de départ pour construire des
familles choisies et décentrer l’amour de la famille nucléaire. En
interrogeant les modèles amoureux et familiaux, cette partie donne des
clés concrètes pour concevoir l’amour comme un ensemble de ressources
(affection, temps, biens matériels et immatériels) partagé au sein d’un
tissu relationnel dont le couple n’est plus au centre.

La deuxième partie montre que les tentatives individuelles de
réinvention des modèles relationnels sont vouées à l’échec si nous ne
prenons pas en compte les structures sociales qui les modèlent. Ainsi,
parler de l’hégémonie du couple monogame et hétérosexuel revient à
questionner la construction du genre en lui-même, et par cela la
création de l’altérité. Nous appliquerons la notion d’intersectionnalité
au domaine du soin, pour comprendre comment les oppressions se cumulent,
mais aussi dans la perspective de construire une société où nous
pourrions prendre soin de nous. Cela nécessite de revoir nos
attachements et nos besoins, car nos organisations sociales dépendent
fortement de nos désirs de consommation et des choix de production de
biens et services. Cette partie se clôture sur un élargissement de notre
réflexion sur l’altérité au vivant. Nous défendons que le binarisme, en
excluant et hiérarchisant, permet de comprendre l’exclusion et la
domination de la « Nature » comme des minorités de sexe et de genre.
Nous proposons d’autres manières de nous relier au vivant, suivant
l’exemple de la communauté biotique.

Après avoir traversé théoriquement plusieurs champs qui pourraient
nourrir la réinvention d’une société autour de l’amour comme commun,
nous nous intéressons à des exemples de lutte qui ont tenté de combattre le capitalisme. Nous parlons de l’âge d’or des pirates comme
insurrection contre le développement capitaliste et impérialiste du
17^e siècle. Nous abordons la lutte de la ZAD de Notre-Dame-Des-Landes,
en ce qu’elle a tenté d’expérimenter un autre rapport au territoire et
au vivant. À travers ces récits, nous questionnons la place du mythe
dans les imaginaires militants. Enfin, nous ouvrons le livre sur une
fiction. L’histoire de Cléo, Petite Tête, Ayo, Elio, Amna. L’histoire de
pirates, de communautés autonomes, de renards et de sauterelles. Une
histoire d’amourS, en somme.

# Vivre ensemble : je t’aime un peu, beaucoup, à la folie, communément 

荣霞燕

Pendant l’été 2021, j’ai lu *Anna Karénine* de Tolstoï. Le roman se
passe à la cour russe du 19^e siècle. C’est l’histoire d’un amour
passionnel entre Anna et le comte Vronski. Anna est mariée à un autre
homme, qu’elle aime finalement peu. Vronski fait la cour à Kitty, qui
est très éprise de lui. Mais lors d’un bal, Anna et Vronski se
rencontrent. Et plus rien n’a d’importance.

Anna est plus vieille que Vronski. Elle est décrite comme une femme
d’âge mûr, sûre d’elle, drôle et désirée. Elle représente quelque chose
d’inaccessible que Vronski veut conquérir. Je me souviens avoir admiré
Anna et m’être identifiée à elle. À l’époque je suis en relation libre.
En fait, je n’ai plus vraiment de relation fixe, je vois des amants, je
veux être une femme mûre, sûre d’elle, drôle et désirée. Or Anna, c’est
la séduction. C’est aussi le désir qui se transforme en amour intense et
la pousse à quitter mari et enfant pour partir avec Vronski.

Ce livre me saisissait parce qu’il décrivait les deux pôles
contradictoires que l’amour romantique valorise : la passion et
l’engagement.

J’ai noté ce passage du livre, prononcé par Vronski à Anna : « *À mes
yeux, vous et moi ne faisons qu’un. Et je ne vois dans l’avenir aucune
tranquillité ni pour vous ni pour moi. Je ne vois en perspective que le
malheur et le désespoir… ou le bonheur, et quel bonheur !… Est-il
donc vraiment impossible ?*».

Je pensais qu’il y avait quelque chose d’irrésistible et de terrifiant
dans cette phrase. Dans cette idée de fusion et de communauté de destin,
je trouvais la figure de l’engagement. Et cela justifiait cette histoire
d’amour compliquée, la souffrance, pour un bonheur potentiellement
partagé.

Mais c’était oublier un personnage. Quand j’ai lu *Anna Karénine*, j’ai
peu valorisé Kitty. Elle est décrite comme une jeune femme -- naïve,
joyeuse -- excentrique, simple -- d’esprit, éperdument amoureuse de
Vronski -- cocue. Après les tirets, tous les jugements que j’ai porté
contre sa personne. Mais en Kitty, il y a une forme de loyauté. C’est
moins valorisé socialement.

À reprendre ma lecture aujourd’hui, je complexifierais les choses.
Disons que dans une logique d’identification, relevant d’un niveau de
cour d’école élémentaire (« *moi je suis elle* »), je naviguais entre
une version de moi-même répondant à une vision moderne de
l’épanouissement amoureux et sexuel (la passion, Anna) et une version de
moi-même attachée à des modèles plus classiques de construction du
couple et de sécurité (l’engagement, Kitty), pour lesquels j’entretiens
une forme d’amour-répulsion. Merci, ça fera 60 euros la séance.

Ceci étant dit, j’ai envie d’interroger ce qui me pousse à être une Anna
quand j’ai envie d’être une Kitty, ou à être une Kitty quand je voudrais
être Anna. Quelles sont les injonctions qui traversent les relations
amoureuses contemporaines ? Comment recomposons-nous nos relations à
partir des principes d’émancipation et de féminisme ? Est-ce compatible
avec l’engagement ? Comment faire pour ne pas tout plaquer dès qu’un
Vronski nous fait une cour passionnée ?

J’aimerais aussi détourner la focale du duo Anna-Vronski, pour regarder
de plus près la relation d’Anna et de Kitty. Avant la rencontre avec le
nom en V qu’il-ne-faut-pas-prononcer, Anna et Kitty entretiennent une
forme d’amitié. Au vu de leur différence d’âge, je dirais même qu’il se
joue entre elles un lien de l’ordre de la sororité, basé sur la
bienveillance d’Anna et l’admiration de Kitty. Cette bienveillance se
transforme en sarcasme, et cette admiration en jalousie, dès que V
rentre dans le jeu.

Alors moi, à l’automne 2022, je me dis : comme d’habitude, il y a un
triangle amoureux dont l’homme est le centre, et comme d’habitude, les
deux femmes en jeu dans ce rapport de pouvoir inégal renoncent à leur
relation, pour entrer en compétition. Qu’est-ce qui se serait joué en
dehors des codes romantiques ? Que ce serait-il passé si nous
valorisions socialement autant les relations amicales que nos liens
amoureux ? Si Kitty et Anna s’étaient retrouvées le lendemain du bal,
autour d’un café-debrief ? Peut-être auraient-elles échangé leurs
impressions et conclu d’un commun accord que V était un vieux mec qui
les avait toutes deux mises dans l’embarras ?

Tolstoï, et si Kitty et Anna avaient lâché Vronski pour aller vivre
ensemble à la campagne ? Avec Timothé, nous vous proposons d’imaginer la
suite de l’histoire.

## De l’amour romantique à l’amour en commun : sortir du phénomène d’enclosure 

L’une des idées de départ de ce livre et de nos questionnements sur l’amour, c’est la réduction de ce terme à l’amour romantique, et donc in fine, la réduction de l’amour au couple.

Un point de définition s’impose. Quand nous parlons d’amour romantique
dans ce livre, nous englobons un certain nombre de phénomènes. Si nous
reprenons la définition de l’amour en Grèce antique, celui-ci se décline
en quatre sentiments : *éros* (l’amour romantique à notre sens),
*philia* (l’amitié), *agapè* (l’amour spirituel) et *storgê *(l’amour
filial). Eros, c’est le sentiment d’être amoureux, c’est le mélange du
désir sexuel et de l’intimité. De ce que j’ai retenu de ma lecture d’Eva
Illouz, historiquement, l’amour romantique, c’est aussi le passage d’une
union économique entre deux familles, à la formation de couples selon le
libre choix des individus (du moins théoriquement, en ce qui concerne
leur partenaire).

L’amour romantique est un ensemble de normes, véhiculée par la culture
occidentale et façonnant des imaginaires largement partagés. Il se base
sur trois piliers : la prédestination, la fusion, l’éternité. La
prédestination, c’est le mythe d’Aristophane. Au banquet de Platon,
Aristophane explique qu’un jour, les hommes et les femmes étaient
androgynes. Après un châtiment divin, ils furent coupés en deux,
laissant émerger le masculin et le féminin. Chacun des êtres fut
condamné à chercher sa moitié, pour retrouver l’unité originelle. Nous
aurions donc une âme sœur (et du sexe opposé). La fusion, c’est l’idée
qu’une fois les amant·es relié·es, iels formeraient une entité
surplombante, ce couple qui a une existence indépendante des êtres qui
le composent. Enfin, l’éternité désigne le fait que cette fusion est
immuable : le grand amour, c’est *jusqu’à ce que la mort nous sépare*.

L’amour romantique est une construction sociale. Sur le modèle de l’âme
sœur, et dans la société occidentale, il s’est historiquement
matérialisé sous la forme du couple. Un amour monogame, tourné vers la
sphère privée, où la promesse de bonheur et d’unité justifiait un ordre
patriarcal et des inégalités genrées, au plus près de l’intime.

Nous ne disons pas que la monogamie pose problème -- en revanche,
certain·es des auteur·ices que nous citons le disent. Ce n’est pas la
pratique que nous remettons en cause, mais son agencement dans un
système d’exclusion d’autres personnes hors du marché amoureux et sexuel
normé, c’est-à-dire monogame et hétérosexuel, ainsi que la compétition
qu’elle crée entre les individus. Peu ou prou, ce que nous disons, c’est
qu’il existe d’autres horizons amoureux et relationnels. Et que oui,
dans un monde merveilleux, nous saurions tous être polyamoureux, mais
que ce n’est pas si simple que ça.

L’idée n’est donc pas de prôner un modèle plutôt qu’un autre, mais
d’ouvrir l’éventail des possibles en questionnant l’hégémonie du
système monogame hétérosexuel, dont nous cherchons à révéler l’aspect
politique. Nous nous appuyons notamment sur le travail de Brigitte
Vassallo, qui montre que le système monogame fonctionne grâce à des
mécanismes d’identification, d’exclusion et de compétition.

Nous recherchons aussi une forme de neutralité vis-à-vis de la
présentation des différents modèles amoureux (monogamie, non-exclusivité
sexuelle et/ou amoureuse, polyamour, réseaux affectifs etc.). Avant
tout, parce que nous ne sommes pas coachs d’épanouissement sexo-affectif
et que nous nous nous garderions bien de vous donner des conseils sur le
« bon » modèle à adopter. Mais aussi, parce qu’il nous semble plus
pertinent d’analyser ce que le néolibéralisme et le patriarcat en tant
que superstructures font à ces différents modèles. Il n’y a pas de
recette magique.

### De la nécessité du dépassement du modèle de l’amour privatif

Nous parlons ici d’amour privatif, en ce qu’il fait de l’amour la
propriété de certains en excluant les autres. Par exemple, la propriété
du couple par rapport aux amis, de la famille par rapport au reste du
monde, du/de la partenaire principal·e par rapport aux relations
sexo-affectives dans le cadre d’une relation ouverte. L’amour privatif
repose selon nous sur une forme de droit de propriété à conserver, pour
les privilèges qu’elle nous octroie. Ces privilèges peuvent varier selon
les formes de relation : partager plus de temps, d’attention, de
capitaux économique, culturel, social…

Nous ne nions pas la valeur de cette mise en partage, ni même le fait de
l’accorder différemment aux personnes avec lesquelles nous relationnons.
Ce que nous souhaitons pointer du doigt, c’est que nous sommes
conditionnés socialement à investir ces ressources exclusivement dans
nos partenaires romantiques. Selon nous, une mise en commun de ces
ressources prenant en compte l’ampleur et la complexité de nos champs
amicaux, sexuels et affectifs permettrait de vivre des relations plus
riches et apaisées.

Le terme d’amour privatif se substitue à la notion de monogamie, de
couple exclusif, parce que nous pensons -- espérons -- qu’il est
possible de vivre le modèle amoureux à deux, sur des bases qui
n’entraînent pas une mise en compétition des autres pour le reste de
ressources que nous voulons bien leur accorder, après avoir tout donné à
notre partenaire romantique. Parce que nous pensons qu’il est possible
de construire son couple à l’intérieur du paysage relationnel de notre
vie, sans être au centre. Parce que *je* pense que la non-exclusivité
amoureuse et sexuelle est un droit et une victoire précieuse de la
révolution féministe, mais que dans un contexte néolibéral et
patriarcal, ce n’est pas la réponse à tout.

#### Un modèle basé sur l’exclusion, la compétition et l’individualisme

Dans cette partie, je m’appuie quasi-exclusivement (hihi) sur l’ouvrage
de Brigitte Vassallo, *Idéologie monogame, Terrorisme polyamoureux
*(2018). Je le précise, parce que je n’apporte pas d’idées nouvelles, je
partage une lecture précieuse dans ma vie, mes impressions et mes
réflexions à son propos. Ce livre n’a jamais été traduit en français, et
il me semblait important de porter la voix de cette autrice, son analyse
fine des systèmes amoureux et de leur imbrication dans le système
capitaliste. Je l’illustrerai aussi de références à des films ou des
livres, parce que je trouve que les représentations culturelles que nous
avons de l’amour parlent beaucoup d’imaginaires socialement partagés.

##### Plus qu’une pratique, un système

Selon Brigitte Vassallo, la monogamie est un système idéologique de
contrôle des affects qui a accompagné la construction historique de
l’État en Occident.

La monogamie, en tant que système, organise un certain nombre de
pratiques (habiter, coucher ensemble) autour d’institutions (le mariage,
le PACS), de valeurs et de règles implicites (la fidélité, la
valorisation de la maternité), aujourd’hui reconnus comme la norme
amoureuse.

En tant qu’idéologie (comprise au sens marxiste, comme une construction
intellectuelle qui explique et justifie un ordre social existant), la
monogamie vend un horizon de bonheur à celui qui entre dans les codes.
La religion vendait le paradis. Le colonialisme vendait le monde
civilisé. Le technicisme vend la résolution de la crise climatique grâce
au stockage de carbone dans le sol. La monogamie nous vend *ils vécurent
heureux et eurent beaucoup d’enfants.*

Et encore une fois, ce n’est pas la pratique qui est problématique. Je
suis contente que des gens autour de moi aient beaucoup d’enfants et
vivent heureux. Le problème, c’est que ce modèle de bonheur n’est pas
réellement accessible à tout le monde. Qu’il ne dit rien des
homosexuel·les, des personnes trans, stériles, asexuel·les, des
personnes en dehors du marché reproductif pour X raisons. Il ne dit rien
non plus des désillusions, des rapports de pouvoir genrés, du travail
gratuit des femmes, des comportements violents adoptés au nom de la
sacro-sainte fidélité… Bref, la monogamie, historiquement, c’est une
affaire de domination.

Selon Brigitte Vassallo, pour adhérer au rêve de bonheur monogame, nous
intériorisons un certain nombre de normes concernant nos affects. À
travers un ensemble de vecteurs de socialisation, nous recevons et
alimentons un imaginaire collectif sur ce que nous sommes censés
ressentir dans telle ou telle situation. Par exemple, nous sommes
heureux·ses quand quelqu’un·e nous offre des fleurs, même si nous
trouvons ça un peu ringard. Nous sommes terrifié·es par l’idée que notre
relation amoureuse se termine un jour, alors que nous savons bien
qu’aujourd’hui, il y a peu de chance de rester avec la même personne
toute sa vie. Nos réactions sont socialement construites par l’idéal
monogame, lui-même renforcé par ce partage collectif des affects.

Parmi les vecteurs de diffusion de l’idéologie monogame, j’ai envie de
m’arrêter sur l’industrie culturelle. Je connais très peu de récits qui
valorisent une femme décidant de vivre seule et de se lancer dans un
élevage de chèvre au fin fond de l’Ariège. Il y a bien *Viendra le temps
du feu*, de Wendy Delorme, ou *Les Guérillères* de Monique Wittig, qui
imaginent des communautés lesbiennes pour alimenter nos imaginaires.
Mais dans la littérature dominante (et donc monogame), l’amour en tant
qu’intrigue prend la forme d’une relation hétérosexuelle, censée aboutir
au couple. Un élément de la trame narrative peut aussi inclure une
tromperie, qui entraînera un drame émotionnel, où l’amour trépasse (ce
qui induirait la fin de l’union) ou dépasse l’événement (le couple en
ressortirait plus fort).

Je ne dis pas que l’intégralité de la littérature ou du cinéma
occidental se base sur une acception étriquée de l’amour. En revanche,
je dis que les scénarios amoureux sont extrêmement prévisibles, car
imbriqués dans l’idéologie monogame. Et que comme tout le monde, j’ai
été biberonnée à cela.

Mon film préféré, de mes douze à mes vingt ans (et je lui garde encore
une place à part dans mon cœur), c’est *Moulin Rouge*. C’est l’histoire
d’Ewan Mc Gregor, un poète maudit *extrêmement* mignon, et de Nicole
Kidman, alias Satine, une courtisane *extrêmement* élégante, qui tombent
amoureux et se chantent des chansons pour se dire ô combien ils sont
merveilleux. Satine veut tout quitter pour le poète, le poète est malade
de jalousie quand Satine doit coucher avec le comte qui paye le
spectacle dans lequel elle joue, ils chantent encore des chansons, ils
se réconcilient… Bref, c’est drôle, c’est attachant, c’est burlesque,
je vous le conseille sincèrement.

Néanmoins, les leçons que je pouvais tirer de mon film préféré, à l’âge
où je vivais mes premières relations et me formais en tant qu’adulte,
c’était :

-   L’amour est une aventure passionnée et extraordinaire

-   L’amour entraîne des sentiments intenses et irrépressibles

-   La jalousie est une preuve d’amour et d’attachement

-   Si quelqu’un m’aime iel refusera de coucher avec un·e autre

-   Si celle·lui que j’aime couche avec quelqu’un d’autre, c’est normal d’être jalouse

-   L’amour romantique triomphe de tout

-   C’est valorisant de quitter travail, famille et ami·es par amour

-   L’amour entraîne un projet de vie commun

-   Etc.

Et nous avons tous notre Moulin Rouge, qui pendant longtemps a justifié
de ressentir certaines choses plutôt que d’autres dans un contexte
amoureux.

Le problème de la monogamie hétérosexuelle, c’est donc sa naturalisation
comme forme logique de l’amour : « l’amour » devient l’amour monogame et
hétérosexuel, sans qu’on ne se pose même la question. C’est le
« naturel », là où la polygamie est le « barbare » ou le « déviant ».
Les tentatives de réinvention du modèle amoureux, notamment par les
communautés LGBTQIA+ sont considérées comme des « alternatives », et non
pas comme des modèles pensés en-dehors d’une comparaison à la
l’hétérosexualité et/ou la monogamie. Ils peinent à exister pour
eux-mêmes dans l’imaginaire collectif. La monogamie en vient à faire foi
et à définir ce qui relève de la relation amoureuse ou non.

J’ai une amie avec qui je discute beaucoup d’amour. Dans ce livre, on
l’appellera Maria. Maria m’expliquait un jour que l’une des personnes
avec lesquelles elle relationnait refusait de s’investir dans un lien
amoureux. Pour iel, l’amour amenait nécessairement de l’exclusivité, ce
à quoi iel n’était pas prêt·e. Dans cette situation, le contrat avec
un·e partenaire impliquant de ne pas avoir de relations sexuelles avec
d’autres est confondu avec la monogamie, qui est confondue avec l’amour.
Or cela empêche cette personne d’envisager de donner des marques d’amour
à ses relations sexuelles. Je trouve que c’est un bon exemple de
catégorisation monogame, où la forme amoureuse se pense à travers le
couple exclusif, sans possibilité de négociation ou de compromis.

La monogamie traverse donc nos vies de manière tentaculaire. Elle
n’impacte pas seulement nos manières de nouer des relations amoureuses,
mais aussi nos façons d’habiter, de consommer, de gérer nos ressources
temporelles et affectives. Ainsi, le projet de vie monogame, tel que
nous l’avons intériorisé dans la ligne droite *sexe -- cinéma -- dîner
-- emménager ensemble -- se marier -- avoir un gosse -- acheter une C4
Picasso*, fournit une trame narrative pour construire nos vies sur un
modèle privatif, tourné vers l’intérieur et l’enceinte familiale.

La monogamie est donc une superstructure qui détermine notre vie privée
par les lois (par exemple, des avantages fiscaux pour les personnes 
mariées), l’architecture (suites parentales et petites chambres pour les
enfants), l’économie (communauté de biens et fondation d’un patrimoine),
la morale chrétienne (devoir de fidélité et de soin mutuel entre les
conjoints et envers la progéniture).

##### Un système basé sur trois piliers

Le système monogame se base sur la suprématie du noyau reproducteur par
rapport aux autres relations, selon trois mécanismes : la valorisation
de l’exclusivité, le lien identitaire ou fusion, le renforcement de la
compétitivité et de la confrontation.

L’exclusion définit le couple par rapport à celleux qui n’en font pas
partie. Dans son livre, Brigitte Vassallo retrace le lien historique
avec l’institution du mariage entre un homme et une femme, leur
accordant des droits sexuels l’un sur l’autre et une responsabilité
parentale sur les enfants issus de l’union reproductive. De la même
manière, dans un couple monogame, les partenaires demandent
l’exclusivité du sentiment amoureux de l’autre et de sa vie sexuelle. Ce
privilège est à l’origine d’une exclusion pour toutes les personnes en
dehors du contrat de couple.

En effet, le marqueur de l’exclusivité sexuelle et affective étant
propre à la relation amoureuse, elle scelle une forme d’unicité. Cette
spécificité du couple légitime d’en faire un lien supérieur aux autres
formes de relations. La monogamie ne nie pas les liens amicaux,
familiaux, militants etc. Seulement, elle minore leur portée et leur
importance face au couple comme instance privilégiée de partage de
l’intimité.

Plutôt que de fonder l’amour sur une mise en commun des affects, elle
organise la répartition de ce que nous donnons et recevons de chacun, en
fonction du type de relation. Nous vivons en colocation un temps avec
des ami·es. Par contre, nous prévoyons d’acheter une maison avec notre
partenaire amoureux·se.

La monogamie nous positionne dans une situation de pouvoir valorisée
socialement et nous octroie des privilèges. La suprématie du couple est
fondée sur l’exclusion des autres, entraînant de fait une mise en
compétition des hommes et des femmes pour faire exister ou maintenir
dans le temps une union.

De la même manière, l’infidélité scelle le rapport entre exclusivité et
compétition. Brigitte Vassallo pointe que nous n’avons pas d’imaginaire
dans lequel être amoureux·se de quelqu’un qui est amoureux·se de
quelqu’un d’autre soit un phénomène positif. Nous nous penchons toujours
vers les personnes dont le·la partenaire va voir quelqu’un d’autre avec
un sentiment d’empathie, en les considérant comme des victimes. Cela
nourrit la peur que notre amoureux·se couche avec quelqu’un d’autre :
cela remettrait en cause l’authenticité de la relation, parce que
« s’iel m’aimait, iel n’aurait jamais fait ça » (exclusivité) et que
« s’iel l’a fait, la personne avec qui iel a couché a forcément quelque
chose que je ne lui donne pas » (compétition).

Même en ayant été dans des couples ouverts, je me rends compte que j’ai
toujours eu une vision très monogame de l’infidélité. Je parle ici
d’infidélité, parce que si nous nous étions laissé la liberté d’aller
voir ailleurs sexuellement, je dépassais scrupuleusement toutes les
limites que nous avions voulu poser pour sécuriser l’expérimentation.
Pour moi, coucher régulièrement avec quelqu’un allait développer un
partage d’intimité, qui m’amènerait à tomber amoureuse.

Et comme j’avais bien préparé mon scénario, c’est ce qui est arrivé. À
ce moment-là, je ne pouvais pas envisager de poursuivre les deux
relations en même temps. J’avais l’impression de devoir choisir --
encore une fois -- entre l’engagement et la passion. Soit le couple
établi surmontait l’infidélité, retournait à un modèle exclusif et en
sortait renforcé, soit je me lançais dans cette nouvelle relation, qui
commençait à sentir l’amour, et tôt ou tard finirait par déboucher sur
le couple.

Au-delà du sentiment de culpabilité, jamais je n’aurais pu imaginer être
amoureuse des deux personnes, ou concevoir que le sexe et l’intimité
n’amenaient pas nécessairement à l’amour. Dans mon schéma, l’un était en
compétition avec l’autre pour bénéficier de l’exclusivité de ma capacité
d’aimer (unique et unilatérale).

Dans cette conception, le bonheur de l’un implique le malheur de
l’autre. Le désir pour une autre personne est vécu comme un manque de
soin, un délaissement, une forme de désamour. Cela justifie d’opposer
soin et reconnaissance pour les siens (amoureux·ses, ami·es, familles) à
la violence sociale pour ceux qui n’appartiennent à aucun de ces
groupes. Et c’est fondamental, parce que ces noyaux amoureux et ces
appartenances groupales créent nos identités.

Selon Brigitte Vassallo, la notion de prédestination et de permanence
entraîne la fusion identitaire des deux partenaires dans le couple.

La prédestination, c’est la notion d’âme-sœur, LA bonne personne pour
soi. C’est un récit qui est très présent dans la série *How I Met Your
Mother* par exemple, où Ted Mosby parcourt New York pendant dix saisons
à la recherche de la femme de sa vie. Dans sa quête, Ted ne rompt pas
ses relations uniquement lorsqu’elles ne sont pas fonctionnelles. Il lui
arrive de cesser de s’investir dans un lien sexo-affectif naissant,
parce qu’il pressent que cette partenaire ne sera pas LA mère de ses
enfants. C’est le cas avec Nathalie dans la saison 1, l’ex qu’il avait
déjà larguée le jour de son anniversaire trois ans auparavant ! Pour
moi, derrière sa poursuite de l’amour romantique se cache une
minorisation de la valeur des partenaires temporaires, de ceux qui nous
aident un bout de chemin, de ceux avec qui nous partageons un soir, une
semaine, quelques mois.

La permanence, c’est la fiction que l’on se crée, même en sachant
« qu’au vu des statistiques, c’est de moins en moins probable », en se
racontant que l’histoire amoureuse que nous sommes en train de vivre
durera pour toujours. Par exemple, lorsque je me projette vivre à la
campagne entourée de mes amis et de mon amoureux, avoir un enfant en
PMA, un chat, vieillir ensemble en faisant de la randonnée et de la
danse. Selon Brigitte Vassallo, c’est dangereux parce que cette
projection nous empêche de fuir quand la situation avec ledit amoureux
devient moins jolie. Selon moi, ça aide à vivre.

Il n’empêche que Brigitte Vassallo montre que la prédestination et la
permanence donnent lieu à une fusion identitaire, augmentant le capital
social du couple vis-à-vis des individus seuls. C’est improbable, mais
je trouve que le film *Matrix 4* exemplifie parfaitement cette idée.

En fait, il faut s’arrêter sur l’évolution de la vision de l’amour entre
le premier film et le quatrième. Dans *Matrix 1*, l’Oracle prédit que
l’homme dont Trinity tombera amoureuse sera l’élu. C’est quand Trinity
révèle ses sentiments à Neo (qui ne croyait pas lui-même être l’élu),
que tout le potentiel de ses pouvoirs se réalise. Dans cette version, le
film verse plutôt dans une éthique de la reconnaissance, où toute valeur
sociale est forcément située dans une interaction. L’amour de Trinity
donne de la force à Neo.

Dans *Matrix 4*, le méga-méchant incarné par Neil Patrick Harris
explique qu’après la mort de Neo et de Trinity, il a voulu les
recomposer pour alimenter la matrice en énergie. Après maintes
difficultés, il a découvert qu’une fois Neo et Trinity mis en contact,
se dégageait d’eux une force surhumaine. Le méchant dit littéralement :
« l’un sans l’autre, vous n’êtes rien ». Et effectivement, dans une
scène de combat à 1000 contre 2, Neo et Trinity joignent leurs mains,
avec l’effet d’une bombe atomique. Ici, c’est le couple qui est
valorisé, pas les individus.

Il y a deux autres scènes, assez anecdotiques au premier abord, mais qui
me semblent parler de cette fusion du couple. À la fin du film, Neo se
rend compte qu’il ne peut plus voler, c’est Trinity qui y parvient et le
sauve. Et là je me dis : c’est chouette comme message en 2022 ! Sauf que
dans la scène finale, Neo vole avec Trinity, par on ne sait quel
miracle. Alors oui, on pourrait dire qu’il s’est souvenu comment voler.
Moi, je dis surtout que comme Trinity le pouvait désormais, et que
finalement Trinity et Neo c’est peu ou prou la même chose, un couple de
héros, le scénariste s’est dit que c’était totalement justifié de
redonner cette capacité à Neo.

Si ces fictions étaient inoffensives, cela importerait peu. Mais elles
nourrissent un modèle de fusion et le rendent désirable. Les ruptures
monogames sont donc forcément violentes, en tant qu’éclatement non pas
seulement du couple mais aussi de l’identité, de l’estime de soi
contingente que nous avons construite dans une relation de dépendance
exclusive à une personne.

Brigitte Vassallo envisage également les conséquences de la fusion du
couple sur nos structures politiques. Elle trace le lien entre la
suprématie du noyau reproducteur et la transmission de l’identité et du
patrimoine économique, social et culturel à travers la filiation. C’est
parce que l’héritage a été valorisé qu’il est devenu si important de
former des couples monogames et fertiles. A travers la famille, se crée
une trace et une identité groupale, une forme de transcendance. Ces
entre-soi fermés se définissent toujours par rapport à l’extérieur.
Cette frontière entre les siens et les autres se défend avec violence.

Et là commence l’analogie avec la fin des communs.

##### L’histoire amoureuse du mouvement de l’enclosure

Brigitte Vassallo dresse une analogie entre l’imposition de la monogamie
et la construction et le fonctionnement de l’État Nation -- en tant que
fermeture des frontières sur un groupe. Selon elle, la monogamie comme
la nation reposent sur une conception binaire de la société : la
différence entre homme et femme nécessaire à la reproduction justifie la
domination du modèle hétérosexuel monogame, la différence de race comme
construction théorique légitime l’existence des nations, la différence
entre nature et culture explique l’entreprise impérialiste, etc.

Selon Brigitte Vassallo, ces frontières entre le nous et l’autre sont
amovibles et définies par les groupes au pouvoir de manière
contextuelle, notamment en désignant un « eux » pour renforcer le
groupe. La Nation réutilise les mécanismes de l’exclusivité, de la
hiérarchisation et de la confrontation pour tenir les groupes ensemble,
c’est-à-dire les uns contre les autres. La Nation, dans son idéal
romantisé, pourrait se définir comme l’administration qui regroupe des
personnes partageant des codes communs et « décidant » de vivre
ensemble. La nation repose sur le mythe de l’égalité du peuple et du
choix, comme la monogamie tient sur le mythe de l’égalité des sexes et
du choix de la manière de relationner.

A l’aide d’auteur·ices comme Silvia Federici, elle retrace également le
lien politique entre le phénomène d’enclosure et la modification des
rapports sociaux, dont ceux du couple et de la famille.

À partir du 17^e siècle s’opère un processus d’enclosure, c’est à
dire de parcellisation et de privatisation de champs et de forêts qui
étaient utilisés par tous·tes pour la vie commune (habitat, médicaments,
cuisine, etc.). Mais le commerce du textile arrivant, les propriétaires
légaux décidèrent de remettre la main sur les terres qu’ils avaient
laissées jusqu’alors aux pratiques communales. Ce passage du droit
d’usage au droit de propriété entraîna la ruine de nombreux paysans qui
n’avaient pas assez de leurs terrains pour survivre et alimentèrent
l’exode rural vers les villes.

Cet exode eut également des impacts sur la vie affective et sociale des
individus : les systèmes d’entraide se délitèrent peu à peu, un
recentrement sur la famille nucléaire s’opéra. En même temps, les
célébrations communales qui créaient de l’interconnaissance et donc de
la solidarité furent interdits et remplacés par les rites de l’Eglise.
Selon Silvia Federici, « l’enclosure physique de privatisation des
terres a été amplifiée par l’enclosure sociale, le déplacement de la
reproduction des travailleurs de la campagne ouverte à l’abri, de la
communauté à la famille, de l’espace public au privé. »

Selon elles, c’est le capitalisme qui a nécessité d’imposer les unions
reproductrices monogames pour assurer la filiation (la reproduction des
classes), la transmission du patrimoine et la création de liens et
d’alliances entre les classes privilégiées. Au contraire, pour les
classes populaires, les liens de solidarité horizontaux étaient
nécessaires à la survie. Pour cela, il fallait fixer de manière immuable
les genres et établir la sexualité reproductrice et le couple
hétérosexuel comme modèle dominant, notamment en pénalisant les autres
formes de sexualité et d’union collectives.

Dans *La Grande Transformation*, Karl Polanyi retrace le passage
historique à une économie de marché comme une construction sociale et
non naturelle. Il remonte aux différents modèles qui ont été adoptés
dans le temps et l’espace pour organiser les échanges humains : la
réciprocité, la redistribution, l’administration domestique. Dans ces
systèmes, qui étaient généralement combinés de manière hybride, la
production et la distribution n’étaient pas pensées pour le gain.
L’économie de marché, elle, ne peut pas coexister avec d’autres formes
d’échange, en ce qu’elle est exclusive : le marché, institution créée
pour son fonctionnement, en vient à maîtriser le système économique,
inhiber les autres modèles et gérer la société.

Le passage de l’économie de marché à la société de marché s’opère dans
un processus de désencastrement. Jusqu’ici, l’économie est régulée par
des institutions sociales. Mais quand le marché libéral devient
autorégulateur, il transforme en marchandises ce qui était considéré
comme des biens communs, hors des logiques économiques : l’activité
humaine (le travail), la nature (la terre) et la monnaie. Les relations
sociales, écologiques et politiques sont désormais comprises dans
l’économie de marché.

À l’aune de cette analyse, Eva Illouz parle de la Grande Transformation
de l’amour qui a lieu à l’époque de la modernité tardive. Dans *Pourquoi
l’amour fait mal*, elle tente de comprendre comment l’individualisation
des modes de vie, l’économisation des rapports sociaux et l’importance
du modèle utilitariste ont impacté nos manières de vivre l’amour.

#### Une sphère marchandisée, l’amour contractuel à consommer

##### L’individualisme affectif

Selon Eva Illouz, la société moderne tardive valorise l’individualisme
affectif, soit la liberté et l’autonomie du sujet comme valeur des
relations ainsi que comme compétence. Autrement dit : être indépendant
vis-à-vis du couple et de l’autre est valorisé socialement, tandis que
la capacité à utiliser son libre arbitre pour faire « le meilleur
choix » amoureux prend de plus en plus de place dans la manière de
considérer nos relations. Or nos choix ne sont jamais totalement libres.

Dans une société de consommation néolibérale, le désir comme moteur de
choix et l’utilitarisme comme modèle de décision traversent les
relations amoureuses. Pour former notre « moi émotionnel », nous
cherchons à faire les choix les plus en accord avec ce que nous
« sommes », pour nous « épanouir », dans une logique de
responsabilisation pour notre propre bonheur qui ne dépend « que de
nous ». Nous essayons donc de mener des calculs rationnels
(compatibilité psychologique, niveau d’éducation, sex-appeal, aisance
sociale, classe sociale, signe astrologique…), reposant sur nous
seuls, afin de trouver le ou la bon·ne partenaire.

Pour l’autrice, plusieurs facteurs expliquent que l’engagement dans une
relation durable soit si compliqué aujourd’hui :

Dans un marché de l’amour qui ne fonctionne plus seulement sur
l’endogamie (se marier avec quelqu’un de la même classe sociale que
soi), et où les critères de choix s’élargissent, l’abondance des
possibilités ne facilite pas la décision. Avec cela, la volonté de
maximiser les bénéfices des relations, donc de chercher non pas ce qui
me satisfait, mais le meilleur pour moi, laisse toujours un doute
insupportable : est-ce que Joseph, le blond à lunettes de cinquième B
qui est devenu aviateur ne serait pas mieux que Julia, mon dernier match
OkCupid ?

Or l’utilisation de l’introspection mêlant choix rationnel (« tout de
même, j’aurais préféré sortir avec quelqu’un qui a fait les
Beaux-Arts ») et essentialisation des caractéristiques psychologiques
(« il téléphone tous les jours à sa mère, je suis sûre qu’il a un
problème de dépendance ») entravent la capacité à décider intuitivement
d’un engagement sur le mode de l’affect (« je l’aime bien, et si je lui
proposais d’aller au cinéma ? »).

Le modèle de l’individualisme affectif valorise donc l’épanouissement
personnel du sujet indépendant, l’amour de soi qui n’a pas besoin de
celui des autres.

##### Reconnaissance vs. autonomie comme marqueurs de valeur

Eva Illouz examine la tension qui réside dans l’injonction moderne
d’équilibre entre reconnaissance et autonomie. Pour l’autrice, la
reconnaissance est forcément obtenue en relation, c’est le gain d’une
interaction réussie avec un autre individu. Or la valeur autonomie,
valorisée dans la société néolibérale, empêche de demander de la
reconnaissance à son partenaire.

Cela me fait penser au problème des définitions : ce moment où, aux
débuts de la relation, l’un·e cherche à définir ce qu’iel est en train
de vivre avec l’autre, à reconnaître ce qui se passe. Je crois que je me
pose souvent cette question, même dans des relations qui ne sont pas
romantiques. J’ai besoin de clarifier ce que nous vivons, nos attentes
mutuelles, ce que nous sommes en droit de demander à l’autre ou pas.
Mais cela me coûte de prendre l’initiative de demander : j’ai
l’impression que cela met en vulnérabilité mon désir de faire vivre
cette relation, me positionnant comme victime si elle ne se poursuivait
pas. Par exemple, je suis restée pendant des mois à voir mon premier
amoureux tous les soirs, en martelant à mes ami·es que non, nous
n’étions pas en couple (et cela n’avait rien à voir avec une
valorisation de la non-exclusivité).

Comme nous jugeons que cadrer une relation consiste à poser des limites
à la liberté d’autrui, chose illégitime dans une éthique de l’autonomie,
demander un engagement consiste en une aliénation de l’autre et de
nous-même. Selon Eva Illouz, nous vivons les relations comme des
échanges marchands dont nous devons sortir vainqueurs, notamment grâce à
des stratégies de détachement. L’indisponibilité fonctionne comme un
signal de valeur économique : « je suis détaché·e, accroche-toi », ou
encore, « tu ne me réponds pas parce que tu n’es pas intéressé·e ».

En gérant nos relations comme l’équilibre des comptes publics (pas plus
de 3% du PIB de déficit), la réciprocité apparaît socialement comme la
preuve d’une relation équilibrée, d’un bon dosage entre autonomie et
reconnaissance. Cette norme s’appuie sur une vision utilitariste du
bien-être, souvent diffusées par les manuels de développement personnel,
nous invitant à rationnaliser notre investissement émotionnel dans les
relations qui nous apportent quelque chose. Or dans la société de
consommation, ce qui donne de la valeur, c’est le désir.

##### Champs sexuel et sexualité sérielle

Selon Eva Illouz, « la société de consommation place le désir au cœur de
la subjectivité, et la sexualité devient une sorte de métaphore
généralisée du désir ». D’un côté l’amour a été désencastré des cadres
moraux. De l’autre, la sexualité se détache de la sphère reproductive,
des liens à long-terme et de la sphère émotionnelle. L’autrice donne là
une analyse historique, pas un jugement.

De ce fait, le capital sexuel en vient à jouer un rôle de plus en plus
prégnant dans la définition de nos identités sociales. Pour les hommes,
coucher avec des femmes est valorisé socialement. Pour les femmes, être
désirée par un grand nombre d’hommes est un marqueur de réussite. Nous
retrouvons la vision guerrière de la conquête et la mise en compétition
systématique, où les hommes accèdent à un grand nombre de femmes, et où
les femmes attirent la convoitise d’un grand nombre d’hommes. On voit
qui a le beau rôle.

Selon Eva Illouz, le capital sexuel se base sur des dispositions et
codes sociaux adaptés à la séduction, qu’elle nomme le sex-appeal, et
sur le nombre de personnes avec lesquelles nous avons couché, soit
l’adoption d’une sexualité sérielle. Selon cette approche, nous
pourrions voir la non-exclusivité comme une stratégie de distinction
dans une société qui valorise de plus en plus le capital sexuel. Cela
montre la nécessité de problématiser le désir : souvent, les personnes
avec le plus de capital désirent cell·eux qui leurs ressemblent,
recréant de l’exclusion.

Cette réflexion a beaucoup accompagné la rédaction de ce livre :
parfois, j’ai l’impression de vivre la non-exclusivité comme une
nouvelle injonction. Une norme socialement partagée par la plupart de
mes ami·es, de mes amant·es, qui définit les modalités amoureuses que
nous sommes censé·es adopter en tant que jeunes déconstruit·es de
gauche. Qu’en étant féministe, il faudrait toujours vivre mon désir et
la non-exclusivité comme quelque chose de joyeux et de libérateur. Or je
ne le vis pas toujours de cette manière. Parfois, j’ai la sensation que
suivre mon désir individuel ne sera pas émancipateur en soi. Que cela
m’amènera à être négligente envers mes besoins et/ou ceux des autres.
Pour moi, la non-exclusivité, c’est une manière individualiste de voir
l’émancipation, qui à mon sens, mériterait d’être collective.

Cela me rappelle une autre discussion, qui date de ma première relation
amoureuse et qui fut récurrente à cette époque-là. L’un de nos sujets de
dispute préférés, c’était les insécurités que je pouvais ressentir
vis-à-vis d’autres filles que mon partenaire fréquentait. Ce qui
m’intéresse, c’est la polarité que nous créions autour du terme
jalousie. Pour lui, la jalousie c’était mal, parce que c’était
irrationnel. Pour moi, la jalousie c’était neutre, parce que c’était
naturel. Et aujourd’hui je pense que nous avions tort tous les deux. La
jalousie est construite socialement par le système monogame
d’appartenance et d’exclusion. Mais la jalousie est une réaction
rationnelle dans un contexte de mise en compétition systématique des
hommes et des femmes dans les champs sexuels et amoureux. Ce qui ne
revient pas à la justifier, mais à montrer que le système la crée. Pour
moi, ça me donne envie d’y aller mollo sur la non-exclusivité tant que
nous n’avons pas collectivement réglé cette histoire de compétition.

Dans le cadre d’une coexistence du marché relationnel de long-terme et
de celui de la compétition sexuelle, les relations suivent des cadres
moins rigides (on ne se marie plus après le premier rapport, et c’est
tant mieux). Une logique contractuelle de court-terme s’installe,
alimentant la peur de l’engagement. Puisqu’il y a plus de personnes et
plus de manières de nouer des relations sur le marché, il est plus
difficile de choisir et d’arrêter de chercher. Dans un contexte
patriarcal et néolibéral, ces recompositions du couple ne sont pas
neutres.

##### L’asymétrie genrée du contrat

L’un des grands concepts qui traversent le livre d’Eva Illouz est le
phénomène de domination affective, cristallisant les inégalités de
genre : *« L’apparition dans le champ des relations intimes des
contradictions du libre marché dissimule le fait qu’elles sont
accompagnées d’une nouvelle forme de domination affective des hommes sur
les femmes, qu’expriment la disponibilité émotionnelle des femmes et la
répugnance des hommes à s’engager auprès d’elles, parce que les
conditions du choix ont changé »* (p.205)

Dans la société contemporaine, le patriarcat et le fait de performer la
masculinité ne se jouent plus dans le rôle du père de famille, du patron
sympa ou au bar avec les copains. Les femmes ont progressivement eu
accès à des rôles sociaux similaires (pas toutes, pas au même prix).
Selon l’autrice, c’est le fait de mener une sexualité sérielle et
détachée de ses émotions qui parle de l’autonomie et du contrôle de soi
« masculins ».

Selon Eva Illouz, si les femmes adoptent des stratégies similaires,
c’est en réponse et comme un attribut du pouvoir. Cependant, ces
sexualités sérielles cohabitent presque toujours avec des stratégies
exclusivistes pour les femmes désirant avoir un enfant. En effet, la
contrainte temporelle biologique les amène à envisager la monogamie en
tant que fin. Pour Eva Illouz, et je la cite encore parce que cela me
parle beaucoup : *« \[cela\] fait peser sur les femmes un certain nombre
de contradictions, l’expérience de désirs conflictuels, des stratégies
affectives confuses et d’être dominées par la plus grande capacité des
hommes à refuser de s’engager ».*

Dans une vision de la relation comme une transaction, demander de la
reconnaissance est perçu comme une mise en fragilité de sa position au
sein du couple. Ainsi, les femmes endossent à la fois le rôle du soin
(apporter de la reconnaissance à l’autre) et celui de garante de leur
autonomie et de celle de leur partenaire (lui laisser de l’espace).
Elles prennent aussi en charge le travail relationnel et la charge
mentale qui en découle.

Je me souviens d’un covoiturage cet été, où j’ai partagé huit longues
heures en camion avec la conductrice. Elle m’expliquait qu’elle
s’attendait à une discussion houleuse avec son compagnon au retour. Elle
était partie une semaine seule après une dispute. Elle me disait que son
copain ne disait jamais ce qu’il ressentait, jusqu’à exploser dans des
colères violentes psychologiquement. Qu’elle tentait toujours d’amorcer
le dialogue, se préparait à l’avance pour parler de leur relation, parce
qu’il avait un esprit rationnel et rejetait ses arguments dès qu’il les
jugeait trop « émotionnels ». Qu’il disait ne pas la comprendre.
Pourtant, il était féministe ! Cette discussion est partie du fait
qu’elle demandait quelle était selon moi la différence entre justesse et
justice.

Ici, ce qui est injuste, selon moi, c’est que les hommes ont été
historiquement éduqués à performer l’autonomie. Dans le recueil *Nos
amours radicales*, Léane Alestra retrace les sources de l’idée de la
complémentarité de l’homme et de la femme. Ça a commencé avec Eve, qui a
été formée de la côte d’Adam par Dieu, pour qu’il ne s’ennuie pas trop
dans l’Eden. La (re)découverte des textes grecs à la Renaissance a
marqué un recul des droits des femmes en Occident, opposant ce qui est
masculin et féminin : la force et l’émotion, l’extérieur et l’intérieur,
le public et le privé. Cela a engendré une éducation différenciée selon
les critères masculins de la réussite et féminins du prendre soin, nous
assignant des rôles et une répartition des tâches. La complémentarité
prétendue entre les sexes s’en trouve faussée : les attentes
asymétriques du couple hétérosexuel créent la dépendance des femmes, qui
doivent servir les hommes.

Les hommes sont donc en position de domination émotionnelle vis-à-vis
des femmes et de leur désir d’attachement, les obligeant à le mettre en
sourdine et à reproduire le détachement des hommes.

#### En conclusion

Pour conclure cette première partie, j’aimerais citer encore Léane
Alestra, dans le recueil *Nos amours radicales* : « *Le cœur du problème
repose, je crois, dans la soif de posséder. Posséder des biens, posséder
les terres et posséder les femmes : sans remise en question de notre
attrait pour le profit et la propriété privée à tout prix, la domination
masculine rôdera toujours. Tant qu’il faudra posséder pour être
valorisé·e socialement, nos relations humaines seront altérées »*. Si
nous voulons changer nos structures intimes, construire des alternatives
à la vision individualiste des relations et de notre rapport à l’autre,
nous pouvons commencer par repenser nos engagements amoureux.

### De nouvelles manières de s’organiser dans les relations amoureuses 

Dans cette partie, nous allons ouvrir l’éventail des modalités
relationnelles, en dehors de la monogamie. Nous allons aussi voir
comment le couple, lorsqu’il choisit d’en être un, peut parer aux
phénomènes d’exclusion, de mise en compétition et d’individualisme.
Notre présupposé, c’est que changer nos comportements dans l’intimité,
ça ne détruira peut-être pas le capitalisme, mais ça nous aidera à
prendre conscience des rapports de pouvoir qui s’y jouent. Et peut-être
même qu’à sa mesure, l’intime peut court-circuiter le système. Quand
nous commençons à créer des relations d’empathie plutôt que de
compétition, de partage plutôt que d’exclusion, d’adaptation plutôt que
de conquête, c’est politique. Et ça nous aide, dans toutes les autres
sphères de nos vies, à concevoir la manière dont nous avons envie de
vivre, en relation avec les autres. Cela crée des nouveaux imaginaires,
où l’amour n’est plus le pré carré du romantisme mais un commun et un
principe que nous mettons en œuvre pour s’organiser.

#### Vivre des tissus relationnels plutôt qu’un couple

Une discussion avec une amie, qu’on appellera Lila, m’a donné l’image
qui pourrait illustrer cette partie. Pour elle, l’amour est un lien.
Qu’il soit amical, amoureux, passager ou durable, c’est l’intensité du
lien qui compte. Nous pouvons vivre des histoires d’amour en un regard
avec un inconnu, comme nous pouvons choisir d’élever des enfants avec
des ami·es, parce que nous les aimons. Nous pouvons aussi nous lancer
dans de grandes histoires d’amour trépidantes parce que c’est ce qui
nous fait vibrer. Il reste que l’amour est un lien, dont nous pouvons
choisir la forme et la force.

Tout de suite, j’ai imaginé modeler avec mes mains l’amour que j’ai pour
certaines personnes, les peindre d’une couleur particulière. Et surtout,
faire en sorte que les formes se fondent dans le paysage de mes
relations préexistantes. Pas seulement mes relations amoureuses, mes
relations amicales, familiales, la relation que j’ai à moi-même, mes
relations professionnelles… Donner une forme juste à un lien, c’est
faire en sorte qu’il ne lèse personne en apparaissant, en tout cas, pas
durablement.

Et je pense qu’il y a autant de personnes que de formes, dont nous
pouvons essayer de tirer les grands traits pour en faire des modèles,
que tout cela est d’une inventivité passionnante, que nous avons besoin
de ces exemples pour construire nos propres singularités. Je vais parler
de concepts, parce que c’est la manière la plus intelligible que j’ai
trouvée pour évoquer les différentes modalités amoureuses que j’ai pu
croiser dans la vie ou mes lectures. Je pense aussi que définir les
relations et partager un langage commun est important. Mais ce qui
m’intéresse, ce sont les expériences derrière, les contrats qui les font
tenir, la façon dont chacun se les réapproprie. Loin de moi l’idée de
réifier le vécu, ou de révolutionner l’amour libre. Je partage ma
compréhension personnelle de ces termes, elle est toujours discutable
(j’adore les cafés gênants !).

Pour entrer dans le vif, ouvrons l’éventail des configurations
amoureuses.

##### **Le célibat**

C’était important pour moi de commencer par le célibat, parce qu’il
prend le contrepied de l’idéal romantique pour valoriser la relation à
soi-même. Dans le recueil de texte *Nos amours radicales*, Sharone
Omankoy explique son choix du célibat. Pour elle, l’intime est politique
puisque les oppressions sociales s’y jouent au plus près de nous. En
tant que femme noire, son corps ne correspond pas au système de normes
hétéro-patriarcal. Or le système romantique hétérosexuel cache des
siècles de domination sous l’illusion du couple heureux. Donc pour elle,
le célibat, c’est une manière de questionner la forme hégémonique de
l’hétérosexualité et nos pratiques en tant que femmes hétérosexuelles.
Sharone Omankoy a d’abord exploré le célibat en butinant, c’est-à-dire
en cherchant à créer des relations de soin et de partage où le sexe
n’était pas la seule fin en soi, sans que cela dégénère vers une
romantisation. Finalement, son célibat est une arme politique de
résistance au système hétéro-patriarcal.

Dans un autre témoignage, celui de Adila Bennedjaï-Zou dans la série
Ex-ologie de LSD, le célibat est présenté comme une situation de fait,
qui n’est ni désirée, ni détestée. Adila Bennedjaï-Zou nous emmène à la
rencontre de nombreuses célibataires dans l’émission. Et un des
arguments qui revient, c’est l’idée de ne pas faire de sexe avec
l’oppresseur. L’intérêt de se rejoindre entre communauté de femmes, même
si ce n’est pas toujours facile, est aussi mis dans la balance avec la
pression d’un modèle social du couple, imposé avec plus d’insistance
quand nous vieillissons. Le célibat a un coût social. Aujourd’hui,
l’espérance de vie des personnes célibataires est moindre que celle des
personnes en couple.

Je discutais avec une amie qui me disait qu’elle s’était blessée cet
été, et que passé 35 ans, sans conjoint·e, elle n’avait pas vraiment su
qui appeler pour en parler. Alors comment créer des réseaux affectifs
qui permettent d’être seul·es amoureusement et relié·es
émotionnellement ?

Une des choses qui m’a touchée dans cette série, c’est que Adila
Bennedjaï-Zou a entrepris une PMA pour avoir un enfant et former une
famille monoparentale. Je crois fort en cette possibilité de faire
famille en dehors du schéma patriarcal, à délier les conditions de
reproduction du couple hétérosexuel, pour en faire autre chose qu’un
système imposé. C’est l’idée que Tim développe dans la deuxième partie,
*Familles, amours, futurs possibles*.

##### La non-exclusivité sexuelle

La non-exclusivité sexuelle, c’est le fait de s’accorder avec un·e
partenaire sur le fait de coucher avec d’autres personnes, d’entretenir
des relations sexo-affectives avec plusieurs partenaires. Cela peut
induire une relation amoureuse principale autour de laquelle gravitent
des amant·es, ou le fait d’avoir plusieurs amant·es, sans relation
principale. Je vais plutôt me focaliser sur le premier cas, où la
relation amoureuse coexiste avec des relations sexuelles extérieures,
parce que c’est une forme qui alimente les recompositions du couple.

Pour aborder la non-exclusivité sexuelle, il me semble important de la
replacer dans un contexte historique. Selon Foucault, l’histoire récente
de la sexualité (depuis le 17^e siècle) a été traversée par des
discours de rationalisation, de gestion et de répression de la
sexualité. C’est-à-dire que les institutions, l’État, l’Église, géraient
les relations sexuelles de leurs sujets en même temps qu’elles
définissaient les interdits (surveillance de la sexualité infantile,
psychiatrisation de l’homosexualité, dévalorisation de l’infidélité…).
Pourtant, le pouvoir est multiple et mobile, il existe toujours une
forme de résistance face aux discours majoritaires.

Le mouvement anarchiste de l’amour libre au 19^e siècle est un exemple
de résistance à l’institution du mariage, considérée comme une mise en
esclavage des femmes. Si leur premier objectif était de dénoncer la
régulation légale et religieuse des formes d’union, certain·es
penseur·euses affirmaient que la sexualité aussi devait être vécue en
dehors de cadres sociaux contraignants, tant que les rapports étaient
librement consentis. Les relations sexuelles devaient donc être déliées
de la visée reproductive, pour que les femmes puissent disposer de leurs
corps sans craindre la grossesse, grâce au contrôle des naissances.

Au 20^e siècle, Emma Goldman critiquait l’ignorance absolue dans laquelle
le patriarcat laissait les femmes avant le mariage, leur demandant de se
« garder » pour l’homme qui les épouseraient. Cette injonction empêchait
les femmes de découvrir la sexualité avant le mariage. Par ailleurs,
elle montrait que le mariage rendait les femmes dépendantes
économiquement et socialement. Elle réclamait la liberté de la femme à
gagner sa vie, à aimer qui elle veut et autant de personnes qu’elle le
souhaite, l’égalité entre les sexes.

Quelques années plus tard, William Reich a théorisé *La révolution
sexuelle* (1936), phénomène par lequel la liberté amoureuse vient
s’opposer à la société patriarcale et capitaliste. En déjouant les
modèles familiaux et les cadres sociaux imposés, l’individu réalise ses
désirs et met à mal l’ordre social. Il vise à reconstruire une société
basée sur le plaisir, en mettant fin à l’exploitation salariale pour
permettre l’émancipation collective.

Ce qui nous amène, avec des ellipses narratives que vous me pardonnerez,
à la révolution sexuelle des années 60-70, où les luttes pour
l’avortement, la pilule contraceptive, la reconnaissance du sexe
non-reproductif, la fin de l’homophobie, etc. nous ont permis d’avoir
certains droits et de continuer à militer pour une sexualité libre et
non stigmatisée.

Ce que je veux dire par là, c’est que la non-exclusivité sexuelle, c’est
l’héritage d’un combat politique pour délier sexualité, mariage et
reproduction. La revendication du droit des femmes à disposer de leur
corps, à ne pas se définir comme mère, mariée ou maîtresse. Elles
voulaient acquérir le droit d’être actrices dans leurs vies amoureuse et
affective. Tout cela dans le cadre d’un combat contre le patriarcat et
le capitalisme, pour cesser de procréer et d’élever la future force de
travail de ces systèmes.

Et aujourd’hui ? En discutant avec un ami à ce sujet, il m’a répondu que
si la non-exclusivité était une condition sociale à l’époque,
aujourd’hui cela pouvait être un choix rationnel.

En ce moment, étudier la non-exclusivité réveille des affects chez moi.
Pour autant, j’ai envie de neutraliser mon point de vue, de voir les
différentes facettes du rubik’s cube. Et pour cela, j’ai pensé parler de
l’intime avec les techniques de l’entraînement mental, parce qu’elles
ont vocation à rendre collectives des questions perçues comme
singulières. Pour un point de définition, l’entraînement mental c’est
*« une méthode d’appréhension de situations complexes pour des prises de
décisions, qui montre tout son intérêt dans un monde contemporain qui se
complexifie de plus en plus* » (selon le mouvement Peuple et Culture).
Parmi les outils que j’ai appris en formation avec le CREFAD (Centre de
Recherche, d’Etude, de Formation à l’Animation et au Développement), le
raisonnement logique, l’éthique et la dialectique forment différents
axes pour étudier une pensée ou une action.

*Alors aujourd’hui, dans quelle mesure la non-exclusivité est-elle un
choix rationnel ?*

Le raisonnement logique consiste à étudier les différentes réalités
d’une situation et à en déduire des faits et/ou un choix rationnel
(c’est-à-dire basé sur notre raisonnement).

Replacer la non-exclusivité comme héritage des combats militants contre
le patriarcat et le capitalisme montre l’importance du droit à une
sexualité libre. L’exercer, c’est le faire exister, valoriser des formes
d’union qui remettent en question le primat de la monogamie.

Dans un contexte où ces formes relationnelles sont acceptées voire
valorisées socialement, il semble également rationnel de les exercer. On
en revient au passage sur Eva Illouz, le capital sexuel comme nouvel
attribut de définition de nos identités sociales. Le jeu de séduction
alimente l’estime de soi des partenaires, renforçant l’image qu’ils se
renvoient mutuellement.

Je pense que nous négligeons l’importance de la séduction dans la
confiance en soi. C’est un panel de codes sociaux qui sont réutilisables
dans de nombreux domaines pour la présentation de soi, la valorisation
d’autrui à travers le corps et l’écoute. La non-exclusivité permet un
épanouissement de ce côté-là, adossé à une image valorisante pour
cell·eux qui sont socialement désiré·es. Je mets un bémol là-dessus, je
parle depuis mon environnement, je ne pense pas que cette valorisation
soit universelle. Et même dans mes cercles sociaux que je dirais
féministes, il y a encore du slut shaming. Quoi qu’il en soit, dans
certains milieux, se définir comme non-exclusif, ça fait briller en
soirée.

Toujours dans le raisonnement logique déduit des affirmations d’Eva
Illouz, dans un champ sexuel où les partenaires potentiel⋅les sont de
plus en plus nombreux·ses, il y a des périodes de vie où la
multiplication des relations sexo-affectives permet de tester (oui,
comme les échantillons de parfum).

C’est ce qu’un ami, qu’on appellera Rémy, m’expliquait à propos de la
non-exclusivité : il n’avait pas trouvé la bonne. C’était aussi une
stratégie pour se protéger et ne pas dépendre d’une seule personne.
Autrement dit, pour lui, dans le cadre d’un champ sexuel où la
compétition s’accroît, une des manières rationnelles de se prémunir de
la dépendance affective liée à la monogamie est de combler ses besoins
émotionnels grâce à plusieurs personnes.

Enfin, dans un cadre relationnel qui le permet, c’est chouette de faire
du sexe avec d’autres personnes. Découvrir d’autres corps, d’autres
façon de fonctionner, ce que nous aimons ou pas, ça passe aussi par la
diversité des expériences sexuelles. C’est une modalité de communication
à part entière, dans laquelle nous connaissons l’autre dans son corps,
dans son intimité, dans une forme de jeu aussi. C’est un peu comme
danser avec quelqu’un. Ça n’aura jamais la même saveur de discuter de la
constitution de la V^e république. Et à partir du moment où notre
désir est délié de la douleur de notre partenaire, cela peut nourrir des
relations riches émotionnellement et physiquement.

*Alors aujourd’hui, dans quelle mesure la non-exclusivité est-elle
éthique ?*

Ce que dit Juliette Rousseau en citant Deleuze dans *La joie militante*,
c’est que l’éthique ne parle pas de ce que nous devons faire, mais de ce
que nous sommes capables de faire dans une situation donnée. C’est une
question de puissance, pas de pouvoir. Ramenée à l’individu, c’est
l’évaluation personnelle de notre action en lien avec les valeurs et les
désirs que nous portons. Elle est fonction de notre expérience, de nos
connaissances, de nos capacités.

Adopter la non-exclusivité, c’est choisir des règles qui favorisent le
désir plutôt que la contrainte. C’est faire l’éloge d’une puissance de
vie plutôt que de cadres sociaux imposés historiquement. C’est dire, je
suis capable dans cette situation donnée de vivre mon désir sans blesser
autrui. En tout cas, c’est sortir le désir et la sexualité d’une vision
culpabilisatrice chrétienne qui voudrait que nous nous excusions d’avoir
envie de quelqu’un·e.

C’est aussi valoriser les liens plutôt que l’exclusion. Dans un régime
monogame classique, les liens sexuels sont réservés à la personne avec
laquelle nous sommes en couple uniquement. Le reste du monde est exclu
de ce rapport de jeu, de toucher, de vulnérabilités partagées. La
non-exclusivité valorise ce lien en soi, en proposant de l’étendre à
d’autres partenaires et d’en faire une expérience nouvelle à chaque
fois.

La non-exclusivité prône une éthique de la fonction plutôt que de la
possession. Dans ce contrat, personne ne possède le corps de l’autre, ne
réclame de droit à la sexualité dessus, contrairement au contrat
monogame classique. Nous prenons plaisir avec le corps de l’autre. Il
existe une vision de la sexualité libre renvoyée à une forme
d’objectification du corps des femmes. Comme si le désir ou l’acte
sexuel nous rendait objet. Alors qu’en fait, en se départant de tout
jugement moral, le fait d’accepter de rendre son corps la source du
plaisir d’un·e autre et vice-versa, pendant un moment donné, c’est un
échange joyeux et consentant.

*Alors aujourd’hui, dans quelle mesure la non-exclusivité est-elle
dialectique ?*

Selon Hegel, les contradictions sont le moteur de la dynamique. La
dialectique étudie les contradictions internes à un système comme base
de la dynamique et de la fabrication de nouvelles structures. C’est ce
qui fait qu’on peut passer d’un modèle problématique à un autre ! (Ça
va, je rigole).

Une dialectique que je perçois entretient une tension entre la
valorisation de la multiplicité des liens et leur consommation.
C’est-à-dire, qu’un partenaire sexuel demande une gestion relationnelle
(dans ma conception de la responsabilité vis-à-vis d’autrui). Et que
cette gestion peut consister à se mettre d’accord sur le fait que nous
ne nous reverrons jamais parce que c’est un coup d’un soir, ou à
discuter ensemble de la place que nous voulons laisser à l’autre et
qu’iel veut/peut prendre à ce moment de sa vie. Pour moi, c’est ce qui
différencie une optique de consommation du sexe et des corps de celle de
construction de liens heureux pour tout le monde.

Une des dialectiques que le modèle monogame dans lequel nous avons
baigné nous a mal appris à délier, c’est la tension entre le sexe et
l’amour. Lorsque nous vivons une relation sexuelle de manière monogame,
celle-ci va forcément mener à une forme de romantisation. Et en même
temps, c’est difficile, dans le partage de quelque chose d’aussi fort,
de ne pas mélanger cette intensité avec un sentiment amoureux. C’est que
l’amour romantique se confond avec la passion. Alors quand une nouvelle
relation vient remplir notre vie d’énergie, nous nous demandons : est-ce
que ce n’est pas une porte de sortie que je m’ouvre pour sortir d’une
relation usée par le temps ?

Encore une fois, tout dépend de comment nous souhaitons définir l’amour.
Comme un bulldozer qui vient défoncer nos paysages relationnels ou comme
une forêt qui a besoin de toutes ses composantes pour faire système.

##### La non-exclusivité amoureuse

La non-exclusivité amoureuse désigne les modalités relationnelles dans
lesquelles on s’autorise, soi et sa·son·ses partenaires, à tomber
amoureux·se d’autres personnes, sans que cela vienne remettre en cause
la relation préexistante.

Au sujet du polyamour, et plus exactement des polyrelations, j’ai
rencontré Nelly, avec qui j’ai pu discuter et partager un beau moment.
Nelly est une femme cis de quarante ans, qui vit à la campagne. Et
depuis quelques années, elle vit sa bisexualité et se définit comme
polyrelationnelle. Pour cette partie du livre, j’ai aussi interviewé
Agnès. Agnès est une femme cis hétéro de 47 ans. Elle vit seule, elle a
3 enfants. Elle a fait des études universitaires, est artiste. Depuis 18
mois, elle est polyamoureuse. Vous retrouverez des parties de leurs
témoignages dans le corps du texte.

Et hop, je reprends mon schéma de l’entraînement mental.

*Alors aujourd’hui, dans quelle mesure la non-exclusivité amoureuse
est-elle un choix rationnel ?*

En fait, je pense que pour la génération de mes parents, il peut y avoir
quelque chose d’assez naïf à réinventer le concept de non-exclusivité
amoureuse et à le nommer. Je crois que c’est pourtant assez logique de
vouloir normaliser des comportements qui sont invisibilisés ou
stigmatisés dans le contrat monogame. La non-exclusivité amoureuse,
c’est aller plus loin dans le questionnement de la possession inscrite
dans le schéma familial monogame. Nous ne renonçons plus seulement à
nous attacher le corps de l’autre, mais aussi à une partie de ses
émotions et de sa vie amoureuse. Et spoiler alert, de toute façon, c’est
ce qui se passe dans la vraie vie.

À mon sens, la non-exclusivité (sexuelle ou amoureuse) vise à déplacer
le concept d’infidélité. Elle nous permet de mieux vivre les relations
sexuelles ou amoureuses de l’autre, en les normalisant et en les cadrant
par un contrat. Dans le podcast *Réinventer l’amour*, Flora Souchier dit
qu’aborder une vie complexe avec des cadres rigides est le meilleur
moyen de se blesser. Lorsque nous construisons des relations à
long-terme, il semble évident que d’autres personnes vont nous attirer,
nous toucher, nous émouvoir.

Lors de notre entretien, pour qualifier ce qu’elle entend par
polyrelations, Nelly explique qu’elle s’est ouvert un éventail des
possibles total, sur des configurations de relations qui pourrait aller
peut-être un jour aller jusqu’à la monogamie, mais ce n’est pas ce
qu’elle veut. Aujourd’hui elle n’a plus envie d’être en couple, d’avoir
un couple principal auquel gravite d’autres relations. Elle est dans un
modèle où elle entretient plusieurs relations en parallèle, sans qu’une
soit centrale et primordiale. Dans certains cas, ces relations sont
charnelles, dans d’autres cas la dimension sexuelle n’existe pas, mais
l’amour est très fort.

Pour Agnès, vivre le polyamour, c’est entretenir plusieurs relations
sexo-affectives qui incluent une connexion intellectuelle et
émotionnelle. Ce sont souvent des relations amicales, qui évoluent vers
la dimension amoureuse du fait de la sexualité. Pour elle, la seule
différence entre l’amitié et l’amour est la sexualité.

Le désir, les relations amicales et amoureuses sont en frottement
constant, à moins de se mettre des œillères. Alors plutôt que de décider
la mort d’une relation dès qu’une autre se crée, la non-exclusivité
amoureuse tente de les faire coexister.

C’est une forme d’expérience de mise en commun de l’amour, au sens
large. Je suis loin de dire que c’est la seule, mais les modèles
organisationnels des relations polyamoureuses ont sans doute beaucoup à
nous apprendre. Dans ce cadre, les partenaires d’une même personne
mettent forcément en commun des ressources temporelles et émotionnelles.
La question de la disponibilité est au centre de mes questionnements sur
ce modèle, mais je crois qu’elle est vécue différemment par les
personnes qui ont des relations amoureuses plurielles. Bien sûr que le
temps et la tendresse sont centraux dans le développement des liens.
Mais comme le dit Angie Triolo dans un article sur Hétéroclite, ces
relations se construisent sur un temps beaucoup plus long. Le rapport à
la disponibilité n’est pas le même que dans les couples monogames. Lors
de notre entretien, Nelly m’expliquait aussi que dans sa vision
« idéale » de la polyrelation, le temps long était au centre. Ce modèle
permet de faire des pauses de plusieurs mois, années, sans se voir
amoureusement, parce que nous savons que cette relation se vit sur des
décennies.

De même pour le rapport aux privilèges relationnels qu’entretiennent la
monogamie. La mise en partage des ressources ne suit pas forcément une
logique égalitaire, où chacun·e demande son dû pour se sentir aimé·e.
Avoir plusieurs relations amoureuses, c’est aussi savoir donner ce dont
les personnes ont besoin à un moment donné, accepter de recevoir moins
quand l’un·e des relations d’un·e partenaire a besoin de présence. Cela
demande d’apprendre à communiquer sur ses sentiments de délaissement, de
jalousie, mais aussi à les gérer soi-même par moment, quand l’autre
n’est pas disponible. Finalement, c’est avoir un sens du bien commun du
groupe avant de se préoccuper de son bien-être exclusif.

Nelly évoquait également la question du « privilège de la tristesse »
qui repose sur nos partenaires. Elle désignait par là le fait de
dépendre émotionnellement d’une personne pour accueillir nos états
d’âmes, de faire peser ce poids sur une seule personne. Répartir ses
coups durs est une autre manière de s’occuper de nous et du bien commun.
Selon elle, le partenaire n’est pas toujours le bon interlocuteur.
Choisir de partager ses joies et ses peines à plusieurs, c’est
décentraliser le couple de nos vies.

Et tout cela alimente la construction d’autres familles. La question de
la co-parentalité se pose dans les relations polyamoureuses. Comment
introduire dans la parentalité le fait d’avoir plusieurs partenaires ?
Agnès a trois filles, dont une qui vit encore chez elle une semaine sur
deux. Elle est transparente vis-à-vis de ses filles sur ses relations
amoureuses. Elles en discutent, ont rencontré les partenaires d’Agnès.
Ce n’est pas son souhait de cloisonner. Et selon les contextes et la
construction des relations, de nombreux rôles peuvent être imaginés pour
chaque membre de la constellation.

Devenir un autre parent, un membre de la famille proche, un adulte qui
partage avec nous le travail de soin et de prise en charge de
l’organisation matérielle liée au fait d’avoir un enfant. Et de la même
manière que nous pouvons construire des familles avec plusieurs
amant·es, nous pouvons construire des familles entre amis, colocs.

*Alors aujourd’hui, dans quelle mesure la non-exclusivité est-elle
éthique ?*

Avant tout, je crois que ce qui porte la non-exclusivité amoureuse,
c’est une éthique du libre choix. Pendant notre entretien, Agnès est
revenue plusieurs fois sur une idée, qui me semble fondatrice dans sa
pratique et sa vision du polyamour : « il est primordial que chacun se
sente libre de vivre ce qu’il a envie de vivre ». Imposer l’exclusivité
à quelqu’un, c’est le contraindre par rapport à ses désirs, ses
relations. Alors que pour Agnès, si la personne est avec elle dans le
cadre de relations polyamoureuses, c’est qu’elle en a vraiment envie et
qu’elle l’a choisi, parce que les obligations de présence du couple
disparaissent.

Il y a clairement une éthique du désir dans les relations
non-exclusives, comme source de joie et d’interactions positives avec
les autres. Lors de notre entretien, Agnès disait qu’avoir plusieurs
relations lui permet d’être mieux avec chaque partenaire. Tout cela la
nourrit et se communique lorsqu’elle arrive dans l’autre relation avec
cette joie à partager.

A mon sens, une relation non-exclusive aimante place le désir en face de
la co-responsabilité. Ce que je veux dire par là, c’est que ce n’est pas
simple de se mettre à relationner dans des contextes ouverts
amoureusement parlant. Sortir du couple cocon monogame, c’est se mettre
dans une situation de vulnérabilité extrême. En tout cas pour moi ça
l’est. Nous quittons un monde privilégié et confortable, en ayant de
nombreux réflexes intériorisés, dans l’objectif d’acquérir des codes
relationnels que nous ne maîtrisons pas. C’est terrifiant. Et en même
temps, expérimenter, mettre nos idées sur le monde en action dans le
corps, c’est jouissif également.

Dans tous les cas, je pense qu’il y a beaucoup de coups durs au départ.
Et qu’il faut prendre soin de la jalousie, des insécurités, du sentiment
perdu d’unicité… Nelly m’a raconté qu’elle s’était beaucoup préparée
en amont à ce sujet. Elle avait écouté des choses sur la compersion,
l’émotion d’être en joie pour la joie de l’autre. Dans la mise en œuvre,
ça n’a pas toujours été facile. Pour elle, c’est important de partager
le sentiment de jalousie, d’en parler avec les ami·es, et pas forcément
avec son·sa partenaire. Savoir que ce n’est pas une émotion naturelle
mais une construction sociale, cela permet de remonter à sa source pour
décortiquer les situations. Nelly me donnait l’exemple d’un jour où son
amoureux·se était parti·e au théâtre avec une autre relation. Au-delà de
la jalousie, elle s’était demandée : « ok, est-ce que j’aurais eu envie
moi ce soir d’aller au théâtre avec iel ? » et la réponse était non.
Dans le processus de déconstruction de la jalousie, cela l’a beaucoup
aidée de rencontrer les autres personnes en relation. Nous y
reviendrons.

Nous pouvons aussi faire attention au contexte dans lequel l’autre
traverse ces expériences. Par exemple : est-ce que mon·ma partenaire
voit quelqu’un·e en ce moment ? Si non, est-ce que c’est ok pour iel que
j’aie d’autres amant·es en même temps ? Est-ce qu’iel vit des choses
aujourd’hui qui demandent que je lui consacre beaucoup de temps et
d’attention ? Et c’est donc peut-être accepter de prendre des pauses
aussi, de faire des allers-retours entre l’exclusivité et la
non-exclusivité, parce que tout change.

Et dans ma vision des couples libres, cela revient à valoriser une
éthique de l’engagement plutôt que de la fidélité. Où plutôt que de se
jurer de ne jamais rien vivre avec qui que ce soit jusqu’à ce que la
mort nous sépare, nous promettons de prendre soin l’un de l’autre tant
que cela nous fait collectivement du bien. Lors de notre entretien,
Agnès m’expliquait que si elle avait d’abord rejeté les relations
hiérarchiques, elle souhaite aujourd’hui avoir une relation principale,
un·e partenaire. Par-là, elle entend une personne support au quotidien,
qui l’aide et la soutient dans sa vie personnelle et professionnelle.
C’est ce que Flora Souchier dit dans *Réinventer l’amour*, être un
binôme dans une situation de lutte, c’est garantir la santé et la
sécurité de l’autre face à un système d’oppression, être acteur de son
émancipation. Et nous pouvons former beaucoup de binômes, amoureux,
amicaux, familiaux. Tant que nous réussissons à prendre soin de tous
ceux que nous nouons.

*Alors aujourd’hui, dans quelle mesure la non-exclusivité est-elle
dialectique ?*

La première dialectique qui m’intéresse au sujet de la non-exclusivité
amoureuse, c’est l’ambivalence entre la multiplication des liens et la
compétition. Je ne dis pas que le modèle non-exclusif est porteur de
compétitivité en soi, seulement qu’il exacerbe les comportements que
nous tendons à adopter dans le reste de nos vies : mise en compétition
pour les écoles, pour trouver un emploi… Et que la rivalité rejaillit
d’autant plus dans la question amoureuse qu’elle est marquée par le
spectre monogame, ayant défini le sentiment amoureux comme unique et
excluant.

Et, ce que j’observe, c’est que les relations entre personnes amoureuses
d’un·e même partenaire peuvent se teinter de ces dynamiques de
compétition. Quand l’amour entre en jeu, le modèle monogame passe
souvent la tête par l’encadrement de la porte. Nous nous mettons à
vouloir plus, plus de temps, plus de week-end à deux, plus d’engagement
émotionnel, ce qui est donné à l’autre semble être pris à notre
détriment.

Ce sont les fameux triangles : Bé et Claude sont amoureux·ses de Andrea.
Bé et Claude n’entretiennent pas de relation d’affection. Disons que
Andrea est dans une relation stable avec Bé. Ou au contraire, qu’il
vient de commencer une relation avec Bé et que c’est intense et
passionnel. Dans les deux cas, Claude se retrouve éloigné de Andrea.
Nous ne connaissons pas les attentes de Claude, peut-être qu’il a un·e
Dominique dans l’affaire et qu’il se fiche qu’Andrea s’éloigne ou
entretienne une relation différente avec iel. Mais il existe aussi le
risque que Claude se sente délaissé·e, tout en acceptant les conditions
de la relation non-exclusive, parce que c’est le contrat de base et
parce que Andrea est dans une position de pouvoir à l’intérieur de ce
triangle. Cela rejoint la complexité évoquée par Nelly d’acter le
consentement dans des modèles qui prônent l’ouverture, même pour soi.
Quoi qu’il en soit, j’ai du mal à croire aux triangles équilatéraux.

Cela nous ramène à la dialectique entre la forme non-exclusive et le
fond monogame. Vécus comme décrits dans les derniers paragraphes, ces
schémas reviennent finalement à multiplier les relations monogames
plutôt qu’à véritablement changer leur nature : communautariser l’amour
pour en faire un bien commun. Ils viennent plutôt exacerber un sentiment
de compétition, les rapports de pouvoir intra-relationnels, voire une
éthique de consommation des affects et du sentiment amoureux. La
non-exclusivité ne porte pas cela en soi, mais peut mener à ces dérives
dans le contexte socio-culturel qui est le nôtre (mise en compétition
néolibérale, norme amoureuse définie par la monogamie).

Ce qui nous amène à la dernière dialectique, entre amour et féminisme.
Souvent, j’ai l’impression que nous sommes si enthousiastes au sujet des
modèles non-monogames que nous en oublions de problématiser le pouvoir.
Or la non-exclusivité sexuelle, amoureuse, le polyamour ne sont pas
exempts de rapports de genre. Dans le cadre d’une relation
hétérosexuelle, je crois que la non-exclusivité peut être un nouvel
élément de la domination masculine. C’est ce que dit Eva Illouz avec la
stratégie de détachement, où l’indisponibilité devient un marqueur de
valeur, et la sexualité sérielle une manière de se prémunir de
l’engagement. C’est aussi un des retours de Victoire Tuaillon, qui après
la réalisation du podcast *Le cœur sur la Table*, témoignait des
nombreux messages qu’elle avait reçus, manifestant l’enthousiasme
soudain des hommes pour le féminisme via l’angle polyamoureux. Ces mecs
qui nous expliquent que si nous ne voulons pas d’une relation amoureuse
non-exclusive, c’est que nous ne sommes vraiment pas assez déconstruites
ou féministes.

Je ne crois pas que nous puissions faire abstraction du contexte dans
lequel nos relations s’inscrivent. En revanche, nous pouvons le
conscientiser, tenter de nous défaire des normes comportementales que
nous avons intériorisées. Nous pouvons essayer de réinventer le fond du
sentiment amoureux, plutôt que sa forme. Pour nous, la proposition est
communautaire.

##### Une proposition communautaire : les réseaux affectifs de Brigitte Vassallo

Le modèle de réseau affectif proposé par Brigitte Vassallo dans son
livre *Idéologie monogame. Terrorisme polyamoureux* est sans doute l’un
de ceux qui m’a le plus enthousiasmée, en me montrant d’autres
possibles. Il ouvre un horizon politique et amoureux dépassant le
tropisme individualiste que peut parfois prendre la non-exclusivité.

*Alors aujourd’hui, dans quelle mesure le réseau affectif est-il un
choix rationnel ?*

Le point de départ des réseaux affectifs, c’est que si nous voulons
supprimer la compétition de nos relations, il faut créer de la
reconnaissance, de l’entraide. Pour Brigitte Vassallo, ce qui définit
une relation comme polyamoureuse, ce n’est pas le nombre de personnes
fréquentées, mais les relations qu’entretiennent les méta-amours. Dit
plus simplement, ce sont les rapports entre Bé et Claude, tou·tes les
deux amoureux.ses de Andrea : de connivence, d’empathie, de soin. Et
pour moi, quand j’ai lu ça la première fois, c’était révolutionnaire.

Quoi ? Je peux me préoccuper du bien-être de la personne avec laquelle
je me sens carrément en compétition aujourd’hui ? Je peux refuser ce jeu
et tenter de nouer des liens avec elle pour créer autre chose ? Et je ne
dis pas que ça fonctionne à chaque fois, loin de là, ou que tout le
monde est prêt à vivre ça. Mais savoir que cette possibilité existe, et
que nous pouvons tendre vers autre chose que de l’indifférence, ça me
donne de l’espoir.

Je me souviens d’une discussion très importante pour moi, que j’ai eue
cet été avec un·e ami·e. Iel vit une relation polyamoureuse stable. Si
on reprend le schéma du triangle, iel est Claude, en relation avec
Andrea, qui est également dans une relation avec Bé. Iel me racontait
son arrivée dans l’ouverture de ce couple, les liens d’amitié qui
s’étaient immédiatement tissés avec Bé. Iels partageaient beaucoup de
choses et cela a permis de créer une relation entre Bé et Claude, qui ne
concernait pas forcément Andrea, mais rendait ce réseau solide.

Ce que j’ai beaucoup aimé aussi, c’est qu’iel m’a raconté l’arrivée de
Dominique, toujours en relation avec Andrea. Et cette fois-ci, c’était
beaucoup plus dur pour Claude. Alors ce qu’iel a fait, c’est qu’iel est
allé·e parler à Dominique pour lui dire « j’existe, je fais partie de la
vie de Andrea, et pour notre bien commun, là tout de suite, j’ai besoin
que tu prennes soin de moi ». Et cela ne voulait pas dire, ne vis pas
cette relation. Cela voulait dire, reconnais-moi, rassure-moi,
montre-moi que tu tiens compte de moi. Je crois que c’est faire preuve
d’une très grande honnêteté que de pouvoir montrer sa vulnérabilité de
cette manière.

L’autre objectif des réseaux affectifs, c’est de dire que si nous
voulons créer nos relations, il faut inventer nos contrats et les rendre
évolutifs. Les règles que nous posons diffèrent selon les partenaires,
elles bougent, ne sont pas essentielles. Nelly constate (et je plussoie)
que les relations polyamoureuses où il n’y a pas de cadre et pas de
règle, en général, « c’est au bénéfice des gars ».

L’une des règles qu’elle a mises en place par exemple, c’est de se
prévenir lors de relations non-protégées pour avoir le temps de se
tester. Nelly n’utilisait plus de préservatifs avec son ex Max. Iels
devaient donc se prévenir quand ils avaient envie de ne plus se protéger
avec d’autres. Cela entraînait un rebond de la responsabilité et de la
confiance envers la tierce personne, notamment en cas de relations
sexuelles non-protégées. Ce que je trouve intéressant, c’est que dans
cette vision des choses, la santé est un bien commun partagé par
l’ensemble des partenaires potentiel·les.

Une autre règle qu’iels avaient instaurée, lorsque Max (son ex), Zoé (la
relation de son ex), et Nelly se retrouvaient en soirée, était de ne
dormir ou de ne rentrer avec personne, car cela cristallisait la
jalousie et un sentiment d’échec à ne pas pouvoir la dépasser. De la
même manière, ici le bien-être collectif passe avant la satisfaction de
désirs individuels.

Pour autant, les règles peuvent bouger. Et nous pouvons instituer ces
moments de remise à plat de la relation. Par exemple avec mon amoureux,
nous prenons souvent des cafés gênants. En gros, ça veut dire qu’on
arrive gênés, on prend un café, on parle de notre relation, et on essaye
de repartir contents. Et depuis que nous sommes en relation, nous avons
été : non-exclusifs amoureusement, en relation non-exclusive
sexuellement, en relation exclusive, négociant les conditions de la
non-exclusivité, et je pense qu’il y a de grandes chances qu’avant la
fin de l’écriture de ce livre, le contrat ait de nouveau changé. En ce
moment, nous ne voyons personne d’autre. Et ça m’a fait du bien de
reconnaître que j’avais besoin d’une pause pour envisager la
non-exclusivité sereinement (pour écrire ce livre par exemple). Et que
c’est ok de ne pas tout déconstruire d’un coup.

*Alors aujourd’hui, dans quelle mesure le réseau affectif est-il
éthique ?*

Là aussi, on retrouve l’éthique de la co-responsabilité. Celle de
communiquer sur ses peurs, mais aussi celle de se retirer d’un réseau
affectif lorsque nous n’avons plus l’énergie de le soutenir, que cela ne
nous convient plus, ou de ne pas entrer dans un réseau qui n’a pas les
ressources pour nous accueillir. Concrètement, cela signifie, dans un
modèle très ouvert, savoir poser ses propres limites et les respecter
pour ne pas se faire mal. Et dans le sens inverse, avoir l’intelligence
relationnelle de détecter quand notre présence met en danger des
relations préexistantes.

Le film *LETO* (2018), de Kirill Serebrennikov me semble très parlant à
ce sujet. Il raconte l’apogée d’un groupe de rock sur la scène
underground de Leningrad au début des années 1980. Mike, leader du
groupe Zoopark, est marié avec Natalia. Ils rencontrent le jeune
musicien Viktor, que Mike accompagne à monter son groupe, Kino. Une
forme d’amour se noue entre Natalia et Viktor. Celle-ci s’en entretient
avec son mari Mike, lui expliquant qu’elle a très envie de l’embrasser
mais qu’elle ne veut pas le faire sans lui en parler. C’est une
amourette de collégien, ils se tiennent la main, parcourent la ville en
bus pour apporter du café à Mike. Et Mike accepte cette romance. Dans le
film, on comprend que la situation n’est pas simple pour Mike. Ni pour
Natalia, qui finit par demander à Viktor de mettre fin à la dimension
amoureuse de leur relation. Ce que je trouve très inspirant dans ce
film, c’est que pendant toute la durée de la relation de Natalia et
Viktor, Mike aide Viktor et met tout en œuvre pour que son groupe
connaisse le succès. C’est comme si, par amour de la musique, de sa
femme, du groupe d’amis qu’ils entretiennent, Mike acceptait un
compromis qui avait du sens dans cette situation-là. Et de la même
manière, Natalia renégocie l’équilibre avec Viktor quand le contexte
change, pour préserver Mike et leur amitié. Les limites sont
changeantes, inspirées par un souci de l’autre.

Cela arrive aussi dans la vraie vie. Une des choses qui m’a le plus
touchée lors de l’entretien avec Nelly, c’est le lien qui l’unissait à
Zoé. Elles se sont rencontrées à un moment où leurs relations avec Max
étaient compliquées, et cela a été très soutenant pour elle. Nelly
dit : « Pour moi l’une des plus belles choses que j’ai vécu dans le
polyamour, c’est la relation avec Zoé ». Derrière le partage d’une
sororité, des joies, des peines, la reconnaissance mutuelle et le
soutien dans les différentes phases de la relation, je vois des communs
qu’elles ont su faire exister au-delà de l’existence de leur partenaire.
Elles ont fait relation.

Les réseaux affectifs sont portés par une éthique du soin qui engage à
reconnaître la spécificité des relations. Lors de notre entretien, Agnès
avait un qualificatif pour parler de chacune de ses relations. J’ai
beaucoup aimé cette idée de nommer les choses, de définir ce qu’elles
ont de spécial : l’amimoureux, avec qui elle partage de la joie et des
bons moments, la relation comète, qui n’a lieu qu’une ou deux fois par
an à cause de la distance, le porte-avion, chez qui elle peut se poser
quand elle veut et aussi longtemps qu’elle le souhaite, la relation
socle, qui partage le quotidien.

Ici, l’éthique du soin ne dépend pas de la romantisation. En règle
général, sous le prisme monogame, lorsque nous couchons avec un·e
amant·e, soit c’est un plan cul, soit nous mettons de l’investissement
émotionnel et nous commençons à romantiser la relation. C’est quelque
chose qui me parle beaucoup personnellement. Je suis souvent prise dans
une contradiction : le sexe « sans rien derrière » ne m’intéresse plus
beaucoup, mais de l’autre côté, lorsqu’une relation de tendresse existe,
je ne la valorise pas ou elle m’effraie parce que j’ai l’impression que
je ne vais pas pouvoir la soutenir. Selon Brigitte Vassallo, c’est parce
que nous avons tendance à confondre le soin et la romantisation. Offrir
un temps d’intimité, des caresses, des mots doux, cela n’engage pas à
vouloir se marier et faire des enfants.

Au contraire, reconnaître les spécificités de chaque relation et en
prendre soin, c’est nouer des contrats uniques, donner à chacun·e se
dont il a besoin, dans une logique de don-contre don, c’est-à-dire du
donner sans contrepartie plutôt que d’attendre de recevoir. C’est aussi
prendre soin des relations de long-terme parce qu’elles sont basées sur
une connaissance mutuelle et une construction commune.

*Alors aujourd’hui, dans quelle mesure le réseau affectif est-il
dialectique ?*

Il existe une dialectique entre le désir et sa réalisation. Brigitte
Vassallo établit un parallèle entre la valorisation du désir dans une
société consumériste et notre volonté de l’accomplir. Pour elle,
renoncer à accomplir un désir, c’est un affront au néolibéralisme, parce
que nous nous défaisons du consumérisme et de l’individualisme, qui nous
poussent à accomplir nos désirs au détriment d’autrui : acheter une
paire de Nike confectionnée par des enfants en Asie, déséquilibrer
une/des relations pour son plaisir personnel.

En ceci, Brigitte Vassallo explore de nouveaux horizons relationnels
créés par la non-réciprocité du désir. C’est aussi ce que fait Monique
Wittig à sa manière, dans *Le corps Lesbien* : le désir est une
circulation, un mouvement vers l’autre qui crée un lien. En fait, cela
nous amène à questionner la centralité du sexe et de l’amour romantique
dans nos sociétés.

J’ai eu beaucoup d’amitiés-amoureuses quand j’étais plus jeune. Et j’ai
des souvenirs très émouvants de ces liens d’amitiés teintés de désir,
d’érotisme, de possibilités entraperçues. Il y a mille manière de
relationner et de partager de l’amour, en dehors de la corporalité.

Enfin, la dialectique entre individu et collectif présente dans les
réseaux affectifs amène à dépasser son simple bien-être personnel. Si je
reprends mon triangle, quand je suis Claude, je n’ai aucun intérêt à ce
que cela se passe mal entre Bé et Andrea, parce que cela rejaillira sur
le bien commun, donc sur moi. Cela amène à prendre soin du collectif, à
avoir une forme de responsabilité sur la gestion relationnelle de mon
rapport à Andrea, à Bé, et du groupe en lui-même. Contre
l’individualisme néolibéral, les réseaux affectifs proposent une
collectivisation des affects, des soins, des désirs et des douleurs.
Brigitte Vassallo souligne d’ailleurs que les valeurs des réseaux
affectifs résonnent beaucoup avec celles des communs du XVII^e^ siècle :
« *mutualisme, réciprocité, conscience de soi, volonté de discussion,
mémoire, célébration collective et aide mutuelle* ». Elle utilise une
métaphore que j’aime beaucoup : les réseaux affectifs comme des forêts
dont les arbres sont connectés par leurs racines…

##### Le couple v.12

Version 12, c’est arbitraire. Et je ne suis pas tout à fait sûre de ce
que j’avance, c’est une tentative. Je crois en la nécessité de changer
nos relations, dans une perspective féministe, anticapitaliste,
personnelle. Mais est-ce que souhaiter partager de l’intimité
sexo-affective avec un·e seul·e compagnon·ne rend inopérante toute
tentative de repenser la hiérarchie ? Comment sortir de l’injonction
pour aimer autrement « *sans y laisser les tripes et le féminisme* »
comme le dit Brigitte Vassallo ? Comment créer un spectre plutôt que
deux systèmes d’imposition ?

Je crois qu’à travers ces interrogations, j’avais aussi envie de
dessiner ce que pourrait être un couple 2.0, c’est-à-dire un binôme
exclusif sexuellement et amoureusement, mais ouvert sur la forêt, sur
les réseaux affectifs amicaux, familiaux, passionnels. Est-ce que la
relation peut être libre sans que cela soit certifié par le sceau du
sexe ? Quelles seraient les dimensions logiques, éthiques, dialectiques
de ce couple en réseau ?

*Alors aujourd’hui, dans quelle mesure le couple V12 est-il un choix
rationnel ?*

L’un des points de départ de ce couple V12 serait de reconnaître nos
vulnérabilités dans un système violent. C’est-à-dire, que la compétition
néolibérale est toujours à l’œuvre entre pairs, que le système
patriarcal nous guette et que la domination masculine se joue
aujourd’hui dans d’autres champs que celui du foyer. Et que construire
des binômes où réinventer l’amour sur la base de l’égalité et d’une
reconnaissance mutuelle, cela peut passer par d’autres éléments que la
non-exclusivité sexuelle ou amoureuse. Que créer des remparts aux
oppressions, ce n’est pas forcément s’y jeter bras ouvert en pensant que
nous serons plus fort que les normes que nous avons intériorisées. Au
contraire, peut-être que créer des cabanes bienveillantes, dans lequel
nous invitons les autres à se reposer, serait aussi efficace.

Un autre raisonnement logique de ce couple V12, serait que puisque
l’amour est multiple, nos relations amicales ont autant d’importance que
notre relation romantique. Si je reprends ma métaphore de la cabane,
cela veut dire que notre partenaire amoureux est un mur de la maison
plutôt que le pilier central. Cela nous permet d’envisager d’autres
horizons de vie : la coparentalité entre amis, l’achat d’une maison pour
vivre entre amis, d’autres manières de choisir familles.

Décentrer l’amour romantique dans nos vies permettrait d’ouvrir
l’éventail des manières d’être ensemble. En parlant du couple avec
Nelly, elle m’expliquait qu’elle trouvait beaucoup plus de plasticité et
de liberté de construire la relation à sa façon dans la polyrelation. De
la même manière, si nous renonçons à être en couple dans le schéma
narratif monogame, nous pouvons faire de l’amour « une toile de fond, un
tapis confortable de personnes dont on sait qu’on les aime et qu’ils
nous aiment aussi ». Nous pouvons remplir notre vie d’amis,
d’engagements, de passions, de relations singulières.

*Alors aujourd’hui, dans quelle mesure le couple V12 est-il éthique ?*

Je crois que les dimensions éthiques d’un couple V12 seraient très
semblables à celles des formes relationnelles non-monogames. Une éthique
du soin, pour l’ensemble du réseau, dans une logique de don contre-don,
sans privilégier le partenaire amoureux vis-à-vis du reste des relations
sous prétexte de hiérarchisation romantique. La co-responsabilité par
rapport à l’évolution des règles et la gestion relationnelle, soit
continuer ou se mettre aux cafés gênants. Considérer que le contrat est
variable lui aussi, l’aménager pour que chacun·e se sente bien. Et
surtout : déculpabiliser le désir et dédramatiser le sexe à l’extérieur
du couple, sans pour autant faire de la non-exclusivité une règle
essentielle.

L’un des autres points communs, qui me semble essentiel aujourd’hui,
c’est le partage de ce faux « privilège de la tristesse », dont parle
Nelly. Trouver les bons interlocuteurs, s’autoriser à partager ses
vulnérabilités, c’est créer les conditions de relations plus intimes.
Réserver ces partages à son·sa partenaire revient à s’attacher à une
seule personne dont dépendrait nos réconforts. Les émotions, parmi le
temps, les ressources matérielles et immatérielles, les logements, les
infos, constitue un autre commun que nous pourrions sortir du couple.

Je pense que cela demande aussi d’accepter la fin. Peut-être que le
couple V12 aurait moins de mal à laisser partir l’autre. Selon Belinda
Cannone, après la révolution féministe et dans une société valorisant le
désir, nous sommes entrés dans une ère de polygamie lente. C’est-à-dire,
que nous allons d’une relation à une autre quand le désir s’éteint. Il
n’y a pas de jugement moral là-dessus. Mais j’ai l’impression que d’un
côté, le couple monogame classique nous berce de « pour toujours »,
tandis que de l’autre, les relations non-exclusives peuvent avoir du mal
à finir, du fait de la nature même du contrat et de la difficulté à
juger de ce qui est ok ou pas. Dans le contrat monogame, nous nous
déchirons, dans le contrat non-exclusif, nous laissons les relations
pourrir, parfois. Dans le couple V12, j’aimerais que ce soit plus simple
de se laisser aller à autre chose.

*Alors aujourd’hui, dans quelle mesure le couple V12 est-il
dialectique ?*

L’une des principales dialectiques à dépasser est celle qui existe entre
exclusivité et possession. En discutant avec Agnès de la domination
genrée au sein des relations amoureuses, elle m’expliquait s’être
construite avec l’idée qu’elle devait forcément répondre au désir
masculin.

Sans parler spécifiquement du cas d’Agnès, l’intériorisation de cette
injonction rejoint tout un pan de la théologie de la culture du viol,
qui normalise l’accès au corps féminin autour d’un droit à la sexualité,
une fois passé un certain nombre d’étapes. Après le bisou, vient le
baiser, vient le sexe, et dès lors il semble naturel dans une relation
amoureuse « classique », que le sexe soit un acquis qui ne sera plus
questionné. Beaucoup de travaux, dont ceux d’Olympe de Gê, autrice et
réalisatrice féministe, ont mis en lumière le potentiel érotique du
consentement. Alors dans le couple V12, on demande la permission !

Cela demande un travail global de remise en cause de la propriété.
Peut-être plus largement, de questionner la création d’une entité, « le
couple », qui repose sur une assise matérielle de partage d’un prêt,
d’un compte commun, d’une maison et d’une C4 Picasso. Une manière de
préserver l’indépendance de chacun, c’est de garder une distance
économique et kilométrique. Lors de notre entretien, Agnès m’a expliqué
que l’une des relations la plus importante pour elle était la relation à
soi-même. Elle a décidé d’habiter seule pour ne plus porter la charge
mentale du quotidien partagé, cesser de tourner toute son énergie vers
le bien-être de son partenaire. Selon Eva Illouz, la distance entre deux
personnes renforce l’image construite du partenaire -- donc
l’idéalisation (et nous avons tous besoin de nous raconter des
histoires). Habiter ensemble, cela ne devrait pas être un choix évident.
Au contraire, ritualiser les moments où nous décidons de voir notre
partenaire, cela préserve une grande autonomie, tout en célébrant les
moments de partage.

Un autre élément dialectique est la tension entre la fusion du couple
monogame classique et l’altérité. Cela rejoint le point précédent,
comment conserver ce qui me rend autre, comment valoriser dans l’amour
ce qui fait que mon partenaire est cet autre qui n’est pas moi ?
Peut-être en préservant nos désaccords, nos disputes, nos points de vue
différents sur le monde, en vivant le conflit parfois. La posture de
compromis qui est en jeu dans les relations amène une reconnaissance de
ce que l’autre est et veut. C’est ce que Hélène Cixous mentionne dans
*La Jeune née* à propos de l’entièreté que permet cet autre, au cœur du
désir et des relations amoureuses : « *chacun \[prend\] enfin le risque
de l’autre, de la différence, sans se sentir menacé(e) par l’existence
d’une altérité* ». Cette conception de l’altérité comme source du désir
ne permet pas l’appropriation.

Si on va plus loin, cela souligne également la tension entre le
« noyau » couple et le reste du réseau. Peut-être que nous pouvons
penser ces noyaux en réseau justement. Peut-être que nous pouvons
envisager l’interdépendance des amours, amitiés, relations qui se lient
dans notre intimité et l’impact que chacune d’elle a sur les relations
interpersonnelles et sur le groupe en lui-même. Si nous portons une
attention au groupe plutôt qu’aux éléments, les binômes sont des liens
qui tissent un ensemble plus vaste, fonctionnant sur le même principe
que les communs : « *mutualisme, réciprocité, conscience de soi, volonté
de discussion, mémoire, célébration collective et aide mutuelle* »
(Brigitte Vassallo, à propos des réseaux affectifs).

Quoi qu’il en soit, les modèles ne sont pas rigides et nous naviguons en
permanence dans des relations intimes multiples. Choisir nos modalités
relationnelles nécessite de prendre en compte les conditions matérielles
de chacun·e et le contexte social dans lequel nos amours s’insèrent. À
mon sens, pour vivre l’amour comme un commun et non pas comme la
justification d’un rapport de domination, il faut le reconstruire dans
une perspective féministe.

#### Déconstruire les normes genrées pour repenser la notion d’engagement 

L’amour hétérosexuel monogame est intrinsèquement lié à la construction
binaire du genre, autour des rôles attribués selon l’assignation homme
ou femme, dans la société et à l’intérieur du couple. Comme le montre
Eva Illouz en étudiant le régime amoureux du 19^e siècle dans
*Pourquoi l’amour fait mal*, l’ordre social valorisait la notion
d’engagement en tant que fondement de l’union. Il existe aujourd’hui
d’autres moteurs dans la décision de construire une (ou des) relation(s)
amoureuse(s).

Je crois qu’il existe plusieurs causes au fait que l’engagement soit
devenu moins central dans nos manières d’envisager l’amour. D’abord,
parce que nous avons assisté aux incohérences et à la faillite du mythe
monogame hétérosexuel « ils vécurent heureux ensemble et eurent beaucoup
d’enfants ». Mais aussi parce que l’Eden monogame dissimule un système
violent psychologiquement et physiquement, où le bonheur intime d’un
homme et d’une femme viendrait justifier des siècles de domination
masculine. Or je pense que l’engagement (militant, amical, amoureux)
dessine une alternative à l’éthique néolibérale de consommation de
liens, des affects, des causes, pour une durée momentanée et dans un
objectif utilitariste. Comment faire pour que l’amour vaille de nouveau
la peine de s’engager ?

##### À bas l’éducation patriarcale

Selon bell hooks dans *All about love*, l’éducation dans la culture
patriarcale est bâtie sur le mensonge, en ce qu’il requiert aux hommes
et aux femmes de construire un « faux soi » (false self). C’est-à-dire
que l’éducation genrée requiert de se conformer aux dispositions et aux
rôles correspondant à notre sexe, féminin ou masculin. Le genre est une
construction sociale qui polarise de manière binaire l’humanité,
associant des valeurs et des représentations à chacune des catégories
des sexes. Cette division est porteuse d’une hiérarchisation, induisant
une asymétrie dans la distribution des ressources économiques,
politiques, symboliques. La subordination des femmes aux hommes, c’est
le patriarcat.

Comme je l’ai montré plus tôt à l’aide du texte de Léane Alestra, tiré
du recueil *Nos amours radicales*, l’idée de complémentarité des sexes
induit une éducation différenciée pour les rôles de chacun. C’est une
des idées qui m’a le plus marquée à l’écoute du podcast *Le cœur sur la
table*, de Victoire Tuaillon. La socialisation primaire des hommes
valorise un contrôle resserré sur leurs émotions, le détachement
vis-à-vis des manifestations de sensibilité jugées « féminines ». Ce
sont les phrases ressassées : « tu pleures comme une fillette », « t’es
qu’une mauviette », « pédale » (avec ce que cela véhicule d’homophobie
et de dévalorisation des sexualités non-hétérosexuelles).

Ainsi, l’amour serait également un concept genré. Dans sa thèse
« *L’enfance des sentiments, la construction et l’intériorisation des
règles des sentiments affectifs et amoureux chez les enfants de 6 à 11
ans* », le sociologue Kevin Diter montre comment l’hétéronormativité
est apprise dès le plus jeune âge. Dans une conférence sur le sujet,
Kevin expliquait qu’à cet âge-là, les enfants ont déjà une
représentation normée de l’amour. Pour eux, c’est un sentiment lié au
genre féminin et aux adultes, alors que l’amitié serait typiquement
enfantine. Dans la pratique, les amitiés mixtes existent très peu. Les
adultes, en tant que détenteurs du savoir, jouent un rôle pour valider
ces représentations. Ils s’adressent à un garçon qui jouerait avec une
fille en lui demandant si c’est son amoureuse, et vice-versa,
requalifiant automatiquement la relation. De plus, l’interlocutrice
d’un enfant pour parler de ses sentiments est souvent une femme,
renvoyant l’amour du côté du féminin.

bell hooks avance également que l’éducation genrée apprend aux hommes à
utiliser le mensonge comme un moyen d’accroître leur pouvoir sur autrui.
Le contrôle sur les autres et sur ses propres émotions, provoque une
incapacité à connecter avec les autres et à assumer sa responsabilité en
causant de la souffrance émotionnelle.

Au contraire, les femmes sont éduquées dans l’injonction de prendre
soin et d’être aimable : « t’es une gentille fille », « fais pas ta
sauvage », « qu’est-ce que t’es jolie dans ta robe, viens me faire un
bisou ». Et pour bell hooks, le mensonge intervient aussi, puisque la
société apprend aux femmes à dissimuler, à surjouer pour plaire en
présentant « une meilleure version de soi-même » aux autres.
C’est-à-dire, à performer les stéréotypes féminins : surémotionnalité,
sensibilité, sensualité, etc.

Collectivement, le mensonge est justifié comme un moyen d’éviter le
conflit. Autrement dit, le mensonge permet de ne surtout pas remettre en
question la naturalité du couple et du système de genre binaire. Or ces
constructions nous affectent dans l’intime. Les relations amoureuses
sont traversées par les rapports de pouvoir, reproduisant les structures
de domination sociétales. Ces dynamiques coexistent avec l’affection et
le mythe amoureux, ce qui rend complexe de se rendre compte et de se
sortir de situations maltraitantes. Selon bell hooks, l’idée que l’amour
puisse coexister avec des formes de domination est patriarcale en soi.

Ainsi, certains hommes, se définissant comme féministes, refusent de
prendre en charge leur partie du travail émotionnel (communication,
expression de ses besoins, écoute) dans leur couple ou restent
convaincus d’une différence naturelle entre les besoins des hommes et
ceux des femmes, cantonnant ces dernières dans le rôle du soin et *in
fine* de l’amour. Ils refusent la position de domination et les
privilèges induits par le fait d’être un mec cis. Par contre, ils se
placent dans des positions infantilisantes et recréent des situations où
leur partenaire se retrouve en charge du travail de *care.*

Les femmes sont encouragées par l’éducation patriarcale à penser
qu’elles devraient être aimantes. Mais cela ne veut pas dire qu’elles
soient dotées de capacités émotionnelles innées supérieures à celles des
hommes. C’est un travail qui s’apprend. Cette pensée favorise des
arrangements genrés où l’homme a plus de chance de voir ses besoins
émotionnels comblés, profitant ainsi d’un bien-être émotionnel lui
permettant de s’épanouir dans la sphère publique au détriment des
femmes.

##### Pour des institutions féministes

Repenser l’intime est politique. Si nous voulons changer les structures
de nos relations amoureuses, cela demande de militer pour des
transformations systémiques, et pas seulement de changer sur un plan
individuel. Rendre la sphère publique plus accueillante pour les femmes,
les trans, les personnes non-binaire ; démystifier la possession du
corps des femmes par l’homme dans le contrat hétérosexuel monogame ;
délier l’amour du sexe pour une sexualité libre… Tout cela passe par
des changements sociaux.

Monique Wittig prône le lesbiannisme politique pour sortir des relations
de domination justifiées par l’amour. Les rapports de pouvoir genrés
sont au fondement de l’hétéronormativité. Mais dans tous les couples (y
compris de même sexe), d’autres enjeux se croisent : les ressources
économiques, le capital social et culturel, la race, l’âge… Lors
d’une intervention sur sa thèse, Kevin Diter expliquait que
l’hypogamie masculine, soit le fait pour un homme de fréquenter des
femmes ayant un statut social inférieur, représente 45% des unions. Dans
l’autre sens, dans le cas d’une hypogamie féminine, le fait que la
femme soit le principal agent économique du foyer atténuerait les
inégalités genrées de répartition des tâches ménagères. Cela dit
beaucoup des structures sociales qui impactent l’amour. Nous aurons
beau changer nos relations, est-ce viable dans un système capitaliste et
patriarcal ?

Dans la continuité des combats militants pour le droit à l’avortement, à
la contraception, il nous reste de nombreux terrains de lutte. Pour le
dire dans les mots de Victoire Tuaillon, dans l’épilogue du *Cœur sur la
table* : il ne peut pas y avoir de révolution amoureuse sans changement
radical de nos conditions matérielles d’existence. Et je m’arrêterai ici
sur deux exemples : le travail et le travail du sexe.

L’institution du travail prend énormément de place dans nos vies. Il
faut penser le travail dans le contexte d’une société (de consommation)
qui valorise la réalisation du désir, misant de plus en plus sur la
productivité pour accumuler le capital (capitaliste), ainsi que sur la
responsabilité individuelle de s’en sortir (néolibérale). Pour bell
hooks, la société de consommation est créatrice d’isolement social.

Concrètement, avoir des ami·es, des amoureux·ses, c’est un
investissement économique, ça coûte de l’argent : aller boire des
bières, aller au restaurant, au cinéma… Aujourd’hui vivre en lien
demande un certain capital social, économique et culturel. C’est un
facteur excluant de la vie sociale.

Pour bell hooks, la culture de la consommation remplace la satisfaction
de nos besoins émotionnels -- engageante et difficile à obtenir -- par
la satisfaction de nos désirs matériels. Mais le propre de la société de
consommation est de sans cesse créer de nouveaux besoins, alimentant une
avidité sociétale parce que la satisfaction n’est que partielle et
momentanée. Cette avidité légitime l’exploitation et la domination, la
compétition économique, qu’on retrouve dans le travail.

L’autrice montre que la « *culture de l’avidité* » a orienté les
politiques sociales des Etats-Unis. Elle pourrait aussi bien parler de
la France : loi travail, réforme des retraites, réforme de l’assurance
chômage, alors que la France rechigne à taxer les superprofits des
grandes entreprises et supprime l’ISF… Ironiquement, les classes
privilégiées exploitant leur capital économique et social hérité sont
les premières à défendre la valeur du travail pour gagner sa vie -- pour
les pauvres bien entendu. Cette argumentation sert non seulement à
protéger leur intérêt de classe à ne pas partager leurs ressources, mais
aussi à nier leur responsabilité envers les personnes précaires.

Or prétendre qu’il faut « aimer ce que l’on fait » et choisir de « faire
ce que l’on aime », c’est un discours qui fonctionne seulement pour les
personnes économiquement indépendantes. Pour les personnes qui voient le
travail comme une vente de leur force productive pour acquérir des
moyens de survie, c’est-à-dire, dans la société capitaliste, de
l’argent, l’amour n’a rien à faire ici. Pour les personnes sans capital
ou rente, le choix de faire ce que nous aimons nous arrime à la
précarité économique (en général, le RSA), sociale (les personnes
n’ayant pas d’argent sont exclues de toutes les activités nécessitant un
investissement financier) et psychologique (la société renvoie
constamment les non-travailleurs volontaires à leur inutilité sociale et
à un comportement déviant). Moi je veux bien écrire des lettres d’amour
à ma conseillère Pôle Emploi, mais je pense qu’elle va me radier.

Nous manquons de temps, d’énergie et de disponibilité émotionnelle pour
aimer. Au-delà du congé parental égal (qui devrait être une évidence),
travailler moins, pour des salaires décents, questionner la centralité
du travail en tant que tâche nécessaire pour réinvestir la notion de
plaisir dans nos vies, me semble être un vrai projet de société. En
rigolant à moitié, nous envisagions avec un ami de travailler un jour
dans des structures où le temps dédié à faire l’amour serait rémunéré,
où nous pourrions nous absenter en pleine journée pour aller voir un·e
amant·e, parce que si nous ne sommes pas indispensables, cela nous
rendra par contre plus efficaces. Partager le travail, le repenser à
l’aune de l’activité amoureuse… Ce sont des sujets que nous devrions
amener aux réunions d’équipe du lundi matin.

Dans son livre, bell hooks mentionne les hippies de la guerre du Vietnam
qui seraient devenus des entrepreneurs embrassant les valeurs et le
système capitaliste. Pour elle, une fois que ces personnes commencèrent
à faire des enfants, ils eurent peur, s’ils continuaient à promouvoir
une vision communaliste, de devoir faire avec moins. C’est typique de la
guerre des imaginaires : nous vivons dans une culture baignée par le
consumérisme et où la capacité à s’en sortir, puis à avoir un métier
stable, une maison pavillonnaire dont nous sommes propriétaire, un
garçon, une fille et une Volvo, constituent un horizon désirable pour la
majorité d’entre nous.

Sortir de la centralité de la valeur travail est un combat politique qui
pourrait réunir hommes et femmes. Comme le rappelle Virginie Despentes
dans *King Kong Théorie*, « *les corps des femmes n’appartiennent aux
hommes qu’en contrepartie de ce que les corps des hommes appartiennent à
la production en temps de paix, à l’État en temps de guerre. La
confiscation du corps des femmes se produit en même temps que la
confiscation du corps des hommes* ». La valorisation de la maternité et
de la production d’enfants est un des rouages du capitalisme, produisant
des mères et des esclaves.

Parler de Virginie Despentes m’amène à l’autre exemple d’institution
monogame hypocrite que nous devrions défaire, soit la condamnation de la
prostitution. Le travail du sexe est souvent épinglé par les médias
comme une transaction violente, orchestrée par un proxénète qui prive
les femmes de leurs papiers et se paye grassement sur la vente de
rapports sexuels moyennement consentis. Je ne dis pas que cela n’existe
pas. Je dis que certains médias en font une vérité universelle, alors
que je pense qu’il existe des situations multiples et que la réalité est
plus complexe. Ce que pointe Virginie Despentes, dans *King Kong Théorie*,
c’est que la monétisation du rapport sexuel dérange parce que le travail
féminin (domestique et sexuel) devrait être bénévole.

En lisant les collages féministes sur les murs à Marseille, je croise
régulièrement la phrase : « un mari = un client pour toute la vie ».
Condamner le travail du sexe, c’est nier que le sexe est toujours une
forme de transaction. Nous ne couchons pas ensemble, les uns avec les
autres, de manière désintéressée. Je couche avec les gens parce que j’ai
envie de recevoir du plaisir, parce que j’en ressens en le procurant,
parce que cela peut fluidifier mes rapports avec mes partenaires, parce
que cela peut me procurer un avantage dans le cadre de notre relation
extérieure au sexe, parce que je me sens seule… Et dans le cadre d’une
relation amoureuse établie, je couche parfois avec l’autre pour lui
faire plaisir, alors que je suis fatiguée, que j’ai envie de lire un
bouquin, ou d’éteindre la lumière. Je ne pense pas que ce soit grave en
soit, mais je pense qu’il faut le conscientiser et le dédramatiser. Si
le sexe est une transaction, il est monétisable. Et ce n’est pas moins
valorisant que d’être vendeur d’assurances.

C’est aussi parce que le travail du sexe touche au sacro-saint union
entre le sexe et l’amour qui fonde le contrat monogame qu’il est
pénalisé. Comme le premier ne saurait exister sans le deuxième,
consommer le travail du sexe est ramené à un comportement déviant,
anormal. C’est une forme de pénalisation du désir. Comme le montre
Virginie Despentes, cette volonté d’éloigner le désir du cœur des
villes, de cacher les failles du mythe monogame fragilise les
travailleuses du sexe, plus que leur emploi. Pénaliser leur activité
rend leurs conditions matérielles d’exercice de leur travail
périlleuses : *« \[…\] les lois Sarkozy repoussent les prostituées de
rue en dehors de la ville, les contraignent à travailler dans les bois
au-delà des périphériques, soumises aux caprices des flics et des
clients \[…\] ; le gouvernement décide de déporter hors des villes le
désir brut des hommes »* (p.81).

Le manifeste féministe pro-droits des travailleuses et travailleurs du
sexe met à jour cette critique. Il montre que depuis 2016, l’évolution
répressive de la législation concernant la prostitution et la
pénalisation du client, amené à prendre plus de risque, le place dans un
rapport de force pour potentiellement négocier l’interaction (tarifs,
lieu, pratiques). De la même manière, les lois encadrant aujourd’hui la
prostitution et le proxénétisme (le fait d’aider, d’assister ou de
protéger la prostitution d’autrui) criminalisent même l’entraide avec
leurs proches : partage de locaux, prêt d’argent, services rendus,
cohabitation… Je vous invite à lire cet article que je résume ici,
mais dont la clarté et la précision permettent de saisir tous les enjeux
qui parcourent la pénalisation du travail du sexe.

Ici encore, le combat politique pourrait nous unir au-delà du genre :
dépénaliser le plaisir, reconnaître le sexe comme un travail méritant
compensation, dissocier le sexe de l’amour monogame, cela donne quelques
espoirs pour reconstruire nos cadres intimes et amoureux, d’une manière
politique.

##### Choisir d’aimer comme une action plutôt qu’un sentiment irrépressible

Bon, une fois que nous avons fait la révolution, est-ce que nous pouvons
repenser les conditions d’un engagement amoureux sans craindre
l’oppression ? Une fois que les conditions matérielles changent, est-ce
qu’on peut envisager l’amour (notamment hétérosexuel) hors du cadre
patriarcal ? Je ne pense pas que nous puissions mettre fin aux systèmes
de domination à travers une approche individuelle. Il faut s’en prendre
à la compétition économique et à l’injonction à la productivité pour
offrir des cadres épanouissants à nos relations.

Dans ces combats sociaux, nous pouvons nous appuyer sur une éthique de
l’amour en commun. Selon bell hooks, l’éthique de l’amour a traversé
tous les mouvements sociaux. En ce sens, elle propose de redéfinir ce
terme en dehors du spectre romantique. Selon l’autrice, l’amour est
souvent défini comme un sentiment alors que nous gagnerions à ce qu’il
soit pensé en tant qu’action, informé par une intention et donc par un
choix. Celui-ci induit une forme de responsabilité sur nos actions et
leurs conséquences.

Dès lors, l’amour se compose de plusieurs ingrédients : le soin,
l’affection, la reconnaissance, le respect, l’engagement, la confiance,
l’honnêteté et la communication ouverte. L’amour permet ainsi de
développer, chez soi et l’autre, la capacité à se réaliser et à
entretenir une ouverture vers le monde extérieur, grâce à un état de
disponibilité mentale, émotionnelle et physique. Le définir ainsi permet
de reconnaître nos aliénations. Par exemple, dans sa conception de
l’amour, des attitudes de négligence ou de maltraitance ne peuvent pas
coexister avec l’amour.

Ici, la vision de l’engagement, au cœur de la définition de bell hooks,
est décalée : il ne s’agit plus de s’engager à vivre heureux ensemble
jusqu’à la fin de ses jours, mais à remplir un certain nombre de
conditions pour assurer l’épanouissement de l’autre et de soi dans la
relation. Cela induit entre autres :

-   Abandonner tout attachement aux formes de pensées sexistes et de répartition genrée des rôles ou des attributs

-   Admettre que nous voulons aimer et être aimées, même si nous ne savons pas ce que cela signifie (qu’on veut des câlins quand on a de la fièvre)

-   Ressentir notre vulnérabilité et la laisser parler, apprendre à gérer notre sentiment d’impuissance face à celle de l’autre (par exemple, savoir exprimer ma tristesse à l’autre, en acceptant sa culpabilité comme un sentiment qui lui appartient, sans mélanger les deux ou me sentir coupable à mon tour)

-   Créer des rituels de partage et d’écoute de nos vulnérabilités (les cafés gênants !)

-   Accepter la souffrance, inhérente à toute relation (en sachant différencier la souffrance constructive, celle qui nous pousse à changer les termes de la relation, de la souffrance gratuite, celle qui est infligée sans questionnement)

-   Partager des ressources : temps, attention, objets, compétences, argent, pardon comme moyen d’exprimer de l’amour (et pas qu’avec son amoureux·se !)

Ce décalage de l’intention de l’engagement me semble intéressant. Je
crois que ce qui est difficile aujourd’hui dans l’engagement, c’est
qu’il nous demande de prétendre que nous serons toujours les mêmes et
que nous aurons les mêmes envies au cours du temps. Or le *je* n’est pas
quelque chose de figé, et il est difficile de se projeter dans l’avenir
avec les intentions du présent. Nous allons changer et nous le savons,
alors nous ne voulons pas fixer la suite. Alors peut-être qu’avec
l’inspiration éthique de bell hooks, nous pourrions faire des promesses
évolutives. C’est-à-dire, se promettre, tant que nous avons les moyens
de maintenir les conditions de notre épanouissement personnel et de
l’autre, de continuer à le faire. De se dire quand nous n’avons plus
l’énergie de transformer les relations, de faire le travail émotionnel
et les compromis que cela demande.

*Le Petit Prince* de Saint-Exupéry est inspirant à cet égard. A
l’origine du voyage se trouve une rose, que le Petit Prince écoute,
protège, mais cela ne suffit jamais à la rose. Alors le Petit Prince
part explorer d’autres planètes, parce qu’il n’a plus l’énergie à donner
à cette relation. Il rencontre le renard, qui lui demande de
l’apprivoiser, c’est-à-dire de créer un lien particulier, qui leur
donnera besoin l’un de l’autre. Avec de la patience, il l’apprivoise. Et
c’est le renard qui lui montre que parmi toutes les roses, la sienne est
spéciale parce qu’il lui a donné du temps et du soin. L’amour est un
acte transformatif, pour soi et pour l’autre. C’est un ancrage (même
temporaire) qui nous permet de changer, d’aller vers l’autre.

##### L’engagement : que peut l’amour face à la mort ?

En parlant d’engagement, j’ai eu besoin de me questionner sur son
importance dans ma vie. Pourquoi est-ce que je tiens tant à réhabiliter
l’engagement dans ce bouquin ? Est-ce que c’est une valeur ou un résidu
de l’éducation patriarcale ?

Je crois que l’amour romantique – le mythe de grandir et vieillir
ensemble, tient beaucoup à notre peur de la mort, à l’angoisse de mourir
seul·e. Plus largement, je crois que l’engagement joue le rôle d’une
sécurité, d’un filet affectif qui nous permettrait de nous accrocher
face à l’imprévu. Il suffit de regarder les mouvements sociétaux de
libéralisation de l’économie, de casse sociale, les conséquences du
changement climatique auxquels nous faisons face ces dernières années :
hausse du prix de l’énergie, réforme du chômage, canicules et sécheresse
estivales. La société change et nous savons que nous ne savons pas à
quoi ressemblera notre avenir. L’engagement, en tant que filet de
sécurité affectif, apparaît comme une manière de nous réassurer
collectivement.

Ici encore une fois, il s’agit de s’interroger sur la manière de définir
l’engagement. Si nous laissons sa dimension temporelle de côté pour nous
concentrer sur les promesses évolutives d’assurer l’épanouissement de
chacun·e, nous pourrions former des alliances et un ancrage solide
contre l’incertitude sociale.

De même, la peur de la mort s’explique par ce besoin collectif de
transcendance et de laisser une trace. Nous avons besoin de savoir que
quelqu’un·e se souviendra de nous. Dans le sens inverse, Buyng-Chul Han
voit notre incapacité à conclure et à s’engager dans une relation
exclusive comme la peur de la mort, soit des conclusions, dont
l’accumulation et la consommation nous détourne.

Dans le film *Inception* (spoiler alert pour ceux qui ne l’ont pas vu),
la scène la plus marquante pour moi se situe dans les limbes. On y
retrouve Mal (Marion Cotillard), qui comprend que Cobb (Léonardo Di
Caprio) a instillé dans son esprit l’idée que son monde n’était pas réel
(cause de son suicide ultérieur). Je trouve sa réaction très
intéressante : elle oscille d’abord entre la tristesse et la colère
d’avoir été trompée, d’avoir construit sa vie sur un mensonge. Mais très
vite, elle essaie de convaincre Cobb de rester : s’il lui a menti, il
peut faire amende honorable en tenant ses promesses, c’est-à-dire de rester
ensemble et de vieillir à deux.

C’est exactement la manière dont je me comporte en déconstruisant
l’amour romantique. J’ai compris que les relations amoureuses basées sur
un principe d’éternité immuable n’ont été fonctionnelles dans l’histoire
qu’au prix de l’oppression, de l’exclusion, de la privation de liberté.
Mais je ne veux pas mourir seule. Alors, à chaque nouvel amoureux, je
projette cette image qu’inventent Mal et Cobb de leur vie fantasmée dans
les limbes : deux vieillards se tenant la main contre le monde.

C’est ok de se raconter des histoires. Je suis persuadée que nous avons
besoin de croire. Mais cette croyance en l’amour romantique pose
plusieurs questions auxquelles j’aimerais trouver d’autres réponses, ne
serait-ce que pour avoir le choix : Pouvons-nous créer des formes de
liens alternatifs à l’amour romantique qui nous permettent de ne pas
vieillir seul·es ? De quoi avons-nous besoin pour accepter la mort dans
nos vies ? Quelles sont les croyances qui peuvent nous aider face au
temps qui passe et libérer nos amoureux·ses de l’injonction à rester
avec nous toute notre vie parce que la mort ça fait peur ?

La peur de la mort entraîne une obsession pour la sécurité (lois de
surveillance, plan vigipirate, *gated communities*, durcissement des
conditions d’obtention de la nationalité française). Elle nous amène à
considérer l’inconnu et ce qui nous est étranger comme un danger. Or,
selon bell hooks, le patriarcat nous apprend que la masculinité doit
être performée en répondant à la peur par l’agression.

Peut-être que nous aurions besoin de parler franchement de la mort. D’en
parler en classe quand nous sommes enfants, d’en parler entre adultes
quand nous commençons à avoir peur du temps qui passe, d’en parler à nos
compagnon·ne·s de vie, de s’autoriser à dire « j’ai peur de mourir
seul·e ». L’anxiété quant à la mort baigne notre société (crise
écologique et insécurités quant au devenir de l’humanité, guerres
lointaines, maladies de nos proches). Pourtant nous refusons d’accepter
qu’elle fait partie de la vie. L’accepter reviendrait à reconnaître
l’inattendu, le manque de contrôle sur nos expériences, l’aspect
transitoire de nos existences, et donc in fine de nos amours. Peut-être
que si nous acceptions de mourir, nous aurions moins peur de la mort de
nos histoires d’amour.

Il y a quelques années j’ai fait une méchante chute à cheval. J’ai eu la
chance de m’en sortir avec une vertèbre cassée et trois mois
d’immobilisation. Mon amoureux de l’époque m’a quittée à cette
période-là. Et je me sentais plus en danger du fait de la mort de cette
relation sur laquelle j’avais projeté mon avenir, que de ladite chute et
de mon corps abîmé.

Je pense qu’inconsciemment, ces deux phénomènes concomitants me
renvoyaient à la même chose : je n’avais pas le contrôle. J’avais beau
faire des plans et imaginer un futur désirable pour moi, la vie ne
fonctionnait pas de cette manière. En même temps, j’avais l’impression
de grandir d’un coup : je découvrais dans ma chair que l’amour
romantique comme mythe n’existait pas. Que je n’étais pas plus maline
que les autres, que je ne passais pas entre les mailles du filet. Les
relations se faisaient, se défaisaient, les partenaires d’un couple et
leurs besoins évoluaient, nous changions. Plus encore, ma relation
amoureuse n’avait pas été exempte de dynamiques de pouvoir genrées, de
jalousies et de compétition.

Ce que cette période a eu de positif et de vertigineux, c’est l’espace
qu’elle a laissé. L’espace affectif et émotionnel, l’espace temporel lié
à mon immobilisation. Le premier m’a permis de souffler et de prendre
conscience de *mon* identité en me posant les questions : qu’est-ce qui
m’appartient ? qu’est-ce qui m’a été légué de cette relation et que je
chéris ? qu’est-ce que je souhaite laisser derrière ? De comprendre
également qu’elle continuait de vivre en moi par tout ce qu’elle y avait
ancré : l’amour du cinéma, de la fête, des paquets de chips, la langue
espagnole, une connaissance de mon corps, de mon plaisir sexuel,
beaucoup de souvenirs.

L’espace temporel m’a permis de me recentrer sur ce qui était important
pour moi. J’ai fini la première version de mon premier roman, je me suis
faite accompagner par l’autrice Laura Vazquez pour cela et j’ai commencé
à percevoir l’écriture comme un « vrai travail ». Je n’étais pas encore
au point d’envisager d’en faire un métier, mais dans mon récit
personnel, je vois ce point comme la bascule qui m’a menée à en faire ma
profession.

Tout cela pour dire que cet été-là m’a fait comprendre que nos
expériences sont transitoires. Je suis encore pleine de la peur du vide,
de l’envie que ça dure longtemps avec mes amoureux, d’angoisses
existentielles quant à la possibilité de vivre de l’écriture et
d’ateliers, mais je crois que je gère mieux l’idée que les choses
puissent se passer autrement que prévues. Je pense que c’est pour ça que
c’est important de parler de la mort, de l’inviter dans nos vies. Cela
nous offre la possibilité de nous rencontrer, d’aimer les autres sans
dépendance.

De la même manière, nous pourrions nous autoriser à parler de la fin de
nos relations. Envisager la rupture à un moment où tout va bien, pour se
demander dans quelles conditions nous souhaiterions qu’elle se passe.
J’entends par là définir la manière de rendre le changement ou la fin
du lien moins douloureux que la tragédie de la fin du couple monogame.
Dessiner quelque chose de plus doux, anticipé et préétabli en fonction
des capacités et des insécurités de chacun·e. Avant que les choses se
délitent, nous pourrions dessiner des scénarios, sinon joyeux, au moins
empathiques.

C’est aussi une façon de définir les limites que nous ne souhaitons pas
franchir, pour l’autre ou pour nous-même. J’ai souvent perdu mon temps
et mon énergie dans la réparation de relations, sans prendre le recul de
savoir si elles étaient encore saines et épanouissantes pour nous. Je
souhaitais sauver l’entité « relation » plutôt que me demander si
elles nous faisaient du bien. Je me dis que si nous avions envisagé
avant les conditions d’un départ apaisé, les éléments que nous
n’étions pas prêts à sacrifier l’un pour l’autre, nous serions
sorti·es moins amoindri·es de ces ruptures. Comme le disait
Nelly lors de notre entretien, si nous nous décentrons du couple,
lorsqu’une relation s’arrête, nous ne perdons pas notre moitié. Nous
sommes entier·es car nous l’avons toujours été.

##### S’engager dans des communautés d’amour

L’engagement dans l’amour n’est pas réservé au couple, à la famille, aux
relations de sang ou romantiques. La structure monogame nous pousse à
mettre les relations amoureuses au-dessus des autres, parce qu’elles
poursuivent un but reproductif et un engagement à durée indéterminée.
Comme le montre bell hooks, cela crée de l’isolement et nous rend plus
difficile l’accès à des portes de sortie, à un filet de soutien et de
sécurité quand ces relations deviennent dysfonctionnelles ou abusives.

Sortir du schéma monogame et romantique, c’est politique. Le couple est
basé sur une répartition inégalitaire des rôles et des devoirs. En se
décentrant du duo, les tentatives de réinvention des modèles amoureux
limitent la prise que les hommes cis peuvent avoir sur d’autres
personnes au quotidien. Cela change aussi notre conception de l’amour,
pour le vivre dans des cadres amicaux, des cadres sans sexualité, etc.

Et cela transforme le paradigme de nos relations au niveau sociétal. En
refusant la fusion identitaire du couple, nous devons repenser les
modalités d’héritage et de transmission du patrimoine, de logement, de
parentalité… Aujourd’hui, une personne seule est désavantagée
économiquement et socialement par rapport à un foyer en couple. Pour
réinventer nos modes de vie, il est important de créer et de partager
des communs, d’apprendre à vivre avec les autres.

En cela, redéfinir l’amour en dehors du carcan romantique, c’est
supprimer la hiérarchie entres les relations importantes de nos vies.
Communautariser les ressources temporelles, économiques, émotionnelles ;
construire sa vie en fonction du réseau affectif plutôt que du noyau
nucléaire.

C’est aussi refuser les dynamiques de compétition entre personnes du
même genre, savoir se retirer ou trouver un compromis pour le bien
commun. Vivre dans un esprit de communauté, c’est comprendre que ce qui
est donné à l’un ne m’est pas enlevé à moi. C’est savoir donner de son
temps et de son énergie à la hauteur de notre reconnaissance de
l’interdépendance du groupe. Alors nous pourrons laisser de la place à
d’autres formes de partage et d’amour, à de nouvelles manières de
*choisir familles*.

Bibliographie

**Livres** :

DESPENTES, Virginie, 2007. *King Kong théorie*. Paris : Librairie générale française. Le livre de poche, 30904. ISBN 978-2-253-12211-1. 848.914 03.

HOOKS, bell, 2018. *All about love: new visions*. First William Morrow paperback edition. New York : William Morrow. ISBN 978-0-06-095947-0.

HUNDT, Hina, 2021. *Nos amours radicales: 8 visions singulières pour porter un regard nouveau sur l’amour*. Vanves : les Insolentes. ISBN 978-2-01-710130-7. 305.42

ILLOUZ, Eva et JOLY, Frédéric, 2014. *Pourquoi l’amour fait mal: l’expérience amoureuse dans la modernité*. Paris : Éd. Points. Points, 744. ISBN 978-2-7578-4576-9. 306.7

SAINT-EXUPÉRY, Antoine de, 2007. *Le petit prince*. Paris : Gallimard. Folio junior, 100. ISBN 978-2-07-061275-8. 809

STRÖMQUIST, Liv, 2019. *La rose la plus rouge s’épanouit*. Paris : Rackham. Sous le signe noir de Rackham. ISBN 978-2-87827-235-2. 805

TOLSTOÏ, Léon, LUNEAU, Sylvie, PAUWELS, Louis et MONGAULT, Henri, 1994. *Anna Karénine*. Paris : Gallimard. Collection Folio. ISBN 978-2-07-039252-0. 823

VASALLO, Brigitte, 2018. *Pensamiento monógamo, terror poliamoroso*. Cuarta edición, revisada. Madrid : La Oveja Roja. Ensayo. ISBN 978-84-16227-24-2.

WITTIG, Monique, 2023. *Le corps lesbien*. Paris : les Éditions de Minuit. Double, 130. ISBN 978-2-7073-4829-6. 801

**Podcasts** :

Ex-ologie, une vie de célibataire, 2022. *France Culture* [en ligne]. [Consulté le 2 août 2024]. Disponible à l’adresse : https://www.radiofrance.fr/franceculture/podcasts/serie-ex-ologie-une-vie-de-celibataire

Le Cœur sur la table - Binge Audio, [sans date]. *Le Cœur sur la table - Binge Audio* [en ligne]. [Consulté le 2 août 2024]. Disponible à l’adresse : https://www.binge.audio/podcast/le-coeur-sur-la-table

Réinventer L’ Amour, 2022. [en ligne]. [Consulté le 2 août 2024]. Disponible à l’adresse : https://reinventerlamour.lepodcast.fr/

**Articles** :

Manifeste féministe pro-droits des travailleuses et travailleurs du sexe, [sans date]. *Manifeste féministe* [en ligne]. [Consulté le 3 août 2024]. Disponible à l’adresse : https://manifestefeministe.fr/

Marriage and Love by Emma Goldman 1914, [sans date]. [en ligne]. Disponible à l’adresse : https://www.marxists.org/reference/archive/goldman/works/1914/marriage-love.htm

Wilhelm Reich et la révolution sexuelle, [sans date]. *Chroniques critiques* [en ligne]. Disponible à l’adresse : http://www.zones-subversives.com/article-wilhelm-reich-et-la-revolution-sexuelle-105132788.html

Wittig avec Cixous : horizons politiques de la réinvention de l’Eros, [sans date]. *Trounoir* [en ligne]. Disponible à l’adresse : https://trounoir.org/Wittig-avec-Cixous-horizons-politiques-de-la-reinvention-de-l-Eros


Entretien avec Angie Triolo par
Athina Gendry. Polyamour : on répond aux clichés, [Juillet 2022].
*Hétéroclite* [en ligne] . [Consulté version du 28 novembre 2022 via archive.org].  Disponible à l’adresse : https ://www.heteroclite.org/2022/07/polyamour-on-repond-aux-
cliches-66015

Films/séries :

Fryman, Pamela, réal. "Return of the Shirt". Dans : *How I Met Your Mother*. Saison 1, épisode 4. États-Unis : CBS, 2005.

Luhrmann, Baz, réal. *Moulin Rouge!*. États-Unis, Australie : 20th Century Fox, 2001.

Nolan, Christopher, réal. *Inception*. États-Unis : Warner Bros., 2010.

Serebrennikov, Kirill, réal. *Leto*. Russie, France : Hype Film, 2018.

Wachowski, Lana, réal. *Matrix Resurrections*. États-Unis : Warner Bros., 2021.

## Choisir nos parentèles : élargir la famille

(éçè)

La famille est pleine de lieux de commun.

Dans la famille, on s’aime. C’est un prérequis. On peut s’engueuler,
mais on s’aime. De toute façon, nous n’avons pas le choix. La famille,
c’est la famille. On doit faire avec. On en a une, on ne peut pas en
avoir d’autres. On se retrouve pour Noël, les anniversaires et les
enterrements, parfois plus, parfois moins. On ne sait pas toujours trop
de quoi parler. De temps en temps, le ton et les tensions montent. On
n’en parle pas, ce n’est pas la peine de se brouiller. En plus, cela
pourrait laisser sortir d’autres rancœurs. On s’aime, c’est suffisant.
Promis, on reviendra l’année prochaine, ça fait tellement plaisir aux
grands-parents.

Devenir parent, c’est montrer que l’on s’aime assez pour s’engager dans
la durée. Les enfants, cela prend du temps et de l’énergie, mais l’amour
qu’on leur porte fera le reste. Les fratries se cherchent des noises,
mais ça passe avec le temps et elles se retrouvent témoins de mariage
les unes et des autres. Être là, ensemble c’est ça qui compte. Si dans
le couple cela chauffe vraiment, on lave le linge sale en famille, on
préserve notre image, on s’affiche comme de bons parents. On attend de
chacun·e de remplir des rôles, de jouer un rôle, ses rôles.

La famille, on est prêt à faire des concessions pour elle, à abandonner
beaucoup. Son importance fait que l’on met de l’eau dans le vin des revendications
personnelles. Si on se sépare, c’est un constat d’échec. L’amour n’était
pas assez fort, on a trouvé mieux ailleurs, on a eu d’autres priorités
que sa famille. Après la séparation, il faut s’organiser pour les
enfants, les pensions, régler (ou pas) les frustrations enfouies. Les
dominations et leurs effets apparaissent au grand jour. Il faut se
reconstruire, balayer cette relation. Surtout, retrouver l’amour, pour
recommencer sa vie avec une nouvelle famille.

Je vais être assez dur avec le modèle familial dominant. Je ne remets
pas en question que cela puisse fonctionner, produire de l’amour et de
la joie. En toute honnêteté, c’est le seul modèle aujourd’hui disponible
pour ne pas vivre seul·e. Comme le dit la chercheuse Kim Tallbear, elle
nous permet de « nous installer dans une relation romantique occidentale
de long terme, qui répond à la peur d’être seul·e. On se dit, il
vaut mieux que j’aie quelqu’un à moi maintenant » que peut-être
personne plus tard.

Néanmoins, ce modèle n’y réussit pas si souvent, produisant son lot de
souffrances. Comment en sortir ? Où aller ? Nous manquons de références
auxquelles le comparer. Quand il y a une seule façon de faire, il est
difficile d’imaginer que si nous avions fait autrement, nous aurions été
plus heureux·ses.

L’institution du mariage n’a plus sa splendeur et son importance d’antan
(tout en étant loin de disparaître). La famille, elle, reste dans tous
les pays européens le « domaine de la vie toujours le plus valorisé,
sous la forme d’un couple où règne la communication entre conjoints et
les relations interpersonnelles » écrit Martine Segalene dans
*Sociologie de la famille*. À cause de cette place dans nos vies et dans
nos relations, nous nous devons de réfléchir à l’amour dans la famille
et dans les couples qui vont souvent avec.

Notre représentation fantasmée des familles ne correspond déjà plus
complètement à la réalité. Rappelons-nous que depuis une cinquantaine d’années, la famille a été fortement transformée d’un point de
vue législatif : la fin des références à l’autorité parentale, le
travail salarié des femmes, l’autorisation de la contraception, le
mariage homosexuel… Dans la pratique, la loi n’est pas toute
puissante et l’égalité proclamée est loin d’être atteinte dans les faits. La famille
est une institution, qui, si elle est affectée par les lois, sait aussi
évoluer sans elles. De fait, elle est souvent en avance sur la
législation, qui vient dans un second temps légitimer des pratiques. On
peut penser au fait que bien avant le PACs (1999) qui est venu leur
donner un statut, « les couples ont pris la liberté de vivre ensemble
sans être mariés » (*Sociologie de la famille*). Les divorces ont mené
aux « familles recomposées », qui après avoir posé question, sont
devenues des familles habituelles, reconnues comme telles et acceptées
dans la société. Elles offrent un premier type de diversité, avec des
enfants de différents parents qui vivent ensemble en acquérant
potentiellement un·e troisième parent·e (ou référente). Ainsi que des
frères et sœurs sans lien de sang (ou à demi). Les familles recomposées
créent des parentalités non biologiques.

Une partie de ces familles fonctionnent bien et surtout, elles sont considérées
par la société comme des familles à part entière. Dans un article sur
l’évolution des modèles généalogique, Agnès Martial rappelle que dans
les années 50, « les beaux-parents avaient un rôle de remplacement pour
adopter l’apparence d’une "vraie" famille, à travers l’utilisation des
termes référant à la filiation (maman, papa, père ou mère) ». Depuis,
avec l’augmentation des divorces et des familles recomposées
(aujourd’hui, 10% des familles), le modèle généalogique a évolué, « la
norme de la substitution a été remplacée par celle de la pérennité des
liens de l’enfant avec ses pères et mères » (d’origine). Néanmoins, en
France, d’un point de vue légal, même si lae beau-parent·e « joue le
rôle d’un parent additionnel, il n’existe pas de reconnaissance du
statut de beau-parent ».

Premier point, aujourd’hui en France, pour faire famille nous savons
nous passer de lien de sang, ainsi que de la reconnaissance des enfants par les
deux parents. De plus, nous savons être parents d’enfants qui ne sont
génétiquement pas les nôtres sans pour autant faire partie d’une
démarche d’adoption. Depuis les années 50, les familles monoparentales
sont elles aussi arrivées sur le devant de la scène. Elles existaient
déjà, mais elles étaient invisibilisées et/ou méprisées. Malgré toutes
les difficultés posées par ce schéma plus souvent subi que désiré, il
montre que deux adultes ne sont pas nécessaires pour faire famille.
Puis, le *Mariage pour tous* (qui pourrait être le *Mariage pour toustes*),
a fait admettre à la société que l’hétérosexualité n’était pas
nécessaire pour former une famille. Enfin, les familles avec un enfant
né d’un don d’ovocyte ou de mère porteuse séparent l’acte biologique de
la conception de celui de la fondation de la famille.

D’après l’Insee, 44% des enfants vivent hors d’une famille
traditionnelle (2022). La famille nucléaire classique, si elle reste
pour beaucoup un objectif de vie, va bientôt dans les faits devenir
minoritaire.

Mettons au clair les objectifs de cette partie :

-   Remettre en question la famille classique nucléaire comme seul
    horizon désirable
-   Imaginer ce qui pourrait venir en plus
-   Inscrire ces modalités dans un futur troublé

### Partir de ce que l’on a : les familles nucléaires

La famille est, comme l’amour romantique ou la binarité, un cadre
normatif. Si ces deux-là commencent à être salement amochés par le
travail des militant·es, des auteurices et des idées qui infusent dans
la société, la famille reste dans les imaginaires assez intacte.
L’évolution (plutôt individuelle) des normes de genre est une
transformation qui va puissamment défier la répartition des rôles dans
les couples, la procréation ainsi que nos méthodes éducatives. Le
mouvement est en route, mais nous sommes loin d’être au bout du chemin
et malheureusement les retours en arrière restent possibles. Je vous
propose de tricoter des réponses, pour les assembler en patchworks de
possibles. Nous devons avancer sans attendre. Alors on va bricoler,
mettre les mains dans le cambouis des idées, tourner des potards, mettre
le contact.

Modifier nos organisations familiales, cela implique des enfants, des
relations, des croyances, des lieux de vie… On peut vite se blesser
avec ça ! Des blessures dans le genre de celles qui laissent des
cicatrices qui suintent longtemps. Alors, il faut essayer, mais rester
prêt à se donner le temps de modifier les réglages, voire de revenir sur
nos pas. Il faut être OK avec l’idée que probablement tout ne marchera
pas. Essayer, c’est préfigurer et c’est important !

La famille est liée directement à la parenté. Nous sommes apparenté·es de
notre venue au monde jusqu’à notre mort. Nous recevons à notre naissance
(sauf cas d’abandon) une famille. En créer une nouvelle se fait en
devenant parent (ce qui je le répète, n’inclut pas forcément une
naissance ni un lien génétique), c’est-à-dire par la reconnaissance
d’un enfant ou l’adoption. La famille, c’est aussi une construction
juridique, qui dit ce qu’elle est et qui peut en faire partie. Le droit
est d’une simplicité enfermante : « de manière large, elle \[la famille]
peut se définir comme un groupe de personnes unies par des rapports de
parenté (filiation) ou d’alliance (mariage) ». Légalement, on ne peut
donc pas être une famille hors de ces deux formes, ce qui restreint
fortement les options légales d’élargir nos liens. C’est aussi ce que
l’on retrouve chez l’antropologue Maurice Godelier qui définit la
parenté comme « un univers de liens personnels – lien de solidarité
et de partage, mais aussi de dépendance et d’autorité – entre les
individus qui composent le groupe, aussi bien que ceux qui sont nés dans
le groupe que ceux qui y sont entrés par adoption ou pas le biais du
mariage » (*Métamorphose de la la parenté*). Heureusement que dans ce
domaine, l’usage peut faire la loi.

En commençant à travailler sur ce sujet, je pensais trouver autre chose.
La famille me semblait être une institution plutôt diverse, avec des
formes variées permettant l’expression de nos individualités.
L’existence d’une multitude de variations, nous permettant de la créer à
notre image (par le foyer, son organisation interne, les parties
prenantes) est une impression trompeuse. Dans les faits, absolument pas :
pour être une famille légale, il faut soit un mariage, soit avoir un
enfant et des liens de parenté légalement établis avec lui. Cela fait
peu de variété. C’est cette étroitesse des liens possibles que je veux
interroger.

Pour y arriver et changer les normes, il faut remettre en cause l’idéal
de la famille, qui reste profondément enraciné dans notre société.
Aujourd’hui encore, d’après l’anthropologue Paul Rasse, « fonder une
famille demeure un horizon d’attente \[\…\], l’idéal sublime, un
refuge affectif, le dernier lieu de solidarité échappant à la
monétarisation des échanges ».   Le nombre d’œuvres basées sur la
beauté de la famille nucléaire dont l’épilogue est équivalent aux
célèbres « ils vécurent heureux et eurent beaucoup d’enfants » n’aide
pas à imaginer d’autres organisations.

Les points de stabilité qui existaient dans les vies de nos parents
comme des emplois durables, l’idée d’un progrès constant… ont disparu.
L’institution de la famille demeure un phare dans nos vies agitées. De
même, avec la disparition de modes de vie communautaire, c’est le lieu
où nous pouvons enchevêtrer nos vies et nous engager. Ce mode de
relation est censé nous apporter, ainsi qu’à nos enfants, le bonheur, la
sécurité et surtout la source d’amour de notre vie. C’est le lieu
privilégié pour exprimer notre amour en tant que partenaires de vie et
parents.

Enfin, c’est aussi une reconnaissance sociale : fonder une famille c’est
montrer que l’on est adulte. Cela va avec des attendus sociaux qui se
superposent à ceux du couple avec le renforcement de la vie à deux
autour des enfants et une socialisation de parents.

Pourtant, encore d’après Paul Rasse, « plus cet idéal est fort, et moins
il convient aux petits arrangements avec la réalité de la vie
domestique ». L’amour ne fait pas disparaître les dominations.

L’institution familiale a-t-elle progressé ? L’autrice de *Sociologie de
la famille* affirme que la famille occidentale s’est libérée de la
domination patriarcale en moins d’un demi-siècle, mais demeure
elle-même, tout en se transformant profondément. Dissociation entre
mariages et filiation, dissociation entre sexualités et reproduction,
l’importance des liens de parenté caractérisent la modernité de la
famille.

Je me permets de douter que tout soit aussi simple. On peut le voir à
travers les violences familiales qui sont maintenant bien documentées :
féminicides, viols conjugaux ou incestes. C’est dans la famille
comprenant les liens germinaux qu’ils sont les plus courants. Le travail
gratuit des femmes, voire la mise en esclavage dans les cas de violence
les plus poussés est toujours d’actualité. L’appauvrissement des
services publics, l’éloignement géographique avec la famille de sang
(souvent pour le travail) et la hausse des exigences sur l’éducation des
enfants renforcent encore le stress sur les parents qui ont moins de
soutien (surtout pour les femmes). Une des conséquences de ces pressions
est le burnout parental qui peut mener jusqu’au suicide.

La famille nucléaire, tout comme le mythe monogame hétérosexuel, doit
laisser de la place à d’autres imaginaires. J’insiste sur le « laisser
de la place », il peut y avoir un développement parallèle de plusieurs
formes de famille, qui peuvent s’intriquer dans nos vies.

### La famille aujourd’hui, un imaginaire de droite

La famille est un sujet de prédilection pour les droites, elle leur
permet de mettre en scène leurs valeurs traditionnelles tout en mystifiant
le passé. Quand elles prennent le pouvoir, elles peuvent s’attaquer à
ces évolutions, comme par exemple avec la remise en cause du droit à
l’avortement aux États-Unis d’Amérique. Plus récemment , l’Italie a également supprimé le
statut de mère dans les couples lesbiens. En France, la famille est
revenue dans les vœux 2023 du président sous le drapeau de la natalité.

Pendant longtemps la famille n’était pas une question de choix,
« c’était l’alliance de deux travailleurs, d’un homme et d’une femme qui
s’associaient pour faire face au dur labeur nécessaire à la survie »
(Anthony Galluzzo dans *La fabrique du consommateur*). Cette alliance
était mise en place par les parents et la communauté proche. C’est à
partir des années 1900 que les jeunes ont « eu la possibilité matérielle
de s’extraire de leur communauté ». Cette extraction s’est déroulée
aussi bien dans les imaginaires que physiquement, avec l’apparition de
lieux de sociabilité nouveaux (et sans chaperon·ne). Ce mouvement a été
accompagné par une marchandisation des loisirs. En ajoutant à cela
l’essor du salariat (qui apporte de l’indépendance) et des systèmes
éducatifs étatiques, « la famille a perdu beaucoup de ses fonctions
originelles : l’État, l’usine, l’école et même le divertissement de
masse l’ont dépossédée des tâches qu’elle assumait autrefois. » Pour en
arriver à la situation d’aujourd’hui « où le foyer se caractérise
désormais exclusivement comme un espace privé de retrait et de
consommation ». Cette perte de contrôle social sur la formation des
familles est positive, tout comme la possibilité pour les enfants de
devenir indépendant de leur famille de sang qu’iels n’ont pas choisies.
Cela n’est pas à remettre en question.

Cette évolution vers des modes de vie qui sont libérés des collectifs,
n’a pas eu que du bon pour les familles. Dans nos sociétés, « la
parenté a été circonscrite à la famille nucléaire, la liberté à
l’individu, et les valeurs à la morale » (*Sociologie de la famille*).
Bien sûr, cela s’appuie sur la pesanteur historique de l’héritage
judéo-chrétien, qui a travaillé depuis des siècles « à briser les
solidarités parentales larges et a préparé l’avènement du couple
conjugal » (Patricia Bessaoud-Alonso et Roberta Carvalho). Le couple
conjugal, c’est se refermer sur une petite cellule d’individus qui
seraient libres de leur choix (surtout de consommation), le tout cimenté
par la force de l’amour qui nullifie la nécessité de lutter contre les
dominations systémiques. En fin de compte, c’est adopter des valeurs
morales de réussite plutôt masculines, bourgeoises et blanches.

Selon Romagnoli, à partir d’une lecture institutionnaliste, « la
fonction première de la famille est de produire des individus prêts à
agir pour préserver et maintenir les établis, les institués ». La
famille « constitue l’un des piliers de la reproduction sociale des
milieux de la bourgeoisie, sur lequel elle a longuement appuyé sa
domination économique ». Pour que cette reproduction fonctionne bien, il
est important d’inscrire les familles dans des lignées ainsi que des
stratégies matrimoniales visant à préserver l’ordre établi. C’est à dire
ne pas déroger à ce que les familles restent définies « comme un groupe
de personnes unies par des rapports de parenté (filiation) ou
d’alliance (mariage) ».

Contrôler les familles, c’est aussi avoir la main sur la natalité. En
ajoutant les crises écologiques à venir et le vieillissement de la
population, il serait tentant pour des gouvernements fascistes de
vouloir reprendre la main sur les familles et la procréation. Est-ce
qu’un scénario type *Servante Écarlate* est possible ? En tout cas,
quand on regarde l’agenda politique des dirigeants comme Trump, cela
semble étrangement réaliste. Pour celleux qui n’ont ni lu le livre, ni
regardé la série, je vous en propose un bref résumé.

L’histoire se passe à une époque qui pourrait être la nôtre, mais où les
États-Unis d’Amérique n’existent plus en tant que tels et où partout dans le monde les
taux de natalité se sont effondrés. Suite à une révolution d’extrême
droite, la majeure partie des états qui composaient les États-Unis d’Amérique font
maintenant partie d’un nouveau pays, Gilead. Ce pays est centré sur
l’objectif de faire des familles, or pour cela il faut faire des
enfants. Pour contrer la baisse de la fécondité, les familles
bourgeoises reçoivent une femme fertile qui devient leur « Servante
Écarlate ». À chaque phase d’ovulation, elle est violée par le chef de
famille dans le but de porter un enfant qui deviendra celui des
bourgois·es qui l’accueillent. Dans le cas où elle enfante, dès le
sevrage de l’enfant, la Servante est déplacée dans une nouvelle maison,
pour transformer un nouveau couple en famille. Bien sûr, pour que cela
fonctionne, un puissant appareil répressif est mis en place et les
références à la volonté de Dieu sont omniprésentes. En prônant une sorte
de retour aux modes de vie passée et semblant être sobre, ce système
donne un exemple de ce que peut-être le fascisme vert.

C’est terrible, mais est-ce que cela peut arriver ? Cela ne me semble pas 
impossible, car ça déjà été partiellement le cas. Pas exactement sous 
cette forme, mais sous d’autres tout aussi inquiétantes. 
De 1966 à 1989, Nicolae Ceaușescu a mis en place en Roumanie un régime 
nataliste qui, en plus d’interdire dans la majorité des cas les IVG, va, 
comme le raconte Wikipédia, pister les femmes par des « examens 
gynécologiques obligatoires et réguliers [qui] sont imposés aux femmes en 
âge de procréer, en particulier sur leurs lieux de travail. D'autres 
mesures empêchent les femmes de bénéficier d'un traitement médical sans 
un examen gynécologique préalable. ». Les cadres de certaines entreprises 
vont même recevoir des primes conditionnées au nombre de grossesses de 
femmes travaillant sous leurs ordres. À l’opposé, on rencontre les 
politiques de stérilisations forcées qui ont eu lieu un peu partout dans 
le monde (en Occident, mais pas que), visant essentiellement des femmes 
racisées, colonisées, pauvres, en situation de handicap… Mettre en place 
ce genre de société ne nécessite pas de technologie nouvelle, seulement 
quelques personnes prêtes à collaborer, des moyens de répression, des 
promesses, des récompenses, de la peur et un peu de propagande préalable. 
Nos sociétés ne sont pas vaccinées contre le contrôle des corps par la 
force, la loi ou simplement par l’idée de ce que doit être une bonne 
famille.

Une autre vision familiale qui combine un aspect droitisant et
d’élargissement est *The Walking Dead*. Au départ une bande dessinée
puis une série à partir de 2011 composée de 11 saisons, 3 jeux vidéo et 6
spin-off. L’histoire commence par le réveil d’un homme à l’hôpital. Il
va rapidement se rendre compte qu’il y a des zombies partout en ville.
Cet homme (Rick) est un policier, père d’un petit garçon avec sa femme
(Lori). Après avoir récupéré sa famille, il va constituer un
groupe pour survivre dans ce monde où la violence règne. Elle est de
deux types, d’un côté celle des morts-vivants, qui visent tous les
humains sans distinction, de façon extrême : ils griffent, mordent,
arrachent, mangent leurs proies. Face à cette violence, il n’est
d’autres réponses qu’une violence aussi radicale, les personnages
utilisent donc tout ce qui est à leur disposition pour tuer les zombies.

Si cette série m’intéresse, c’est parce qu’à côté de la lutte pour ne
pas être mangé, il y a l’objectif de trouver un endroit où survivre (un
foyer). De nombreux épisodes se concentrent sur le groupe, les relations
dans celui-ci et aux autres survivant·es. Ces liens sont des variations
autour d’oppositions entre le groupe de Rick qui se voudrait plutôt
humain et les autres qui sont des « méchants ». Finalement, Rick et son
groupe vont tuer ces méchants pour montrer qu’en dehors de son cercle,
de sa famille, il n’y a pas de rédemption. Plus la série avance, plus le
véritable danger vient des autres groupes d’humains, renvoyant les
hordes de morts-vivants à des protagonistes de second plan, se résumant
à une perturbation pour faire avancer l’intrigue.

Le message : pour survivre à la fin de notre civilisation, il faut avoir
sa famille, des leader·ses et être prêt·es à se battre pour protéger les
ressources que l’on a pu acquérir, car les autres humain·es sont un
danger réel ou potentiel. Au bout, l’idée que présente souvent la
culture est que si notre monde social s’effondre, nous ne pourrons plus
faire confiance qu’à notre groupe, que tous les autres seront un risque
pour nous et qu’avoir des sentiments à l’extérieur de notre famille
risque de la mettre en danger. Cette série noircit énormément les
relations humaines, car en réalité, quand les êtres humains sont face à
une crise commune, iels se montrent majoritairement
solidaires, mettant aussi en scène la création d’une famille élargie.

## Faire de la famille un terrain de lutte collective

Si nous voulons faire évoluer les relations dans nos familles, nous ne
pouvons pas laisser les réactionnaires dicter leurs thèmes. L’amour ne
supporte pas la domination, si nous aspirons à vivre la puissance des
émotions dans nos familles, il faut y porter la lutte. C’est-à-dire à
la fois combattre nos propres habitudes, venant des normes que nous
avons intégrées au cours de notre vie, aider nos partenaires à les
déconstruire et agir sur l’extérieur autant qu’il est possible. C’est un
lieu puissant pour agir contre les normes patriarcales tout en armant
nos enfants pour qu’iels sachent résister face à la violence.

Pour cela, il faut passer à une échelle collective, les personnes queer
y travaillent déjà. Pour les hétéros cis, nous devons trouver nos
propres combats et soutenir celleux qui sont déjà engagé·es. Les femmes,
puis les personnes homosexuelles et trans n’ont cessé de militer pour
gagner de nouveaux droits et des conditions matérielles d’existence
dignes. Les hétéros cis se doivent de rejoindre la danse. Dans les
dernières années, les reculs sur le terrain social en France ont été
nombreux sur le droit à la parenté. Néanmoins, la famille « apparaît
comme une source de changement agissant comme un lieu d’invention »
(*Sociologie de la famille*) qui peut être en avance sur les
évolutions de la société et les amplifier.

Si les décisions dans les familles sont profondément individuelles, la
politique à de fortes influences sur elles. L’État, par des choix
politiques comme les allocations ou le logement, peut encourager un
certain type de famille plutôt que d’autres. La natalité reste une
question que nous ne pouvons pas négliger. Des luttes contre le droit à
l’avortement à la promotion de la natalité pour créer plus de
travailleureuses, c’est un champ de bataille que les droites occupent
largement. Le droit pour toustes de faire ou d’adopter des enfants dans
de bonnes conditions, sans discrimination de genre ou d’orientation
sexuelle, de race et de classe sociale, dépend fortement de la
législation. Si la lutte de la part de personnes trans et de femmes
lesbiennes pour pouvoir être enceint·es par le moyen qui leur convient
commence à être entendue, celle de l’adoption ou de l’accès à la PMA
pour des personnes défavorisées ou racisées est encore un sujet de
seconde zone. À titre d’exemple, en France, il est plus compliqué
d’avoir accès à un don d’ovocyte pour un couple non blanc. Cette
difficulté est en partie liée à l’idée qu’un couple doit avoir des
enfants de leur couleur de peau. On touche à la fois au racisme et
à l’idée de ce qu’est la parenté.

Pour ce qui est du genre des parents et donc de leur rôle, la porte est
ouverte à toutes les peurs. Nous devons investir politiquement ce
terrain. Si la déconstruction des genres fait voler en éclat les tâches
dans les couples monogames hétérosexuels, ne devrait-elle pas dans le
même mouvement brouiller les rôles de papa et de maman ?

J’aimerais beaucoup être *maman*. Je suis cis, je sais que je ne pourrais
pas porter un enfant, mais être *maman* ce n’est pas que cela. C’est
avant tout prendre en charge des tâches, des rôles, de la tendresse et
de l’amour. Si l’on commence à penser hors de la binarité, il faut aussi
que les cis hétéros le mettent en place dans la parentalité (et je ne
vous dis pas si les enfants ont plus de deux parents). Si un parent mec
n’est plus fort, courageux, impressionnant et bâtisseur, est-il encore
un papa ? Si un parent femme n’est plus doux, tendre, réconfortant
est-elle encore une maman ? Si les rôles se mélangent, que certains se
doublent, y aura-t-il encore un sens à parler de papa et de maman ?
Est-ce que les désignations des parents ne vont pas devenir multiples,
variées, voire changeantes, suivant les familles et en fonction des
répartitions qui y sont pratiquées ? C’est un discours politique à vivre
et que nous pouvons porter fièrement comme alternative au conservatisme.

### Se rappeler (ou découvrir) que d’autres modèles ont existé

D’après Paul Rasse (dans *Sexualité et communauté familiale, le regard
de l’anthropologue*), pour des anthropologues qui font école comme
Claude Lévi-Strauss et Françoise Héritier, il semble que malgré la
diversité des formes de famille, il existe des structures universelles
qui seraient : « la répartition sexuelle des tâches, la prohibition de
l’inceste, l’instauration de formes reconnues d’union matrimoniale »,
soit « les trois piliers de la famille et de la société ».

En face, l’[anthropologue](https://fr.wikipedia.org/wiki/Anthropologie) [David
Graeber](https://fr.wikipedia.org/wiki/David_Graeber) et
l’[archéologue](https://fr.wikipedia.org/wiki/Archéologie) [David
Wengrow](https://fr.wikipedia.org/w/index.php?title=David_Wengrow)
dans *Au commencement il était…* s’intéressent à la préhistoire en
suivant les évolutions sur le temps long. Les auteurs démontent notre
imaginaire sur l’histoire et les sociétés. D’abord, une des découvertes
importantes de ces deux chercheurs est que l’hypothèse « selon laquelle
une société ne peut progresser qu’en franchissant, dans le bon ordre,
une série d’étapes évolutionnistes » est une « ineptie ». Une autre est
que nous manquons profondément d’imagination sur ce qui a été possible
et sur ce qui le serait. Ou du moins, que touts modification radicale de
nos sociétés est bloquée par l’idée que  « ce ne serait pas possible » à
cause de grandes idées sur la nature humaine.

Il faut nous enlever de la tête que les structures actuelles sont
immuables. Déjà parce que les structures passées ne l’étaient pas et que
certaines étaient complètement aberrantes. Pour le prouver, ils mettent
en regard les données historiques provenant de « la dernière période
glaciaire, marquée par de forts contrastes saisonniers » pendant
laquelle « nos lointains ancêtres ont vécu une existence très similaire
à celle des Inuits, des Nambikwaras ou des Crows ». Que ce soient nos 
ancêtres de la préhistoire ou de sociétés plus récentes, 
elles étaient « apparemment guidées par le
sentiment qu’aucun ordre social n’est jamais définitivement fixé ni
immuable, ils ne cessaient d’en changer, bâtissaient des monuments
qu’ils finissaient par abandonner, laissaient se développer des
structures autoritaires à certaines périodes de l’année pour mieux les
démanteler à d’autres. Un même individu pouvait donc vivre
alternativement dans une société clanique, une société tribale et ce que
nous identifierions aujourd’hui comme un embryon d’État ».

En partant de ces deux points, je vous propose d’ouvrir nos esprits avec
quelques exemples de parentalités très différentes, ainsi les
propositions à venir vous sembleront somme toute très raisonnables.
L’idée n’est pas de mettre ces exemples face à face, juste de montrer
que des sociétés ont expérimenté des institutions familiales qui nous
semblent déconnectées de toute faisabilité. Et donc qu’aujourd’hui, il
est possible de faire évoluer la famille avec l’objectif d’y créer de la
liberté et de combattre les oppressions.

Commençons par l’exemple d’une société matriarcale comme les
Trobriandais de Mélanésie décrits par Bronislaw Malinowski. Dans ce cas,
« les hommes assurent leur filiation par leurs sœurs, ils sont, une fois
pour toutes, débarrassés de l’inquiétude généalogique tenace qui taraude
les sociétés patrilinéaires : savoir s’ils sont bien le père. Le couple
biologique (père géniteur/mère de l’enfant) est différent du couple
parental légitime composé du frère et de la sœur; comme si, pour fonder
durablement la famille et assurer sa perpétuation, les sociétés
matrilinéaires s’étaient méfiées du sentiment amoureux ayant sa source
dans la libido et l’accouplement, en lui préférant les sentiments
maternels et fraternels. De fait, la sexualité est disjointe de la
reproduction de la famille, puisque les femmes ont un ou plusieurs
amants qui les fécondent, avec lesquels elles ont des relations
épisodiques ou suivies, avec lesquels elles peuvent vivre ou non ; peu
importe en définitive, puisque de toute façon l’enfant appartient au
clan de la mère, c’est-à-dire, en fait, à celui du frère de la mère, que
l’enfant ira rejoindre au plus tard à la puberté ».Cela nous montre
qu’il est possible de séparer le sentiment amoureux de l’amour parental,
qui est exercé par d’autres que celleux qui ont participé à la
conception.

Un autre exemple intéressant est celui des Moso (vivant en Chine), pour
qui le concept de père est étranger, au point que le « mot père n’a
d’ailleurs été que très récemment introduit dans la langue » (Ouvrage
collectif, *Avons nous besoin de père et de mère*). Les pères
biologiques sont considérés comme n’ayant peu à voir avec la conception
génétique des enfants, ce sont les oncles et les frères de la mère qui
se sentent responsables d’elleux. Dans le cas où un amant vient vivre
« dans la maisonnée de son amante \[…\] il y est considéré comme un
servant ». Le père est ici carrément exclu (de façon mythique) de la
reproduction, la sexualité est très libre et les familles (dont tous les
membres vivent sous le même toit et participent à l’éducation des
enfants) sont dirigées par deux chefs de famille : un frère et une
sœur.

Le concept même de père ne semble pas être nécessaire à une société.
L’éducation peut être prise en charge par d’autres que les parents
biologiques et la responsabilité de la maisonnée assumée à égalité entre
un homme et une femme qui ne couchent pas ensemble. Il est amusant de
voir que l’auteur de ce chapitre prend la peine de préciser que les
« élaborations psychanalytiques traditionnelles pourraient nous amener à
penser que la plupart de ces Moso sombrent dans la psychose ou la
perversion. Or, ni la lecture des ouvrages des anthropologues ni les
observations que j’ai pu faire sur place ne me permettent de penser que
c’est le cas ».

Plus proche géographiquement de nous, en Albanie, une coutume (étudiée
par Anttonia Young et rapporté dans *Les Vierges jurées d’Albani*)
encore pratiquée veut que des personnes nées femmes puissent devenir
socialement des hommes (devenant des Vierges jurées). Ce changement de
genre est effectué sans traitement médical, il se fait par les habits,
la posture, et le changement de regard des autres. Le plus souvent,
cette transition est causée par la nécessité qu’une femme devienne chef
de famille, soit suite à l’incapacité d’un enfant à assumer ce rôle,
soit à la suite d’une vendetta. En devenant chefs de famille, iels en
adoptent les rôles que sont « le partage des richesses et du travail,
mais aussi de tous les cas de reprise de sang. Sa tâche est d’équilibrer
harmonieusement tous les aspects de la vie ». Néanmoins ce statut
nécessite un vœu de célibat (qui parfois n’est pas respecté).

Aucune de ces normes n’est exemplaire, je ne veux pas faire la promotion
de modes de vie patriarcaux où l’honneur est une question de vie ou de
mort, ou bien de l’idée qu’un géniteur n’a rien à voir avec ses enfants.
Ce qui est intéressant, c’est l’existence de ces cadres qui démontre que
l’on peut faire famille autrement.

Rapprochons-nous encore un peu, en arrivant à la société occidentale et
contemporaine. La critique de la famille nucléaire et du genre amène
celleux qui ne rentrent pas dans ses cases à assembler différemment les
pièces du puzzle de la parenté. Avec *Faire famille autrement*,
Gabrielle Richard s’attaque directement à la parentalité (dans le sens
qu’elle serait unique et devrait être unique) qui « est le bastion le
plus imprenable de la normativité et de l’hétérosexualité ». Plein de
personnes queer et cis avaient sûrement besoin d’un livre comme
celui-ci. Queer sera ici entendu comme « une proposition visant à
décentrer le regard de la binarité du genre, de la présomption
d’hétérosexualité et de l’alignement attendu entre le sexe, l’identité
de genre et la sexualité d’une personne ».

L’essai montre que « dire ce que doit être une famille, c’est aussi
politique que de dire ce qu’elle ne doit pas être ». Pour ce faire,
elle donne la parole à des personnes, qui *queerent* la famille et
séparent au moins en partie la biologie de la parentalité. L’autrice
donne la parole à des personnes mettant en œuvre toutes les
configurations possibles. Par cela, j’entends des combinaisons multiples
de sexes, de genre, de moyens de procréation, de lien ou non avec les ou
donneurs et donneuses. Cela peut nécessiter des arrangements avec des
donneur·ses en créant un accord moral (hors de tout cadre légal), pour
s’accorder sur les droits et les devoirs de chacun·e envers le futur
enfant. Comme nous l’avons vu, cette séparation existe dans d’autres
cultures et ne pose pas forcément de soucis. Dans un des récits, deux
femmes ayant eu un enfant racontent à leur petite fille comment s’est
passé sa conception. Le but est d’éviter le choc de la révélation.

*Queerer*, c’est aussi créer d’autres modes de cohabitation, par exemple
décider d’avoir un·e enfant seul·e et de s’en occuper seul·e, « notre
histoire, avec ma fille, elle s’est tellement construite à deux que je
ne vois pas comment intégrer une troisième personne ». Pour en arriver là,
il faut résister à l’injonction sociale de « trouver quelqu’un
\[d’autre\] qui joue un rôle parental ». Ou bien séparer les rôles
importants entre plusieurs individus, sans désirer à tout prix que la
même personne soit à la fois :  « un·e ami·e, un·e amoureux·se, un·e
partenaire sexuel·le, un co·parent et un·e colocataire·trice ».

C’est beaucoup demander, et si comme l’avance Liv Strömquist dans *La
rose la plus rouge*, l’amour est un « rite social dont l’objectif est de
produire des émotions », dont la force est entretenue par
l’accomplissement de rituels faits avec « discipline » ; quand on aime
une personne, il faut se mettre dans des conditions permettant
d’accomplir correctement le rituel. Y arriver en demandant tant de
rôles est une sacrée gageure.

Il peut être plus facile de préserver l’amour sans se forcer à 
vivre avec l’autre parent de ses enfants. Un des exemples est le cas
d’un couple (un homme trans et une femme) avec enfant. Les parents,
après s’être séparés, s’installent chacun·e avec leurs nouveaux
partenaires dans deux maisons très proches. L’enfant du couple initial
dort une semaine chez un parent et une semaine chez l’autre. Les adultes
s’entendent et partagent des moments toustes ensemble, ce qui évite une
cassure nette pour l’enfant, comme souvent dans les séparations. De
plus, cela permet à l’enfant d’avoir « plein de modèles d’identités ».

Ce genre d’organisation pousse à se doter « d’une routine permettant à
tout le monde de faire le point chaque semaine ». Créer un espace de
discussion autour de l’organisation collective et des émotions de
chacun·e (enfant compris) est un outil qui manque trop souvent dans les
couples, où ces arrangements sont vécus comme un échec face à des
difficultés que l’amour n’a pas su régler. Et encore plus dans les
divorces et les conflits souvent associés. Enfin, il traite de la
répartition genrée des rôles (qui serait un des « piliers de la famille
et de la société » (*Sexualité et communauté familiale, le regard de
l’anthropologie*).

Ces pratiques familiales actuelles et proches spatialement permettent de
retrouver de l’imagination et d’avoir un point de départ d’où continuer
à construire. C’est à dire devenir parent avec « comme ligne
directrice, cette idée que (presque) tout est possible » (*Faire famille
autrement*), notamment pour sortir de la répartition genrée des rôles,
voire même de l’idée de rôle monolithique. Cela inclut de pouvoir être
enceint pour un homme trans, ou de pouvoir donner le sein pour un homme
cis, un homme trans, une femme cis ou une femme trans (certains de ces
cas nécessitent un traitement hormonal). Ainsi, plutôt que d’attribuer
par nature des rôles en fonction du genre, nous pourrions penser que
« il y a des fonctions et des parents pour les accomplir ». C’est aussi
accepter de négocier, de dire ce que l’on aime et de chercher un
consentement mutuel « de façon consciente et réfléchie ».

La remise en question des pratiques dans la famille peut aller de pair
avec un changement sur les noms donnés par les enfants. Si les parents
ne jouent plus des rôles de père et de mère, mais des rôles entre les
deux ou complètement différents, est-ce qu’il y a encore un sens à les
appeler ainsi ? L’image du bébé dont les premiers mots devraient être
« maman » et « papa » est gravée dans nos imaginaires, mais ce n’est pas
un passage obligé. Nous sommes libres d’imaginer comment nous aimerions
que nos enfants nous appellent, d’en discuter avec elleux et de les
laisser choisir. À ce propos, les enfants des familles présentées dans
*Faire famille autrement* semblent heureux·euses, en particulier car
iels ont souvent plus d’adultes à qui se référer. Un des impacts
potentiels sur les enfants de la forte présence des questions de genre
dans leur entourage, est qu’iels « vont être plus susceptibles de
questionner leurs orientations sexuelles; elles et ils ne sont pas plus
susceptibles d’être gay ou lesbiennes » (revue *La Déferlante*, n°7). Et
là où la droite s’époumone, nous disons tant mieux : iels vont gagner du
temps !

Pour les hétéros cis, c’est une démonstration qu’iels peuvent ne pas
respecter les rôles prévus pour leur genre, sans forcément faire courir
de risques à leur enfant ou à leurs relations. Par contre, il faudra
assumer d’être des casseuses d’ambiances dans les dîners, voire d’être
durement jugé·es. Pour la faire courte : toutes les configurations de
familles sont possibles et peuvent être fonctionnelles. Si un enfant
peut s’adapter sans séquelles (sans pour autant que cela soit toujours
facile) à ce que son père biologique soit une femme et aussi sa mère,
c’est que la forme normative hétéro de la famille n’est pas une
nécessité pour les enfants ou les parents. La séparation entre la
biologie et la parentalité a deux effets intéressants : diminuer
« l’obligation d’y recourir si elle est physiquement possible » et
participer à faire descendre la parentalité biologique de sa position
prédominante. Dans nos sociétés, qui manquent profondément d’imagination
sur ces sujets, c’est une bouffée d’air.

Ce ne sont que quelques exemples de modèles familiaux et de pratique sur
toutes celles qui existent et ont existé. Plutôt que de vouloir avoir
une famille, nous devrions vouloir faire famille. Faire famille, plus
que des schémas, ce sont des pratiques et des arrangements que nous
sommes libres de « bidouiller pour trouver une organisation qui convient
aux adultes et aux enfants ».

### La parentèle, comme alternative à la famille nucléaire

La parenté reste centrée sur le mariage, la naissance et l’adoption.
Cela ne nous permet pas d’ouvrir la porte de nos relations pour
pratiquer le rite social de l’amour (et son engagement) hors du cercle
familial, car comme l’a écrit précédemment Margaux, «* l’exclusion
définit le couple par rapport à celleux qui n’en font pas partie ». Elle
rend difficile de nous projeter sur le long terme et de faire de grands
projets avec des personnes extérieures à la famille et à l’amour
monogame. Quand j’ai dit à mon père qu’un de mes colocs faisait, pour
moi, partie de ma famille, il l’a très mal pris. La famille, c’est la
famille. Le mot est peut-être trop connoté pour en faire quoi que ce
soit.

Alors faisons un pas de côté en allant voir vers la *parentèle* : « Dans
cet océan de familles nucléaires, où n’existe pas de principe général
d’affiliation qui les regrouperait plus ou moins mécaniquement en
lignages et en clans, il existe cependant des groupes de parentés aux
contours fluides dont l’existence n’est jamais assurée et la durée
toujours temporaire. \[…\] Une parentèle n’est donc pas un groupe de
descendance, tel un lignage ou un clan, mais un ensemble de parents de
diverses sortes, plus ou moins proches\[…\] ». (M. Godelier,
*Métamorphose de la parenté*)

Si la famille est une organisation à laquelle nous sommes habitués, les
parentèles nous sont étrangères. À la différence de la famille dont les
contours sont assez clairs, la parentèle peut se permettre d’être floue,
mouvante, de se mouler aux contextes. Elle nous permet de raconter
autrement nos histoires et de créer des alliances qui sont hors de
propos en régime nucléaire. Leur objectif n’est pas centré sur la
lignée. La parentèle n’est pas un mot usuel, il est défini par les
scientifiques, mais il est assez vaste et libre pour que nous puissions
le charger de contenu symbolique et de pratiques.

Surtout que d’autres ont déjà commencé ! Il y a bien des chemins de
traverse pour être apparenté·es. Une partie de la population a une
utilisation très ouverte de « cousin », « mon frère », utilisation qui
m’a souvent mis mal à l’aise. Je ne me sentais pas parent de ces
personnes. Peut-être devrions-nous réfléchir à ce que nous pourrions
tirer de cette habitude. Désigner comme « frère » ou « cousin » ou que
sais-je encore nos ami·es et nos camarades, cela pourrait avoir un
impact sur notre pensée et nos façons de vivre les relations. Dans bien
des cultures, il se crée des parentés fictives, qui sont un « *lien de
parenté sociale créé par choix réciproque et selon certaines conventions
entre deux individus non apparentés, ou entre un individu et un groupe
de parenté qui le reconnaît comme sien sans cependant formellement
l’adopter* ». Copier des cultures, en particulier quand nous
occidentaux les avons colonisées, n’est pas une bonne façon de faire.
Plutôt devrions-nous, en sachant que cela est dans les cordes des
relations humaines, le garder dans notre tête. Probablement, comme les
queers le font pour les dénominations des parents, devrions-nous
inventer de nouveaux mots pour ces liens, qui marqueraient le fait que
nous donnons à ces proches une position particulière dans nos vies.

Certes, la parentèle, n’offre pas le confort rassurant de la famille,
dont l’existence est assurée pour « toujours ». À côté de cela, elle
apporte la liberté dont nous avons besoin quand les temps sont troublés.
Dans un futur soumis aux conséquences du changement climatique, il sera
peut-être nécessaire de savoir créer des alliances de vie plus fluides.

### La ColoCation famille comme une étape vers la parentèle

Je me rends bien compte que l’idée de parentèle n’est pas opérationnelle
en l’état. Nous en sommes trop loin dans nos habitudes, nos lois, les
contraintes de la société. D’ici là, une étape intermédiaire, plus
accessible, est la création de la ColoCation famille. Elle est bien
différente de l’image habituelle de la colocation, qui se forme
essentiellement autour de la division d’un loyer, sans que partager plus
soit un objectif.

Par ColoCation famille (que j’abrège ensuite par ColoC), j’entends le
fait d’habiter dans une colocation par choix, avec des personnes
choisies, qui partagent un espace, un bail collectif, leurs quotidiens,
des communs et des règles librement décidées. En cela, elle va à
l’encontre de la volonté des propriétaires de découper de grands
appartements pour les louer à la chambre, ce qui augmente leurs revenus.

La colocation est souvent vue comme un mode de vie transitoire, adapté
aux étudiant·es et aux jeunes actif·ves, qui vont les quitter dès
qu’iels en auront les moyens pour s’installer seul·e ou en couple. Il
est du reste amusant de voir que les couples ne se voient absolument pas
comme une ColoC de deux personnes. Mais un couple est fait pour durer
alors que la colocation, elle, est passagère. Pourquoi diable
voudrions-nous continuer à partager notre quotidien avec des personnes
sans être en couple avec elles… Parce que le couple n’est pas un
absolu indépassable et si comme l’écrit Margaux nous supprimons *la
hiérarchie entres les relations importantes de nos vies*, il n’y aura
plus de raison de voir la ColoC comme devant se terminer avec le passage
à l’âge « adulte » et au couple. Le film *L’auberge espagnole* en est un
bel exemple. Même s’il y a des tensions (c’est un film, il faut en
créer), lae spectateurice n’a qu’une envie : que tout le groupe vive
ensemble pour toujours ! Cela n’arrive pas, la fin de l’année
universitaire sonne la fin de la récréation, chacun·e doit reprendre sa
vraie vie, même si elle semble fade. Nous, nous ne sommes pas dans un
film, nous pouvons réécrire le scénario.

N’ayant pas trouvé de références satisfaisantes sur les ColoCs, pour en
parler, je m’appuie sur mon expérience personnelle (plus de 30
colocateurices, sur 10 ans dans 6 logements différents) ainsi que mon
observation de dizaines d’autres colocations. Je vais utiliser le
« nous » pour parler des aspects récurrents de la multitude de ces
ColoCs. En quoi les ColoCs se rapprochent-elles d’une parentèle ?

Pour commencer, les colocateurices sont sélectionné·es, par différent
critères, en particulier sur les affinités et par le désir de partager
un mode de vie commun qui varie de lieu en lieu. Nous organisons des
entretiens plus ou moins sérieux, dont l’objectif est de faire
connaissance dans l’optique de vivre ensemble. Même pour un·e ami·e, des
entretiens sont organisés. Ce n’est pas la même chose d’être ami·es que
de projeter une vie commune, qui a ses propres difficultés. Avant même
de commencer à vivre ensemble, nous discutons de comment nous vivons et
voulons vivre. Nous posons nos pratiques et nos désirs comme préalables
à la vie commune.

C’est l’inverse des couples qui posent la vie commune un préalable aux
pratiques et aux désirs. Alors que notre société valorise tant
l’individualité, la pression de fonder une famille tend à faire oublier
ce principe qui me semble pourtant essentiel. On peut aimer une
personne, mais ne pas être compatible dans le quotidien, c’est OK. Le
quotidien est le ciment des relations, mais peut être dur, usant, en
particulier à cause des enjeux de domination.

Les difficultés auxquelles se confrontent les ColoCs et les familles
sont similaires : répartition du travail domestique, utilisation de
l’espace, bruit, questions financières, séparations, besoin d’intimité.
Ce sont des sujets qui doivent être mis à la discussion, pour qu’il soit
possible de les négocier sur des bases claires. Naturellement, les
colocateurices n’étant pas tenu·es par le dogme du couple dont l’amour
résout les problèmes, ils sont obligé·es (et c’est bien) de poser des
moments pour tenir conseil. Les couples (encore plus en famille)
subissent l’attente sociale d’un quotidien de famille épanouissant qui
doit alimenter la dynamique du couple, pour l’aider à supporter ses
difficultés. Que c’est ambitieux ! Surtout dans le cadre de l’enclosure
des familles nucléaires.

De ce que j’ai observé, les ColoCs n’ont pas cette ambition. Et quand
bien même, créer du renouvellement est tellement plus facile à plus de
deux ! Sans pression sur le quotidien, il est plus facile d’y créer des
rituels doux. Notre objectif est plutôt de créer un espace où toustes
ses membres sont confortables, qui va s’adapter à l’évolution des
habitant·es. C’est une exigence élevée. Être confortable au quotidien
nécessite de passer des moments de qualité pour graisser les rouages,
pour accepter avec amour les mille et une contrariétés de la vie commune.

Le partage du quotidien inclut les espaces, des meubles, des
appareils… C’est autant de gain financier et écologique. Un peu comme
dans les pays nordiques, où l’on partage une buanderie pour l’immeuble,
mais en poussant l’action plus loin. La ColoC est une optimisation de
l’espace et des biens. Les ColoCateurices choisissent ce qu’elles
mettent en partage et comment. Par exemple, je partage mon
vidéoprojecteur avec les trois autres habitant·es de la maison, mais
pour du loisir, pas pour les réunions de travail ou d’association qui
ont lieu chez nous, et je ne partage pas ma tasse chat. Le partage des
espaces est souvent fluide, nécessitant d’informer les autres
habitant·es des invitations ou hébergements ponctuels. Un point central
du quotidien est le partage de la nourriture, où les variations entre
les groupes s’étendent sur un continuum allant de l’ensemble des courses
séparées avec préparation de repas individuels au communisme total sur
la nourriture (achat et préparation en commun).

Partager le quotidien, c’est aussi apprendre à être malléable sur la
façon dont doivent être faites les choses. Il est possible de se mettre
d’accord dans une certaine mesure, pourtant chacun·e va avoir ses
petites habitudes qui pour les autres vont aller du chouette à
l’exaspérant.

Partager autant n’est pas neutre sur les relations. Vivre quelques mois
en ColoC, c’est passer plus de temps avec nos ColoCateurices qu’avec ses
meilleur·es ami·es. Partager pleinement le quotidien crée l’espace pour
les émotions. Il y a des humains proches qui sont là, qui se font
confiance, nos joies et les malheurs ressortent. Notamment pour parler
des questions de couple extérieures à la ColoC. L’existence de cercles
relationnels intimes et séparés permet qu’ils se soutiennent
mutuellement.

Si je suis malheureux, le dire à mes ColoCs leur permet de comprendre
mon changement de comportement, de s’ajuster. Tout comme je peux
m’ajuster à leurs besoins. Par exemple, j’ai habité pendant deux ans avec
une interne en médecine qui rentrait tard et épuisée. Savoir que son
travail la mettait dans cet état m’a permis pendant un temps de me
mettre dans une position de soin à son égard. Le soin dans le quotidien
cela peut être juste avoir préparé le repas à l’avance, faire attention
le matin à ne pas réveiller les autres, prendre pour un temps plus de
travail ménager. Si les membres du groupe font attention les un·es aux
autres, le temps fait le reste du travail, les moments de vie créent des
souvenirs et les relations perdurent.

Il reste encore difficile pour bon nombre d’entre nous de nous projeter
sur plusieurs années avec des ColoCs. Majoritairement, nous sommes
jeunes (les plus vieux et vieilles sont dans la trentaine), nous ne
sommes pas encore « posés » dans la vie avec des CDI et des crédits.
Nous ne savons pas de quoi demain est fait. Nous n’osons pas forcément
nous dire que nous aimerions habiter ensemble pour plus longtemps, car
s’engager entre ami·es n’est pas dans nos coutumes. C’est en
franchissant ce pas que nous ouvrirons la voie à la création de
parentèles, nous faisant glisser du statut de ColoCateurices à celui de
« parents de diverses sortes ».

## Quelques possibilités …

Les bases pour créer les familles hors du couple hétérosexuel monogame
sont déjà présentes. Il faut les laisser atteindre nos imaginaires et y
fermenter pour qu’elles deviennent désirables. J’ai envie de vous
proposer d’explorer trois thématiques, que nous pouvons investir sans
attendre.

### L’apport de l’amitié dans les couples

Prendre soin de soi, c’est en bonne partie prendre soin de ses
relations. Que ce soit pour notre estime, notre confort, notre sécurité,
c’est un lieu de joie et de soutien trop peu valorisé hors du couple, en
particulier dans le cadre de l’amitié. Avoir des ami·es est valorisé, le
lien d’amitié lui ne l’est pas.

Je ne doute pas, qu’il est parfois agréable de se refermer sur une
petite équipe à deux, mais l’amour romantique, l’hétérosexualité et le
patriarcat veillent. Les couples et les familles sont des lieux
d’oppression, nous le savons, s’y renfermer est risqué. Dire que la
famille a besoin de liens d’amitiés puissants à l’extérieur, c’est
entrer en contradiction avec le produit qui nous est vendu. Lae
princes·sses et lae princes·sses vivent toustes les deux et ont beaucoup
d’enfants qu’iels élèvent ensemble avec l’aide des grands-parents et de
serviteur·ses. Il n’y a pas les copines de la princesse qui viennent
prendre le prince entre quatre yeux pour lui parler de ses crises de
colère avec les enfants. Les familles sont des unités indépendantes sur
lesquelles le monde extérieur n’a pas à donner son avis, surtout si
elles respectent les normes.

Les apports de l’amitié aux familles pourraient être immenses. Sans
aller bien loin, juste en poussant ce qui est déjà là ! Les relations
sont chronophages, il n’est pas facile d’entretenir à la fois sa famille
ET ses propres amitiés. En plus, il peut être tentant de se glisser dans
le groupe amical de son amoureux·euse en négligeant le sien. De l’autre
côté, en tant qu’ami·e de personnes en couple, ce n’est pas facile de
s’ajuster. Il n’est pas rare que la création d’un duo rebatte la
carte de nos autres relations amicales pour
éviter des conflits dans le couple, 
ce qui est encore amplifié par notre difficulté à 
imaginer des liens forts sans sexualité entre hommes et femmes. Le
couple va donc limiter les possibilités de partage intime avec ses
ami·es.

Posons que nos amitiés sont essentielles au bonheur, que par une juste
répartition du travail domestique, nous devons avoir du temps libre pour
les vivre. Posons que nous pouvons être ami·es, peu importe notre genre
et notre sexe, sans que cela mette en insécurité notre partenaire.
Posons que les liens amicaux sont un filet de sécurité qu’il faut
entretenir pour qu’il soit là en cas cas de chute.

Les amitiés offrent un espace où se réjouir, parler librement, comparer
des pratiques, avoir un regard critique. Pour cela il faut que chaque
membre du couple ait ses propres liens d’amitiés, en qui iels a
confiance. Et surtout, dans nos familles, nous devons être OK avec
l’idée que les difficultés ne disparaissent pas magiquement sous les tapis. L’usage
veut que le couple (surtout hétéro) soit secret sur son intimité.
Sortons les secrets du placard, surtout les moches, qui nous pèsent, ne
restons pas seul·e, les ami·es sont là pour ça.

Celleux qui pensent à fonder une famille devraient se poser la question
de qui sera là pour les soutenir par gros temps et pas seulement dans
leurs familles de sang. De nos jours, nous sommes souvent loin de nos
ascendant·es, nos relations avec elleux peuvent être difficiles et les
désaccords politiques profonds. Enfin la pression sociale sur les parents
est énorme et pleine d’injonctions contradictoires. Ces raisons et
d’autre font que la parentalité met le couple en danger (tout en
renforçant les inégalités existantes). Une étape intéressante serait
d’oser parler de notre futur avec nos ami·es en leur donnant une place !
D’oser nous engager réellement avec elleux.

Pour cela nous devons dépasser le modèle de l’amitié néolibérale. Les
auteurices de *Joie Militante* la décrivent comme où l’amitié est « une
affaire banale de préférences privées : on sort, on partage des loisirs,
on bavarde » constat que je trouve un peu dur. Par contre, là où je
les rejoins complètement, c’est qu’entre ami·es « nous ne nous soutenons
pas vraiment, et nos vies ne sont pas entremêlées ». De plus,
l’amitié, « implique une relation beaucoup plus faible et insignifiante
que celle avec un·e amoureux·se ne le sera jamais ». Enfin « L’un de ces
effets les plus brutaux \[de la part de la famille\] est qu’elle rend d’autres
formes d’intimité difficiles voire inimaginables, pour la plupart
d’entre nous ». Les termes sont dits.

Dans *Désirer à tout prix*, Tal Madesta propose de voir les liens
d’amitié comme « des passions amoureuses qui n’ont pas besoin de sexe
pour se maintenir et grandir ». Je ne suis pas pour l’utilisation du
concept de passion dans l’amitié, car dans la passion « on aime l’état
plus qu’une personne » (*Réinventer l’amour*, Mona Chollet), ce qui n’est
pas propice à la soutenabilité. J’imagine que « la passion », à laquelle
fait référence l’auteur, doit plutôt être entendue comme une émotion
forte. L’amitié autorise une grande flexibilité. Il est plus
simple d’y vivre des relations qui se construisent dans un continuum
d’évènements depuis la rencontre à celui d’amitié, sans forcément
rencontrer de point de rupture engendrée par le sentiment amoureux.
L’absence de bond émotionnel et sa construction plus lente, basées sur
des actes renouvelés, lui donnent une fiabilité que n’a pas le sentiment
amoureux. Je ne dis pas que l’amitié ne peut pas connaître de rupture de
ton, mais il ne change pas la base du fonctionnement du lien. Alors que
« l’amour parfois disparaît comme ça INEXPLICABLEMENT pour la raison que
la déesse nous a abandonnés. Et on n’y peut rien ». L’amitié ne
disparaît pas dans un nuage de fumée.

À l’inverse de l’amour et de son pendant, le couple monogame se
prolongeant dans une famille, l’amitié n’inclut pas une institution
formalisée avec ses règles toutes prêtes. Si l’on accepte de considérer
l’amitié au même niveau que l’amour, parce qu’elle aussi peut nous
transporter et changer nos vies, cela nous oblige à en tirer les
conséquences.

Une des premières serait de nous projeter dans l’amitié comme nous le
pouvons dans l’amour et la famille. C’est-à-dire faire avec nos ami·es
des projets de longs termes et s’aventurer main dans la main vers le
futur. Aujourd’hui, ce n’est pas dans la norme de faire des choix de vie
en fonction de ses ami·es. C’est complètement hors de propos !
Majoritairement, nous organisons nos vies par ordre de priorité, en
commençant plus ou moins par notre travail, puis nos couples, nos
familles de sang, nos enfants puis nos projets immobiliers et enfin les
copain·es ou les vacances et éventuellement nos hobbies.

Faire des arbitrages qui ne vont pas dans le sens du travail (dans le
sens rémunérateur) dans notre société, c’est difficile, ce n’est pas
accessible à toustes. La famille nucléaire, c’est l’alpha et l’oméga qui
vient après. C’est une des matrices de la domination masculine, pourtant
nous faisons comme si nous ne le savions pas. Même sans en avoir la
connaissance théorique, la difficulté de faire tenir un couple est
sensible. Donc créer des possibilités face à la famille nucléaire et sa
centralité est indispensable. C’est le nœud de bien des histoires et
nous pouvons observer autour de nous le nombre de couples où la relation
entre les partenaires est tendue pour ne pas dire délétère. Certes, pour
des raisons économiques, il n’est pas simple de garder un appartement
chacun, mais alors installez-vous, chacun·e dans une colocation avec
d’autres belles personnes. Ne pas habiter ensemble semble poser des
questions insolubles aux hétéros. Des queers ont pourtant trouvé des
solutions pour « co-parenter sans avoir de relation amoureuse, et sans
vivre nécessairement sous le même toit ». Expérimenter apporte souvent
les réponses aux questions insolubles. Par contre, il faut être prêt à
se confronter aux normes.

Imaginez la scène :

« Salut chéri·e, depuis un petit moment, c’est tendu entre nous. Je sais
que l’on s’aime, mais il faut admettre que l’on n’arrive pas à bien
gérer la maison ensemble. Tes tasses de café qui traînent, je n’en peux
plus. Je ne veux pas que l’on se quitte, je t’aime. Alors, je pense à
aller m’installer à quelques rues avec des connaissances, je pense que
cela me ferait beaucoup de bien d’habiter avec d’autres personnes et que
nous ayons chacun·e notre espace. On continuera à se voir presque tous
les jours, ne t’inquiète pas. Tu pourras venir, moi je viendrai, on se
verra pour des moments de qualités. »

Cela semble complètement hors de propos, alors que la proposition
suivante, elle, peut se tenter :

« Salut chéri·e, depuis un petit moment, c’est tendu entre nous. Je sais
que l’on s’aime, mais il faut admettre que l’on n’arrive pas à bien
gérer la maison ensemble. Alors, je te propose que l’on prenne un
nouveau départ. Au boulot, iels m’ont proposé une super opportunité de
taf à Shangaï. C’est la chance de nos vies et il y a de supers
conditions ! De nouvelles rencontres, l’exotisme, le voyage c’est tout
ce qu’il nous manque. On part dans trois mois, on pourra sûrement
revenir pour Noël, qu’en dis-tu ? »

La vie en couple est une histoire avec une logique connue et des
mécaniques bien huilées. Une fois pris dans la spirale du couple, s’il
n’y a pas trop de soucis au début, tout s’enchaîne. À partir de là, les
vies s’entremêlent et il est difficile de s’en extraire sans casser tout
ce qui semble stable dans sa vie. C’est une grosse prise de risque, sauf
qu’elle est vécue comme une étape quasiment nécessaire dans la vie
humaine. En face, la vie en colocation permet aussi d’entremêler
les vies tout en présentant bien moins de risques. Les attendus y sont
flexibles et surtout, il est infiniment plus facile d’y entrer et d’en
sortir sans danger pour ses relations.

En réalité, il y a bien un cas où la place des ami·es redevient
centrale. C’est en cas de rupture de couple. Les séries et les films sur
le sujet sont légion. Quand une personne se fait quitter, elle retrouve
le numéro de téléphone de ses ami·es et elleux rappliquent pour la
soutenir et dire du mal de l’ex. Sans rupture, les ami·es sont bon·nes à
être témoins, venir dîner ou faire un weekend, pas à être parties
prenantes.

J’admets que bousculer la hiérarchie des valeurs est une gageure, c’est
pour cela qu’il faut nous y lancer en équipe, dans des groupes en qui
nous avons absolument confiance. Pour la mettre en place, il nous faudra
du temps et la mettre en mot pour nous assurer que nous sommes d’accord
sur l’amplitude de l’amitié que nous nous vouons. La mise en mot n’est
pas une petite affaire. Comme dire « je t’aime » ce n’est pas rien, cela
à des conséquences, un avant et un après. Pouvoir faire des déclarations
d’amitié à celleux que nous aimons d’amitié·es cela devrait être la même
chose. Parlons, à nos ami·es de la façon dont nous les aimons, de
l’importance qu’iels ont dans nos vies.

Pour donner de la puissance à l’amitié face à l’amour, la mise en scène
lors de célébrations est une option. Globalement, notre société fait
comme si elle ne croyait plus à rien (sauf peut-être aux dettes, aux
marchés et à la fête), pourtant nous continuons à organiser des
évènements à Noël, au nouvel an, les anniversaires….Nous n’y croyons
pas vraiment, mais nous ressentons que c’est important de poser des
actes et des rituels. Et bien sur les mariages, la fêtes immenses, le
plus beau jours de notre vie ! Pour élever la valence de l’amitié, lui
faire de la place face à l’amour romantique, nous pouvons offrir un
rite, une mise en scène, une occasion de créer des engagements et de
leur donner une valeur collective. Nous pourrions les proclamer en
public. Cela serait en plus un beau pied de nez au mythe du mariage
hétérosexuel, qui lui seul compte et vaut d’être annoncé publiquement.
Que cela serait beau de voir des ami·es faire des vœux.

« Bienvenue et merci d’être venu·es aujourd’hui pour partager cette
merveilleuse occasion. Aujourd’hui, nous sommes réunis pour unir A et B
dans leur amitié. L’amitié est un engagement entre deux personnes, ou
plus, à lier leurs vies. C’est un lien de partage et de patience, qui ne
doit pas être entrepris de manière irréfléchie ou irresponsable, mais
plutôt avec maturité et honnêteté. L’amitié est une communauté dont les
membres s’aideront, se soutiendront et s’apprécieront l’un et l’autre
dans les bons moments comme dans les moments difficiles. C’est la
relation que ces personnes souhaitent voir déclarée et célébrée
aujourd’hui. »

Je ne propose pas ici de contractualiser quoi que ce soit. Plutôt de
créer un espace pour nous autoriser à dire ce que nous ressentons
profondément à nos ami·es, à quels points nous les aimons, que nous
voudrions lier nos vies aux leurs. Cet espace pourrait aussi servir à
créer des engagements moraux soutenus par une symbolique. Les symboles
ont du pouvoir. Ils aident à changer notre perception collective des
priorités.

Bien sûr, cela ne changerait pas instantanément la hiérarchie de nos
relations. Néanmoins, cela créerait le début d’une nouvelle institution
dont les règles sont encore à écrire. Une institution est un contre-pouvoir social. Avoir un réseau, dont nous refusons de nous éloigner et
c’est pouvoir résister ensemble à des pressions du monde du travail et
de la famille. Quand un couple se forme, leurs membres sauraient
qu’iels se mettent aussi en relation avec un cercle d’ami·es protecteur
et pouvant intervenir en cas de difficulté.

C’est encore une fois étrange d’imaginer laisser de la place pour des
ami·es dans un couple. Nous sommes très conditionnés par l’idée que
c’est « mon couple » et que personne n’a rien à y faire à part moi et
l’autre. C’est aussi par-là que passe l’enfermement dans la
domination. Il est souvent difficile de sortir de ce schéma sans regards
et allié·es extérieurs. En donnant à des proches l’autorisation morale
de nous dire s’iels voient des dérives, de la violence… nous aurions
un bouclier contre les dérives de l’autre et encore plus contre nos
dérives à nous les mecs. Ce seraient des parrains et marraines de nos
relations les plus fortes. Ma proposition n’est pas de faire intervenir
des gens extérieurs à tout bout de champ, mais qu’un·e ami·e puisse se
sentir vraiment légitime à me dire « Timothé, là tu fais de la merde »,
ou bien « Tim, je crois que ton amoureuse déconne ». La peur de perdre
un lien peut nous rendre stupides, nous empêcher de parler… avoir un
tiers qui soit d’une certaine façon officiellement désigné (par
nous-même) comme notre bonne fée, c’est ouvrir la famille en dehors du
centre nucléaire. En solidifiant et en actant l’existence claire d’un
« *filet de soutien et de sécurité* » dans lequel les membres se sentent
engagé·es, ouvre une vrai porte de sortie en cas de « situations
dysfonctionnelles ou abusives ».

Les non queer qui s’y essayent doivent se rappeler d’où vient l’idée
d’utiliser l’amitié pour cela. Tout d’abord pour ne pas se l’approprier, mais aussi pour ne pas la vider de sa substance. Ce sont des personnes souvent
rejetées par leur familles de sang et mises à la marge par la société qui
ont pensé à utiliser cette ressource. Alors non queer, soyons modestes
et soutenons celleux qui ont débroussaillé le chemin.

### Avoir plus de deux parents

Augmenter le nombre de parents d’un enfant est un enjeu pour les deux
parties. Qu’en serait-il si cette multiplication des référent.es
éducatif.ves était organisée dans le but de rendre la vie plus agréable
à tous.tes ? Car plus de parents, c’est aussi plus de possibilités pour
elleux de faire des pauses. C’est pouvoir échanger avec plus de
personnes sur la situation, ses questionnements. Possiblement, c’est une
voie pour lutter contre le burnout parental qui sévit partout et met en
danger les enfants. Aujourd’hui, être parent est devenu extrêmement
exigeant. Cela reste souvent un passage obligé, perçu socialement comme
un accomplissement personnel. Les standards de parentalité ont beaucoup
changé dans les cinquante dernières années : nos grands-parents seraient
probablement aujourd’hui vus comme étant au minimum négligeant·es.

Le burnout parental est une réalité qui touche au moins 5% des parents
(à cause de sa faible prise en compte, il est difficile d’évaluer le
nombre réel de personnes touchées). D’après cette étude, il est en
augmentation, en particulier à cause de la hausse de la pression sociale
et des attendus envers les parents. Le burnout cause des problèmes de
santé divers, de la dépression, des risques d’addictions, des idées
suicidaires (car on ne peut pas cesser d’être parents facilement) ou des
idées de fugue parentale, pour disparaître et se soustraire à ce rôle
qui est devenu ingérable. Pour les enfants, c’est de la violence
physique et/ou verbale exercée par le parent, un manque d’attention de
celui-ci à leur vie d’enfant, à sa santé, à ses difficultés… Bref
c’est dangereux et c’est honteux, alors les parents le cachent. Lae
parent en burn out va négliger de plus en plus son ou ses enfants, car
iel n’arrive plus à s’en occuper.

De façon assez surprenante, il est plus lié à des causes sociales
qu’économiques et a une plus grande prévalence dans les pays où les
parents sont stressés et les familles individualistes. D’après les
auteurices, un facteur essentiel pour le diminuer est le renforcement
des réseaux sociaux autours des familles pour les soutenir. Dans le
burnout parental, « lae parent qui est surmené va changer de
comportement, abandonnant la charge de l’enfant à son/sa partenaire.
Cette modification de comportement, qui est souvent incomprise, va mener
à des tensions voire de la violence dans le couple ».

Une des bases des thérapies pour le burnout parental est de sortir de la
culpabilité pour solliciter de l’aide. Elle peut venir de professionnels
ou du réseau. Pour qu’il y ait intervention de professionnel·les, il faut
que les difficultés amènent les pouvoir publics à en faire la demande,
ou les parents décident d’aller chercher de l’aide. Mais les problèmes se sont
souvent déjà bien installés. Alors que des proches, pourraient elleux intervenir bien
plus tôt en voyant la situation se dégrader. D’où l’importance qu’iels
se sentent en position de proposer de l’aide et qu’elle puisse être
acceptée par les parents, sans que tout cela ne soit perçu comme un échec.

Mais quels parents laissent vraiment rentrer un tiers dans l’intimité profonde
de leur famille ? Souvent personne… D’où le besoin d’ouvrir le cercle
de la famille nucléaire pour y laisser entrer de nouveaux parents.

Dans ces cas-là (et dans d’autres moins graves), il serait possible de
réactualiser une pratique dont le nom a presque disparu du français : le
forestage. À l’opposé de l’adoption qui est définitive,  « le forestage
ne concerne qu’un remplacement momentané des ascendants par des tuteurs
désignés par ces derniers ». Ce remplacement momentané ne change ni
l’identité, ni la position sociale de l’enfant. Bien sûr, il est
possible d’imaginer une infinité de variations autour de cette
possibilité, allant de quelques heures régulières à de plus longues
durées. C’est l’occasion pour les parents de se décharger momentanément
de leur rôle pour reprendre leur souffle et retrouver le désir de
s’occuper de leur enfant. Dans la foulée de la libération autour de la
question du post partum est arrivée celle du regret, en particulier de
la maternité. Ce dernier ne touche pas forcément le fait d’avoir eu un
enfant mais certaines des implications sur la vie et la position
sociale. Pour rendre ce *remplacement* imaginable, il faudrait sortir du
mythe selon lequel les enfants nous appartiennent. Nous ne sommes pas
propriétaires, même si nous sommes géniteurices.

Pour décrire les modes de suppléance imaginables, je vais m’appuyer sur
des concepts tirés de la parentalité en famille d’accueil. Avant cela,
je dois préciser que ce n’est ni pour attaquer ni pour encenser le
fonctionnement de la protection de l’enfance. Cela dépasse mes
connaissances et le cadre de cet essai. En revanche, il semble clair,
que les personnes qui sont passées par l’aide sociale (que ce soit en
foyer ou en famille d’accueil) ont plus de difficultés dans leur vie
d’adulte. Mon objectif n’est donc absolument pas de défendre l’idée que
le placement est positif, simplement de montrer qu’il existe des formes
de soutien aux parents qui peuvent fonctionner. Le placement d’enfants
venant de familles vivant de fortes difficultés ne peut pas être comparé
avec des parentalités élargies et ou soutenantes, formées avec le
consentement de toutes les parties prenantes (allant si possible jusqu’à
l’enfant). Cette dernière s’inscrit dans un cadre préventif et
bienveillant dont l’objectif est avant tout le bien être de toustes.

Il existe trois grands types de suppléances. Elles peuvent être
substitutives, c’est-à-dire que la famille d’accueil va remplacer dans
la parentalité les géniteurs. Il n’y a pas de remplacement légal dans la
filiation, sauf en cas d’adoption. Bien sûr, ce n’est pas le type de
suppléance qui nous intéresse dans la constitution de parentèles.
Pourtant, savoir que si demain il arrive quelque chose de grave aux
parents, il y aura des proches qui pourront prendre entièrement en
charge les enfants le temps que la situation s’améliore constitue un
grand confort émotionnel. On peut penser à des maladies, des accidents
voire à de la prison pour un engagement militant.

La suppléance peut être aussi soutenante. Cette forme-là rentre dans
l’imaginaire de la parentèle, elle est finalement souvent pratiquée à un
petit niveau entre ami·es ou en famille. Cela est couramment pratiqué : des grands-parents qui vont
garder les enfants, ce qui va permettre aux parents de faire des
économies de garde pendant les vacances ou leur permettre d’avoir du
temps. Des adultes vont être présent·es
dans la vie des enfants qui ne sont pas les leurs, les prendre en charge
pour aider les parents, voire faire plaisir à leurs propres enfants
quand celleux-ci sont ami·es. Cela ne pose aucune difficulté sociale ni de problème de développement aux enfants et pourtant c’est une
forme de suppléance.

Dans le cadre d’une parentèle, nous pourrions facilement dire que les
différent·es adultes vont pouvoir se soutenir. Cela diminuerait les risques
d’épuisement et de burnout parental. Face à la distance géographique qui existe souvent entre les générations, il peut être difficile pour
les parents de recevoir l’aide des grands-parents. De plus, ceux-ci à
la retraite (ou pas) n’ont pas forcément le désir, l’énergie ou la
disponibilité pour prendre soin des petits enfants.
Aujourd’hui, des ami·es peuvent prendre en charge une partie du soutien.
Cela dit, même en vivant dans la même ville, cela n’est pas forcément
simple. Les ami·es ne sont pas habitué·es à s’organiser ainsi, à se
libérer, à donner du temps pour faire de la garde d’enfant. De l’autre
côté, les parents n’ont pas forcément la confiance nécessaire pour
confier leur enfant à des ami·es, pour des périodes un peu longues, ou
juste le temps d’aller au sport. Et ce phénomène semble encore plus rare
entre parents et non-parents. D’où la nécessite de lier nos vies et de
partager les savoir-faire de la parentalité par d’autres lien que la
transmission intergénérationnelle.

Sans que cela n’ait pour l’instant de reconnaissance légale, une
pratique régulière de *forestage* créerait pour les enfants de nouvelles
figures d’attachements et déchargerait les géniteurices d’une part de
leurs charges de parents. En sus, les autres *parents* pourraient tout à
fait être reconnu·es, soit juridiquement comme l’imagine Donna Haraway
dans sa nouvelle *Les enfants du compost*, soit socialement, en ayant un
avis à donner sur les choix concernant l’enfant.

La suppléance peut aussi aider les couples où seulement l’un·e des
membres veut être parent. Dans la culture hétéro, c’est souvent un
motif de séparation. Pourtant, il existe plein d’enfants et de parents
qui auraient besoin d’amour supplémentaire. Les modalités restent à
imaginer en fonction de tous les paramètres de la vie de chacun·e.
Mettre en place des moments de suppléance ne serait pas forcément plus
difficile que le processus *séparation – rencontre – création d’une
nouvelle famille nucléaire*. Bien sûr, cela nécessite d’accepter de ne
pas avoir son propre enfant rien qu’à soi.

Une autre attaque contre le modèle « parent enfant » vient des doutes
sur l’avenir et la responsabilité découlant de la parentalité dans un
futur incertain. Ce trouble a concerné de nombreuses époques : nos
grands-parents et ceux d’avant ont vécu des guerres mondiales, la vie au
19^e siècle était marquée par la précarité. Pourtant, la question de ne
pas avoir d’enfant par peur de l’avenir me semble assez nouvelle et
rebat les cartes. Cela reste une préoccupation liée au fait que nous
sommes « riches », que nous avons des systèmes de retraites et un accès
à des moyens de contraception. Si je doute que ce questionnement soit
déjà visible au niveau statistique, il l’est dans bien des têtes.

C’est un choix fort. Décider de ne pas procréer, c’est une remise en
cause profonde de la famille. C’est se couper de la manière la plus
simple de créer une famille. Même en 2023, alors que le « wokisme bat
son plein », il n’en reste pas moins que décider de ne pas fonder un
foyer, c’est se mettre à la marge. Pourtant, une proportion croissante
de la population se pose cette question, à mesure que la certitude que
« ça va être la merde » gonfle. Cela reste un tout petit ballon, bien
loin d’éclater, car si nous avions vraiment peur, nous agirions en
conséquence. C’est-à-dire que nous prendrions les menaces au sérieux,
que notre survie future deviendrait notre principale préoccupation.
Aujourd’hui, l’écrasante majorité des personnes agit comme si cela n’allait pas arriver, y compris celles qui ont
compris le problème,  qui savent que nous allons dans le mur et qu’il est
quasi certain que rien ne changera avant que nous ne prenions des chocs
violents. Je suis l’une d’entre elles.

Décider de ne pas faire d’enfant c’est une façon de mettre une ceinture
de sécurité, de s’enlever de la culpabilité et de se simplifier la vie
en prévision de quand ce sera compliqué. Si certain·es sont prêt·es à
faire ce choix radical, c’est que la famille telle que nous la
connaissons ne représente plus cet idéal de vie qui nous a été vendu.

Et pourtant, celleux qui désirent ne pas avoir d’enfants (ou pas pour
l’instant) peuvent vouloir vivre et relationner avec elleux. Ce n’est
pas contradictoire. C’est une chance pour peu que nous poursuivions le
mouvement d’ouverture des familles. Sans que cela semble
révolutionnaire, le fait de donner des responsabilités envers ses
enfants à d’autres adultes non professionnels hors de la famille de sang, 
ce n’est pas rien. En particulier si ces responsabilités
laissent de l’autonomie et vont jusqu’à une participation à la prise de
décision.

Cela touche à un des piliers de la famille nucléaire : la possession des
enfants par les parents. C’est une question sensible à un niveau
intime car nous sommes habitué·es et attaché·es à avoir « nos enfants ».
C’est aussi politique, car il a rarement été bon que l’État récupère la
parentalité. Mais ce n’est pas la question ici. Je ne propose pas que
les enfants ne soient plus ceux « de leurs parents ». Ils pourraient
être aussi « les enfants » d’autres adultes, qui par leurs actions
produisent de la parentalité dans un consentement multipartite. Bien
sûr, il n’y aurait aujourd’hui aucun formalisme légal à cela, cela importe peu
et comme je l’ai présenté précédemment, les familles savent avancer
avant la loi. C’est à nous d’oser proposer et demander. Que celleux qui
le désirent s’impliquent et que celleux qui peuvent en avoir besoin
acceptent l’aide, en ayant à l’esprit qu’un jour peut-être il sera
possible d’acter que son enfant ait plus de deux parents.

Les adultes ont pris l’habitude d’appeler « tonton » ou « tata » les
ami·es qui sont proches de leur enfants. On pourrait penser que c’est un
pas dans la direction de la famille élective, mais ce n’est souvent
qu’une marque d’affection sans conséquence concrète. Les termes de
parrain et de marraine seraient bien plus adaptés au mouvement que je
propose, en tant qu’étape avant la parenté. Plutôt que de les nommer à
la naissance, c’est la pratique qui devrait ouvrir la voie à la
proposition. En effet, on ne peut pas présager de comment deux personnes
ne se connaissant pas vont s’entendre. Et puis ces rôles n’ont pas à
être fixés, nous pouvons nous autoriser à rajouter des nouveaux parrains
et marraines au besoin, voir à en enlever comme dans cet extrait : « Mon
ainée avait deux marraines au début, mais l’une d’entre elles ne fait
plus partie de notre entourage. Du coup elle a un nouveau parrain
maintenant, un mec cis avec une grande capacité de remise en question,
une grande sensibilité ».

Ne plus être (pour les géniteurices) les seul·es à devoir assumer la
responsabilité d’un enfant pourrait être vu comme un risque de
déresponsabilisation. Je préfère penser que le(s) adultes s’engageant
dans la conception d’un enfant, dont iels imaginent partager la
responsabilité avec des coparents le font avec une profonde conscience
de leur engagement.

S’occuper d’enfants, c’est une expérience souvent difficile et pourtant
nous n’y sommes jamais concrètement formé·es. Expérimenter la
parentalité à temps partiel, en ressentir les joies et les difficultés,
n’est-ce pas une bonne manière de faire des parents ? La distance
physique entre les générations ainsi que l’évolution des modèles de
parentalité nécessite de recréer des moments de transmission de ce
savoir-faire. C’est aussi cela qu’apporterait le partage de la
parentalité. Ce serait pour les adultes la possibilité d’apprendre entre
elleux, au contact de divers enfants dont iels auraient la
responsabilité temporaire ; et pour les enfants, de développer leur
réseau relationnel dans la diversité de pratique parentales.

« Faire famille » pour les enfants, c’est aussi suivre la norme, être
comme les autres. C’est très questionnant de vivre différemment. Dans
certaines familles, « les enfants préfèrent, malgré les disputes, la
non-séparation des parents » parce que cela « fait famille ». Parce que
c’est la norme et que cela a un côté rassurant. C’est à prendre en
compte dans la création de parentèles ou de foyers comme les appelle Tal
Madesta. Créer des lieux et des modes de vie hors normes, c’est
difficile. A fortiori pour celleux qui ne l’ont pas choisi, comme les
enfants !

La parentalité est un étau normatif. C’est probablement lié à la
pression de devoir s’occuper des enfants, de le faire bien, d’être sans
cesse jugé·es par les autres et par soi-même. Suis-je un bon parent ?
Est-ce que je mets en danger mon enfant, son avenir ? Qu’est-ce que je
fais mal ? Puis viennent les questions de l’enfant, 
légitimes. Pourquoi est-ce que notre famille n’est pas comme les autres
familles ? Est-ce que l’on ne pourrait pas faire comme les autres ? Lui
baigne toute la journée, à l’école, dans le bain de la norme, ou de la
performance de la norme. Sortir du rang à l’école c’est être exposé aux
moqueries, voire au harcèlement. L’école n’est pas un cocon doux et
protecteur. La situation peut-être encore plus complexe si l’enfant est
aux prises avec l’intersectionnalité des oppressions. Alors il pourra
être tentant pour les enfants de faire comme les autres, de demander à
ce que à la maison ce soit plus conforme à ce qui se fait chez les autres.

S’il y a consensus dans le groupe pour qu’il y ait des enfants (car on
peut très bien imaginer une parentèle sans enfant), il est nécessaire de
leur faire une place. Les enfants n’ont pas choisi de ne pas être dans
une famille habituelle, alors il faut que tous les adultes acceptent de
prendre en compte leurs besoins et désirs. Du reste, dans les familles
classiques, les enfants devraient aussi avoir le droit de participer aux
décisions du groupe.

C’est comme cela que j’imagine la création de parentèles dans le cadre
de ColoCs, de communautés de vie, ou que sais-je. Ici il n’y
aurait pas défaillance des parents, ou pas sur le long terme. Il serait
possible d’imaginer qu’en cas de burnout parental, ou juste
d’épuisement, d’autres adultes du groupe se substituent temporairement
aux parents biologiques, bien que « les parents restent les parents ».
Il est certain que le décalage entre l’impossibilité légale de créer une
pluriparentalité et la pratique créera forcément des difficultés d’ordre
pratique. Néanmoins, c’est un lieu d’expérimentation puissant pour nos
émotions et pour offrir de l’amour à un/des enfants sans devoir être
géniteurice soi-même. Tout comme les familles queers le font déjà, il
faudra inventer des noms pour les fonctions que va créer la
multiplication des figures composant une parentèle. Il va y avoir du
tâtonnement, des essais et des retours en arrière. Pour l’autrice de
*Faire famille autrement*, il n’y a pas de risque pour l’épanouissement de
l’enfant, ni de « conflits internes chez l’enfant liés à la présence de 
plusieurs figures maternelles », même si celui-ci « a plusieurs figures d’attachement ». C’est ici une confirmation de la possibilité pour
les enfants de bien se développer dans des configurations familiales originales. Ce qui compte c’est que les enfants soient entourés,
aient une place dans la parentèle et reçoivent soin et amour.

Posons-nous un instant et essayons d’imaginer ce que cela donnerait dans
les faits qu’un enfant ait plus de deux parents… Et nous prenons conscience qu’il existe de très nombreuses familles recomposées dont les enfants se retrouvent
à avoir trois ou quatre parents. Vu que ces familles restent souvent accrochées à
l’idée d’un papa et d’une maman, les répliques du genre « tu n’es pas ma mère » peuvent exister. C’est en
particulier dû au fait que la situation n’a pas été choisie par les
parents biologiques. De plus chaque parent va a priori vouloir garder sa
position sociale de père ou de mère. Il est donc bien précisé à l’enfant
qu’iel n’a qu’un père et qu’une mère. La proximité avec un nouveau
parent et/ou la distance avec un ancien peut venir flouter les cartes.
Néanmoins, je pense que rares sont les parents qui voient dans la garde
partagée la possibilité de donner à leur enfant 4 parents plutôt que 2.
En tout cas, vu le nombre de cas (480 000 rien qu’en France), il
semble que la multiplication des parents, même si elle n’est pas simple
pour des questions d’organisation, de rancœur, de compétition, etc, ne
met pas en danger la santé mentale des enfants.

S’il ne faut pas oublier à quel point notre modèle familial crée des
situations difficiles pour les enfants, il ne faut pas négliger les
problématiques nouvelles que causerait l’addition de parents pour
l’enfant. Dans le cas de la co-parentalité homosexuelle (« par exemple
un homme gay et d’une femme lesbienne, vivant ou non par ailleurs en
couple avec un compagnon ou une compagne »), les enfants peuvent
rapidement avoir 4 parents (les deux couples de leur géniteurices). Dans
les cas de recompositions familiales, « la création de nouveaux liens
\[…\] semble particulièrement difficile du fait d’un nombre déjà
important d’adultes référents au moment de la « composition » familiale
initiale ». L’enfant peut se retrouver dans des conflits de loyauté
« vis-à-vis des parents qui l’ont fait naître tout en s’étant construit
avec d’autres figures présentes dans son entourage depuis sa
naissance » . Un des enfants prenant part à l’étude citée comprend
« très bien ce qui constitue les liens biologiques et juridiques
(institués) de ses deux parents, les fonctions éducatives de sa marraine
(compagne de sa mère à sa naissance) et de son quasi-parrain (compagnon
de son père à sa naissance) ». En cas de séparation, la différence entre
« la filiation instituée, celle de l’arbre généalogique, d’une filiation
plus élective, celle de son cœur », prend de l’importance.

Si la création de liens nouveaux peut être difficile après une
séparation, n’est-ce pas avant tout à cause de cette dernière ? Je n’ai
pas trouvé de réponse à cette question. Une proche qui a souvent changé
d’école jeune m’a raconté qu’à un moment, elle ne voulait plus se faire
d’ami·es car elle ne supportait plus de les perdre. Ne peut-on pas faire
un parallèle entre les deux ? À quoi bon pour un enfant créer des
relations avec de nouveaux parents, si celles-ci ne sont pas fait pour
durer ? Si s’engager avec ses ami·es revient à accepter que cela
entremêle nos vies, cela devrait être la même chose avec les enfants.
Ces liens structurants obligent les adultes et devraient pouvoir modeler
d’autres sphères sociales, comme le travail.

Les conflits de loyauté entre les types de parents n’est-elle pas en
lien avec l’inégalité que constitue dans notre société la position de
parent biologique ou institutionnel face à celle de parent électif ? La
loi pose cette différence, mais il revient aux parents de décider quoi
en faire. Il est entièrement possible d’imaginer pour les parents des
modes de décisions variés, qui vont éviter à l’enfant de se retrouver
dans un conflit de loyauté. S’iel se retrouve dans cette position, c’est
que les parents n’ont pas réussi à s’accorder. Encore plus que dans
un foyer nucléaire, une famille élective (parce qu’elle sera remise en
question) doit être capable de gérer la discorde, de communiquer,
de faire simplement ce que font des humains qui s’aiment.

De tout ce que j’ai pu lire, la recherche sur les habitats partagés
montre que lorsque le niveau de partage du quotidien diminue trop, les
problèmes commencent. Cela conforte ce que j’ai vécu au long de mes
années de colocation. Le quotidien est le terreau de la relation, mais
il est usant aussi. Alors, il faut entretenir l’amitié et l’amour par
des gestes et de l’attention. Un peu comme dans une relation de couple,
mais avec un niveau d’intimité physique moindre, plus d’une personne et
un engagement moral très différent. S’il y a des proximités dans
l’objectif, la mise en pratique n’a rien à voir. Et probablement que
dans une famille choisie, les modalités et besoins seraient encore
différents.

Les conditions nécessaires à l’addition de parents sont a minima : la
capacité des adultes à vivre au quotidien ou régulièrement avec
d’autres et pour les parents biologiques à accepter que les enfants ne
leur appartiennent pas. Cela peut sembler bloquant pour la majorité des
parents. Pourtant c’est le chemin que j’aimerai emprunter pour voir à
quoi ressemble l’autre côté de la colline, et surtout à quel point cela
peut créer de la joie dans ces nouvelles familles, électives plutôt que
nucléaires.

### Des possibilités de mise en commun du patrimoine

Les émotions et relations ne peuvent pas faire l’impasse sur des
considérations matérielles, comme avoir des lieux pour les mettre en
œuvre. S’il me semble possible de partager de l’amour hors du cercle
familial restreint, il me semble difficile de l’envisager sans un foyer
où établir ses liens. Si « dans les années 50, l’accès à la propriété
s’est démocratisé, c’est à dire que des gens issus de classes sociales
plus diverses sont parvenus à accéder à la propriété », aujourd’hui, la
précarisation des carrières et le resserrement des crédits font qu’il
est plus difficile de devenir propriétaire. Si cette époque peut faire
encore rêver, elle n’a été possible que par des raisons conjoncturelles, c’est à dire «  une très forte croissance
économique » qui est maintenant révolue, des revenus « sécurisés et des
politiques économiques libérale ». Depuis les années 90, il y a « de
moins en moins de primo-accédentant·es », par contre il y a une
concentration du patrimoine immobilier.

S’il y a un domaine où l’institution famille est réellement efficace,
c’est l’héritage. « Depuis Bourdieu et Durkheim, il y a eu un fort focus
sur l’héritage culturel et symbolique ». Si cette transmission de
position sociale est importante, elle ne doit pas cacher la puissance
fondée sur l’héritage matériel qui occupe de plus en plus de place dans
la sphère économique (comme le montre entre autre Thomas Piketty). Notre
relation à l’héritage est constitutive de la famille nucléaire, au point
que « l’apparition de la famille conjugale serait liée à une phase
historique voyant l’émergence d’un mode d’appropriation privée et du
désir du chef de ménage de transmettre l’héritage ». La place du chef de
famille est affaiblie, pourtant l’attachement à l’héritage reste
viscéral. Même en étant peu fortunées, les personnes tiennent à le
transmettre et à le recevoir. Même dans *Les jours heureux*, un roman où
un mouvement gagne les élections présidentielle et législative sur un
programme issu de *La paresse pour tous*, le gouvernement a plus de
facilité à faire passer la semaine de 15 heures qu’une vrai réforme de
l’héritage.

En plus de maintenir les inégalités, dans les faits, par l’action des
notaires, l’héritage est sexiste. Ainsi, les fils vont être avantagés
sur les filles. Ils vont davantage hériter des entreprises et la part des
filles va être minorée. Cet imaginaire social autour de l’héritage se
concentre en particulier sur l’idée de protéger ses enfants de la
spoliation collectiviste de l’État. Que même les petits patrimoines
partagent cette croyance constitue un réel avantage pour celleux qui ont
vraiment de gros héritages à exclure du partage. Pour éviter la
dispersion du patrimoine, celui-ci est attaché par le droit à la lignée,
qui verrouille « largement la transmission à l’intérieur de la
famille ». Pour mettre à mal cette puissance reproductrice, il faut à la
fois agir sur la transmission et sur les lignées.

En faisant des parents, il est possible de prolonger le lien de
parentalité multiples à travers des liens de parenté reconnus.
L’adoption simple de majeurs semble permettre par consentement mutuel de
l’adopté et de l’adoptant créer un lien de filiation juridique.
L’adoption simple permet à l’adopté d’acquérir de nouveaux parents
« tout en conservant tous ses liens avec sa famille d’origine ».

C’est une occasion de célébration, en mettant en scène les liens
électifs créés durant l’enfance. Il y a quelque chose de beau à
imaginer de jeunes adultes acceptant de nouveaux parents avec qui ils
relationnent depuis des années. En plus de la force symbolique du geste,
il peut être utilisé pour détourner les règles d’héritage de la
transmission dans la famille de sang. Avec ce subterfuge, l’enfant
adopté se retrouve à égalité dans la ligne d’héritage avec les enfants
biologiques. Au moyen d’héritages croisés et multiples, ainsi qu’avec
l’appui de notaires militant·es, nous pourrions partager les héritages
hors de la lignée.

Un autre intérêt serait de maintenir sur la durée des propriétés
collectives, sans passer par des organisations complexes. Sans projet
politique commun, l’indivision des biens peut les enfermer dans la
ruine. Les propriétaires n’arrivent à décider de rien, personne ne peut
rien faire. Alors qu’adossé à un objectif politique de partage du
capital, c’est un moyen d’étendre la possession de lieux ou de moyens
pour en rendre toustes les utilisateurices responsables. Bien sûr ce ne
serait qu’un premier pas pour ouvrir une brèche culturelle dans l’idée
de l’héritage.

Si ces micmacs juridiques peuvent être utiles pour sécuriser ses enfants
(génétiques ou choisis) et co-parents, cela ne devrait être qu’une étape
vers l’abolition des héritages au niveau de la société. À l’instar des
familles électives, il ne faut
jamais croire que nous pourrons tranquillement faire notre popote de
notre côté comme si nous étions hors de la société. Si nous allons
explorer des marges, c’est pour les renforcer et qu’elles puissent
abreuver le reste du corps social de possibilités et du désir de liberté
qu’elles contiennent.

Un moyen de nous départir en partie des effets d’accumulation qui
existent dans l’alliance et la descendance est l’utilisation de modèles
de possessions collectives et d’adoption mutuelle.

La propriété collective, en particulier des logements, a mauvaise presse
à cause de l’imaginaire attaché à l’URSS. De nombreux champs doivent
sortir de la propriété, néanmoins celle d’un foyer n’en fait, je pense,
pas partie. Le « besoin d’un espace de repli et de refuge », c’est ce
qu’appelle Aurélien Berlan la « dimension existentielle de la
propriété ». Si on peut l’imaginer collective, nous devons pouvoir,
« formuler le besoin d’un chez-soi, d’une intimité et d’une vie privée »
sans nous traiter mutuellement de bourgeois·es.

Il existe plusieurs exemples de propriétés collectives qui peuvent nous
inspirer. Longo Mai fait durer la propriété collective depuis des
dizaines d’années en utilisant différents montages juridiques, dont une
fondation et des coopératives. Un autre exemple intéressant est
l’adaptation du Mietshäuser Syndikat (adapté en France par Le Clip).
L’idée est que la propriété foncière ne soit pas possédée par les
personnes physiques habitantes, mais par au moins deux associations, une
d’habitant·es et une du Syndikat qui a pour objectif de bloquer toute
revente (pour le sortir de la spéculation), ainsi que d’organiser un
transfert de solidarité entre les lieux ayant adhéré. D’une certaine
façon, la multiplication des propriétaires va forcer l’organisation
collective. Conjuguer un projet politique à des propriétaires nombreux
semble être une arme redoutable face au pouvoir (capitaliste ou
étatique). Cela avait aussi été une des méthodes utilisées par les
paysans du Larzac dans les années 70. L’État voulait les exproprier pour
agrandir une base militaire. L’une des nombreuses actions pour mettre
des bâtons dans les roues de l’administration a été de vendre des
parcelles à plusieurs personnes (morales ou physiques). Cela multiplie
les interlocuteurices et augmente la durée ainsi que le coût des
procédures.

Un des modes d’organisation simples, régulièrement utilisé par les
personnes voulant posséder ensemble est le détournement du statut de
Sociétés Civiles immobilières (SCI) : la structure morale est
propriétaire du bien et les habitants sont actionnaires de la SCI. En
retravaillant les statuts, il est possible de jouer avec beaucoup de
liberté sur le mode d’organisation (en particulier sur la prise de
décision) de cette structure. Encore une fois, la possession
individuelle du lieu disparaît, pour la confier à une structure tierce
sur laquelle les personnes physiques ont des droits limités par le
collectif. À la base, cet outil juridique a été conçu pour simplifier la
gestion d’actifs immobiliers. Il est donc possible de revendre ses parts
(sans que cela ne cause forcément la vente du bien). Si dans la majorité
des cas, c’est l’occasion pour des investisseurs de faire une
plus-value, la souplesse des statuts peut encadrer la revente pour
éviter que cela n’entraîne la cission du groupe.

Ces outils, nous pourrons les utiliser à petite échelle pour créer
de chouettes lieux, qui seraient détenus collectivement, voire pourraient
échapper à la spéculation immobilière par des règlements juridiques bien
pensés. Mais pour pallier le manque de logements, il faudra dans le même
temps les diffuser largement dans la société, pour qu’ ils créent un
mouvement de propriété largement partagée. En agglomérant différents
résultats, une étude de Terra Nova sur le logement en 2023 a montré qu’il
faudrait produire entre 400 000 et 500 000 nouveaux logements par an
pour absorber l’accroissement de la population, son vieillissement, la
décohabitation des ménages et le mal logement. Sachant que dans le même
temps, il faut diminuer l’utilisation de matériaux et diminuer
l’artificialisation des sols.

La création de parentèles peut avoir un impact positif sur deux aspects
qui contribuent à diminuer le nombre d’habitant·e par logement : le
vieillissement et la décohabitation. Il y a énormément de retraité·es
qui habitent à une ou deux dans des maisons faites pour au moins quatre.
Je le vois au quotidien dans mon travail qui me permet de visiter des
retraité·es pour les aider à obtenir des subventions pour l’aménagement
de leur logement. C’est tout à fait compréhensible, les maisons sont
construites pour des familles avec enfants. Lorsque ces dernier·ières partent, le
logement est à moitié vide, et quand un parent meurt, le logement est au trois
quarts vide. En mettant bout à bout ces situations individuelles, un
grand nombre de pièces habitables sont vacantes dans des maisons sous
habitées. C’est une conséquence directe de nos modes de vie et de nos
attentes vis-à-vis des logements. D’autre part, concernant la
décohabitation des couples qui se séparent, nous devons apprendre
comment gérer collectivement les espaces laissés vacants par cette
réalité. C’est l’ensemble de notre chemin de vie qui fait varier notre
surface par personne. Quand on ajoute à cela qu’environ un tiers de la
population française aura plus de 60 ans en 2040, la question de
l’autonomie ne peut être éludée.

Il n’est pas question d’imaginer remplir autoritairement les maisons
sous habitées. Par contre, nous pouvons créer des initiatives politiques
visant à modifier notre rapport à l’habitat : colocation pour des
personnes âgées, partage d’habitat multigénérationnel, baisse des
loyers, reconnaissance en cas d’héritage de colocateurices qui auraient
partagé la vie d’une personne âgée…

Les ColoCs puis la création de parentèles sont un chemin pour modifier
nos comportements sur l’habitat. Elles permettent de ne pas laisser
d’espaces vides en remplaçant celleux qui partent, tissant de nouveaux
liens, insufflant une dynamique aux lieux. Socialement nous pouvons
expérimenter des alternatives, par exemple sur les chambres des enfants
partis faire des études. Oui, c’est super d’avoir une chambre toujours
prête chez mes parents, mais elle est vide plus de 330 jours par an.
C’est à la fois en jouant sur les imaginaires, mais aussi grâce à des
actions politiques qu’il serait possible de remédier à cela.

Une problématique qui freine les imaginaires et leur mise en
action est la disposition des logements actuels et nos habitudes de
propriété. L’arrangement intérieur n’a pas été pensé pour que des
adultes y vivent ensemble, et encore moins pour que des familles les
partagent. Le mouvement assez récent de découpage des appartements pour
en faire des airBnB ou des locations à la chambre renforce cette
difficulté. Les espaces sont un véritable sujet quand il est question de
vivre à plusieurs en se dérangeant le moins possible. Si des adultes
sont capables de faire attention aux bruits qu’iels causent, pour des
enfants c’est différent.

Il n’y a pas de solution d’aménagement magique. Le partage de la
parentalité rend acceptable une partie des difficultés qu’elle génère.
Pour les espaces, il faudra être imaginatif, les arranger et remembrer
des logements (qui sont aujourd’hui découpés en petite surface par les
bailleurs). Une méthode pratique est l’installation d’habitats légers
autour d’une maison qui sert de partie commune. Cela permet à la fois
aux parents d’avoir un espace privatif suffisant pour être avec lae
petit·e et de préserver le reste du groupe de la contrainte que
représente la vie avec un nourrisson. L’organisation d’une maison avec
autour des habitats légers me semble être la plus simple à mettre en
place et la plus adaptable à la taille du groupe, notamment pour
répondre à l’arrivée d’enfants ou de nouveaux habitant·es. En permettant
d’avoir des espaces extérieurs, elle offre aussi la possibilité pour les
collectifs de cultiver des espaces et de poser des prémisses
d’autonomies.

### Amour comme jeux d’engagement

Une réflexion qui m’a beaucoup travaillé pendant la préparation de cette
partie a été de trouver le moyen de différencier les typologies d’amour
qui s’intersectent sur une personne, notamment dans le cadre des
relations familiales. Comment différencier correctement l’amour qu’une
personne peut porter à ses parents, ses enfants, sa/son/ses
amoureux·ses, sa fratrie, ses ami·es, autrement que « je les aime
toustes, mais pas pareils » ? Définir son amour, ce n’est pas évident.
Malheureusement je n’ai pas été capable de trouver de la matière pour
m’éclairer. Je vous propose donc la vision suivante :

-   L’amour est un lien d’engagement et de devoir que l’on se crée
    vis-à-vis d’autres entités que nous-mêmes. Cet·te autre peut être
    humain·e, non-humain·es, lieux, choses, imaginaire… Ce lien et sa
    mise en œuvre ne sont pas une obligation imposée de l’extérieur.

Ce n’est pas un engagement imposé, en particulier ce n’est pas une
obligation que tu t’imposes à toi-même. C’est différent d’aller
t’occuper d’un grand-parent, qui ne te reconnaît plus, parce que tu te
sens obligé de le faire et pour éviter la culpabilité, ou que tu dois le
faire pour ne pas être jugé par la société. L’amour ce serait plutôt
aller t’occuper de tes grands-parents qui ne te reconnaissent plus,
parce que même si cela t’arrache les tripes de douleur, tu ne peux pas
faire autrement, car c’est important pour toi, pas vis-à-vis des autres.

Cela rejoint l’idée avancée par Liv Strömquist que l’amour est une
mini-religion où chaque participant·es est à la fois divinité et
prêtre/prêtresse. Chacun·e va faire des offrandes (pouvant passer par
différentes modalités : contacts physique, mots valorisants, cadeaux,
services, moments de qualités) par amour, sans attendre de réciprocité,
mais dans la seule attente que leur geste soit accepté. Ce n’est pas un
don, car un don appelle dans le cas des relations suivies un contre-don.
L’amour lui n’en appelle pas.

Cette grille de lecture a l’intérêt de différencier les liens amoureux dont
nous sommes les hôtes plus simplement qu’en essayant de définir des
émotions mouvantes. Tu portes de l’amour à plein de personnes, mais pas
de la même façon, car tu ne te mets pas les mêmes engagements envers
elles.

Bien sûr il y a aussi des différences dans chaque type de groupe
relationnel. On ne porte pas le même amour à toustes ses ami·es, ni à
ses deux parents, ni à ses enfants. Même face au fait de ne pas
« aimer à égalité » tous ses enfants, cette analyse apporte des pistes
de réflexion. Vous ne les aimez pas plus ou moins, certains ont une
relation plus forte avec vous que d’autres. C’est normal, vous êtes
humains. Tous n’ont pas les mêmes besoins, c’est normal, ce sont des
humains. Et vous allez vous mettre des engagements différents envers
chacun. L’important ce n’est pas de compter le nombre d’engagements, ce
n’est pas de les évaluer, c’est que tous les enfants d’une famille
reçoivent l’attention, le soin et l’affection dont ils ont besoin. Au
moment où un enfant quitte la maison, les parents continuent à l’aimer,
mais modifient vis-à-vis de lui leur champ d’engagement. Leur amour
évolue de celui pour un enfant à celui pour un adulte.

Avec le temps, la relation avec un·e amoureux·ses change dans sa
pratique, sans être forcément corrélée aux émotions. Il n’est pas
contradictoire d’avoir des sentiments moins intenses et en même temps
une pratique de l’amour plus forte. Un bon exemple me semble être le
passage de la passion à la création d’une histoire. Sortir de la passion
pour s’engager, c’est accepter que l’on ait maintenant des devoirs
envers l’autre.

Ainsi on peut déplier cette grille d’analyse pour la superposer avec les
différentes relations d’amour que l’on vit pour mieux comprendre nos
comportements vis-à-vis de gens que l’on aime. Enfin, le fait de ne plus
vouloir offrir d’engagement envers quelqu’un est peut-être la fin de
l’amour. Cela n’empêche pas de se sentir obligé vis-à-vis de la société,
vis-à-vis d’un engagement extérieur à soi-même, mais à ce moment ce
n’est plus une offrande.

Voilà une piste de réflexion pour charger nos relations de parentèles.
Créons de l’engagement, remplissons-le de vie pour nous lier. Ce n’est
pas rien ! Savoir que l’on peut être accueilli·e pendant une longue
période, c’est un joli parachute de secours. Mais
globalement cela ne se fait pas, car c’est le type de chose que seuls
nos parents sont en capacité d’accepter. Personne d’autre n’est prêt à
s’engager autant. Probablement parce que nous aussi nous ne sommes pas
prêts à faire de même. Cet engagement n’a du reste pas forcément besoin
d’être symétrique, parce que l’amour ne l’est pas. Même dans un couple
classique, personne ne s’attend à ce que les deux partenaires se donnent
de l’amour exactement de la même façon. De la même manière, tous les
membres des familles électives vont donner et recevoir de l’amour sous
différents biais.

Autour de toi, y a-t-il des personnes que tu aimerais avoir dans ta
famille élective ? Que serais-tu prêt·es à faire pour elleux que tu ne
ferais pas pour d’autres ? Peut-être que tu peux leur en parler. Cela
ne mènera sûrement à rien de particulier, à part leur faire plaisir.
Dans certains cas, elleux te diront qu’iels sont prêt·e à s’engager
aussi de leur côté parce qu’iels ont de l’amour pour toi. Si c’est le
cas, vous êtes en train de créer une union, c’est beau.

Les familles sont un genre de relation qui ne peut pas s’entendre sans
engagement. On y donne du soin à celleux qui sont dépendants·es, cela la
société doit le reconnaître.

Ici, la gauche détient une carte à jouer en investissant le terrain de la
famille. En considérant les soins prodigués dans la sphère familiale
comme un travail ayant la même valeur que celui dit « productif », elle
s’inscrirait ainsi dans un combat pour l’émancipation. Cela ne peut se
faire sans se placer dans une perspective de transformation sociale
complète. Sinon cela ne serait sûrement qu’une nouvelle prison pour les
femmes.

Pour cela, j’imagine qu’une option pourrait être de donner des décharges
(tout comme pour les délégué·es syndicaux) aux parents, et encore mieux
à l’ensemble de la parentèle, que ce soit pour s’occuper d’enfants, de
personnes âgées ou tout autre sorte de dépendance. Ces statuts devraient
bien sûr être indépendants du travail salarié, être rémunérés à leur
juste valeur et facilement accessibles pour que celleux qui font
aujourd’hui du travail gratuit puissent rapidement être reconnu·es.

L’amour n’est pas une question individuelle ou cantonnée à la sphère
familiale, même élargie. C’est un phénomène structuré par la société
dans laquelle nous vivons. C’est à dire alors que j’écris une société capitaliste et
patriarcale. Dès lors, changer l’amour nécessite de repenser les
institutions qui le structurent, les rapports de pouvoir qui le
traversent. Ce qui va nous amener dans ce livre à questionner la société
plus largement. En commençant par deux interrogations : qu’est-ce que
l’hétérosexualité fait à l’amour ? Qu’est-ce que le travail fait à
l’amour ?

Bibliographie

**Livres** :

BERGMAN, Carla, MONTGOMERY, Nick et ROUSSEAU, Juliette, 2021. *Joie militante: construire des luttes en prise avec leurs mondes*. Rennes : Éditions du commun. ISBN 979-10-95630-37-1.322.409 05

CHOLLET, Mona, 2021. *Réinventer l’amour: comment le patriarcat sabote les relations hétérosexuelles*. Paris : Zones. ISBN 978-2-35522-174-3.HQ801 .C4823 2021

COUM, Daniel, 2016. *Avons-nous besoin de père et de mère ?* Toulouse [Brest] : Éditions Érès [Parentel]. ISBN 978-2-7492-5085-4.158.24

GALLUZZO, Anthony, 2020.* La fabrique du consommateur: une histoire de la société marchande*. Paris : Zones. ISBN 978-2-35522-142-2.306.3

GODELIER, Maurice, 2010. *Métamorphoses de la parenté*. Paris : Flammarion. Champs. ISBN 978-2-08-123174-0.306.85

GRAEBER, David, WENGROW, David et ROY, Élise, 2021. *Au commencement était: une nouvelle histoire de l’humanité*. Paris : Éditions les Liens qui libèrent. ISBN 979-10-209-1030-1.909

HARAWAY, Donna Jeanne et GARCÍA, Vivien, 2020. *Vivre avec le trouble*. Vaulx-en-Velin : les Éditions des Mondes à faire. ISBN 978-2-9555738-4-6.304.201

MADESTA, Tal, 2022. *Désirer à tout prix*. Paris : Binge audio éditions. La collection sur la table. ISBN 978-2-491-26011-8.306.709 051

RICHARD, Gabrielle, 2022. *Faire famille autrement*. Paris : Binge audio éditions. La collection sur la table. ISBN 978-2-491-26013-2.306.85

SEGALEN, Martine et MARTIAL, Agnès, 2019. *Sociologie de la famille* [en ligne]. 8d. Armand Colin. [Consulté le 30 juillet 2024]. ISBN 978-2-200-28601-9. Disponible à l’adresse : http://www.cairn.info/sociologie-de-la-famille--9782200286019.htm

STRÖMQUIST, Liv, 2019. *La rose la plus rouge s’épanouit*. Paris : Rackham. Sous le signe noir de Rackham. ISBN 978-2-87827-235-2.805

YOUNG, Antonia, 2016. *Les vierges jurées d’Albanie* [en ligne]. Non Lieu Editions. Grand Format. ISBN 978-2-35270-223-8. Disponible à l’adresse : https://www.decitre.fr/livres/les-vierges-jurees-d-albanie-9782352702238.html

**Colloques** :

*Habiter sans posséder: la vie collective à l’épreuve de la propriété. Trois jours de réflexions et de débats, 15, 16, 17 juin 2018 à Dijon*, 2021. Nancy : Éditions les Presses du Faubourg. ISBN 978-2-9576904-1-1.335

**Articles** :

L’immobilier va baisser… Tant mieux ? | Terra Nova, 2023. *Terra Nova : think tank progressiste indépendant* [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://tnova.fr/economie-social/logement-politique-de-la-ville/limmobilier-va-baisser-tant-mieux/

BESSAOUD-ALONSO, Patricia et ROMAGNOLI, Roberta Carvalho, 2019. La famille comme institution entre pratiques sociales et éducatives. Un dialogue France- Brésil. *Le Sociographe*. 2019. Vol. 65, n° 1, pp. XXVI‑XXXVII. DOI 10.3917/graph.065.0090.

CONFAVREUX, Joseph, 2023. En Italie, les familles LGBTQI+ dans le viseur de Meloni. *Mediapart* [en ligne]. 12 mai 2023. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.mediapart.fr/journal/international/120523/en-italie-les-familles-lgbtqi-dans-le-viseur-de-meloni

GRATTON, Emmanuel, 2017. L’invention du « faire famille » du côté de l’enfant en situation de co-homoparentalité. *Dialogue*. 20 mars 2017. Vol. n° 215, n° 1, pp. 21‑35. DOI 10.3917/dia.215.0021. 

MARTIAL, Agnès, 2006. Qui sont nos parents ?: L’évolution du modèle généalogique. *Informations sociales*. 1 juillet 2006. Vol. n° 131, n° 3, pp. 52‑63. DOI 10.3917/inso.131.0052.

RASSE, Paul, 2014. Sexualité et communauté familiale, le regard de l’anthropologie. *Hermès*. 2014. Vol. n° 69, n° 2, pp. 135. DOI 10.3917/herm.069.0135.

ROSKAM, Isabelle, AGUIAR, Joyce, AKGUN, Ege, ARIKAN, et al , 2021. Parental Burnout Around the Globe: a 42-Country Study. *Affective Science*. 2021. Vol. 2, n° 1, pp. 58‑79. DOI 10.1007/s42761-020-00028-4. 

**Podcasts** :

L’autodéfense des enfants, 2023. Un podcast à soi [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.arteradio.com/son/61676665/l_autodefense_des_enfants

**Web** :

Adoption simple et adoption plénière : quelles différences ?, [sans date]. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.service-public.fr/particuliers/vosdroits/F15246

Isabelle Roskam - Burn-out parental: Comment l’éviter et en sortir? - YouTube, [sans date]. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.youtube.com/watch?v=CbWqjcSK2eU&t=1399s

La personne et la famille | vie-publique.fr, [sans date]. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.vie-publique.fr/dossier/277474-la-personne-et-la-famille

Les familles en 2020 : 25 % de familles monoparentales, 21 % de familles nombreuses - *Insee Focus* - 249, [sans date]. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.insee.fr/fr/statistiques/5422681

Making Love and Relations Beyond Settler Sexualities, 2016. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.youtube.com/watch?v=zfdo2ujRUv8

# L’amour n’est pas qu’une question de sentiments

Si nous considérons que l’amour est traversé par des rapports de force
préexistants (classe au sein du capitalisme, genre au sein du
patriarcat, etc.), il faut nous interroger sur la matrice que nous
voulons subvertir. Cela passe par un rapport violent, dans la lutte pour
nos conditions de vie. Il s’agit ici de penser l’amour, non pas depuis
un prisme individuel, mais dans le cadre d’un combat social. Dans cette
partie, nous interrogerons les systèmes d’oppression qui étriquent
l’amour. Nous nous raconterons des histoires, tenterons de donner vie à
d’autres horizons que ceux du Capital.

Comment, dès aujourd’hui, se donner de la puissance ? Quelles sont les
conditions matérielles nécessaires pour aller bien ? Qu’est-ce qui germe
déjà, quelles sont les luttes sociales que nous souhaitons soutenir ?
Quelles sont les institutions à subvertir pour pouvoir mieux s’aimer ?

Nous partons du présupposé suivant : c’est dans la relation qu’on trouve
la détermination de poursuivre le rapport de force.

荣霞燕

## Pour un amour post-structural

Ce que le patriarcat et le capitalisme font à l’amour, nous ne le
contournerons pas par nos tentatives individuelles de réinventer nos
relations. Pour le couple comme pour la famille, nos velléités de
réinvention s’embourbent dans la matrice où elles sont nées : un monde
capitaliste et monogame qui nous rattrape.

Si nous avons tant de mal à vivre nos relations en dehors des schémas
tracés, ou quand nos histoires d’amour libre virent au mélodrame France
Télé, « *Tentation, passions et jalousie* », il est sans doute
intéressant d’interroger les structures qui conditionnent nos amours.
Cette partie vise à sortir l’amour du prisme individuel, de celui de la
responsabilité, de la culpabilité, de la liberté contre l’autre, du
développement personnel. Elle invite à penser les conditions sociales
nécessaires pour mieux s’aimer.

Nous parlerons de l’hétérosexualité comme système obligatoire,
alimentant, par la binarité de genre et le rapport de domination
masculin, un terreau propice au développement du capitalisme. Faire la
révolution, c’est d’abord sortir de l’hétérosexualité obligatoire, des
rapports de genre grâce à l’alliance des combats féministes et queers.

Nous interrogerons ensuite le lien entre les luttes pour :

-   Faire du travail reproductif (le soin, le travail sexuel, etc.) un
    enjeu militant
-   Sortir le soin des rapports de domination (la classe, le sexe et la
    race)

Pour cela, commençons par définir ce qu’est le travail reproductif. Le
terme trouve sa source dans les théories marxistes féministes : là où le
travail dit productif engendre la création de biens ou de services
valorisés dans le système capitaliste et entraînant une rémunération, le
travail reproductif renvoie à toutes les tâches effectuées gratuitement
(considérées comme non-productives de valeur) dans la sphère privée :
faire les courses, la cuisine, le ménage, éduquer les enfants…

Dans une optique marxiste, cette notion peut en fait recouvrir tout ce
qui permet de reproduire la force de travail : dès lors, elle comprend
aussi le sexe, l’affection, les efforts relationnels. Finalement, le
travail reproductif reviendrait à prendre soin sans rémunération.

De ce dénigrement des tâches essentielles, mais non-productives, découle
une dévalorisation du travail de soin dans nos sociétés, ainsi que de
celleux qui le pratiquent. Nous verrons comment ces deux combats se
joignent dans la notion d’intersectionnalité. Nous explorerons le soin
communautaire comme horizon désirable.

Mais d’abord, sortons de l’hétérosexualité.

### Sortir de l’Hétérosexualité, prendre soin de nous

Penser le féminisme ne peut se faire qu’à partir d’une critique de
l’hétérosexualité, en tant que système de production du genre et des
oppressions qui en découlent. Pour Frederico Zappino, dans *Communisme
Queer* (2022), le capitalisme se nourrit des oppressions créées par
l’hétérosexualité (entre autres) pour s’affirmer. Selon lui, le
capitalisme est blanc, hétérosexuel et masculin : il tire profit de
l’exploitation des minorités.

Pourtant, la fin du capitalisme n’entraînera pas de facto l’institution
de relations plus égalitaires et apaisées. Il suffit de regarder les
oppressions de genre qui se reproduisent dans les milieux militants pour
comprendre que l’anticapitalisme ne suffit pas à éradiquer la domination
masculine. Les relations entre hommes et femmes sont à penser avec la
lutte des classes, comme un autre rapport de force et d’exploitation.

En cela, le féminisme devient pertinent lorsqu’il agit dans ses
alliances : il est queer, il est critique du genre, puisque nos
oppressions sont intrinsèquement liées par la binarité et la
hiérarchisation au profit du masculin. Il s’ouvre à l’intersectionnalité
autour de champs concrets comme celui du soin, en questionnant le
travail reproductif et la précarité qu’il engendre. Il propose d’en
faire un terrain de lutte et de réappropriation collective de nos
capacités à prendre soin.

#### Subvertir l’hétérosexualité comme sous-bassement du capitalisme

##### Oppression genrée et exploitation masculine : pour une lecture féministe du marxisme

Cette partie est née de la lecture de *Communisme Queer* de Frederico
Zappino. Elle a été enrichie d’autres rencontres, des thèses de Silvia
Federici, de Monique Wittig, de fictions et de discussions informelles,
ainsi que d’un entretien avec Jane, une femme trans lesbienne de 36 ans,
autrice et amie.

Pour être plus juste, nous pourrions dire que ce livre de Frederico
Zappino est arrivé comme une réponse, un nouveau chemin à creuser pour
relier les luttes queers et féministes. J’ai toujours eu le sentiment
confus, sans être capable de l’expliquer avec des arguments militants,
que le féminisme et la remise en question du genre étaient comme deux
degrés différents du même combat contre la domination masculine. Et
qu’en étant féministe, la curiosité sur les fondements de notre
oppression et ses justifications m’emmènerait rapidement du côté de la
barrière « queer ».

Intellectuellement queer. L’adverbe est important parce qu’à l’époque où
je commence à me poser ces questions, je me sens extrêmement attirée par
ce qui contredit la binarité du genre autour de moi, tout en étant une
femme cis, dans une relation hétérosexuelle. Loin de pouvoir me
revendiquer du queer en termes identitaires donc.

Et ce livre arrive entre mes mains comme une réponse. Ce qui compte, ce
n’est pas le combat de la catégorie, de l’étiquette, du groupe social
d’appartenance qui revendique son identité pour qu’elle soit reconnue.
Ce qui est politique dans le queer, soit dans la critique de la binarité
du genre, c’est de proposer un autre horizon que l’hétéropatriarcat. En
cela, il peut s’allier au féminisme. Et la question que ce livre nous
invite en creux à nous poser est donc non plus qui suis-je, mais
pourquoi et avec qui je me bats ?

Zappino, lui, se bat pour proposer une autre vision de la société, à
partir d’un positionnement d’opprimé, pour penser la subversion du mode
de production hétérosexuel. Si Zappino utilise le terme « mode de
production », c’est parce qu’il applique une grille de lecture marxiste
aux relations entre personnes de sexe ou de genre différents. Pour lui,
l’hétérosexualité est un mode de production, en ce qu’elle sous-tend
« la production matérielle des « hommes » et des « femmes » en tant que
tels, de même que la relation inégale, violente, et en tout cas,
obligatoire qui donne lieu à cette production et qui ne cesse d’en
découler ». Autrement dit :

-   L’hétérosexualité produit des hommes et des femmes, elle crée le 
 genre binaire puisqu’elle pense depuis la reproduction. 
 Par exemple, peut-être que le système de production homosexuel 
 produirait une plus grande catégorie de genre. C’est ce que nous 
 pouvons imaginer avec les catégories déjà existantes de fem, de butch, 
 etc. Cette projection vise à montrer qu’avec un autre système dominant 
 que l’hétérosexualité, on peut imaginer (sans être sûr⋅e de ce qui 
 adviendrait), que le genre serait recomposé, ou n’existerait pas. Et 
 qu’en cela, il est le fruit d’un système, et non pas une donnée 
 naturelle.

-   Cette production du genre s’opère dans un rapport hiérarchique, où 
la différence entre les sexes entraîne des inégalités. 
Si je nais femme et que j’arrive dans le monde du travail en 2023, mon 
salaire sera environ 4 % inférieur à celui de mon homologue mâle, 
selon l’INSEE. Et cela, parce qu’à compétences égales, nous vivons 
dans une société hétérosexuelle où la différence entraîne hiérarchie, 
au profit du masculin.

-   La « différence » sexuelle devient un jugement dont dépendent 
implicitement l’évaluation aussi bien que toute possibilité de 
conformité, d’inclusion conditionnelle ou d’exclusion. 
Nous pouvons être cis et hétérosexuel.le et donc dans la norme, ou 
nous pouvons être un·e bonne homosexuel.le, qui accepte le mariage et 
cherche à s’inclure dans la société hétérosexuelle. Nous pouvons 
rejeter le modèle dominant qui nous est proposé, et se retrouver 
marginalisés, comme la plupart des personnes trans qui refusent le 
genre par exemple.

-   L’hétérosexualité a un caractère hégémonique dont nous ne pouvons 
pas sortir : c’est un non-choix ; qui conditionne nos goûts, nos 
projections, depuis notre enfance. C’est une matrice que nous 
sommes obligé·es de reproduire pour acquérir de la reconnaissance 
sociale.

Nous pouvons tisser un lien entre la pensée de Zappino et le film Matrix
comme allégorie de la transidentité. Je tiens d’autant plus à créer ce
pont que cette vision de l’œuvre contredit la lecture que j’en ai faite
dans la première partie, à propos de Matrix 4. Mais je pense que les
fictions sont porteuses de nombreuses graines que nous pouvons faire
germer. Et quoi qu’il en soit, j’évoque ici le premier épisode.

Dans Matrix 1, Neo découvre peu à peu l’existence de la matrice : une
réalité virtuelle, prenant la forme du quotidien, projeté dans le
sommeil des humain·es. Ces dernier·es sont en fait élevé·es pour
produire de l’énergie à destination des machines qui ont pris le
contrôle. La matrice est donc un artefact, un voile posé sur la réalité
et permettant de prétendre vivre normalement. Or dans la lecture de
Matrix comme une allégorie trans, la matrice représente la binarité de
genre qui nous empêche d’accéder à la liberté d’être soi.

Dans sa quête de vérité, Neo cherche aussi à vivre réellement, sans
entrave, contre la norme, contre l’illusion du confort représentées par
les agents de la matrice. Sur ce point, Matrix illustre la définition de
Zappino de l’hétérosexualité : soit Néo reste intégré dans la société en
se conformant à ce qu’on attend de lui (pilule bleue), soit il choisit
de se précipiter dans le gouffre du questionnement du genre, et il ne
pourra plus jamais retrouver le confort de la matrice.

La matrice ne se nomme pas, elle est là, elle n’a besoin que d’exister.
Tout comme Wittig dit que le masculin est le genre neutre par
excellence, Jane souligne lors de notre entretien : « l’hétérosexualité
ne se nomme pas, c’est une figure de l’omniprésence, oppressive, au
détriment de minorités qui ne correspondent pas à cette idée du sujet
rationnel, hétérosexuel, binaire ».

Cette matrice, si nous revenons à Zappino, désigne « les systèmes
normatifs et économiques fondés sur le mode de production
hétérosexuel », qui permettent aux hommes de tirer profit de leur
oppression, en tirant des ressources matérielles ou symboliques de ce
rapport de force.

Je pense qu’un détour historique, narré par Silvia Federici dans *Le
capitalisme Patriarcal* (2019), permettrait de mieux comprendre cette
lecture féministe du marxisme proposée par Zappino.

Federici raconte le processus de réforme des années 1870 en Angleterre
et aux États-Unis, qui a conduit à la création de la famille prolétaire.
À cette époque, la première révolution industrielle laisse la place à la
deuxième. Autrement dit, l’industrie du charbon et de l’acier remplace
celle du textile, nécessitant un flux régulier d’ouvriers nombreux et
résistants. Il faut donc prendre soin de la santé des travailleurs dans
les usines. Jusqu’ici, 20 à 30% des femmes actives occupaient des postes
dans les manufactures. Mais cette allocation des forces féminines dans
l’industrie empêchait d’assurer la reproduction des forces de travail.
Cette mutation technologique s’accompagna donc d’une transformation
sociale, où des lois de protection furent votées pour réduire le temps
de travail, puis expulser progressivement les femmes de l’industrie.

Il est intéressant d’observer comment l’État intervient pour relier
l’organisation de la famille aux besoins du marché. Ici, ce sont les
intérêts du capital qui régissent les principes organisateurs de la
famille, car c’est l’outil de production de la main d’œuvre. Selon
Federici, « la planification de l’État sur la taille, les conditions de
vie, le logement, le contrôle, l’éducation, la médicamentation et
l’endoctrinement de la famille n’a cessé de s’étendre » à cette époque.

L’éloignement des usines relègue les femmes à l’enceinte du foyer pour
prendre soin de la vie domestique. L’idéologie de l’amour et de
l’instinct maternel vient justifier la position subalterne des femmes et
la gratuité des services rendus. Cela signifie non seulement que la
femme est responsable de procréer pour la reproduction effective de
générations d’ouvriers, mais aussi de fournir le travail de soin,
d’entretien, d’affection et de sexualité pour restaurer la capacité de
travail des hommes.

Au-delà de la justification idéologique par l’amour, les femmes sont
désormais dépendantes du salaire masculin, marqueur d’une nouvelle
inégalité où l’homme « a le pouvoir du salaire et il devient le
contremaître du travail non rémunéré de la femme ». Federici va plus
loin : « on peut lire la naissance de la ménagère prolétaire à temps
plein – un phénomène que le fordisme a accéléré – comme une tentative
de restitution aux travailleurs salariés hommes des communs qu’ils
avaient perdus avec l’avènement du capitalisme, sous la forme d’un vaste
réservoir de travail non payé effectué par les femmes ».

Sur cette dimension, Federici pense que les intérêts des travailleurs et
des capitalistes coïncidaient : gagner un salaire pour entretenir sa
famille était un gage de respectabilité masculine qui séparait les
classes supérieures de la classe ouvrière. Il est donc frappant de voir
dans cet exemple comment l’hétérosexualité nourrit le capitalisme : elle
produit des ouvriers, compense la destruction des communs et de la
solidarité en fournissant le travail reproductif des femmes non-payées,
bénéficie aux hommes exerçant le pouvoir tiré de leur salaire dans le
foyer, et n’ayant ainsi aucun intérêt à subvertir le système.

Le capitalisme vient donc se nourrir de l’oppression hétérosexuelle et
en sort renforcé : il s’alimente des ressources humaines et symboliques
produites (la hiérarchisation entre hommes et femmes, entre travail
productif et reproductif) pour s’affirmer.

Dans son livre, Zappino insiste sur le fait que la fin du capitalisme
n’entraînera pas la fin des relations de pouvoir genrées, sans
subversion de l’hétérosexualité au préalable. Le rapport d’exploitation
des personnes assignées « femmes » par les personnes assignées
« hommes » continuerait à exister dans d’autres formes d’organisation
sociale et la violence hétérosexuelle à se répandre : exclusion sociale
et exploitation liée au sexe ou à la sexualité, binarisme du genre…

En revanche, s’attaquer à un des présupposés du capitalisme,
l’hétérosexualité, fondant elle-même le patriarcat, revient à affaiblir
le premier en attaquant la « condition de sa possibilité ». Pour
Zappino, « ce mode de production \[hétérosexuel\] est entièrement
social : nous-mêmes, qui en sommes le produit, sommes aussi contraints à
reproduire l’hétérosexualité si nous souhaitons parvenir à un forme
quelconque d’intelligibilité ou de reconnaissance. Et être reconnus
signifie être mis en valeur et au travail par le capital. Voilà sur quoi
le capitalisme se fonde. En conséquence, « les hommes » et « les
femmes » ne peuvent être qu’objet de subversion, et non pas de
défense ».

La subversion de l’hétérosexualité est donc comprise dans la lutte des
classes. D’abord, parce que de par leur position dans le système
hiérarchique genré, les femmes et les minorités de genre sont aussi
exploitées économiquement et socialement, précarisées. Ensuite, parce
qu’une lutte des classes qui ne pense pas le féminisme reproduirait les
rapports de pouvoir genré, laisserait de vieux hommes blancs décider des
sujets qui importent pour mener la révolution, et de ceux qui sont jugés
secondaires.

Un exemple extrêmement intéressant à ce titre se trouve dans le
documentaire « *Les lip, l’imagination au pouvoir* », réalisé par
Christian Rouaud (2007). Il retrace la première expérience d’autogestion
d’une entreprise, où, lors de l’occupation de l’usine de montres, les
travailleurs reprirent leurs activités et la vente illégale pour sauver
leur emploi. Ce film est beau en ce qu’il est porteur d’espoir, de
spontanéité, de créativité dans les manières de faire vivre une lutte.
En revanche, je trouve que les témoignages des « femmes de », femmes
d’ouvriers, femmes de militants, femmes de syndicalistes, montre bien
comment les questions de famille et de gardes d’enfants ont été
renvoyées en annexe. Elles n’ont pas existé dans la lutte. En
invisibilisant ces enjeux, la lutte se prive de toute une partie de sa
force militante : les femmes qui elles aussi sont impactées par la
fermeture de l’usine.

Nous pouvons mettre cela en parallèle avec un autre documentaire sur
Lip, réalisé par Carole Roussopoulos en 1976. Dans « *Christiane et
Monique, Lip V* », nous retrouvons le témoignage de l’ouvrière Monique,
au sujet de la lutte en cours. Elle nous parle des rapports
hommes/femmes en utilisant un stratagème de dénonciation consistant à
remplacer le mot homme par le mot blanc, et le mot femme par le mot
arabe. Soudain, les inégalités présentes au sein de la lutte deviennent
criantes : pas de prise en compte dans les décisions des « problèmes
d’arabes », qui sont renvoyés à des soucis accessoires, éviction de tous
les arabes des réflexions stratégiques, car seuls les grands chefs
blancs ont le droit à la parole, renvoi des arabes à des tâches
subalternes, le ménage, le soin, la cuisine. Et quand un grand chef
blanc daigne s’occuper de ces occupations humbles, il faut lui offrir la
reconnaissance du temps passé, gâché, à trimer aux emplois pour lesquels
les arabes ne reçoivent en temps normal aucune rétribution. Pourtant, la
lutte des grands chefs blancs pour l’emploi souhaite bénéficier à tous,
y compris aux arabes : pour la simple raison que l’usine ne pourrait pas
tourner sans eux.

Le témoignage de Monique rejoint la proposition politique de Zappino :
il faut élargir la notion marxienne de « classe », car le rapport entre
exploitant et exploité dépasse le seul rapport de production travail. En
tant que minorité de genre, comme en tant que minorité de race, nous
pouvons faire partie de la classe « pauvre » parce que nous ne
respectons pas les modalités du système hétérosexuel.

La fin du capitalisme nécessite donc de penser nos relations en dehors
du prisme hétérosexuel, hiérarchique et inégalitaire. En ceci, le
féminisme est au cœur de la lutte des classes, puisqu’il vise à saper
une des oppressions sur laquelle le capitalisme se construit. Et
subvertir l’hétérosexualité, cela passe par la remise en cause du genre.

##### Subvertir l’hétérosexualité : il n’y a pas de féminisme sans queer

Pour Frederico Zappino, l’objet du communisme queer n’est pas de lutter
pour une reconnaissance des droits des minorités, mais pour la
subversion des processus de subjectivation (le genre), de relations et
de production sociale (l’hétérosexualité). Autrement dit, l’objectif
n’est pas de revendiquer le droit à exister dans la norme hétérosexuelle
(un gentil gay cis, une lesbienne féminine et pacsée), mais de
questionner la matrice qui forme ce système et exclut les personnes qui
n’adhèrent pas à ce modèle : la construction binaire du genre et
l’hétérosexualité obligatoire.

Cela rejoint la définition que Jane, femme trans et lesbienne me donne
du mot queer lors de notre entretien : « Pour moi le mot queer, c’est
dérangé, déviante, perverse, monstrueuse. Je pense qu’il faut maintenir
cette position du monstre, pourquoi s’en couper ? Pour la normalité ? Le
problème c’est que même si nous arrivions à nous conformer et à obtenir
des droits, cela ne va pas empêcher la mutilation des personnes
intersexes, les lois transphobes dans d’autres pays… Je pense que le
mot queer est un mot à tenir. Nous faisons partie de cette histoire des
monstres ».

Et pour lutter contre le système hétérosexuel, il est nécessaire de
converger en tant que minorités de genre et de sexe, entre personnes
assignées femmes, personnes trans, au sein de la communauté LGBTQI. Le
rêve semble vaste. Mais le féminisme ne peut se faire sans les un·es ou
les autres.

Le point de jonction entre les luttes des minorités de sexe et de genre
apparaît clairement dans la définition que Monique Wittig donne de
l’hétérosexualité, dans *La pensée straight* (1992) : c’est le « système
social qui se fonde sur l’oppression des femmes par les hommes, et qui
produit la doctrine de la différence entre les sexes afin de justifier
cette oppression ». La domination masculine est justifiée par la
construction binaire du genre, où les femmes se voient dotées de
caractéristiques spécifiques, de rôles et de valeurs, différentes et
complémentaires des hommes. Et cette même doctrine justifie l’exclusion
des personnes qui ne se rangent pas dans la binarité : les lesbiennes
qui refusent la complémentarité à l’homme, la position subalterne, les
trans qui nient l’herméticité de la frontière du genre.

Pour Zappino, dans *Communisme Queer*, les femmes sont en première ligne
du système hétérosexuel dans le sens où « la condition d’existence pour
la grande majorité des femmes ne cesse de dépendre de leur lien avec un
homme, c’est-à-dire qu’un homme jouit le plus souvent d’un accès
facilité à l’économie formelle ». Leurs vies s’organisent autour des
hommes qu’elles aiment et auxquels elles font attention par un travail
de soin et de reproduction (ménage, cuisine, relations sexuelles). Ces
tâches restent dites « féminines », précisément parce que les hommes ne
s’en occupent pas.

De la même manière, les minorités de genre subissent la violence d’un
système hiérarchique où ceux qui peuvent accéder à la reconnaissance
sociale doivent répondre au genre typé « masculin » ou « féminin ».
Zappino souligne que dans le sens inverse, les personnes qui ne se
conforment pas à la binarité du genre sont exposés à des risques, à
savoir : « le harcèlement, le viol, la pathologisation, la
criminalisation, le silence, la pauvreté, l’indigence, l’inclusion
conditionnelle, l’exclusion radicale, le suicide, la mort précoce ».

Lors de notre entretien, Jane montre d’autres endroits où les luttes
queers et féministes se rejoignent : la reproduction et le contrôle de
nos corps. Elle prend l’exemple de Reagan, le président des États-Unis
de 1981 à 1989. S’il critiquait l’homosexualité, c’est parce que les
gays ne se reproduisent pas. De même, face à l’injonction
à se reproduire, les corps assignés femme sont privatisés. Dans la
course à la vie, le corps féminin comme outil de production doit être
mis au travail, et sur elles pèsent la pression de la reproduction.

Un autre exemple donné par Jane, c’est celui de l’Androcur. C’est un
médicament prescrit aux femmes trans, pour bloquer la testostérone, qui
peut provoquer des thromboses et des pensées suicidaires. Or c’est aussi
un médicament qui est fréquemment prescrit aux femmes cis présentant un
problème hormonal lié à un haut taux de testostérone. Selon Jane, « cela
pose la question de notre autonomie corporelle, la possibilité de la
formation au soin pour nous, de notre indépendance vis-à-vis du corps
médical, qui est hégémoniquement hétérosexuel ».

Si la matrice de notre oppression est la même, nous devons lutter
ensemble. Pour Wittig, c’est le conflit qui va permettre de montrer le
caractère social de ces données dites « naturelles » et provoquer le
changement : « tant qu’il n’y a ni conflit ni lutte – il n’y a pas de
dialectique, il n’y a pas de changement, pas de mouvement ». C’est comme
cela que nous pouvons comprendre sa phrase célèbre « les lesbiennes ne
sont pas des femmes ». En effet, en refusant l’hétérosexualité
obligatoire, la relation de subordination à l’homme, elles
contreviennent à la binarité et à la définition de la femme vis-à-vis de
l’homme. Elles offrent un autre horizon possible, une définition pour
soi-même en dehors du modèle dominant.

Construire des perspectives hors du système hétérosexuel ne revient pas
à demander la parité ou la reconnaissance des droits des minorités.
Nous pouvons retrouver cette vision de l’émancipation de la communauté
LGBTQI dans le mariage pour tous par exemple. Aujourd’hui, vivre
normalement, cela veut dire vivre de manière hétérosexuelle. Adhérer à
l’institution du mariage, avec ce qu’elle a de patriarcal et ses
héritages douteux. Selon Zappino, la demande de reconnaissance de droits
ne facilite pas la coexistence de différents modes de vie. Au contraire,
elle renforce la normalisation de la hiérarchie sociale (de
l’hétérosexualité), puisque les minorités sont intégrées si et seulement
si elles se fondent dans la culture hégémonique. L’homosexuel marié est
accepté socialement. La trans aux multiples relations et refusant le
couple est marginalisée.

En fait, la reconnaissance des droits des minorités (à vivre comme des
hétérosexuels) normalise les inégalités et les amplifie. Pour Zappino,
cela nourrit la vision libérale de l’émancipation des minorités de genre
et de sexe, qui se fonde sur une approche profondément individuelle.
Pour réussir, en tant que femme ou que personne trans ou homosexuel.le,
il suffirait de le vouloir, de s’en donner les moyens. Cette vision est
sous-tendue par l’idée de libre-accès au marché, où les places au soleil
de notre société sont accordées selon le mérite de chacun·e, sans
discrimination. C’est le discours qui remet en cause les inégalités
contemporaines parce que « j’ai un ami gay qui travaille dans la
finance » ou car « ma femme est directrice de cabinet ». Les parcours
individuels des personnes ayant souvent choisi de se conformer au modèle
hétérosexuel en lissant leurs singularités ou en en performant ce qu’on
attendait d’eux, sont pris en preuves universelles d’un système qui
serait miraculeusement devenu égalitaire et juste.

Cette vision libérale est dangereuse parce qu’elle renvoie les pauvres,
ceux qui échouent, les marginaux, non pas à la conséquence systémique de
leur oppression, mais à leur responsabilité individuelle. Puisque nous
avons « le choix », nous choisirions nos existences à la marge.

Jane explique : « Je pense qu’il n’y a pas une séparation nette entre le
combat individuel et politique. Déjà parce que le privé est politique.
Et parce que lorsqu’on vit en tant que trans, lesbienne ou pédé, on vit
en dehors des cases. Individuellement c’est une souffrance, une
solitude, un certain écart à la société. Le passage de l’individuel au
politique se fait par la lutte, et nous sommes acculés à cela. Et c’est
très fatigant, en vrai on n’a pas le choix. Il y a eu à un moment donné
cette croyance que le libéralisme allait nous suffire, mais on voit très
bien avec ce qui se passe en ce moment aux États-Unis et en France
aussi, que ce système veut notre peau. Je pense à la pénurie de
testostérone injectable, de pilules abortives en France… Dans le
système libéral on pensait qu’on pouvait avorter, qu’on pouvait
transitionner avec la testostérone et maintenant c’est mis en crise ».

Face à l’illusion des parcours individuels, les lacunes structurelles
renvoient à la nécessité de lutter pour ses droits. Ce discours « du
choix » et de l’autodétermination ne tiennent pas debout sans une
pluralité de possibilités, aussi soutenables les unes que les autres. Le
jour où il sera aussi facile de trouver un travail (ou de le refuser) en
tant que personne cis et en tant que personne trans, le terme
d’autodétermination pour désigner nos choix individuels trouvera son
sens. Le jour où une femme pourra choisir de ne pas avoir d’enfants et de
refuser le couple sans subir de pression sociale, et même en étant aussi
valorisée socialement qu’une femme menant de front emploi et vie de
famille, nous pourrons parler d’une pluralité d’options soutenables.

Autrement, l’autodétermination est une normalisation des inégalités
existantes, où, en tant que femme, ou en tant que personne trans, ou en
tant qu’homosexuel·le, nous aurons toujours plus d’efforts à fournir
pour arriver à nos fins qu’un mec blanc. C’est le discours du
développement personnel, de l’empowerment vicieux, rabâchant que « tu
peux le faire, si tu voulais vraiment vivre une vie épanouie, ton
orientation sexuelle ne devrait pas t’empêcher de devenir X ».

Je crois que dans le film *Nomadland*, de Chloé Zhao (2021), nous
trouvons une vision douce-amère de l’autodétermination. Il raconte la
vie de Fern, partie sur la route à la suite de la crise économique de
2008 et de la disparition de son mari. Sur ce point, le film nous montre
que loin du discours du « choix », une femme seule qui ne rentre plus
dans le cadre hétéronormé de la famille classique tombe beaucoup plus
rapidement dans la pauvreté. Fern n’a plus d’emploi, plus de mari pour
la soutenir, pas de famille sur qui compter, elle est contrainte de
vendre ses biens.

Par contre, la romantisation de l’autodétermination pointe le bout de
son nez autour du thème du nomadisme. Plutôt que de devenir SDF, Fern
choisit d’être nomade. Autrement dit, elle choisit de dormir sur des
parkings, faire ses besoins dans les toilettes publiques, négocier des
douches chaudes ou des places sur des stations essence d’où on essaie de
la chasser. Personnellement, je trouve qu’il y a plus romantique, et je
ne considère pas qu’il y ait égalité des termes entre les deux options
(celle d’être SDF ou nomade), donc je ne crois pas à cette proposition
de l’autodétermination. Ce discours du choix est pourtant symbolisé par
ces deux lignes de dialogue avec son ancienne élève, croisée au
supermarché :

« My mom says that you’re homeless, is that true? [Ma mère dit que tu es sans-abri, c’est vrai ?]

No, I’m not homeless. I’m just house-less. Not the same thing, right ? [Non, je ne suis pas sans-abri, je suis sans-maison. Rien à voir, n’est-ce pas ?]»

Là où je trouve une forme de choix dans ce film, c’est dans la fuite du
modèle familial classique. Fern refuse de rester chez sa sœur, quand
bien même elle vient de lui demander l’argent. Elle part de chez David,
l’homme qui vient de devenir grand-père et qui l’invite à vivre avec
lui. Et là, sans doute, pouvons-nous parler d’un vrai choix, entre la
vie de nomade et la vie sédentarisée et confortable. Mais encore une
fois : ce n’est pas de l’autodétermination, puisque l’un des choix est
beaucoup plus lourd de conséquences que l’autre. D’un côté, la pauvreté
et une forme de liberté, de l’autre le confort et les normes.

En réalité, loin d’être un phénomène naturel ou le fruit d’une
responsabilité individuelle, la pauvreté des minorités de genre est
systémique. Dans *Communisme queer,* Zappino étudie l’ampleur de ce
phénomène en Italie. Même s’il se base sur les études et les chiffres
italiens, j’ose penser que nous pouvons extrapoler au cas français.
Zappino note donc que l’absence de revenus pour les personnes trans est
fréquente, liée à la difficulté à accéder au marché du travail. De même,
il n’y a quasiment pas de personnes trans propriétaires. Cela se double,
au niveau locatif, d’une difficulté d’accéder au logement du fait de la
réticence des propriétaires à louer leur appartement à des personnes
trans, souvent associées à l’image de la prostitution. Le genre définit
donc qui est « digne d’avoir un abri, de l’assistance, de la
crédibilité, du support et du revenu ».

D’un point de vue relationnel, Jane me parle de la solitude structurelle
des femmes trans : « La lesbophobie structurelle rend compliquée pour
une meuf d’aimer une meuf : il y a peu de lieux de rencontre, etc. Et
quand tu es une meuf trans lesbienne, c’est augmenté. Il y a les
lesbiennes qui veulent de toi, et il y a les autres. Et parmi celles qui
veulent bien de toi, il y a celles qui veulent de toi en tant qu’amie,
mais la question du désir est totalement évacuée ».

Nos oppressions singulières, en tant que minorités de sexe et de genre,
sont donc liées dans la négation des modes de vie des personnes qui ont
été jugées secondaires dans un rapport hiérarchique. Nos oppressions
peuvent se retrouver dans la lutte anticapitaliste parce que ce système
vient renforcer des rapports de force préexistants et joue des formes de
vulnérabilité pour s’affirmer. D’où la nécessité de former des alliances
entre les différentes formes d’oppression dans leurs luttes. Comme
l’explique Zappino, savoir mener des combats spécifiques contre la cause
de son oppression est une manière de se rejoindre dans le cortège de la
lutte anticapitaliste.

Jane me parle aussi de l’importance de la création de solidarités dans
le milieu queer, que j’ai envie d’extrapoler pour y inviter les
féministes : « Une communauté se pose à partir de principes (ce qu’on
fait, ce qu’on ne fait pas). Quand on est queer, on n’est pas exempts de
se poser la question de nos normes, qui permettent aussi de délimiter
des contours de ce que nous acceptons ou pas. Quand on sort d’un système
oppressif, on a un peu l’impression de porter notre vérité : c’est quoi
être queer, c’est quoi être non-binaire, c’est quoi être gouine etc. Et
donc si quelqu’un n’est pas d’accord, on ne va pas être solidaire. Et
cela se fait aux dépens des personnes précaires dans le milieu. Mais
pour moi, la solidarité, ce n’est pas un réseau de potes. La solidarité,
c’est un réseau politique qu’on tisse avec des personnes avec qui on
n’est pas d’accord. C’est un projet politique pour peser face à
l’hégémonie ».

Et je crois que cette invitation à créer des liens de solidarité entre
opprimé·es, d’accord ou non, est puissante. Dans une perspective
marxiste et féministe, penser le lien entre combats queers et
féministes, c’est aussi penser la question du travail reproductif, comme
un autre rapport d’exploitation entre groupes sociaux. Autrement dit,
comment le « prendre soin », exercé majoritairement par des minorités
(de sexe, de classe ou de race) bénéficie aux dominants ? Qu’est-ce que
le soin nous dit de l’intersectionnalité ? Comment nous inclure, sinon
nous dédier le soin ?

#### Ce que le soin nous dit de l’intersectionnalité

##### Le care en question : conflits et rapports de domination

Selon Federici, le monde du soin est un « terrain essentiel de
transformation et de lutte » dans une perspective féministe. Il ne
s’agit pas de le dévaloriser, mais de rendre compte que ce travail est
opprimant aujourd’hui. D’une part, parce que nous n’en contrôlons pas
les conditions matérielles d’exercice, souvent indécentes. De ce fait,
le soin précarise les soignants. D’autre part, selon Joan Tronto dans
*Un monde vulnérable. Pour une éthique du care* (2009), le soin est
ramené à une dimension individuelle et romantisée en tant que
*disposition*, à la sphère privative et aux moyens de chacun. En
reconnaissant sa centralité et sa fonction support de nos modes
d’organisation, la revalorisation matérielle de ces activités de soin,
leur réappropriation par tous·tes permettrait de construire une société
fondée sur l’amour en commun.

Pour Joan Tronto, « les conceptions actuelles du care \[…\]
contribuent à perpétuer les structures de pouvoir et de privilège, de
genre, de classe et de race au travers de la construction de
l’altérité ». Autrement dit, le fait que le domaine du soin soit
dévalorisé, socialement et matériellement, invisibilise les rapports de
force qui s’y jouent. En marginalisant le soin, les privilèges sont
maintenus, puisque le travail de reproduction nécessaire à la réussite
des un·es est délégué aux personnes minorisées. Selon l’autrice, cet
arrangement est rendu possible par le mythe individualiste de
l’autonomie, là où l’interdépendance des êtres nous inviterait à
reconnaître la centralité du care dans nos sociétés.

Nous pourrions reprocher au terme care (que nous traduirions par soin en
français), d’être un concept-valise, trop flou pour recouvrir des
aspects concrets de notre vie. Je crois que c’est à la fois l’angle mort
et la puissance de ce mot. Joan Tronto réussit à le manier comme un
outil théorique, avec l’objectif d’élargir nos conceptions du champ de
la vie politique, mais aussi en tant qu’activité pour étudier les
inégalités matérielles liées à l’exercice du soin. Les écrits de Joan
Tronto constituent selon moi une bonne base pour comprendre ce qui se
joue dans le mot-valise « soin ».

L’autrice part de la définition élaborée avec sa collègue Berenice
Fischer. Le care serait « une activité générique qui comprend tout ce
que nous faisons pour maintenir, perpétuer et réparer notre monde, de
sorte que nous puissions y vivre aussi bien que possible ». Si les deux
premiers verbes renvoient à une vision assez conservatrice du soin, il
s’agit en revanche de dépasser l’horizon unique de la réparation, quand
les dégâts sont déjà causés. C’est un processus, ce sont des actions sur nos
corps, l’environnement, nos structures, en soutien à la vie.

Dans cette définition assez large, nous pouvons estimer que le care
recouvre autant les opérations de nettoyage, d’aide à domicile que du travail social, de la médecine, etc. Pour reprendre un autre
exemple, développé plus tôt dans le livre, le travail du sexe, en ce
qu’il répond à des besoins affectifs et sexuels, peut faire partie du
champ du care.

Joan Tronto explique en effet que le care est le fait de tendre vers
autre chose que soi, d’agir et d’endosser une charge pour cela. Ce n’est
pas une interaction individuelle, cela comprend une dimension
collective. Selon elle, le soin suit plusieurs séquences, parfois
conflictuelles : reconnaître des besoins, prendre en charge (mettre en
place des dispositifs pour y répondre), exercer le soin, le recevoir.

Et ce sont dans ces séquences que s’insèrent les rapports de pouvoir.
Qui définit les besoins des patients ? Qui applique le soin au sein d’un
dispositif pensé par d’autres ? Qui alloue les ressources dédiées aux
dispositifs ? Qui évalue l’adéquation entre le soin effectué et les
besoins des personnes ? Quelle place pour le patient dans l’orientation
de son parcours de soin ?

Interroger les séquences constitutives du care nous permet aussi de
constater que le soin est affecté par les champs de classe, de genre et de
race. Il existe une différence entre le statut social de celleux qui
pensent les dispositifs de soin, prennent en charge, et celleux qui
effectuent les actes. À l’appui des statistiques états-uniennes en la
matière, Joan Tronto montre que les séquences effectives du prendre soin
sont réalisées tendanciellement par des personnes racisées ou des femmes
au statut d’ouvrières. Au contraire, le prendre en charge reviendrait
souvent aux hommes, généralement blancs, au statut de cadres.

En France, il est difficile d’appuyer cette thèse à travers des chiffres
car les données raciales sont proscrites des enquêtes statistiques
depuis 2007. Cependant, nous pouvons nous appuyer sur l’article de
Marguerite Cogné, publié en 2010 dans l’Homme et la Société, *Genre et
ethnicité dans la division du travail en santé : la responsabilité
politique des États* (URL :
https://www.cairn.info/revue-l-homme-**et-la-societe-2010-2-page-101.htm#no29).
Cet article étudie l’impact de la rationalisation des coûts dans le
service public sur la division du travail dans les métiers du soin.
Cette analyse met en exergue une articulation entre ethnicité, classe et
genre, expliquant l’attribution et la stratification des emplois dans le
soin, en lien avec les politiques publiques menées dans les pays étudiés
(la France et le Canada). Il montre ainsi que « en 2007, parmi
l’ensemble des immigrées actives, six sur dix exerçaient comme employées
de services – notamment dans les soins et services aux particuliers où
elles comptent pour près d’un quart, alors que c’est seulement le cas
d’une française sur dix. ».

La pression sur les coûts dans le domaine de la santé entraîne
l’externalisation (et donc, la privatisation) d’une partie des soins qui
étaient auparavant prodigués par le public. Le développement du service
à la personne crée un besoin de main-d’œuvre en France, pallié par le
recours à l’immigration « choisie » pour les métiers en tension du soin.
La France accorde des titres de séjour temporaires aux pays partenaires
dont les ressortissants viendraient combler le manque d’acteurs du
médico-social.

Dans le même temps, les moindres ressources accordées aux hôpitaux
entraînent une précarisation des soignant·es, de leur statut et un plus
grand recours à l’intérim, employant des personnes migrantes. La
dévalorisation du statut de soignant entraîne son attribution à des
catégories de population altérisées. L’article confirme donc la
pertinence de l’analyse des champs classe, genre, race au sujet des
métiers du soin en France, montrant que l’attribution des places
sociales les moins rétributrices se lie aussi aux préconçus de la
société française du rôle féminin dédié au care et de l’histoire
coloniale de délégation des tâches jugées avilissantes.

Cette idée est corroborée par *les chiffres de l’INSEE* (URL :
https://www.insee.fr/fr/statistiques/6209656)**, confirmant la place des
femmes cette fois-ci, dans les métiers jugés indispensables à la société
française. Cette étude de 2022 montre que les métiers clés, du soin
notamment, sont souvent moins qualifiés. Les postes de contact dans des
structures de soin comme ceux d’infirmières, d’aide-soignante ou
d’agentes hospitalières sont occupés à 85% par des femmes en
Bourgogne-Franche-Comté. Selon cette étude, « les travailleuses clés
salariées sont payées 8 % de moins que leurs homologues masculins ». À
la conception de tâches dites féminines, s’ajoute la dévalorisation
salariale et statutaire. Il est donc urgent de revaloriser le soin en
tant que travail à part entière, au-delà de sa conception comme une
disposition et une tâche dégradante.

De la même manière que son exercice, l’accès au soin est différencié.
Nous rebouclons avec l’analyse de Zappino portant sur le traitement des
minorités de genre et de sexe. Il dénonce la pathologisation des
personnes trans souhaitant opérer une transition, tant dans l’accès aux
traitements, faisant l’objet d’une validation obligatoire par un
psychiatre, que dans la difficulté de remboursement par la sécurité
sociale des traitements.

C’est ce que Jane explique lors de notre entretien : « Moi je suis
butch, je porte l’idée d’une féminité masculine. Quand j’ai dû passer
par l’accord tripartite pour mon opération, entre le psychiatre,
l’endocrinologue et le chirurgien, il fallait présenter ce que les
personnes s’imaginent qu’est une femme : c’est de la stratégie, de la
ruse. C’est chiant parce que c’est une certaine charge mentale de faire
ça pendant une journée. Parce que ça nous expose aussi potentiellement à
des agressions dans la rue, ou des regards de travers. Moi il y a cette
idée maintenant que j’ai ce que j’ai voulu, que je n’ai plus à dealer
avec ces injonctions. Je porte très fort cette idée de vivre un max en
tant que lesbienne, c’est-à-dire de mettre en crise ces préconçus de ce
que c’est une femme etc. ». Pour elle, le fait que le corps médical soit
hégémoniquement masculin explique aussi la manière dont il nous traite :
mutilation des personnes intersexes, gestion des problèmes hormonaux via
l’Androcur, processus de validation avant la ligature des trompes…

Nous retrouvons ici la question centrale du care : qui définit les
besoins des patients, qui décide des dispositifs pertinents pour les
parcours de soin ?

Selon Zappino, la difficulté d’accès au soin des personnes trans pose la
question du traitement différentiel exercé par l’Etat, dissimulé sous
l’illusion du choix individuel et de la valorisation de la diversité. La
contestation de la norme genrée n’est pas une alternative symétrique à
l’adhésion au système binaire, cela a un coût et ne permet pas l’accès à
la même reconnaissance. Pour le citer, nous vivons dans « un régime qui
alloue inégalement les pouvoirs et les oppressions entre les
« *bonnes* » et les « *mauvaises* » modalités de subjectivation et de
relation, entre les « *bons* » et les « *mauvais corps* ». Au
contraire, une vision politique de la transidentité parle d’ « une
révolte exercée par le corps tout entier, un acte subversif vis-à-vis de
la norme qui structure ce monde social ».

Selon Joan Tronto, les personnes privilégiées bénéficient de la possibilité 
de ne pas nommer leur besoin, sans que ceux-ci soient oubliés. 
C’est la caractéristique du traitement différentiel, les classes
dominantes n’ont pas à énoncer leur demande de soin, ce serait se rendre
vulnérable. Ces groupes délèguent le care pour poursuivre d’autres fins,
leur vulnérabilité est cachée par l’injonction à la productivité, ce qui
en retour dévalorise les soignants et les tâches prodiguées.

L’autrice s’arrête sur la figure du « self-made-man » : la
déresponsabilisation des charges reproductives, accompagnée d’une forte
croyance en l’individualisme, invisibilise toutes les personnes qui ont
travaillé pour lui, le temps et les tâches dont il s’est déchargé pour
construire cette réussite. Revaloriser le soin ruinerait la répartition
inégale des ressources et des privilèges dont les self-made-men
jouissent.

Plus globalement, l’autrice montre que la vision individualiste de la
société contribue au dénigrement du care aujourd’hui. Elle pointe qu’en
le qualifiant de disposition plutôt que de compétence, il est ramené au
plan individuel et romantisé, ce qui empêche de le considérer comme un
travail. Le care est de plus en plus régulièrement ramené à la sphère
privée, qu’elle évoque le rôle de la famille, d’associations ou de
structures lucratives, amenées à prendre le relai des pouvoirs publics
suite à la restriction des moyens alloués au champs de la santé. Cela a
aussi pour conséquence de définir le fait d’avoir besoin comme le
contraire de l’autonomie, ce qui peut se manifester sous la forme de
dédain envers les destinataires de soins.

Ces réflexions sur le soin nous invitent à penser l’intersectionnalité,
avec Sarah Mazouz et Eleonore Lépinard, dans l’ouvrage *Pour
l’intersectionnalité*, paru en 2021 aux éditions Anamosa. Ce concept de
sciences sociales vise à montrer comment les rapports de classe, de
genre et de sexe s’imbriquent les uns aux autres. Il comprend aussi
d’autres champs comme celui du handicap, de l’âge, du statut
administratif… Plutôt que de présumer une accumulation des
désavantages, l’intersectionnalité étudie comment le croisement de
plusieurs oppressions agit et se module selon les situations. Étant le
fruit de rapports sociaux et historiques, les discriminations et la
marginalisation sont variables. Dans notre exemple du soin, ce n’est pas
la même chose d’être une sage-femme blanche, une aide à domicile noire,
ou un agent de nettoyage immigré des pays de l’Est.

Et au-delà de ce constat, l’intersectionnalité invite à nouer des
alliances entre groupes minorisés, à partir des spécificités de leurs
oppressions. Ainsi, le soin est une question profondément
intersectionnelle. Le soin affecte de nombreuses minorités, de manière
particulière. Par contre, nous pouvons envisager une manière de relier
différentes oppressions pour repenser le domaine du soin. Comment, donc,
reconstruire le care depuis nos perspectives minoritaires ?

##### Construire nos soins : pour une approche communautaire

Pour imaginer de nouvelles manières de prendre soin de nous, nous
pouvons partir des constats évoqués par Joan Tronto dès 2009. Penser le
care autrement, c’est d’abord nommer les relations de pouvoir qui s’y
jouent : reconnaître qui s’occupe de qui et comment, en lien avec les
catégories de race, genre, classe, pour discuter d’une répartition juste
des avantages et des soins.

Selon l’autrice, c’est aussi rompre avec le mythe individualiste qui
oppose l’autonomie à la dépendance. Dans une vision intégrée du care,
l’interdépendance des individus invite à penser les besoins comme des
préoccupations sociales, et ce faisant à repenser la séparation
public/privé qui existe dans le travail reproductif. Elle prend
l’exemple de la garde d’enfant, systématiquement renvoyée au domaine
familial. Pourtant, cette tâche engage une responsabilité sociale, et
devrait être considérée comme un travail plutôt qu’une disposition ou un
devoir familial.

Plus globalement, Joan Tronto montre que repenser le soin nous offre de
questionner notre rapport au travail. L’éthique du travail invite les
individus à se responsabiliser pour répondre à leur besoin. Cela
invisibilise le travail du care dès qu’il est non-rémunéré. Or les
travailleur·euses du care ne sont pas producteur·ices de valeur, au sens
matériel du terme. Iels ne sont donc pas considéré⋅es comme utiles
socialement. Politiquement, une société basée sur le care traiterait les
problèmes sociaux en se basant sur l’interdépendance des groupes, en
demandant le care, en nommant ses besoins et en le proposant de
différentes façons, plutôt qu’en considérant les acteurs comme
autonomes, égaux et rationnels.

Selon l’autrice, la puissance du care aujourd’hui réside dans la
réappropriation des savoirs de terrain pour en faire une puissance.
Contre la domination des soignants sur les soigné·es, c’est se
débarrasser de la morale de la charité pour inviter les personnes
soignées à participer à leur parcours de soin et aux choix qui les
impactent.

Bien que Joan Tronto ne s’en réclame pas, voire s’en protège, ces
réflexions me font fort penser à l’approche du soin communautaire. Nous
la retrouvons notamment dans les centres de santé communautaires, visant
à impliquer les patient·es dans les instances de décision et leur prise
en soin. Il s’agit pour elleux de prendre en compte le contexte
environnemental, social, psychique et physique dans le parcours.

L’un des partis-pris est aussi de mutualiser les moyens, les lieux et
les ressources pour des activités médico-sociales qui n’entraîneraient
pas la même rémunération pour les intervenants, dans le monde du soin
classique. Ces lieux se basent sur le partage des tâches, des
compétences, des rôles et des fonctions émancipatrices. Par exemple, je
peux, en tant que médecin·e généraliste, prendre les rendez-vous pour
l’assistant·e sociale du centre, faire le ménage, ces tâches tournent et
ne sont pas affectées à un·e salarié·e en charge des fonctions supports.

La notion de soin communautaire rejoint un autre enjeu qui me tient à
cœur, la Réduction Des Risques et des dommages en milieu festif
(ci-après, RDR). Je fais partie du collectif STARS, un groupe d’ami·es
informel, formé pour intervenir en festival sur l’information, la
sensibilisation et la fourniture d’outils pour consommer (de la drogue,
de l’alcool ou du sexe) de manière safe.

Si nous faisons un petit historique, la RDR prend de l’ampleur dans les
années 80, avec l’épidémie de SIDA, réunissant des usager·es de drogues
et des acteur·ices du médico-social pour demander l’accès à des
seringues stériles et éviter la transmission du VIH. La RDR va
progressivement s’institutionnaliser en tant que politique de santé
publique, et recouvrir d’autres champs, comme l’usage de drogues,
d’alcool, la santé sexuelle, afin de proposer de l’information et du
matériel réduisant les conduites à risques.

L’idée principale de la RDR est de mettre l’usager au centre, en prenant
en compte le contexte dans lequel il consomme, ses besoins. Elle
implique une posture de non-jugement et d’humilité face à des
consommateur·ices qui en savent souvent beaucoup plus que nous sur les
produits concernés. Dans son approche militante, elle lutte contre la
répression et la criminalisation des drogues, qui affecte
majoritairement les personnes précarisées. Une société sans drogues
n’existant pas, la RDR lutte contre la prohibition et la culpabilisation
des usagèr·es, qui entraîne de plus grands risques pour elleux. Elle
vise à favoriser l’entraide entre pairs, à collectiviser les savoirs et
le partage d’expériences pour limiter la prise de risque. L’idée n’est
pas de prendre soin des toxicomanes, mais de fournir à des personnes le
cadre pour prendre soin d’elleux (sans viser forcément l’arrêt de la
consommation).

Concrètement, pour nous, il s’agit d’intervenir dans de petits
festivals, où la réduction des risques n’est pas une pratique connue ou
habituelle. En intégrant l’organisation bénévole, nous visons à prendre
part à l’équipe et à proposer une réflexion globale sur ces sujets :
pourquoi, même dans des milieux jugés à faibles risques, est-il
important d’avoir de la documentation, du matériel, une équipe pour
discuter avec les usager·es ? Qui est invisibilisé·e, voire évincé·e de
nos événements en l’absence d’une approche de RDR ? Quelle est la
position du festival sur la prise de substances illicites, quels sont
les préjugés ou l’image qu’iels se font des usager·es ? Quel lien peut
être fait entre la santé sexuelle et la prévention des violences et du
harcèlement sexuel et sexiste ?

Fournir du matériel gratuit, en accès libre et de manière anonyme, de la
documentation informant sur les risques liés aux consommations, un
espace d’échange, d’écoute et de lutte contre nos oppressions,
constitue pour nous le cœur de la RDR. En cela, je pense que cette
approche peut fournir des valeurs et des pratiques inspirantes pour
construire différemment nos rapports aux usager·es de soins.

Des auteur·ices comme Cy Lecerf Maulpoix, dans *Ecologies déviantes*
(2021), proposent de s’intéresser également aux superstructures du
soin. Selon lui, « la production et la commercialisation de médicaments
répondent à des logiques capitalistes vis-à-vis desquelles nous ne
disposons que de très peu de marges de manœuvre, si ce n’est en nous
battant sur le prix et l’accès aux médicaments ». Comme dans de nombreux
secteurs (l’exemple du textile est connu et frappant), la sous-traitance
délocalisée permet aux entreprises de baisser leurs coûts de production
et d’éviter les législations qui empêcheraient la distribution de
produits causant la destruction du vivant. Nous dépendons de médicaments
dont les matières et la conception nous échappent. Se rendre acteur·ices
de nos soins pose la question de la relocalisation de ces
infrastructures, et de nos capacités à nous soigner. Mais aussi des
maladies causées par le mode de vie qui nous est imposé.

Mais l’auteur pointe qu’une politique de relocalisation doit répondre
aux questions suivantes : quelles nouvelles fractures et quelles
nouvelles exclusions, quels nouveaux rapports de force et d’exploitation
cela créerait ? Ici, le prisme de l’intersectionnalité nous invite à
penser la répartition des tâches et dégâts environnementaux causés par
nos pratiques de santé. C’est dans cette perspective que Cy Lecerf
Maulpoix invite à penser un « continuum entre les pratiques de soin de
l’environnement et de notre propre corps ».

### En conclusion

Pour conclure cette partie, nous aimerions citer Silvia Federici, dans
son ouvrage *Le capitalisme patriarcal* : « Comment définir le travail
socialement nécessaire si le premier secteur d’activité, et le plus
indispensable, n’en est pas reconnu comme une part essentielle ? Et
quels critères, et quels principes, devront gouverner l’organisation du
travail de soin, du travail sexuel et de la procréation si ces activités
ne sont pas considérées comme relevant du travail socialement nécessaire
? ».

Alors comment penser les soins essentiels, d’attention, de relation, de
prise en soin des personnes diminuées, s’ils sont romantisés comme des
dispositions naturelles ? Comment se répartir les tâches qui ne sont pas
valorisées socialement ? Peut-être faut-il reconnaître que le soin est
un travail. Cela nous montre, une énième fois, la nécessité d’inclure le
rapport de force hétérosexuel au sein de la lutte des classes, parce
qu’il fait partie de nos conditions matérielles d’existence.

Le soin est un besoin essentiel. Il est indispensable d’élargir sa
reconnaissance comme un travail. Ce serait une victoire, mais sans
changer les infrastructures de production de soin, elle serait de courte
durée. Ce besoin, comme tous les besoins modernes, nécessite des corps
mais aussi des machines, beaucoup de machine, des matières premières,
beaucoup de matière premières et encore plus d’énergie. Déplier la
moindre de nos activités revient à se perdre dans l’immensité des
chaînes de production et de transports.

Il nous faut donc voir nos besoins comme une préoccupation
sociale : les remplir est un enjeu collectif sur lequel
nous devons acquérir de l’autonomie vis-à-vis des infrastructures
capitalistes. Cela nécessite de penser ce qui nous est nécessaire pour
vivre, à la lumière de nos attachements et des conséquences collectives
de nos besoins. Il nous faut le faire sans oublier personne en chemin.


Bibliographie

**Livres** :

FEDERICI, Silvia et DOBENESQUE, Étienne, 2019. *Le capitalisme patriarcal*. Paris : la Fabrique éditions. ISBN 978-2-35872-178-3. 330.082

LECERF MAULPOIX, Cy, 2021. *Écologies déviantes: voyage en terres queers*. Paris, France : Cambourakis. Sorcières. ISBN 978-2-36624-599-8. HQ76.5 .L43 2021

LÉPINARD, Éléonore et MAZOUZ, Sarah, 2021. *Pour l’intersectionnalité*. Paris : Anamosa. ISBN 978-2-38191-026-0. 305

TRONTO, Joan C., MAURY, Hervé et MOZÈRE, Liane, 2009. *Un monde vulnérable: pour une politique du care*. Paris : Éd. la Découverte. Textes à l’appui. ISBN 978-2-7071-5711-9. 172

WITTIG, Monique, 2019. *Les guérillères*. Paris : les Éditions de Minuit. Double, 118. ISBN 978-2-7073-4570-7. 803

WITTIG, Monique et BOURCIER, Sam, 2018. *La pensée straight*. Nouvelle éd. Paris : Éditions Amsterdam. ISBN 978-2-35480-175-5. 305.420 1

ZAPPINO, Federico, CARISTIA, Stefania et DESCOTTES, Romain, 2022. *Communisme queer: pour une subversion de l’hétérosexualité*. Paris : Éditions Syllepse. ISBN 979-10-399-0039-3. 306.76

**Films** :

Rouaud, Christian, réal. *Les Lip, l'imagination au pouvoir*. France : ISKRA, 2007.

Roussopoulos, Carole, réal. *Christiane et Monique Lip V*. France : Les Films de la Fronde, 1976.

Wachowski, Lana et Lilly, *Matrix*. États-Unis : Warner Bros. Pictures, 1999. 

Zhao, Chloé, *Nomadland*. États-Unis : Searchlight Pictures, 2020. 

**Web** :

COGNET, Marguerite, 2010. Genre et ethnicité dans la division du travail en santé : la responsabilité politique des États. *L’Homme & la Société*. 2010. Vol. 176‑177, n° 2‑3, pp. 101‑129. DOI 10.3917/lhs.176.0101. 

BRION, David, DESNOYERS, Caroline et MAISONNEUVE, Jean-Noël, 2022. Une majorité de femmes dans les métiers indispensables à la population - *Insee Flash Bourgogne-Franche-Comté* - 149. [en ligne]. 2022. Disponible à l’adresse : https://www.insee.fr/fr/statistiques/6209656

## Attachements et besoins

(éçè)

Pourquoi repenser nos besoins, leurs conséquences et encore plus les
productions ? Nous sommes persuadé·es qu’à un moment ou un autre, une ou
plusieurs crises systémiques vont mettre à mal les approvisionnements et par
là même notre organisation sociale qui leur est intimement liée.

Dans une de ses conférences, Arthur Keller (consultant sur les risques
systémiques et sur la résilience) propose un exemple de crise systémique
causée par une canicule record. Elle affecte l’ensemble des secteurs
économiques, sociaux et vivants. Voici un résumé de son scénario (avec
quelques modifications).

Nous sommes à l’été 2028, une canicule record affecte toute l’Europe de
l’Ouest avec plus de 40°C tous les jours dans plusieurs régions
françaises. Cette forte température créée des pics de pollution
notamment à l’ozone. Cela fait 3 semaines que même la nuit, la
température n’est pas descendue sous les 30°C en agglomération. D’après
les prévisions, la canicule devrait durer encore au moins 2 semaines.

Il n’y a plus un ventilateur ou un climatiseur disponible. Tous ceux
installés tournent à plein régime, rendant les villes encore plus
chaudes. Certains commencent à tomber en panne. Les parcs sont pleins à
craquer, certain·es y dorment la nuit pour profiter de leur relative
fraicheur, même si l’herbe desséchée a disparu à force d’être écrasée. Des
départs de feux dans des forêts causent des évacuations. Sans
climatisation intérieure la chaleur est suffocante : après 10h il est
pénible d’être dehors, des alertes demandent à la population de ne pas
sortir entre 12h et 15h.

Durant les heures les plus chaudes, la couche superficielle des routes
fond. Les premiers jours, les voitures laissent des traces de goudron
mélangé à du pneu quand elles circulent. Rapidement, certaines routes
sont interdites durant une bonne partie de la journée. De nouvelles
alertes préviennent que se déplacer à pied ou à vélos en dehors de zones
ombragées est dangereux. Après les routes, ce sont les trains qui sont mis
à l’arrêt : les rails se dilatent trop. Aller travailler, faire les
courses ou livrer devient compliqué.

Cette canicule est celle de trop, le prix des produits alimentaires
s’envole. Les difficultés d’approvisionnement mêlées aux pertes
agricoles sont le signal qui permet aux marchés d’être dans la certitude
qu’ils peuvent parier sur l’augmentation des prix. Quand certains rayons
restent vides plus d’une journée, les prix s’envolent partout. L’État
intervient en essayant d’importer des produits de pays moins durement
touchés, mais eux restreignent leurs exportations pour préserver leur
population. Il devient difficile de s’approvisionner, en particulier
pour les moins riches et les moins mobiles. Il faut plusieurs heures par
jour pour acheter de quoi manger, les restrictions de vente causent des
échauffourées dans les magasins.

Les associations de solidarité font ce qu’elles peuvent, mais leurs
moyens sont insuffisants. Ni elles ni l’État ne sont préparés à cela. Le
niveau de pression sur les services publics combiné aux nombreux arrêts
de travail à cause des malaises met à mal les services d’urgences. Le privé
est également touché, les employé·es étant plus occupé·es à chercher des
magasins approvisionnés qu’à travailler. Des jardins et des champs sont
pillés. Pour la première fois depuis longtemps, la France connaît des
émeutes de la faim. Face aux tensions, l’armée intervient violemment
dans les quartiers périurbains…

Une telle crise est rendue possible d’une part par le changement
climatique, causé par les consommations humaines, par la complexité des
chaînes de production, de transport, de distribution ; par l’absence de
réflexion contrainte sur quoi faire si la machine s’enraye, et bien sûr
par l’absence d’action pour minimiser les crises systémiques à venir.

Pour les prévenir, il nous faut questionner nos besoins : que
devons-nous maintenir, que devons-nous diminuer, que devons-nous arrêter
pour vivre en phase avec les contraintes matérielles du système Terre ?
En cas de choc systémique, ce qui fera la différence sera notre
préparation et les alliances en place pour assurer une réponse
collective et en sortir par le haut. Sinon, comme dans le scénario de
Keller, ce seront les plus fragiles qui une fois encore subiront le plus
de désagréments. Nous ne pouvons pas faire l’impasse sur une réflexion
au sujet de nos modes de consommation.

Et même dans le cas où aucune crise grave n’arrive dans l’immédiat,
poser la question de nos besoins revient à repenser la place et le sens
du travail dans nos vies. Une certaine forme de travail reste
indispensable (j’entends produire des choses, accomplir des tâches : 
donner du soin par exemple). Ceci car la diminution de
l’énergie disponible ne laisse pas imaginer qu’un jour des robots
accompliront toutes les tâches que nous aurions la flemme de faire. Nous
allons devoir continuer à travailler jusqu’à la fin des temps, alors, en
nous questionnant sur nos besoins, essayons que ce soit moins, mieux et
utile.

Il nous faut à un moment nous poser la question de ce qui doit être
produit et comment. Des expert·es pourront s’affronter jusqu’à la fin
des temps sur les ressources et l’énergie nécessaire pour faire tourner
notre société dans le futur. Puis sur les manières les plus pertinentes
de les produire, selon une multitude de critères, et ainsi de suite.
Mais sans politique, nous ne pouvons pas sortir de cette boucle. Boucle
par ailleurs très intéressante : je viens des sciences physiques et je
trouve tout cela passionnant. Sauf que c’est un sujet d’étude aux
conséquences très concrètes. C’est génial de comparer les balances
coûts/bénéfices des modes de production décarbonés. Mais cela ne règle
rien. RTE (Réseau du Transport d’Électricité) peut faire des dizaines de scénarios sur l’énergie nécessaire
pour chaque usage, mais sans se poser la question de la pertinence de
ces usages, à quoi bon ?

J’avais prévu d’écrire sur le travail et les liens au travail, comment
l’organiser pour qu’il tourne dans le bon sens. En fin de compte, je
n’aurais sûrement fait qu’adapter les idées de Bernard Friot sur le
*salaire à vie,* ou parler du droit à la paresse. Organiser le travail
est nécessaire, mais son organisation n’est pas suffisante pour nous
assurer qu’il soit utile à la société. Et puis, les relations vis-à-vis
du travail peuvent vite être romantisées.

Si nous avons une idée de ce dont nous avons besoin, nous pouvons
réfléchir à comment produire cela et arrêter les productions peu ou
totalement inutiles. Cet éparpillement a un prix en ressources, en déchets et bien
sûr en exploitation de créatures humaines et non humaines. Cette
question ne pourra pas être épuisée par un seul chapitre, elle est
immense, dépend des lieux, des temps et des humains qui y vivent. Mon
objectif est de poser des mots et des idées dont pourraient sortir des
propositions à première vue utopiques, mais concrètes et applicables. 
Pour y arriver, je vais devoir passer par des simplifications.

### Généalogie des besoins

Pour commencer à penser les types de besoins, je vais me servir de
l’essai de Razming Keucheyan *Les besoins artificiels*, dans lequel, il
propose une analyse critique des besoins humains, notamment en
s’appuyant sur des auteur·ices communistes.

Pour lui, nos besoins sont de trois ordres : les *besoins biologiques
absolus*, *qualitatifs*, ou *superflus*. Les premiers
nous maintiennent en vie. Ils peuvent nécessiter le recours à de l’aide
extérieure, à des médicaments, des prothèses… Les besoins qualitatifs
permettent de créer une vie bonne. Et les besoins artificiels sont tous les
autres. Il nous faut aussi faire la distinction entre désir et besoin,
le premier étant la façon dont nous voulons répondre au deuxième. Il
existe d’innombrables moyens de répondre à un même besoin, vous en
désirez certaines et pas d’autres, qui néanmoins pourraient le combler.

D’après Razming Keucheyanle, le bien-être dépend entre autres de
l’assouvissement de besoins biologiques absolus : « il suppose la
survie, c’est-à-dire la satisfaction des besoins biologiques absolus.
Mais il la transcende ». C’est-à-dire que le seul fait de ne pas mourir
n’est pas suffisant au bien-être. Il nous faut donc satisfaire toute une
panoplie d’autres besoins, les besoins authentiques.

Je me dois d’ajouter à cela que la vie bonne ne nécessite pas que les
besoins vitaux soient tous parfaitement assouvis, ni qu’ils le soient en
continu. Nous pouvons supporter que nos besoins absolus ne soient pas
complètement assouvis et tout de même avoir une vie bonne. Prenons
l’exemple de la qualité de l’air que nous respirons. Le besoin de
respirer est absolu, mais dans une certaine mesure, nous pouvons tout à
fait survivre en respirant un air pollué. Il est aussi possible d’avoir
une vie bonne, tout en ayant une maladie grave, une situation de
handicap ou tout simplement en n’étant pas tout le temps en parfaite
santé (et d’ailleurs personne n’est pas parfaitement valide et en parfaite
santé).

Pour ce qui est des besoins qualitatifs, ils sont « constitutifs d’une
vie humaine, d’une vie bonne ». Ils sont en bonne partie socialement et
historiquement construits. « Les besoins ont une histoire, en même temps
qu’ils sont biologiques ». Cela veut dire que suivant les lieux et les
époques, il n’est pas seulement nécessaire d’assouvir un besoin, mais de
l’assouvir d’une façon socialement acceptable par soi-même et la
communauté. Le *comment* satisfaire un besoin, même vital, est
éminemment subjectif et normatif (habitation, habit, nourriture, type de
relations). Dit d’une autre façon, pour qu’un besoin qualitatif nous
semble rempli, il faut qu’il le soit d’une façon qui réponde à notre
désir.

D’après Razming Keucheyanle, ces besoins indispensables à remplir pour
que la vie en vaille la peine sont soumis à deux paradoxes en système 
capitaliste. Par sa capacité à produire pour une partie de la population
humaine des besoins superflus, le capitalisme libère « les individus de
l’obligation de se préoccuper au quotidien d’assurer directement leur
survie ». Cela permet de créer de l’espace mental et temporel pour le
bien-être. En même temps que les surplus nous libèrent, ils nous
enferment par la division du travail (nécessaire à les produire) qui
asservit, et par le consumérisme qui « substitue aux besoins
authentiques des besoins factices ». Ces besoins factices sont en grande
partie liée à l’accumulation (de biens, services, relations).

Le deuxième paradoxe est qu’en système capitaliste, plus nous semblons
avoir des besoins nombreux et vastes, plus les manières de les remplir
deviennent standardisées. Pour en fin de compte, faire preuve de peu de
créativité pour répondre à nos besoins. Ainsi, l’époque d’infinies
productions et de possibilités variées que nous vivons n’est ni capable
de nous apporter le bien-être par l’accomplissement de nos besoins
authentiques, ni de nous réaliser en sublimant nos capacités. Je pense
qu’il faut en rajouter un troisième : quasiment tous nos désirs
contribuent à mettre en danger l’habitabilité de la Terre et de ce fait
rendent plus complexe la réalisation de nos désirs futurs.

Enfin, et c’est peut-être une des clés de la solution, la notion de
besoins n’a de sens que s’il existe une solution possible de les
satisfaire : « la nécessité et l’histoire ne s’opposent pas ». Il n’y a
pas de sens au besoin de se téléporter si c’est impossible. En revanche,
il est envisageable d’effectuer des recherches sur ce sujet et la
découverte d’une technique de téléportation créerait instantanément le
besoin de le faire. Tant que c’est impossible, nous pouvons nous dire
que nous en avons envie, mais ce n’est pas un de nos besoins. Partant
de là, la disparition de certains produits, si elle n’efface pas le
désir, supprime le besoin. Si plus aucune boisson à l’aspartame n’est
produite, plus personne n’aura besoin de boissons à l’aspartame, tout
comme personne n’en avait besoin avant qu’elles ne soient développées.
Il en va de même pour des produits répondant à des besoins impérieux. Si
une chose n’existe plus, les personnes qui vivent après « n’auraient pas
le même besoin, car sa satisfaction ne serait pas concevable ».

Bien sûr, les conséquences découlant de l’impossibilité de remplir un
besoin qui, jusque-là l’était, ont leur importance et peuvent aller
jusqu’à la mort. C’est pour cela qu’il faut se poser la question des
besoins avant d’en arriver là, et ne pas négliger les différences entre
les corps.

Pour les cas (nombreux) où la réponse à un besoin que nous avions
l’habitude de remplir disparaît (sans attenter aux besoins absolus),
cela peut conduire à un état de déprivation. C’est l’état qui dure tant
que nous sommes hantés par « la mémoire de la satisfaction passée ».
Résoudre cette douleur nécessite une « auto-guérison » qui ne pourra
avoir lieu que par une modification de la structure de nos désirs (et
pas de nos besoins). C’est ce qui arrive à toustes avec la vieillesse.
Nous finissons par ne plus pouvoir faire ce que nous faisions. C’est
difficile à accepter. Pourtant, dans bien des cas, les humains savent
faire. Nous savons nous auto-guérir et sortir de la tristesse ou de la
colère que cause la déprivation.

En revenant à la question de la production de réponses à nos besoins,
les conséquences ne sont absolument pas comparables si ce qui disparaît
est le golf, la chimiothérapie ou l’eau courante. Derrière les besoins,
il existe des situations concrètes et elles peuvent être impactées
avec une grande violence.

Nous devons faire le deuil de nos habitudes de consommations et tout de
suite commencer à réfléchir à la manière d’organiser l’auto-guérison
collective par laquelle nous devrons passer. Sinon, nous nous exposons
au risque que certain·es soient prêt à recourir à la violence pour
maintenir des besoins au prix de la domination. On peut déjà le voir
aujourd’hui avec les riches qui se font construire des bunkers et
préparent des milices privées pour continuer à garder les mêmes besoins
quand ils deviendront inaccessibles aux autres.

Pour résoudre l’insoluble problème des besoins de marchandises, il nous
faudrait trouver une « norme du suffisant » par opposition à un
« toujours plus ». Mais cette norme ne peut pas être établie avec
unicité et au doigt mouillé. D’abord parce qu’elle dépend des normes
sociales, des habitudes et des possibilités de produire : toute norme
n’est valable qu’en un lieu et un temps donné. De plus cette décision va
se heurter à l’inévitable : un individu qui la refuse par choix ou 
qui ne pourra pas vivre si cette norme est appliquée.
C’est le même problème avec la « vie bonne ». Qui détermine ce que cela
signifie ? C’est un enjeu politique majeur. Comme le rappel Judith
Butler dans *Qu’est-ce qu’une vie bonne ?*, il y a des « des genres
de vie qu’on considère déjà comme des non-vies ». La vie bonne est
normative et politique. Considérer que certain·nes vivent des
« non-vies » n’est pas sans conséquences. Si la collectivité considère
que des vies ne valent pas la peine, elle engendrera une prophétie
auto-réalisatrice, telle une dystopie eugéniste.

Pour éviter de nous égarer en soutenant des propositions aux
conséquences lourdes pour celleux déjà en difficulté, il nous
faut nous éloigner de l’idéal de définir ce qui serait une « norme du
suffisant » ou « une vie bonne », partant du principe que chacun·e est
légitime de la déterminer pour soi.

### Attachements & Infrastructures

Vu le niveau de danger que font planer sur le vivant nos productions
actuelles, et en faisant l’hypothèse conservatrice (peu risquée) qu’il
n’existe pas de solutions technologiques pour continuer le business *as
usal*, nous devons nous rendre à l’évidence : demain on ne pourra pas
et on ne devra pas *tout* maintenir. Ici le *tout* fait référence aux
« communs négatifs », qui sont des « réalités problématiques »,
c’est-à-dire qui mettent en danger le présent et le futur des vivants.
Les communs négatifs, tout comme les communs positifs doivent être
gérés collectivement, car ils nous affectent toustes et dépassent
l’individu. On peut penser aux déchets, à la production de S.U.V, à la
dématérialisation totale des démarches administratives. Ces concepts, je
les tire d’Alexandre Monin dans *Politiser le renoncement*. Ils vont
nous aider à regarder avec hauteur les questions de renoncement (à des
biens et services) et les conséquences techniques associées. Son travail
soulève des points qui rejoignent nos préoccupations sur les liens, en
particulier par la question des « attachements » qui sont « ce à quoi on
tient et ce qui nous tient ».

Nous avons des relations intenses à l’égard des productions humaines qui
nous entourent. Ces productions sont matérielles, concernent des
activités, des organisations, des relations… Nous dépendons d’elles,
nous en désirons, nous en détestons et nous en aimons. Malheureusement,
une grande partie d’entre elles sont des « communs négatifs ». Il
semble, comme pour les besoins, qu’ils ne sont pas définissables a
priori et changent selon le point de vue adopté. C’est là que la
proposition d’Alexandre Monin est à la fois simple et efficace : nous
devons en passer par l’enquête. Pour décider démocratiquement, il faut
écouter les concerné·es. Les enquêtes doivent aider à savoir quels
attachements sont légitimes, voire indispensables. Ces enquêtes doivent
prendre en compte « les corps concernés », et ne pas oublier que « les
corps sains ne sont pas la norme ». Par ce chemin, il devient imaginable
de trier les attachements.

Il nous faut éviter autant que possible les a priori sur les 
attachements. Trop de vies dépendent de technologies pour que nous les
supprimions à la hâte. Même s’il nous faut imaginer comment vivre sans
certains objets ou services, d’autres ne doivent pas être négociables
quand les personnes concernées en sont dépendantes pour vivre. Les 
attachements ne peuvent pas être tous traités à égalité.

Souvent, même si cela n’est pas une question de vie ou de mort, notre
quotidien est « pris au piège de nos attachements ». On peut penser à
l’attachement à la voiture, qui maintient notre dépendance vis-à-vis des
automobiles, des routes, du pétrole et nous lie à l’étalement urbain,
aux maladies respiratoires, à la violence automobile. Et pourtant, il
est si difficile de se *désattacher*… Ou bien, il peut s’agir de
travailleur·euses dépendant·es d’une industrie. L’exemple des
agricultureur·ices fonctionne bien : pour une partie d’entre elleux,
c’est une activité nécessitant d’énormes investissements, les faisant
dépendre de forts rendements. Dans ce cas, iels sont pris·es au piège
d’un commun négatif dont la solution nécessite des décisions à grande
échelle.

Si nous voulons concrétiser des utopies, elles doivent prendre en compte
les attachements qui ne sont pas les nôtres. Nous allons devoir trouver
des moyens pour rencontrer et comprendre celleux qui sont aujourd’hui
nos adversaires, ou attaché·es à elleux. Que ce soit pour les rallier à
nos causes ou les pousser à la défection. Cela n’arrivera pas par la
seule prise de conscience ou l’invective. Il nous faut constituer des
mondes possibles, créer des solutions et des ouvertures qu’elleux
pourraient rejoindre. En réalité, rien que par le travail, nous sommes
nombreux·ses à être attaché·es à des organisations qui exploitent les
vivants, la Terre, créent des besoins artificiels, etc. Si avec Margaux
nous explorons l’amour, c’est aussi pour le comprendre et essayer de
trouver des voies pour nous libérer de « ce qui nous tient » et à quoi
nous ne tenons pas.

L’objectif n’est pas de sortir de la société, mais de créer une
déchirure qui laisse imaginer un autre espace-temps.

Combien de fois a-t-on acheté telle tenue, poster, bijou, appareil,
des « objets signes » parce que cela faisait partie de ce à quoi l’on
s’identifiait ? Ou pour être jugé·es positivement ? Anthony Galluzzo,
dans *La fabrique du consommateur*, parle de l’anticonformisme des
années 60-80 : « il semblerait, rétrospectivement, que l’idéologie
contre-culturelle et libertaire soit des plus adaptées à
l’épanouissement du capitalisme ». C’est l’occasion de créer de petites
autonomies qui desserrent le nœud des besoins. Le simple fait de faire
partie d’un groupe d’ami·es qui ne promeut pas la possession de marques
d’appartenances permet de sortir d’attachements qui nous tiennent. C’est
nécessaire, mais insuffisant.

L’autonomie semi-individuelle, c’est un chemin qui peut être tentant
pour une classe de la population. À la fois pour agir avec vertu et pour
se libérer un peu. Face à la détérioration des services publics et des
conditions de chômage, il est séduisant de se dire que si je n’ai besoin
de rien, je suis intouchable, et ainsi gagner en liberté en essayant
d’avoir besoin de dépenser peu. C’est le chemin cognitif que j’ai suivi
ces dernières années. À 20 ans, j’imaginais qu’après mes études,
j’allais avoir de bons revenus, habiter dans une grande ville et prendre
souvent l’avion pour partir en weekend. À 33 ans, après de longues études, j’ai quitté le monde de la recherche, je me demande si je reprendrais
l’avion un jour, je suis devenu végétarien, j’imagine vivre pour
toujours en colocation et pour Noël, je peine à trouver ce qui me
manque. Cela donne une position de surplomb. Surtout que je vis dans un
groupe social où la sobriété est valorisée. Cela donne l’occasion de
juger, de se sentir meilleur. De me dire que j’ai réussi tout seul à
m’auto-détacher et à me guérir de la déprivation. C’est probablement
constitutif de l’image négative sur l’écolo-bobo. Si cela peut énerver
la droite et les bourgeois·es, aucun problème. En revanche, si cela crée
une division avec les classes populaires, c’est problématique. Myriam
Bahaffou incarne cette émotion. Dans une interview donnée au podcast
*Présage* à la sortie de son livre *Des paillettes sur le compost.
Écoféminisme au quotidien*, elle arrive à joindre le besoin de sortir du
système capitaliste et les attachements des personnes à ce qu’il
produit.

En le faisant, elle distribue quelques gifles à l’idée d’une société
écologiquement pure et sortie de la technologie. Encore plus que ne le
fait Alexandre Monin, elle en appelle au corps et aux rapports sociaux :
« ce que les féministes qui travaillent sur le handicap aujourd’hui
proposent en fait c’est incroyable quand on parle d’écologie, car on
parle de corps, on parle de moyen, on parle de dépendance à la
technologie \[…\] On parle très vite de corps qui sont au cœur d’une
violence systémique profonde, mais cette violence elle s’appuie tout le
temps sur cette idée de nature, sur quels corps sont dispensables et
quels corps sont utilisables ».

En quelques phrases elle pose ce qui compte. Encore plus, elle montre ce
qui manque souvent à un bon nombre de pratiques écologistes visant à
l’auto-réduction des besoins et au détachement. En mettant trop l’accent
sur les individus, nous négligeons les rapports sociaux dont dépendent
aussi les attachements. La rhétorique de la sobriété heureuse se heurte
violemment aux rapports de classes sans arriver à suffisamment les
prendre en compte pour les dépasser : « Les personnes qui justement ne
sont pas attirées, n’ont jamais été considérées, n’ont jamais considéré
le capitalisme comme un espace de réussite et d’émancipation, voire de
rachat, voire de réparation pour leur famille, pour l’endroit d’où ils
viennent, ce sont des personnes pour qui la classe n’a jamais été un
vrai problème, donc c’est des personnes bourgeoises ».

Dans une interview donnée à la revue *Silence*, le chercheur Hadrien
Malier (dont la thèse s’intitule *Populariser l’écologie, éduquer les
classes populaires ? : ethnographie critique d’interventions publiques
et militantes en France et en Argentine*) analyse des actions de
sensibilisation menées par des miliant·es écolos dans des quartiers
populaires. D’après lui, leurs interventions visent essentiellement à
faire baisser les consommations (surtout d’eau et d’électricité), qui
par sobriété subite sont déjà basses. Le tout est teinté de préjugés de
classe. Comme par exemple l’idée que « les classes populaires auraient
un goût particulier pour le jetable » ou que « les habitant·es \[de
quartiers populaires\] manquent de respect pour l’environnement ».

Ces jugements culpabilisants font peser les conséquences d’une
organisation sociale et du manque d’investissement public à celleux qui
le subissent au quotidien. En demandant à des personnes vivant dans un
quartier populaire de faire attention à leur consommation, alors qu’elle
est déjà contrainte par l’économie, l’action politique rate sa cible et
soutient l’idée bourgeoise selon laquelle les pauvres ne sauraient pas
gérer leur argent. En plus de cela, elle ne prend pas non plus en compte
le racisme structurel de notre société. D’après Fatima Ouassak dans
*Pour une écologie pirate*, s’il y a un désintérêt d’une partie des
quartiers populaires pour l’écologie il n’est pas tant dû au fait que le
sujet ne semble pas important aux habitant·es, mais que « tout est fait
pour que ces populations ne s’ancrent pas dans cette terre européenne et
pour les condamner à l’errance. Mille frontières physiques et
symboliques les y contraignent : *tu n’es pas d’ici, tu n’es pas de là-bas,
tu es de nulle part*. ». Être de quelque part, se sentir chez soi,
socialement accepté, c’est un besoin qui, s’il n’est pas vital, est à
minima qualitatif.

Je trouve que ces idées résonnent puissamment avec *Reprendre la terre
aux machines*, qui affirme que « vouloir seulement agir de manière
vertueuse au niveau local, donner l’exemple pour inspirer les autres
autour de soi par capillarité ne changerait rien ou si peu au désastre
global qui vient ». Il faut donc en passer par plusieurs actions
simultanées et cohérentes. C’est-à-dire assumer le besoin « d’arrêt, de
fermeture voire de démantèlement des communs négatifs ». Pour expliciter
les termes, l’arrêt correspond à la non-réalisation d’un projet qui
n’est pas ou peu commencé, en tout cas non exploité et qui donc n’a pas
encore créé d’attachement. La fermeture à l’arrêt d’un commun négatif
existant, mais qui ne pose pas de difficulté hors de ses attachements.
Enfin le démantèlement est la fermeture d’une structure existante,
dont l’arrêt est difficile soit techniquement (centrale nucléaire) soit
socialement, comme la production de nourriture à bas prix, mais
détruisant le vivant et malmenant des non-humains.

### Les besoins futurs

Cette partie va se solder par plus d’interrogations que de réponses.
Néanmoins, se poser de meilleures questions est déjà une avancée
significative. Que ce soit dans nos comportements, nos jugements ou les
propositions politiques que nous formulons, c’est en nous posant les
bonnes questions que nous avons une chance d’emporter les imaginaires.
Ce n’est pas en proposant la fin des possessions et en faisant de la
« charge de la Terre un sacerdoce » que nous créerons une utopie ! Cette
phrase de Benoit Berthelier provient de *La clinique de la dignité*,
dans l’objectif de trouver comment se comporter avec les vivants non
humains.

Ce qui impacte les vivants de toutes sortes, ce sont les modes de
production des humain·es. Prendre cela en compte peut nous aider à
dépasser (sans abandonner) le cadre d’une réflexion individuelle sur les
besoins. Nous ne pouvons pas l’abandonner, car une part de la
décroissance des consommations doit être volontaire et désirée. Mais
c’est par une synthèse collective de nos besoins que nous pourrons
sortir de l’ornière. En nous reformant comme un groupe hétérogène, mais
uni, nous pourrons alors arrêter de voir dans les comportements
individuels un moyen de s’élever au-dessus de la masse. Il nous faut
sortir de tout idéal de pureté, pour plutôt chercher des alliances.

Pour y arriver, il faut répondre à des questions essentielles que pose
Fatima Ouassak : « Nous sommes d’accord pour régler le problème
climatique, mais du point de vue de qui et dans l’intérêt de qui ?
Est-ce l’humanité que l’on veut sauver, ou juste sa fraction blanche ?
Quelle écologie garantit toutes les libertés, dont celle de circulation
et d’installation pour tous·tes sans distinction ? Quelle écologie
défendons-nous ? Une écologie qui viendrait ajouter des frontières aux
frontières ou une écologie qui cherche à casser des murs ? » 

Repassons par Benoit Berthelier, qui, en parlant du rapport aux vivants,
dit que « la tâche de la Terre et des vivants », ne doit pas être
comprise comme « une tâche spirituelle, intérieure et purement
individuelle ». Ainsi, « se soucier de la Terre et des vivants n’est pas
un devoir moral, c’est une charge publique ». En quelques phrases, les
liens s’épaississent. Si nous voulons vraiment sauver ce qui peut l’être
et atterrir le mieux possible, c’est par la force de l’action publique.
On ne peut pas ouvrir les frontières par l’action individuelle (même si
encore une fois quelques un·es arrivent à porter des actions qui
comptent). On ne peut pas sortir du béton en construisant sa maison en
terre paille. Il faudra des organisations convaincues de l’importance
d’agir sans laisser qui que ce soit derrière.

C’est de cela dont nous avons véritablement besoin : d’organisations
collectives à même de changer le cours des choses, car elles auront été
constituées dans ce but. Plutôt que de nous débattre dans des questions
impossibles comme celles consistant à équilibrer les puits de carbone et
les émissions, ou bien celles d’essayer d’imaginer ce que chacun·e
pourraient consommer avec un budget carbone donné. Il faut travailler
à une organisation collective centrée sur les attachements et capable
d’arrêter des infrastructures qui emprisonnent, de mettre en place des
mécanismes de guérison face aux déprivations. Bien sûr, il nous faudra
prendre aussi en compte les attachements des non-humains.

J’aurais aimé proposer des typologies de biens, des idées plus
pratiques, mais le faire, ce serait essentialiser les besoins et décider
pour les autres ce qui est bon pour elleux. Alors ce que j’aimerais vous
laisser en cette fin de chapitre, c’est une citation d’Adèle Gascuel,
dans le spectacle *Sirène 2428* « quand la maison brûle, avant de sortir
on attrape une ou deux choses essentielles comme la Sécurité sociale ».

C’est avec des communs sociaux et matériels que nous pourrons avoir
besoin de moins. Certains d’entre eux sont des objets immenses comme la
Sécurité Sociale ou le système de production d’électricité. Leur taille assure leur
résistance à des chocs, que ce soit une pandémie ou la mise à l’arrêt de
centrales. D’autres peuvent être plus petits, comme des organisations
locales visant à créer des possibilités de subsistance hors du marché,
à l’image de la Sécurité sociale alimentaire. Ils peuvent à la fois desserrer
l’étau économique et notre dépendance quasi totale aux chaînes
d’approvisionnement fragiles et inégalitaires. D’autres sont à
inventer, car dans le climat du Capitalocène, des évènements hautement
improbables au début du siècle deviennent d’actualité. Il nous faut
imaginer des infrastructures (matérielles et sociales) pour nous abriter
au plus fort des crises, mais aussi pour accueillir celleux qui auront
été déplacé·es.

Dans nos sociétés, « l’objet détermine le besoin, la production
détermine l’objet, donc la production détermine le besoin ». Inverser ce
dictat capitaliste en nous mettant sur le chemin d’une détermination de
nos besoins qui précède la production est en soi révolutionnaire.
C’est le meilleur moyen de combattre la création de « besoins
artificiel » et la surconsommation. Asservir la production aux besoins
réels, c’est une utopie. Elle est simple, mais puissante, elle nécessite
que nous soyons capables de mettre (et remettre quand ils évoluent) sur
la table ce que sont nos besoins « absolus » et « qualitatif ». Plutôt
que de chercher des normes générales, il est en fait nécessaire de lier
les besoins collectifs et individuels, puis d’y soumettre la production.

Les conséquences de cette utopie sont d’abord de diminuer nos besoins
artificiels et donc de moins produire, de fabriquer du nécessaire et par
conséquent du sens pour les travailleur·euses. J’espère que cela sera
moins chronophage qu’aujourd’hui, mais peut-être pas… Si notre temps
de travail est utile, profondément utile, cela change beaucoup. Bien
sûr, cela n’a de sens que si ce travail est fait dans une quête de soin
collectif entre humain et non-humains.

La question du vivant non humain a commencé à apparaître dans cette
partie, car nombre de nos besoins dépendent des non humains. Penser nos
besoins, c’est aussi imaginer des solutions pour que les créatures qui
peuplent la Terre aient les conditions matérielles d’existence pour vivre leur vie. D’une certaine manière, elles subissent elles aussi les
conséquences des différents systèmes d’oppression vus jusqu’ici. Des
espèces entières sont rayées de la carte et oubliées. D’autre, car elles
sont charismatiques, sont mises dans des réserves et contrôlées de près.
Nous les optimisons, nous en faisons des décorations vivantes… Les non
humains devraient être des allié·es face aux dominations. Pour cela, il
faut déjà les reconnaître comme tel·les, puis trouver comment nous
pouvons agir en tant qu’allié·es.

Pour nous allier avec des non-humains, il nous faut comme dans chaque
lutte commencer à nous intéresser à celleux qui sont victimes, à les
écouter, à apprendre et à nous remettre en question. C’est-à-dire
politiser notre relation aux vivants qui peuplent nos mondes et
travailler à développer avec elleux des relations qui font concrètement
exister leurs besoins.

Bibliographie

**Livres** :

BAHAFFOU, Myriam, 2022. *Des paillettes sur le compost: écoféminismes au quotidien*. Lorient : Le Passager clandestin. ISBN 978-2-36935-536-6.HQ1194 .B34 2022

FLEURY, Cynthia, 2023. *La clinique de la dignité*. Paris : Seuil. Le compte à rebours. ISBN 978-2-02-151425-4.179.7

GALLUZZO, Anthony, 2020. *La fabrique du consommateur: une histoire de la société marchande*. Paris : Zones. ISBN 978-2-35522-142-2.306.3

L’ATELIER PAYSAN (éd.), 2023. *Reprendre la terre aux machines: manifeste pour une autonomie paysanne et alimentaire*. [Éd. augmentée d’une] postface inédite. Paris : Éditions Points. Points, 23. ISBN 978-2-7578-9947-2.338.109 05

MONNIN, Alexandre, 2023. *Politiser le renoncement*. Paris : Éditions Divergences. ISBN 979-10-97088-53-8.363.705 25

OUASSAK, Fatima, 2023. *Pour une écologie pirate* [en ligne]. La Découverte. Petits cahiers libres. ISBN 978-2-348-07544-5. Disponible à l’adresse : https://www.editionsladecouverte.fr/pour_une_ecologie_pirate-9782348075445

**Articles** :

REVUE SILENCE, 2024. Quelles écologies dans les quartiers populaires ? [en ligne]. janvier 2024. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.revuesilence.net/numeros/528-Quelles-ecologies-dans-les-quartiers-populaires/

**Web** :

écoféminismes révolutionnaires, pour de nouvelles perspectives écologiques, [sans date]. [en ligne]. [Consulté le 30 juillet 2024]. *Présage*. Disponible à l’adresse : https://www.presages.fr/blog/2023/myriam-bahaffou

Rencontre avec Arthur Keller et les étudiants de l’Institut Agro Rennes Angers, 2022. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.youtube.com/watch?v=4MfAL6FVaOs

**Spectacles** :
SPECTACLE, **SIRERE 2428**, Cie les 7 sœurs, Mise en Scène Adèle Gascuel

## De la nature au genre, en finir avec les systèmes binaires et hiérarchiques

L’un des objectifs de ce livre visait à relier nos questionnements sur
les manière de vivre des relations interpersonnelles non-oppressives,
aux conditions sociales nécessaires à ce qu’elles puissent advenir. Cela
s’ancre dans un cadre de réflexion plus vaste sur les mécanismes du
capitalisme, qui se nourrit des rapports de domination pour en tirer
profit et s’enraciner.

Nous avons donc parlé de l’hégémonie du système hétérosexuel, de
l’aliénation des minorités de genre et de sexe. Elles entravent autant
l’abolition du capitalisme que l’avènement de relations apaisées entre
les humains, hors des structures patriarcales.

Mais à parler des conséquences du capitalisme sur nos manières de nous
lier, nous avons souhaité aller plus loin. Il nous semblait nécessaire
d’ouvrir le champ de pensée vers la relation au vivant : comment le
capitalisme se nourrit de notre rapport hiérarchique avec la « Nature »
? Comment nos relations et leurs modes de production s’insèrent dans un
système écologique donné, quels cadres de pensée rendent possible
l’exploitation de ses ressources ?

荣霞燕

*Dans Par-delà nature et culture* (2005), Philippe Descola montre que la
société occidentale se représente le monde dans ce qu’il appelle une
« ontologie naturaliste ». Ce qu’il veut dire par là, c’est que l’une
des spécificités de notre vision de l’univers repose sur notre capacité
à altériser. Il y a nous, les humains et la culture, il y a les autres,
renvoyés au champ de la « Nature ».

Paradoxalement, cette catégorie de « Nature » permet aussi de faire
référence à ce qui *doit* être, à l’immuable, à l’ordre naturel des
choses. C’est là que cela m’intéresse : je pense que la « Nature »
permet à la fois de justifier l’exploitation du vivant (qui après tout,
ne fait pas partie du « nous » humain et civilisé) et l’aliénation des
minorités de genre et de sexe (au nom de ce qui est naturel et immuable,
de l’ordre reproductif, de ce qui se passerait dans le monde du vivant).

Au contraire, cette partie se propose d’avancer que le vivant est queer,
l’évolution se basant sur un principe de divergence permettant de voir
se côtoyer une multiplicité de formes de vie, qui au fond, sont
interdépendantes et assurent la survie les unes des autres (pardon pour
cette biologie de bas étage, je vous invite à lire Darwin pour confirmer
cela avec un argumentaire plus châtié). Dès lors, plutôt que de faire
appel à la « Nature » pour réifier la vie humaine, observer ce qu’il se
passe réellement dans le vivant nous inviterait à inventer de nouvelles
manières de définir nos identités, en relation.

Dans son livre *Vivre avec le trouble* (2020), Donna Haraway fait le
pari que le Capitalocène, soit l’ère géologique marquée par le
développement capitaliste comme agent modificateur des conditions
d’habitabilité de la Terre, est le produit de relations. Observer
d’autres liens, d’autres rapports au monde, décentrant l’Homme de
l’Histoire évolutive et contournant la pensée occidentale binaire, offre
de nouveaux imaginaires. Écrire un autre récit des horizons
post-humains. Voilà de quoi traite cette partie.

### Nos relations constitutives

L’espèce humaine s’est construite sur la distinction basée sur sa
capacité de pensée et de compréhension du monde. Dans l’ontologie
naturaliste théorisée par Descola, le mythe de l’Homme comme seul être
pensant lui permet de s’extirper de son environnement, dans un système
d’opposition nature/culture, humain/non-humain. Dès lors,
l’environnement et le reste des espèces sont « autres », et le rapport
de hiérarchie s’enclenche. L’infériorisation amène à considérer les
non-humains appartenant à la « Nature » comme des êtres sans
intériorité, que nous pouvons exploiter pour en tirer une plus-value.

La pensée occidentale se construit sur ces systèmes binaires. Et comme
je le disais un peu plus haut, je crois que nous pouvons tracer une
analogie entre l’extériorisation de tout ce qui a trait à la « Nature »
et l’exclusion des personnes qui ne se conforment pas aux catégories de
genre et de sexe. Mais avant d’aller plus loin, j’aimerais définir les
ponts que je me propose de créer.

Selon le Centre national de ressources textuelles et lexicales,
l’analogie est le rapport de ressemblance, d’identité partielle entre
des réalités différentes préalablement soumises à comparaison ; trait(s)
commun(s) aux réalités ainsi comparées, ressemblance bien établie,
correspondance.

Je n’assimile pas la violence vécue par les minorités de genre à celles
que subissent les non-humains. Par contre, je soutiens que l’exclusion
des premiers et l’exploitation des seconds peuvent tous les deux
s’expliquer par notre système de pensée binaire. La binarité traverse
nos identités : je suis humain·e ou non-humain·e, je suis homme ou
femme, je suis cis ou trans.

Nous héritons de catégories pour voir, classer, comprendre le monde. Ces
cases sont naturalisées et réifient la vie-même. Leur visée descriptive
devient normative : nous ne disons plus « les choses sont ainsi,
c’est-à-dire *peuvent* l’être », mais « les choses sont ainsi,
c’est-à-dire *doivent* être comme cela ». Ainsi, au nom d’une « Nature »
sanctuarisée, d’un ordre immuable des choses, les catégories de sexe et
de genre sont délimitées selon la binarité homme/femme, cis/trans.

Cela ne signifie pas qu’il faudrait abandonner toutes les catégories
pour verser dans un universalisme neutralisant. Les termes « homme »,
« femme », « cis », « trans », « humain », « non-humain » sont utiles en
ce qu’ils décrivent des rapports au monde différents voire des oppressions.
Ils éclairent aussi que de l’opposition binaire naît la hiérarchisation.
L’autre n’est pas seulement différent de moi, il est *autre* et donc
potentiellement exploitable.

Cette analogie vise à montrer que le vivant ne s’épanouit pas dans des
cases. En refusant la catégorisation binaire, nous subvertissons un des
axiomes qui sous-tend la domination de l’Hhomme (avec un grand ET un
petit h). En partant des principes d’organisation présents dans les
écosystèmes, et grâce à des auteur·ices complices, cette partie
interroge la construction de nos identités, hors des systèmes de pensée
binaire.

Nous avançons que nos relations à l’autre sont constitutives,
c’est-à-dire qu’elles nous forment en tant que sujets. Le vivant nous
offre le modèle de la diversité et de l’interdépendance plutôt que celui
de la domination pour la survie. Cette idée est ancienne, dans
*L’Entraide, un facteur de l’évolution* (1902), Kropotkine tirait déjà
des principes d’organisation sociale de ce qu’il avait observé dans le
monde animal. C’est cette piste, et celles d’animaux-amis, que nous
suivrons pour reconstituer nos identités et le lien au vivant.

#### La pensée binaire bénéficie aux hommes

Dans *Les Diplomates. Cohabiter avec les loups sur une autre carte du
vivant* (2016), Baptiste Morizot s’allie au loup pour révéler que la
manière dont nous pensons la « Nature » est devenue inopérante. Selon
l’auteur, le retour du loup met en échec nos modes de gestion du
sauvage : la régulation des populations par la chasse, et la
sacralisation du sauvage qui condamne toute tentative d’interférence
avec le comportement de l’animal. Ici aussi, l’altérisation du loup
fonctionne avec la hiérarchisation. Le loup est soit un prédateur que
nous voulons exterminer pour le bien supérieur de l’espèce humaine, soit
une créature sauvage aux qualités mystiques qu’il ne faudrait pas
déranger, au nom d’une « Nature » sanctuarisée.

Ces deux tendances se rejoignent dans notre conception du vivant, qui
repose sur la scission Nature/culture. Dans ces deux visions, nous
sommes toujours extraits de notre écosystème. Dans la première, nous
devons le contrôler pour assurer l’accès aux ressources nécessaires à
notre survie. Dans la seconde, l’homme serait avili par rapport à une
pureté première du sauvage.

Selon Baptiste Morizot, la « métaphysique néolithique » fonde nos
rapports au vivant. L’expression désigne le récit fondateur de la survie
de l’Homme grâce au développement de l’agriculture et de l’élevage, dans
une guerre perpétuelle contre le sauvage. Cette métaphysique va
classifier le monde selon les usages humains, prioritaires sur le reste
du vivant : les espèces domestiques, en tant que ressources alimentaires
ou force de travail, et les espèces nuisibles qui entravent notre
contrôle sur les ressources. Dans cette vision du monde, les choses
doivent nous servir ou disparaître. Ici, la pensée binaire bénéficie aux
Hommes.

Nous retrouvons aujourd’hui l’héritage de la « métaphysique
néolithique » dans nos manières de cohabiter avec le vivant. Et
peut-être plus particulièrement dans la gestion des friches. Je voudrais
m’arrêter sur le cas de la Renouée du Japon, que j’ai appris à connaître
grâce à mon ami, le docteur en génie de l’environnement Mathieu
Scattolin.

La Renouée est originaire du Japon, elle naît sur les pentes de volcans.
Là-bas, les sols se forment au fur et à mesure des éruptions et des
fusions. Particulièrement coriace, la Renouée réussit à s’y enraciner
pour transformer ces sols nouveaux. Elle est capable de résister à des
teneurs en métaux très importantes dans les coulées volcaniques, quand
le ventre de la terre refait surface. La Renouée du Japon est introduite
en 1850 en Europe pour son caractère ornemental. Elle arrive par l’île
de Sakhaline, rejoint l’Angleterre. Rapidement, elle sort des jardins
muséaux pour se propager sur les berges des rivières.

Les fleurs s’échappent des endroits où nous aimerions les contenir.
Elles prennent possession des terres, réussissent à survivre dans des
environnements contraints, et continuent aujourd’hui d’égayer nos vies
au milieu du béton. D’espèce ornementale, la Renouée serait devenue
nuisible, notamment pour la biodiversité, car toujours aussi coriace --
et donc invasive.

La Renouée se plaît sur les sols dégradés, notamment dans les friches
urbaines et industrielles que nous pouvons retrouver en ville. Les
stratégies municipales d’entretien de ces espaces vacants consistent
souvent à tenter de l’éradiquer, à faire la guerre aux rhizomes qui
repoussent entre chaque passage des agents.

Elle n’a pas la chance de sa voisine, l’espèce Robinier faux-acacia, qui
a été introduite plus tôt depuis les États-Unis et que les différentes
formes de valorisation ont rendu productive, donc acceptable à nos yeux
humains. Nous retrouvons ici la métaphysique néolithique. Les espèces
nous servent ou meurent.

Or cette vision du monde a conduit à enclore la société humaine sur
elle-même, dans un rapport d’exploitation et de contrôle du vivant. 
Si on l’ancre dans nos réalités économiques et politiques actuelles,
il ne s’agit plus de dominer la « Nature » pour survivre, mais pour
asseoir et perpétuer le modèle capitaliste. De la même manière que le
loup pose problème parce qu’il remet en cause le principe fondamental du
droit de propriété, en tuant les brebis qui appartiennent à un éleveur,
la Renouée du Japon dérange parce qu’elle s’oppose à la vision des
friches comme des terrains vierges et constructibles, prêts à l’emploi
pour les promoteurs immobiliers.

Les friches ne sont pas des espaces vacants. Ce sont des réserves de
biodiversité en ville, des espaces où rêver, des odes à
l’improductivité. Mais cela ne rentre pas dans la catégorie
nuisible/valorisable. Alors nous construisons des parkings, et cela
semble « profiter » à l’Homme.

De manière analogue, le dualisme Nature/culture autorise la
sacralisation d’un ordre naturel, bénéficiant à l’homme (avec un petit
h). Là où la culture renvoie au malléable, aux constructions humaines,
la « Nature » renvoie à l’immuable, aux lois universelles. Or il est
fréquent que le registre de l’ordre naturel soit utilisé pour
stigmatiser les minorités de sexe et de genre, ou justifier la
domination masculine. Combien de fois avez-vous entendu que les
inégalités entre hommes et femmes étaient naturelles, qu’il n’y avait
qu’à se référer aux capacités physiques des un·es et des autres,
argument souvent exemplifié par des exploits sportifs ? C’est un cas
typique de recours à la « Nature » pour justifier une oppression.

Dans *Écologies déviantes : voyage en terres queer* (2021), Cy Lecerf
Maulpoix montre que la pensée occidentale est « une pensée hiérarchique,
fondée sur la présence de nombreux dualismes normatifs qui organisent le
monde et notre rapport à lui. Chaque terme au sein de ces dualismes est
perçu comme exclusif et oppositionnel, puisqu’une valeur plus importante
est accordée à l’un des termes ».

Autour des dualismes homme/femme, hétéro/queer, cis/trans, l’auteur
souligne notamment que les débats sur la PMA (Procréation Médicalement 
Assistée) et la GPA (Gestation Pour Autrui) ayant agité la 
France lors de la discussion sur le projet de loi bioéthique en 2021 ont
révélé une frontière à ne pas dépasser. Au nom de la naturalisation du
système hétérocispatriarcal, c’est-à-dire, « des lois naturelles de la
procréation et d’une biologie de la reproduction hétérosexuelle
inaliénable », le droit à la parentalité des personnes trans ou non
hétérosexuelles semblait inaccordable.

Les tollés provoqués par le projet de loi font appel à la conception de
la reproduction en tant que phénomène naturel, et ce faisant, de la
hiérarchisation des corps aptes à porter la vie et à éduquer des
enfants, et des corps contre-nature, antisociaux. Cela renvoie à la
question de l’adéquation entre le sexe et l’identité de genre :
certaines vies dérogeraient à ce que la « Nature » a formé. Pour encore
citer Cy Lecerf Maulpoix, « l’espace et le corps apparaissent comme des
entités prédéfinies, closes sur elles-mêmes, des espaces intouchables
sous peine de profanation de leur équilibre interne, qu’ils prennent le
nom de Dieu, de Nation ou de Nature humaine ».

De la même manière que le dualisme Nature/culture permet d’exploiter les
ressources et les non-humains à notre guise, il légitime l’exclusion des
vies jugées déviantes de toute tentative de résister au cadre binaire
de notre pensée. Nous accorderions vaguement le droit de questionner
l’orientation hétérosexuelle obligatoire à la sphère de la « Culture »,
mais le sexe comme fondateur d’une identité de genre et du droit à la
procréation (hétérosexuelle) seraient des données immuables, relevant de
la « Nature ».

Pour concevoir le monde et notre rapport à lui en dehors de cette vision
binaire, il convient d’isoler le commun et le particulier. Le commun,
c’est l’ensemble des comportements et des rituels de vie qui font corps
au-delà des catégories naturalisées. Dans *Les Diplomates*, Baptiste
Morizot nous invite à étudier les comportements biomorphes, c’est-à-dire
les comportements vitaux qui nous lient avec les autres espèces. Comment
le partage de séquences de vie, comme la naissance, la mort, nous
inscrit-il dans une communauté de sens ? Selon Anna Tsing, citée par
Donna Haraway dans *Vivre avec le trouble* (2020), la vie et la mort de
toutes les espèces sur Terre se caractérisent par la précarité, signe de
l’échec des promesses du progrès moderne.

Isoler le particulier, c’est suivre la voie des diplomates de Baptiste
Morizot : « Les systèmes écologiques méritent qu’on fasse l’hypothèse
que tout se comporte ». L’avocat qui s’oriente vers le soleil pour
chercher la lumière, le merle qui garde une distance courtoise pour
aller dévorer les coquilles d’œufs émergeant du compost… L’instinct
n’est pas mécanique, les espèces sont composées d’individus qui
réagissent en fonction de multiples facteurs. L’évolution fonctionne
selon un principe de divergence qui multiplie les formes de vie.

Selon Catriona Sandilands, citée dans *Écologies déviantes* par Cy
Lecerf Maulpoix, l’écologie queer est « une constellation vaste et
interdisciplinaire de pratiques dont le but est de perturber de
différentes manières les discours et les articulations institutionnelles
de la nature et de la sexualité, de réimaginer des processus
d’évolution, des interactions écologiques et des politiques
environnementales à la lumière de la théorie queer ». À mon sens, en
questionnant à la fois le concept de « Nature » et son utilisation pour
normer nos sexualités, l’écologie queer nous offre une porte de sortie
des murs clos de la pensée binaire.

#### Le vivant est queer : l’identité ne pré-existe pas, elle est produite dans l’interaction

Si nous reprenons l’hypothèse de Baptiste Morizot selon laquelle, dans
les systèmes écologiques, tout se comporte, nous pouvons analyser les
manières d’être au monde des espèces non-humaines. L’éthogramme d’une
espèce, soit sa gamme de comportements, est une combinaison qui varie
selon les individus, les contextes sociaux et affectifs. Cela remet en
cause la spécificité de l’Homme comme seule espèce pensante, puisque
d’une certaine manière, les espèces non-humaines reçoivent les
informations, les analysent, et réagissent au prisme de leurs
expériences.

J’ai longtemps fait de l’équitation. Autrement dit, j’ai longtemps
côtoyé des chevaux, appris à les connaître, à accorder nos volontés
mutuelles pour que nos balades se passent bien (c’est à dire pour
éviter de me faire jeter de leurs dos). Aujourd’hui, je vois le rapport
d’exploitation qui se joue dans un sport visant à dresser un cheval et à
lui monter dessus pour son loisir. En revanche, c’est un bon poste
d’observation du comportement de l’espèce.

Je crois que toute personne ayant vécu à proximité d’animaux peut
reconnaître sans anthropomorphisme que ceux-ci sont dotés d’une
personnalité, d’un caractère, forgé par leurs interactions avec
l’environnement et les autres espèces autour d’eux. Qu’ils ont leurs
affinités, leurs accointances, mais aussi leurs antipathies entre eux et
avec nous.

Selon Baptiste Morizot, cette variabilité des comportements et des
cultures animales nous enseigne que « la divergence fondamentale qui
constitue le vivant produit tous azimuts des exo-rationalités,
exo-moralités, exo-intentionnalités et exo-créativités… ». Chaque être
vivant serait donc en capacité de former une intention.

Dans nos sociétés naturalistes occidentales, outre le fait que nous
n’accordons pas cette subjectivité aux non-humains, la supériorité de
l’Homme lui arroge le droit de définir celle des autres. Il faut se
méfier de la transposition de concepts dans une culture inchangée.
Aujourd’hui, même si nous reconnaissions ces intériorités, de la
différence pourrait naître une nouvelle forme de hiérarchisation.
Reconnaître que les oiseaux se comportent, qu’ils ne sont pas en train
de chanter mais de se disputer un territoire, que le cheval ne veut pas
que nous le montions particulièrement, mais avoir accès à une grande
étendue d’herbe, ne va pas forcément changer notre attitude envers eux.
Quand la différence entraîne la hiérarchisation, en nous constituant, le
regard des autres peut aussi nous aliéner.

Définir c’est décider. Ainsi, nous pouvons tisser une nouvelle analogie
entre l’exploitation des non-humains et l’aliénation des êtres humains
dans les processus de subjectivation et d’assignation d’un sexe et d’un
genre à la naissance. Le fait de définir l’autre permet de la sujétion.
Autrement dit, de manière analogue, l’Homme exploite le vivant, l’homme
contraint les expressions de sexe et de genre et exclut celleux qui ne
s’y conforment pas, et tout cela sous couvert du binarisme.

Dans son livre *Communisme queer* (2022), Frederico Zappino trace un
lien entre le combat des minorités de sexe et de genre et
l’antispécisme. Ils se rejoignent dans l’enjeu de la « sacrifiabilité »,
soit la lutte pour définir quelles vies méritent d’être protégées et
quelles vies ne le méritent pas. Selon lui, « de la même façon que la
production hétérosexuelle du genre, la production anthropocentrique de
l’espèce constitue l’un des présupposés sur lesquels se fonde le
capitalisme, car elle constitue un axe autonome d’allocation d’avantages
et de coûts ».

Il consacre le chapitre « Performativité du genre et allégories de la
transexualité » aux écrits de Judith Butler. Les deux auteur·ices
critiquent la vision libérale et constructiviste du genre, qui le
présente comme quelque chose pouvant dépendre de la volonté du sujet
autonome. Or le genre est performatif, c’est-à-dire qu’il se réalise en
s’énonçant, parce qu’il intervient dans un contexte social, historique
et symbolique préétabli. À la naissance, nous venons au monde dans un
ordre binaire où nous sommes fille, garçon, ou corrigé. Nous n’avons pas
le choix. Contre l’idée d’une liberté individuelle à affirmer ses droits
à vivre autrement, Butler parle de la « vulnérabilité ontologique et
prédiscursive du sujet » face à un pouvoir qui le précède.

Elle prend l’exemple de la naissance d’un enfant : le choix du prénom
est un non-choix. Les tous premiers moments de l’existence d’un
nourrisson sont des non-choix. L’enfant est vulnérable et ne choisit pas
ses attachements : il dépend de celleux qui sont là avant pour prendre
soin de lui. Son identité est constituée par la vision que les autres
ont de lui. Selon Butler, le genre est un acte répété, en relation avec
l’autre et l’ensemble des significations qui nous précèdent et le
définissent.

Ce que conclue Frederico Zappino de notre vulnérabilité individuelle
face au pouvoir du genre, c’est que détruire l’hétérosexualité nécessite
« d’en finir avec la féminité, avec la masculinité, avec les hommes,
avec les femmes, avec la catégorie de sexe, avec celle de genre, avec
les soi-disant « orientations sexuelles », soit avec tous les marqueurs
du régime politique hétérosexuel. Il s’agit de tracer son chemin et
d’imaginer une autre vie ».

C’est ce que tente de faire Donna Haraway dans *Vivre avec le trouble*.
Elle imagine la société après la chute des universalismes masculins et
transcendants. Pour l’autrice, elle serait marquée par le trouble des
genres indéterminés et les catégories mouvantes qui définiraient de
nouvelles manières de vivre. À la place de l’Anthropocène et de sa
focalisation sur l’Homme, nous nous considérerions, avec l’ensemble du
vivant, comme partie composante de la famille du humus. S’ensuivraient
des formes de vie créatives et expérimentales, enchevêtrées.

Si nous reconnaissons que chacun·e est fait·e des relations qui le
tissent aux autres, nous pouvons imaginer d’autres manières de produire
de la subjectivité, qui ne soient pas basées sur la domination. Penser
nos identités en lien, cela pourrait consister à créer des modes de
production moins prédateurs, s’inspirer du vivant pour cohabiter avec
lui, bref, miser sur l’interdépendance et la diversité plutôt que sur
l’hégémonie de l’Hhomme. Les communautés biotiques peuvent nous fournir
un exemple à suivre dans la recomposition de notre rapport au monde.

### La communauté biotique comme modèle de relation

#### Les récits de l’interdépendance

La communauté biotique est un concept inventé par le philosophe Aldo
Leopold. Elle désigne l’ensemble des relations entre les êtres vivants
(espèces végétales, animales, humaines) et non-vivants qui cohabitent en
interdépendance dans une même région. Cette focale déplacée sur les 
relations de dépendance au niveau d’un espace donné interroge la 
conception homo-centrée de l'homme, profondément ancrée en Occident, 
au profit d'une vision plus écologico-centrée , dans ce commun que 
nous partageons avec les autres vivants. Sur un territoire
commun, nous avons besoin des autres espèces pour survivre et nous
constituer en tant qu’êtres humains. Selon Baptiste Morizot, l’Homme
doit être pensé dans ses liens avec les non-humains, que ces liens 
soient liés à une histoire commune et évolutive, ou au partage d’un 
espace d’habitat et de conditions écologiques.

Dans la littérature, *Les Furtifs* (2019) d’Alain Damasio illustre très
bien cette idée. Les furtifs sont des animaux que les humains ne peuvent
pas voir. D’une extrême rapidité, ils se cachent dans les angles morts,
circulent en dehors de la vision humaine. Ce qui détonne chez les
furtifs, c’est leur vitalité, leur capacité d’adaptation extraordinaire.
Ils métabolisent les plantes, les animaux, les minéraux, les morceaux de
déchets pour alimenter leurs transformations. Les Furtifs sont
insaisissables car ils reconnaissent et vivent leur interdépendance,
chaque relation à leur environnement les enrichit et les rend plus
complexes. Or les mouvements militants vont s’inspirer d’eux pour lutter
contre la surveillance généralisée d’un État allié aux multinationales.
Les humains évoluent vers une forme de furtivité en apprenant à se
relier à ces créatures, et à leur capacité *à faire avec* leur
environnement.

Nous pouvons aussi penser à Camille, l’héro·ïne du chapitre
science-fictionnel de *Vivre avec le trouble*, de Donna Haraway. Camille
fait partie des communautés du compost, visant à apprendre à vivre avec
les manière d’être et de faire des différentes espèces ayant habité les
lieux. Les enfants sont lié·es à la naissance en symbiose avec une autre
espèce en voie de disparition. Iels sont couplé·es génétiquement à ces
êtres et en développent certaines particularités (nous pourrions
imaginer des dons de vision, odorat, goût pour les grands espaces…).
Iels partagent aussi un destin de vie : leur mission est de restaurer
leur habitat commun pour rendre la continuation des espèces et de la
communauté possible.

Cette histoire rejoint la pensée que l’autrice défend tout au long du
livre. Selon Donna Haraway, la vie est tentaculaire, c’est-à-dire
constituée des imbrications intimes de chacune des formes de vie sur
Terre, que ce soit par l’évolution biologique, écologique ou sociale.
Pour sortir du Capitalocène, il faut reconnaître que cette ère est le
produit historique de relations. De ce fait, d’autres types de liens
peuvent le défaire.

Face aux mythes surplombants de l’Homme et du développement qui
accompagnent le Capitalocène, Donna Haraway imagine le Chthulucène, qui
décentrerait l’Homme pour raconter les histoires non-humaines, leur rôle
fondamental en tant que support de vie et de biodiversité. Reconnaître
que les manières de vivre des non-humains ont de l’importance nous
permet de comprendre les implications et les interdépendances au sein
des communautés biotiques, notamment en cas d’extinction, car si « rien
n’est lié à tout, tout est lié à quelque chose ».

Nous pouvons déjà déceler des manifestations du Chthulucène autour de
nous, dans ce que l’autrice appelle « l’écologie des mauvaises herbes ».
Nous abordions tout à l’heure comment le destin de la Renouée du Japon
et celui des friches étaient liés dans le projet humain d’éradication de
la plante invasive et des espaces improductifs. Pourtant, d’autres
manières d’être au monde et de considérer les friches sont possibles.
Avec le projet *Renouer*, le designer Jean-Sébastien Poncet étudie les
opportunités de transformer les friches du territoire stéphanois en
fermes, portant des systèmes agricoles post-anthropocène, grâce à
l’alliance avec la Renouée du Japon pour dépolluer les sols autour des
gestes rituels *faner – broyer – composter*.

Ces projets nous invitent à remettre en perspective nos rapports au
vivant. Si les relations avec les non-humains nous fondent en tant
qu’êtres humains, l’inverse est vrai aussi. Nous avons rendu la Renouée
du Japon invasive en la prélevant de son milieu naturel pour en faire
une plante ornementale. De la même manière, dans *Les Diplomates*,
Baptiste Morizot montre que le pastoralisme, en sélectionnant les gènes
favorisant la docilité et en domestiquant le mouton, a créé la relation
de conflictualité avec le loup. Quant à lui, il a aussi été modelé par
nos relations, nos activités infléchissant son mode de vie en
fragmentant l’espace sauvage et ses corridors.

Se pose alors la question de notre responsabilité. Le loup remet en
cause le modèle de l’action directe positive qui « implique un rapport
au vivant qui exige d’abord de le rendre dépendant pour le rendre
manipulable, ce qui induit dans un second temps une nécessité
paternaliste de le conduire et le protéger ». Selon l’auteur, l’une des
pistes de cohabitation alternative se trouve dans la recherche des
mutualismes, c’est-à-dire de prendre soin de la relation en s’appliquant
à trouver des bénéfices mutuels dans l’interaction entre espèces. Pour
lui, ces mutualismes doivent se penser sur le temps long.

Si l’on reprend nos connivences herbacées, les plantes invasives sont de
bons témoins de ces échelles de temps. D’un côté, le Robinier
faux-acacia introduit depuis les États-Unis au XVIIIe siècle a remplacé
tous les individus de notre espèce locale d’acacia. De l’autre, nous
utilisons son bois imputrescible, sans se soucier des rejets qui
transforment nos jardins en forêts de racines. Plus loin dans la friche,
l’ambroisie à feuilles d’armoise, plante introduite à la fin du XIXe
siècle et jugée indésirable, provoque des allergies à travers la
dissémination de son pollen. Pourtant, une étude de 2018 (Francesco
Molinaro, Olaf Tyc et al.) montre que certains de ses composants
seraient actifs contre les souches du staphylocoque doré. À long terme,
notre adaptation et notre résistance à ses propriétés allergisantes
pourraient renforcer notre système immunitaire.

Donc, pour en revenir à nos moutons (hihi), la diplomatie mutualiste
théorisée par Baptiste Morizot consiste à « se saisir d’un concept
descriptif (le mutualisme) renvoyant à un type de relations omniprésent
dans le vivant, et à se demander dans quelle mesure, parce que ce type
de relation est valorisé par les vivants, et valorisable à de multiples
degrés, il ne mérite pas dans certaines situations, d’être érigé en
valeur éthique dans nos relations à lui ».

#### Se réinsérer dans le vivant

Mener à bien cette diplomatie mutualiste nécessite de se réinsérer dans
le vivant. Baptiste Morizot montre que l’étude des espèces et de leurs
éthogrammes nous permet de comprendre et de signifier dans le langage de
l’autre, pour cohabiter et partager les usages d’un territoire, au
bénéfice de chacun. Il s’agit de « transmettre des messages, poser des
limites et signifier des interdits ». Dans le cas de son exemple lupin,
il faut s’insérer dans les conventions politiques du loup afin de
communiquer avec lui. En ce sens, la relation à l’animal n’est pas
uniquement une projection de qualités humaines sur l’espèce, mais une
interaction dans laquelle l’animal est porteur « d’invites sociales » à
son tour.

Pour se réinsérer dans le vivant, nous pouvons nous aider de
littérature. Dans *L’Appel de la forêt* (1903), Jack London se saisit de
l’invite sociale d’un chien domestiqué nommé Buck, qui retrouve sa
condition de loup en éprouvant la rudesse du Grand Nord, à travers
différentes expériences en tant que chien de traîneau. De ce livre, nous
pourrions faire la lecture d’une forme de romantisation du « Wild »
américain, d’une application littéraire du darwinisme dans la lutte pour
la survie contre les autres et l’environnement. C’est sans doute
légitime. Nonobstant, le récit prend chair avec les amitiés canines qui
se lient, sa rencontre d’un membre d’une meute de loup, leurs jeux, les
jours passés en relation avec les proies qu’il guette pour se nourrir…
Jack London nous raconte l’histoire d’une communauté
biotique.

Plus encore, l’auteur invite le lecteur, à travers son personnage
principal, à naviguer entre différentes conventions politiques, celle du
chien domestiqué, du chien de traîneau, puis du loup. Et contre la
critique qui l’accusa d’humaniser son héros, il rétorqua : « ces héros
chiens ne sont pas gouvernés par un raisonnement abstrait mais par
l’instinct, la sensation, l’émotion et un raisonnement simple. Je me
suis également efforcé de mettre mes histoires en accord avec les faits
de l’évolution ». À l’aube du XXe siècle et avec les outils
scientifiques de son temps, Jack London improvise une forme d’éthologie,
déplaçant le regard du narrateur vers un être non-humain pour analyser
ses manières d’être au monde.

Dans un registre académique, Baptiste Morizot s’intéresse aussi à ce qui
singularise les modes de vie des êtres non-apprivoisés par l’Homme. Il
propose le terme « féral » pour parler des formes de vie qui ne sont pas
modelées par la domestication. Là où l’animal dressé dépend
essentiellement de l’homme pour sa survie, l’être féral se caractérise
par la multiplicité des liens qu’il entretient avec le reste des
espèces, dans un espace donné. C’est le « par soi-même », défini comme
le fait « d’être autonome, au sens d’être bien relié à toute la
communauté biotique, c’est-à-dire de manière plurielle, résiliente,
viable, de manière à ne pas dépendre absolument d’un exploitant qui
sélectionne et protège ». Si nous voulons réhabiliter le féral dans nos
sociétés, nous devons donc nous défendre d’apprendre aux non-humains à
parler notre langue, comme nous entraînons le chien à s’asseoir à notre
signal, ou le cheval à accélérer lorsque nous pressons nos talons. Mais
comment communiquer hors de l’apprivoisement ou de la destruction ?

Selon Baptiste Morizot, connaître les manières de signifier des limites
ou des encouragements dans le langage d’une autre espèce permet de
négocier les usages sur un même territoire. Il parle de la « négociation
contributive », consistant à maintenir une relation durable et de
qualité, parce que les éléments sont amenés à cohabiter sur le
long-terme. Ainsi, il souligne que de manière indirecte, les loups ont
des effets positifs sur la biodiversité, vivifient les populations
d’ongulés, entraînent la multiplication des oiseaux nicheurs…

Nous pourrions prendre un exemple plus familier. À la maison, nous avons
plusieurs bacs de terre sur la terrasse qui font office de potager
urbain. Nous faisons pousser de la roquette, de la rhubarbe, des radis,
un arbuste de verveine… Grâce à cela, nous profitons de la compagnie
de deux espèces d’oiseaux, que nous avons baptisés « La grosse
merlette » et son copain « Jackie le merle ». J’ai souvent l’impression
qu’elleux aussi sont porteur·euses d’invites sociales. À les observer,
essayer de les approcher lentement, je peux déceler les gestes d’alerte,
de curiosité, les mouvements de recul qui me somment de rester à
distance. Ou du moins me les figurer, tenter de pénétrer leur
éthogramme.

En hiver, nos amis se nourrissent du compost dans les bacs. C’est au
printemps que les choses se compliquent, quand nous commençons à faire
germer nos plants, que nos rhubarbes s’épanouissent. Les deux compagnons
délaissent les restes alimentaires pour picorer les feuilles. Et voilà
un conflit d’usage sur un territoire. S’offre alors à nous la
possibilité d’accrocher des disques à un fil pour les effrayer –
signifier les limites – et nous priver de leur présence, ou d’accepter
de manger des légumes picorés, en partageant les ressources. La suite au
prochain épisode.

Dans *Vivre avec le trouble*, Donna Haraway entrevoit les rapports entre
les humains et les animaux approchés comme des relations de
co-domestication. Il s’agit de se rendre mutuellement capables de
pratiques sociales, écologiques, cognitives et comportementales situées.
L’autrice prend l’exemple des pigeons voyageurs qui ont longtemps permis
aux humains de passer des messages ou d’espionner. Contre la
vision utilitariste de l’animal, Donna Haraway montre que les pigeons
peuvent être vus comme acteurs de leurs pratiques plutôt qu’en tant
qu’outils, puisque ce sont eux qui ont formé les humains à la
communication longue distance, ont permis d’imaginer de nouvelles
techniques d’espionnage. Loin de la vision de l’animal-machine dont
l’homme se rend propriétaire, Donna Haraway valorise la créativité
animale comme source d’inspiration humaine.

Cela rejoint la vision de Baptiste Morizot sur l’élevage, où les brebis
sont des espèces que l’agriculteur contribue à faire vivre, mais
demeurent inappropriables. Elles sont d’une certaine manière un flux de
la circulation de l’énergie solaire, à travers le fourrage qui les ont
alimentées, un produit plus ou moins direct de la photosynthèse. Elles font
partie de la communauté biotique, et comme pour le reste du vivant, une
part de ce flux d’énergie solaire revient aux autres habitants de
l’écosystème (dont fait partie le loup, qui, parfois, prélève une partie
de ce flux, sous la forme de steak de brebis).

Les pensées de ces deux auteur·ices se rejoignent dans l’idée de
composer avec le vivant en tant qu’égal. Or voir les non-humains comme
des partenaires nécessite de se mettre à leur place pour comprendre la
manière dont ils fonctionnent, ce dont ils ont besoin. Selon Donna
Haraway, c’est un travail de collaboration qui revient à proposer
quelque chose et à accepter des contraintes que l’on n’avait pas
demandées, découlant de la rencontre de deux formes de subjectivités.

Nous pouvons retrouver ce rapport de co-domestication dans le film
*Bella e Perduta* (2015), de Pietro Marcello. Il raconte l’histoire d’un
voyage pastoral, où le bufflon Sarchiapone est recueilli par Tommaso
dans le palais en ruine de Carditello, qu’il gardienne bénévolement. Le
film offre une voix off et un regard à Sarchiapone (sur certains plans, 
la caméra montre la vision du buffle), pour nous raconter l’histoire
de ses humains. À la mort de Tommaso, le buffle portera la mémoire de
son maître, premier à l’avoir nommé. Il rencontre alors Polichinelle,
sommé de l’amener à un berger loin de là pour prendre soin de lui.
Polichinelle parle aux morts pour comprendre les vivants. De cette
connexion nait un espace de dialogue avec Sarchiapone. Les deux êtres
discutent et voyagent ensemble. À son tour, Sarchiapone reconnaît à son
ami le droit d’être nommé, derrière son masque de polichinelle. À la
fin, quand Sarchiapone est passé entre les mains d’un paysan qui l’a
engraissé pour le manger, il parle une dernière fois :

« Quand les hommes nous laisseront-il enfin à notre destin ? \[…\]  
Dans un monde où l’on nous prive d’âme  
Être un buffle est un art »

Polichinelle a su reconnaître la parole de Sarchiapone. Il a agi selon
la logique de « devenir-avec », de Donna Haraway, c’est-à-dire en se
rendant mutuellement capables de bien vivre et de bien mourir ensemble.
Sa manière d’être au monde se constitue en interactions avec les morts
et le vivant, sans distinction. Dans l’autre sens, Sarchiapone, par sa
subjectivité, a contribué à former Polichinelle en tant que sujet.

Dans le cadre d’une conception du vivant comme un tissu de liens,
Baptiste Morizot oppose la diplomatie des relations à la diplomatie des
termes. La première postule que pour viser le bien d’un des partis, il
faut viser le bien de la relation elle-même. Si nous reprenons notre
métaphore cinématographique, c’est Polichinelle qui décide de traverser
l’Italie par amour pour Sarchiapone, qui accepte son rythme de jeune
bufflon, pour le bien de leur amitié naissante. Au contraire, dans la
vision de la diplomatie des termes, les êtres sont séparés, « il faut
viser le bien de l’ensemble auquel on appartient (son espèce, son pays,
sa classe sociale) avant le bien des relations, considérées comme
secondaire ». C’est le berger qui ne peut pas entendre Sarchiapone
parler car il le considère comme animal, donc différent, et décide de le
manger, pour son intérêt humain.

Evidemment, Baptiste Morizot se positionne pour la diplomatie des
relations. Mais je souhaite tout de même m’attarder sur la notion de
diplomatie des termes. De manière inattendue, je me suis surprise à
tracer un lien avec l’ouvrage de Brigitte Vassallo, *Idéologie monogame.
Terrorisme polyamoureux*. Souvenons-nous, elle soulignait l’analogie
entre la monogamie et la Nation, reposant tous deux sur la
différenciation pour créer un sentiment d’appartenance. La différence
des genres pour nos relations intimes crée le noyau reproducteur, les
différences de race comme mythe construisent la nation, la différence
entre « Nature » et culture fonde l’anthropocentrisme. Ces frontières
sont amovibles et définies par les groupes au pouvoir de manière
contextuelle, notamment en désignant un « eux » pour renforcer le
« nous ». Elles réutilisent les mécanismes de l’exclusivité, de la
hiérarchisation et de la confrontation pour tenir les groupes ensemble,
c’est-à-dire les uns contre les autres.

Selon Baptiste Morizot, cette conception d’un *nous* à défendre
vis-à-vis des *autres*, repose en partie sur une compréhension erronée
de Darwin et de sa théorie de la sélection naturelle comme « loi du plus
fort ». Nous retrouvons ici l’héritage de Kropotkine, qui s’élevait dans
*L’entraide, un facteur de l’évolution* (1902) contre les lectures
fallacieuses de Darwin et leur application au monde social. Selon lui,
non seulement la compétition n’est pas le seul moteur de la survie des
espèces, mais la coopération jouerait un rôle plus important. Il se base
sur une relecture des écrits de Darwin, pour écrire « qu’admettre une
impitoyable guerre pour la vie, au sein de chaque espèce, et voir dans
cette guerre une condition de progrès, c’était avancer non seulement une
affirmation sans preuve, mais n’ayant pas même l’appui de l’observation
directe ».

De la même manière, la relecture de Darwin par Baptiste Morizot nous
invite à considérer que c’est l’insertion dans un champ de relations
variées, stables, dont nous dépendons, qui assure notre survie. Elle ne
se joue donc pas dans la lutte mais la dépendance plurielle. C’est un
renversement de la vision de l’évolution, mais aussi des rapports
sociaux. Et encore une fois, l’analogie avec les réseaux affectifs de
Brigitte Vassallo se tisse dans mon esprit. Et si la métaphore des
rhizomes pour symboliser l’ensemble des relations qui nous soutiennent
n’était pas seulement une image ? Et si nous tentions de nous réinsérer
dans le vivant, et par cela, d’élargir notre conception du *nous* pour
prendre soin des autres espèces comme des partenaires, amis et
complices ?

Bibliographie

**Livres** :

DAMASIO, Alain, 2019. *Les furtifs*. Clamart : la Volte. ISBN 978-2-37049-074-2. 
803

DESCOLA, Philippe, 2015. *Par-delà nature et culture*. Paris : Gallimard. Collection Folio, 607. ISBN 978-2-07-046587-3. 
304.2

HARAWAY, Donna Jeanne et GARCÍA, Vivien, 2020. *Vivre avec le trouble*. Vaulx-en-Velin : les Éditions des Mondes à faire. ISBN 978-2-9555738-4-6. 
304.201

KROPOTKINE, Pierre, GARCIA, Renaud et GUIEYSSE-BRÉAL, Louise, 2020. *L’entraide: un facteur de l’évolution*. Paris : Nada. ISBN 979-10-92457-39-1. 
320.570 1

LECERF MAULPOIX, Cy, 2021. *Écologies déviantes: voyage en terres queers*. Paris, France : Cambourakis. Sorcières. ISBN 978-2-36624-599-8. 
HQ76.5 .L43 2021

LONDON, Jack et KLEIN, Frédéric, 2021. *L’appel de la forêt: texte intégral*. Paris : Flammarion jeunesse. ISBN 978-2-08-024530-4. 
809

MORIZOT, Baptiste, 2016. *Les diplomates: cohabiter avec les loups sur une autre carte du vivant*. Paris : Éditions Wildproject. Collection « Domaine sauvage ». ISBN 978-2-918490-55-5. 
MLCS 2018/47986 (Q)

VASALLO, Brigitte, 2018. *Pensamiento monógamo, terror poliamoroso*. Cuarta edición, revisada. Madrid : La Oveja Roja. Ensayo. ISBN 978-84-16227-24-2. 

ZAPPINO, Federico, CARISTIA, Stefania et DESCOTTES, Romain, 2022. *Communisme queer: pour une subversion de l’hétérosexualité*. Paris : Éditions Syllepse. ISBN 979-10-399-0039-3. 
306.76

**Films** :

Rossi, Pietro Marcello, *Bella e perduta*. Italie : Fandango, 2015. 

**Web** :

LA FERME DES RENOUÉES | RENOUER, [sans date]. [en ligne]. Disponible à l’adresse : https://www.renouer.org/la-ferme-des-renouees/
Renouée du Japon, 2024. *Wikipédia* [en ligne]. Disponible à l’adresse : https://fr.wikipedia.org/w/index.php?title=Renou%C3%A9e_du_Japon&oldid=216525806

## Nous n’aimons pas la nature

(éçè)

Après la partie théorique de Margaux, je ressens le besoin de descendre
à un niveau d’analyse concret vis-à-vis de nos relations au vivant
non-humain. Nous ne pouvons plus nous permettre de jouer contre la vie,
alors je vais dire les choses plus crûment : il n’est plus
possible de penser que l’on aime la nature sans penser ce que « aimer »
veut dire.

Aujourd’hui, nous sommes loin d’avoir une source d’énergie infinie,
immatérielle et disponible pour alimenter des machines. Nous avons
besoin du reste du vivant. Il est tentant de croire que nous pouvons
nous passer de lui, mais c’est un pari risqué. Pourtant, c’est celui que
fait notre société, en lui laissant toujours moins de place, dans des
écosystèmes abîmés. L’humanité parie son confort et son avenir sur la
capacité de toutes les autres espèces à s’adapter au monde que le
Capitalocène façonne. Nous avons déjà « éradiqué les ⅔ des populations
d’insectes, les ⅔ des mammifères sauvages, les ⅔ des populations
d’arbres. Cette disparition ne peut pas être considérée comme un danger
pouvant induire une catastrophe. Elle est, en tant que telle, la
catastrophe ».

Ou bien c’était peut-être les plus faibles. Nous n’aurions fait
qu’accélérer une sélection naturelle inévitable. Sauf que les espèces
disparues, en plus de ne plus faire leur vie qui nous rendait
d’immenses services, libèrent des niches écologiques. Une fois vacantes,
elles sont occupées par les espèces invasives. Comme en parle Margaux,
nous pourrons souvent nous entendre. Mais parfois, ce sera plus
compliqué. Je parie que certain·es ne serait pas contre l’idée d’un
futur où notre principale activité sera la lutte pour les écosystèmes
contre d’autres humain·es, des fourmis et des frelons. Sans oublier les
bactéries, virus et champignons qui sauront se joindre à la fête.

Pour éviter cet avenir, il est urgent de créer des relations avec les
non-humains. Ces relations doivent être suffisamment fortes pour être
des arguments valables face au capitalisme et à la destruction qu’il
entraîne.

À ce jeu, les plus riches ont un impact démesuré. Je ne m’adresse pas à
elleux. Iels ne peuvent pas être convaincu·es par les mots. Iels
semblent même prendre plaisir à ce que des scientifiques viennent les
critiquer. Leur niveau de déconnexion de la réalité des destructions est
trop fort, tout comme leur capacité à s’imaginer qu’iels pourront se
protéger des catastrophes par leurs propres moyens. Je veux bien
imaginer que certain·es sont en fait terrifié·es, ne pouvant que se
raccrocher aux branches qu’iels connaissent déjà. Et alors ? Il n’est
pas nécessaire de perdre plus de temps à négocier avec elleux. Il nous
reste à les affaiblir, par les actions politiques, en démantelant ce qui
peut l’être : pipe-lines, yachts, usines ou moteurs… Cela
demandera de l’organisation et une sacrée dose de courage.

Reconnaître leur culpabilité ne doit pas nous servir à nier nos
responsabilités individuelles. Nous avons participé activement à la
guerre contre le vivant. Nous l’avons fait en nous convaincant que la
nature était sale, que nous devions tout faire pour qu’elle soit bien
propre, bien droite. Nous avons coupé les tiges qui dépassaient de
l’herbe, nous avons mis la terre à nu dans les parterres, nous avons
mis du glyphosate sur les terrasses, de l’anti-limace dans les potagers,
nous avons coupé les vieux arbres, nous avons repoussé les vivants en
recouvrant les sols… et nous l’avons fait en disant que nous aimions la
nature. Certes, nous avons été conditionné·es par la force des communicant·es. Mais maintenant nous savons, alors si nous voulons espérer 
qu’il se passe quelque chose au niveau collectif, il faut immédiatement 
commencer à lutter contre ces imaginaires.

Un exemple pour illustrer mon propos. Le weekend dernier, j’étais en
Alsace dans une ville de taille moyenne et le kayak club du coin
organisait une tombola. Le principe, déjà vu ailleurs, était une course
de canards en plastique, où lae propriétaire du canard gagnant
empoche le premier lot. Le départ se fait attendre. Au micro, deux
adhérent·es du club essayent de mettre l’ambiance, tout en rassurant sur
le côté écologique de la course. On ne peut pas en attendre moins des
pratiquant·es d’une activité où l’on fait corps avec l’eau et pour qui
il est important de protéger les rivières et leurs habitant·es.
Plusieurs fois iels répètent que c’est une « course ZÉRO déchet », que
tous les canards seront ramassés, « que le club organise deux fois par
an un nettoyage des rivières », etc. C’est super ! J’imagine que lâcher,
même 6 000 canards, qui vont flotter sur 100 mètres de rivière, ça
n’abîme pas grand-chose. Même les récompenses sont éthiques : le gros
lot c’est un vélo électrique !

Et là, au moment du lâcher des canards depuis un pont, des feux
d’artifice (installés par les organisateur·ices) sont allumés en
direction de la rivière. Une vingtaine de gerbes d’étincelles partent
directement dans l’eau et sur les berges où elles déclenchent un petit
feu. Oui, c’était joli. Mais à quel moment des « amoureux·ses » de la
nature se disent que c’est une bonne idée d’envoyer des feux d’artifice
dans une rivière qu’iels aiment ? Oui, la dilution fait que l’impact
est minime. Comme l’impact environnemental de chacun·es qui ne
représente que le pouième du total. Tout comme l’impact de chaque
secteur de l’économie ou de chaque État, qui finalement ne représente
pas grand-chose de la production mondiale. Cet évènement est à la fois
anecdotique et révélateur. Ces feux d’artifice étaient jolis, mais
absolument inutiles et ne laisseront aucun souvenir qui en vaille la
peine. Par contre, ils laisseront une pollution qui mettra des années à
disparaître.

Un autre exemple, le magazine *Outdoor*, sous-titré « Rando, vélo,
Kayak… La nature en mouvements » dans son édition de
Juillet/Août/Sept.2023 *Ultimes Aventures*, propose à celleux qui
aiment la nature de partir dans les Vosges, mais aussi en Tasmanie ou
dans les îles Féroé. Alors que les ultra-riches font des croisières de
luxe en Antarctique, laes sympathiques randonneureuses se disent qu’iels
ont aussi le droit à l’exceptionnel. Qu’iels peuvent bien partir au
bout du monde voir des paysages encore sauvages. Iels vont acheter du
matériel éthique, sans manger de viande et en prenant le train pour
aller à l’aéroport. Iels se réjouiront de la beauté des parcs naturels,
en faisant abstraction de leur nature coloniale. Et de l’impact délétère
de l’ensemble de leur voyage pour cette nature qu’iels pensent aimer.
Iels n’aiment pas la nature, iels aiment les paysages, qui doivent être
protégés pour leur beauté, mais rester accessibles aux touristes. Iels
n’aiment pas les autochtones, iels aiment voir de la différence exotique.
Iels sont un pendant moderne des hippies que dénonce bell Hooks (Margaux
en a déjà parlé). Je suis un peu iels. Iels pensent aimer, mais sont
justes intéressé·es. Ce n’est pas de l’amour, c’est une relation
toxique.

#### L’amour ne supporte ni violence ni domination

Aimer la nature n’a rien à voir avec la pratique d’activités
extérieures même si elles sont bien faites. Ce n’est pas l’exploiter
sans se soucier du futur, vouloir la mettre sous cloche ou la contrôler
pour son plaisir.

Aimer, c’est offrir son amour en offrande, en espérant que l’autre
l’accepte. Le faire vis-à-vis de la nature, cela manque de sens. Ce
n’est pas un tout uniforme. Elle est peuplée de formations géologiques
et organiques, de mécanique des fluides et vit par les transformations
des vivant·es qui la pratiquent. Il n’y a aucun sens humain à penser
aimer la nature, personne n’aime la grippe ou la bouse. Même aimer une
espèce n’a aucun sens, on peut apprécier certains individus avec qui
l’on interagit, mais pas l’espèce, sauf à l’essentialiser.

Si nous ne pouvons pas aimer la nature, il nous faut penser d’autres
liens en partant de ceux qui existent déjà. Pour créer des relations, il
faut des interactions multidirectionnelles qui fassent sens. Celles-ci
peuvent être matérielles ou en partie fantasmées, ce n’est pas grave. Le
monde moderne dans lequel nous vivons nous a fait perdre presque tout
lien d’interaction avec des vivants non-humains, les privant de toute
intériorité et donc des possibilités d’avoir un intérêt individuel ou
collectif.

Si nous n’avons plus aucune idée de comment nous comporter avec les
autres *bestioles* (par-là, j’entends vivant humain et non-humain),
c’est que cela n’a aucun intérêt pour le capitalisme. C’est même néfaste
pour lui, car en sachant comment nous comporter, nous risquerions de
créer des relations. Or qui relationne peut aimer et qui aime peut
vouloir se battre. Ainsi, nous pourrions défendre les vivants, pas pour
ce qu’iels représentent d’abstrait, mais parce que nous tenons à elleux.
L’attachement est une émotion puissante, capable de s’opposer aux
sirènes du productivisme.

Historiquement, le capitalisme a eu besoin de détruire tout ce en quoi
nous pouvions croire. Les sciences, sous toutes leurs formes, l’ont aidé.
Proclamer la primauté de la rationalité et qu’il n’y ait *Ni Dieu, Ni
Maître* devrait être une avancée incontestable contre les dominations.
Finalement, nous avons moins de Dieu, mais toujours quantité de Maîtres.
Et iels ont acquis une puissance démiurgique. Je ne fais pas référence à
l’idée qu’il existe un complot quelconque. Ce sont simplement des
intérêts communs qui ont réussi à asservir le vivant. Je pourrais aussi
comme dans Joie Militante parler de « *L’Empire*, comme le régime de
destruction organisée sous lequel nous vivons. \[…\] L’Empire
administre une guerre aux autres formes de vies ». Ce n’est pas un
individu, ni quelques-uns, ni beaucoup, c’est un système.

Il a existé des idéaux qui ont pu lui tenir tête, parce que nous y avons
cru. La croyance dans le communisme a été un puissant contrepoids. Les
expériences d’autonomies anarchistes ont été des respirations, mais
n’ont pas fait le poids. Les victoires ici et là contre *l’Empire*
doivent être partagées, un gros travail a été fait autour de Notre Dame
Des Landes, mais leur impact sur nos consciences est resté marginal. En
plus, comme en parlera Margaux, les expériences de ZAD, même si elles
permettent de garder un rapport de force, ne sont pas à l’abri des
reproches.

En sus, il faut nous attaquer à nos croyances, pas nos seulement
imaginaires, aller chercher plus loin, plus profond. C’est là que
*l’Empire* s’est niché. Il a remplacé toutes les autres croyances. Nous en
lisons les augures dans les trends Twitter. Tout son édifice est basé
sur la croyance qu’il va continuer, qu’il est irremplaçable, que les
actifs d’aujourd’hui auront toujours de la valeur demain. Nous croyons
et nous en sommes récompensés. Nous ne comprenons pas comment
fonctionnent concrètement les lignes d’approvisionnement qui font venir jusqu’à nous
de l’eau, de l’énergie sous diverses formes, des biens, des services et
des divertissements. Tout cela s’arrange comme par magie et c’en est presque de l’art.
L’important demeure que nous sommes persuadés que demain, il y a aura des
produits dans les magasins. Ceux que nous apprécions, nous l’avons vu
avec la pandémie. Si nous sortons de la zone de confort des
organisations, que la machine se grippe sur quelques biens et dans ce
cas-là, la croyance s’amenuise, le zbeul n’est pas loin !

En temps normal, nous dédions nos vies à respecter scrupuleusement les
dogmes. Je ne blâme personne, je fais pareil. Nous y croyons toustes et
cela fonctionne. Nos prières sont récompensées : nous
obtenons de quoi sustenter nos appétits ou au moins l’idée que nous
pourrions le faire si nous étions plus riches… La droite nous rappelle
régulièrement qu’il ne faut rien faire qui puisse gripper le système,
que nous devons rester dévoué·es au travail et qu’ainsi notre pouvoir
d’achat finira par s’améliorer. Qu’en cas de blasphème (trop de dettes,
irrespect envers les marchés…) *l’Empire* peut nous punir, arrêter de
remplir les magasins, refuser notre monnaie pour nous apprendre à ne
plus nous rebeller (les Grecs l’ont vécu). Heureusement cela arrive
rarement dans nos pays riches, alors nous pouvons continuer à croire et
à recevoir des biens et services.

Malheureusement, nous avons cru trop longtemps et trop fort. Maintenant
les limites planétaires ont toutes été dépassées. Nous avons cru que
l’océan était si grand, l’atmosphère si immense que nous ne pourrions
jamais les modifier significativement. Trop tard, nous l’avons fait.
Certains savaient depuis les années 70 que cela allait arriver. Tout a
été fait pour nous maintenir dans le doute. Encore aujourd’hui, nous
continuons à croire que cela va aller, qu’il n’y aura pas grand-chose
à changer pour continuer à vivre nos vies.

Cette perte d’attachement envers les non-humains ne s’est pas faite
seule. D’abord il a fallu ouvrir nos imaginaires, jusqu’à en arriver
« à permettre pour la première fois aux gens du commun de briser
l’autarcie psychologique, de se constituer un imaginaire marchand et
d’entretenir par l’image de nouveaux désirs de consommation ». Ainsi
nous n’étions plus dépendant de la nature, du contrôle social des
parents, des autres, c’était une révolution, une libération.

Après la Deuxième Guerre mondiale, il a fallu couper le « lien
fusionnel » entre les paysans et la terre, pour la transformer en une
« *pure ressource productive* ». L’objectif était le remembrement des
parcelles pour les mécaniser et en augmenter la productivité. Pour
rendre cela possible, comme l’écrit Stépanoff dans *L’animal et la
mort*, il a « fallu que les eaux et les arbres perdent d’abord leur
sacralité pour pouvoir être arasés. Il a fallu que la terre cesse d’être
le substrat de relations sociales avec les vivants, les ancêtres et
l’invisible, pour qu’elle puisse devenir une simple Nature, dépouillée
de ses prestiges, disponible pour sa mise au travail rationnel ». Nos
grands parent·es sont allé·es travailler dans les villes. Les humains,
plutôt que de continuer à négocier avec les non-humains, ont eu le
pouvoir de les soumettre et d’imaginer se passer d’elleux. Et nous
n’avons presque plus interagi. Nous avons fait la guerre. La nature est
devenue un décor et une ressource.

Finalement, celleux qui ont encore une vraie relation avec des
non-humains sont celleux qui prennent des risques pour protéger les
vivants contre le productivisme, qui passent du temps pour les observer en
voulant les comprendre, à prendre soin d’elleux. Parce qu’aimer les
non-humains, ce n’est pas tout à fait comme aimer les humains. Nous ne
sommes pas de la même espèce. Nous nous questionnons sur leur sentience
(capacité d’éprouver des choses subjectivement, d’avoir
des expériences vécues) alors qu’aujourd’hui nous ne le faisons plus
pour les humain·es. Vis-à-vis des non humains, il ne s’agit peut-être
pas de parler d’amour. Pour celleux qui forment des alliances afin de
créer de la nourriture hors de l’extractivisme, celleux qui chassent en
relationnant hors des normes capitalistes, il me faut convoquer ici
d’autres concepts comme la dignité, la sollicitude et l’amphibiose (ils
seront précisés en temps utile).

Pour mieux m’expliquer, l’actualité me fournit un superbe exemple avec les
militant·es en grève de la faim ou de la soif contre l’autoroute A69.
Mettre en danger sa vie et être prêt·e à assumer dans son corps les
conséquences de sa lutte me semble pouvoir être vue comme une marque
d’amour. De même, étudier des espèces, les suivre, les compter, savoir
les différencier demande d’offrir à ces vivants une immense attention,
sans rien attendre en retour. Cela me semble être une marque d’amour.
Pas pour la nature mais pour certains lieux et les vivants qui y
résident.

Pour ce qui est de celleux qui produisent de la nourriture, que ce soit
à travers le travail du sol ou de l’élevage, la ligne est plus
complexe. Il y a là une forme de domination et de violence. Nous entrons
dans une zone grise des relations interspécifiques, qui est marquée du
sceau de la nécessité. Comme le dit Morizot dans *L’inexploré* en
parlant des non-humains, pour certains d’entre eux, ils seront toujours
en partie des moyens (il faut bien vivre), mais pourraient devenir *plus
seulement* cela.

Il faut bien vivre. Ce point fait d’ailleurs dissension entre des
auteurices connu·es comme Morizot, Latour ou Despret et le courant
animaliste. Nous avons besoin de récolter des plantes et vu
le nombre d’humain·e présent·es sur Terre, nous n’avons d’autres choix
que de faire des cultures, donc de soumettre à notre volonté des
terrains et des végétaux. Pour ce qui est de l’élevage, la question est
plus difficile, du moins dans les pays occidentaux où cette industrie ne
serait pas nécessaire pour nous nourrir. Par contre, il est nécessaire
par exemple que des prairies soient broutées pour éviter que la forêt ne
les occupe, mettant en danger de nombreuses espèces. Puisqu’il n’existe
plus assez de non-humains libres pour le faire, je ne vois d’autres
solutions qu’une sorte d’élevage pour cela.

Un élevage qui ne nécessiterait pas l’abattage pourrait avec le temps
permettre de libérer ces animaux de la domination humaine. Imaginons de
façon pragmatique que nous continuions à faire de l’élevage pour
maintenir les paysages ouverts (vis-à-vis des forêts fermées) et éviter
la disparition d’habitats. C’est là une vie agréable qui leur serait
offerte en échange de leur aide et de leur excrément dont une partie
remplacerait les engrais de synthèse. De grands espaces protégés des
prédateurs, des abris pour l’hiver et de la nourriture à foison ! Une
modalité d’alliance me semble ici envisageable.

Est-ce que le fait de s’occuper d’êtres vivants pour ensuite les tuer
empêche de les aimer… Probablement. Notre démarche repose précisément
sur le fait de ne pas dévoyer l’amour. Même s’il est tentant de dire
qu’un·e éleveur·seuse aime ses bêtes en sachant qu’elles se feront
abattre, ce serait accepter une immense contradiction. Pourtant, hors de
l’élevage intensif, la relation affective et interactive aux animaux va
de soi. Accompagner un·e vivant·e de sa naissance à sa mort, ce n’est
pas rien. Le faire au mieux en résistant à l’appel de
l’industrialisation mérite de la reconnaissance. Cette relation devrait
être nommée.

Si l’amour est exclu, nous vous proposons (l’idée est de Margaux), d’y
voir de la sollicitude en la définissant comme des « soins attentifs et
affectueux, constants, prodigués envers une personne ou une
collectivité ». Cette modalité de relation peut, il me semble, convenir
sans être dégradante. Se lever la nuit pour s’occuper d’un bébé qui
n’est pas le sien, prendre soin d’une bestiole de sa naissance à sa
mort, cela correspond bien à la définition de la sollicitude. S’occuper
d’un troupeau nécessite de passer beaucoup de temps au contact de ces
êtres vivants, de les connaître et de vivre des liens avec les
individualités qui le composent, de faire attention à leurs relations
entre eux et à celles avec d’autres non-humains.

De ce travail qui met en jeu les interactions entre les humains, les
non-humains et les lieux, de cette matrice relationnelle, l’amphibiose
peut naître. Elle dépasse le cadre de l’élevage pour se tourner vers les
autres vivants, celleux qui vivent libre des humain·es. C’est comme le
définit Stepanoff, « un mode relationnel ambigu et souple qui ne se
laisse figer de façon définitive ni dans l’amitié ni dans l’hostilité.
En reprenant cette métaphore, on peut dire que l’attitude des
paysans-chasseurs à l’égard de nombreuses espèces sauvages
potentiellement plus ou moins contrariantes est une forme d’amphibiose :
ni une volonté d’extermination guerrière ni une interdépendance sans
ombre, mais une cohabitation à bonne distance, mêlant selon les
circonstances, respect, consommations réciproques, défense et
représailles en cas d’empiétement ». On peut la retrouver dans les
témoignages d’agriculteurices engagé·es : « On s’attache plus à
l’écosystème général qu’à des espèces endémiques ou rares. On ne fait
pas le tri. À la ferme, tout ce qui vit compte ». Ce n’est pas une
relation individuelle, mais un véritable acte politique : « Grâce à mon
travail, je peux soustraire ces espaces aux logiques destructrices du
vivant ».

#### Les prémices de relations biotiques

Il s’agit de trouver quels types de relations nous pourrions développer
avec les non-humains. Pour commencer, nous devons prendre conscience des
problématiques liées à nos deux principaux modes de relation qui sont
ceux d’*animal-enfant* et d’*animal-matière*, pour ensuite pouvoir
en dessiner d’autres.

L’animal-enfant c’est « l’animal de compagnie qui est nourri, intégré à
la famille humaine, toiletté, médicalisé, privé de vie sociale et
sexuelle avec ses congénères, rendu éternellement immature par une
castration généralisée ». Les animaux de compagnie ne sont pas une mince
affaire. « En 2010, en
[**France**](https://fr.wikipedia.org/wiki/France), un sondage révèle
que 48,7 % des foyers posséderaient au moins un animal de compagnie,
chiffre en constante baisse comparé à 2006. Soit 59 millions d’animaux
familiers en 2010. Si le nombre de chiens, petits mammifères et poissons
est en baisse, en revanche le nombre de chats et d’oiseaux (6,04
millions, soit 70,6 % de plus qu’en 2008) augmente.

L’appellation d’*animal-enfant* est particulièrement bien trouvée, mais
il lui manque la prise en compte du statut actuel de l’enfant qui a
acquis une intériorité propre et sur qui les violences éducatives sont
maintenant proscrites. Pour reprendre la culture geek, je vous propose
donc que nous les nommions nos familiers. Dans un jeu vidéo, un
familier est souvent un animal, qui suit le joueur, lui sert pour certaines 
tâches, lui obéit et qui peut être sacrifié, tout comme nous pouvons le faire
avec nos animaux si iels nous posent soucis. Cela n’empêche pas de
développer de l’attachement à son égard, mais cela reste un esclave
non-humain, utile et décoratif. Notre société artificialisée produit des
non-humains qui, leur vie durant, seront soit inadaptés soit soumis par
le dressage. Si iels deviennent une charge, nous avons le droit de les
faire tuer sans autre forme de procès.

Nous pouvons les pleurer, les incinérer comme nous le ferions pour nos
proches humains, pourtant nous ne pouvons pas dire que nous les aimons.
Comment serait-il possible d’enfermer toute sa vie dans un appartement,
un être aimé, de le castrer, de l’empêcher de sortir quand iel le veut,
de relationner avec son espèce, de nier son autonomie ?

Historiquement, les humains ont optimisé des non-humains pour qu’iels
les aident au quotidien. Parce qu’un animal est une bouche à
nourrir, sans l’industrie agroalimentaire, avoir un chien de compagnie
constituait un poids. Maintenant, l’immense majorité des animaux que
nous côtoyons n’ont pour seule utilité que d’être des décorations dont
les désirs ne sont pas pris en compte. Nous ne pouvons pas aimer tout en
niant l’autonomie de l’autre et en contrôlant étroitement sa
sociabilité. Et ce n’est pas une question individuelle, notre organisation
sociale nous oblige à être maltraitant·es envers les animaux
domestiques.

Aimer des non-humains, ce n’est pas un objectif en soi. Une bonne
entente, voir même de l’affection, cela ne nécessite pas de l’amour. Par
contre si nous nous soucions réellement du bien-être des animaux de
maison, nous devrions essayer de comprendre ce qu’iels veulent. Il
semble que ce n’est pas ce que nous leur donnons aujourd’hui. Une étude
récente (citée par Charles Stépanoff dans *Des hommes et des chiens : modes de
vie partagés et coopération cynégétique*) est « venue souligner que la
faculté d’accomplir librement des choix, d’être autonome et d’utiliser
ses capacités olfactives dans des tâches sont des dimensions
essentielles du bien-être du chien. Or celles-ci trouvent difﬁcilement
satisfaction dans le mode de vie captif associé au statut d’animal de
compagnie » (*Duranton & Horowitz 2019*).

Il est probable qu’il en soit de même pour les autres animaux de maison.
Des sociétés humaines arrivent à donner une place aux chiens comme les
« Evenki \[qui\] sont un peuple autochtone de chasseurs et éleveurs de
rennes en Sibérie. De leurs chiens de chasse, ils attendent une capacité
à agir en partenaires et à prendre leurs propres décisions. Un bon chien
Evenki n’est pas issu d’une reproduction contrôlée ni du dressage, mais
d’une socialisation auprès des enfants qui n’est pas dénuée de
brutalité. Les chiots trop faibles meurent, ceux qui sont trop agressifs
sont tués. \[…\]Les observateurs sont frappés par « l’extraordinaire
harmonie avec laquelle l’homme et le chien coordonnent leurs actions,
comme si un ﬁl invisible les connectait. » (*Safonova & Sántha, 2013*).
Selon un autre auteur, les Evenki regardent leurs chiens comme « des
êtres férocement indépendants, ﬁers et intelligents, ayant droit à leur
propre autonomie. » (*Anderson, 2014*).

Ou bien, chez les aborigènes australiens : « Le manque de traits du syndrome
de domestication chez les dingos ne signiﬁe pas que les dingos ne soient
pas en relation étroite avec les humains pendant une grande part de leur
vie. Cela indique plutôt que, du fait de la philosophie écologique et
cosmologique des Yolngu, les relations humains-dingos exigent que les
dingos adultes gardent des vies indépendantes, maintenant le contrôle
sur leur reproduction, même s’ils vivent avec les humains » (*Fijn,
2018*).

À vouloir considérer les chiens ou les chats comme des presque humains,
mais tout de même des animaux, nous cloîtrons leur volonté en les
obligeants à respecter en tout temps les règles sociales humaines. Que
dire des millions de poissons dans des aquariums et des oiseaux dans des
cages ? Iels ne bénéficient même pas de tendresse.

Dans son article cité précédemment, Charles Stépanoff met en avant
le cas des équipages de vénerie utilisés pour la chasse à courre comme
un mode de lien qui n’interdit pas une vie sociale aux chiens (cet
auteur semble assez fan de la chasse à courre). Cette pratique est
peut-être intéressante d’un point de vue ethnographique, mais hors de la
seule question de la chasse, c’est une pratique bourgeoise que l’on ne
peut pas imaginer fonctionnelle au niveau de la société. Néanmoins c’est
intéressant de se rappeler qu’en France les chiens de fermes, de
troupeau ou de chasse peuvent avoir une existence qui leur permet d’user
de leur capacité de chien. Est-ce que le bonheur du chien repose plutôt
sur la possibilité d’éprouver la liberté ou sur le droit à se coucher
sur le canapé et d’être un *animal de maison* ?

Il me faut ajouter que les animaux de maison dépendent des « 
*animaux-matière* » pour vivre. L’animal-matière c’est « l’animal de rente,
éloigné des habitations humaines, désocialisé dans des bâtiments
industriels, réduit à une fonction productive ». Ce sont les animaux que
filme L214, qui vivent dans des conditions ignobles. Pourtant, sans eux
et la viande à bas prix qui composent leurs corps, nous aurions bien du
mal à nourrir nos familiers, pour qui la chasse est proscrite mais qui
restent carnivores. C’est un des fils qui vient quand on tire la pelote
des réflexions.

Pour enfermer nos animaux de maison, nous avons besoin d’enfermer des
animaux-matière. Vis-à-vis d’elleux, nous n’avons aucune forme de
relation. Nous ne leur dénions pas seulement la sentience mais jusqu’à
la nature de vivant avec des droits. Ce sont des machines biologiques
optimisées pour la production de lait, de viande et bientôt de méthane.
Pour L214, « 8 animaux abattus sur 10 viennent d’un élevage intensif ».
Ces élevages sont clairement minoritaires en nombre, mais produisent une
majorité des cadavres qui nous nourrissent, au point que « 60% des
animaux d’élevage sont concentrés dans ces fermes qui représentent 3%
de l’élevage ». Changer notre rapport aux animaux que l’on pense aimer,
c’est aussi agir pour que d’autres ne vivent plus cet enfer. Si nous
voulons continuer à relationner avec des chiens, des chats et des
rongeurs, il faut que la collectivité en assume les conséquences. C’est
à dire que nous décidions d’adapter notre société pour qu’elleux y aient
une place, qu’iels soient considéré·es comme des sujets, avec des désirs 
propres et des droits.

Une forme de relation possible est la *respons(h)abilité* de Donna
Haraway qui apparaît « quand on tombe sur quelques proches (ne faisant
pas partie de notre famille biologique), on engage la conversation, on
pose des questions intéressantes, on répond à d’autres. Ainsi on
propose, ensemble, quelque chose d’inattendu et on accepte des
contraintes que l’on n’avait pas demandées, mais qui découlent de la
rencontre ».

Cette déconstruction du statut d’animal de maison va de pair avec
d’autres : la fin de l’utilisation de non-humains comme objet décoratif
(aquarium…), comme marqueur social, comme prémisse à avoir un enfant
et en particulier la production de ces animaux pour en faire commerce.
Le commerce lié aux animaux de maison en France représente 5 milliards d’euros
par an, 100 millions de tonnes d’aliment et 100 000 abandons. Ce sont
des foires au chiot, des élevages, du dressage, des jouets, des
mutuelles, bref un business capitaliste comme un autre. Cela nécessite aussi des
tonnes de médicaments dont une une partie est rapidement rejetées dans
l’environnement, tels les néonicotinoïdes dans les antiparasitaires.

Un des malheurs, c’est qu’il n’existe aujourd’hui, pour la majorité
des gens, aucun autre moyen de se lier à un animal que de le posséder. Cette
construction sociale ne peut pas être modifiée à l’échelle individuelle
et constitue potentiellement un grand danger pour les autres
non-humains. Alors qu'un chat bien nourri tue seulement une trentaine de 
proies par an, un chat errant en élimine environ 270. Qu’arriverait-il aux
espèces déjà fragilisées si demain, pour des questions économiques ou de
production de viande, une part significative des 15 millions de chats
domestiques tuait près de 10 fois plus qu’aujourd’hui ? Tout comme nous
allons devoir apprendre à fermer des infrastructures pour éviter
qu’elles ne causent encore des dégâts, nous allons devoir repenser
rapidement notre rapport à la possession d’animaux pour le loisir, pour
ne pas provoquer des catastrophes dans le futur.

Accepter que les non-humains soient libres de leurs mouvements, capables
de partir si iels le désirent, me semble être les premières contraintes
à accepter. Cela inclut qu’iels sachent se nourrir et qu’il existe des
espaces où iels peuvent vivre sans nous, nécessaires prémisses à
l’existence d’une relation d’amour interspécifique. Ne plus se voir
comme maître et chien mais comme des partenaires qui se choisissent, qui
partagent leurs vies, développent aussi des relations avec d’autres
individus. Oui, dans ces conditions nous pourrions avoir une relation
d’amour qui ne soit pas hautement toxique avec des non-humains.

À côté de cela, il faudra bien sûr penser comment vivre avec les autres
non-humains, comment travailler avec elleux dignement. Il faut le
prévoir dans notre monde dévasté pour ne pas provoquer de catastrophes
chez d’autres espèces ou dans les communautés humaines. C’est en partie
le travail des scientifiques de trouver comment rendre cela possible. Par
exemple, il s’agit de redonner de la place à des prédateurs pouvant
réguler certaines espèces sans causer d’extermination. Cela n’irait pas
sans difficulté, et il nous faudrait assumer de tuer des non-humains
devenu·es agressives, mais c’est peut-être le prix à payer pour que nous
leur rendions la liberté à laquelle iels ont le droit.

D’ici là, il est possible de relationner avec des non-humains libres et
ainsi de générer des pratiques de *respons(h)abilité*. C’est déjà faire
attention, nous intéresser et regarder (on peut repenser aux merles
que Margaux a mentionnés). Apprendre comment aider les non-humains qui
sont là, dans notre environnement proche. Nos actions individuelles
pèsent peu face aux destructions causées par les grands travaux et les
productions dont dépendent nos modes de vie. Néanmoins, les relations
entre individu·es sont personnelles. C’est à chacun·e de nous de
s’ouvrir à l’idée de relationner avec des non-humains. Cela commence
par changer notre vision du propre, du contrôle et de l’autre. La
*respons(h)abilité* peut apparaître en laissant les araignées vivre dans
nos maisons. Elles ne présentent souvent aucun risque pour les humains
et mangent quantité d’insectes désagréables. Elles demandent à ce que
nous les laissions un peu tranquilles. C’est diminuer le contrôle sur
nos espaces en utilisant moins de produits agressifs, qu’ils soient
« chimiques » ou « naturel ».

Accepter les contraintes peut aussi passer par le fait de produire moins
de lumière en extérieur la nuit, ce qui perturbe de nombreux insectes.
Les jardins privés représenteraient 2% de la superficie totale en
France, soit 4 fois la superficie de toutes les réserves naturelles du
territoire national. Nous vivons dans un pays plein de pavillons avec
jardin. Cela va avec des soucis, mais c’est aussi une chance. Un jardin
n’a pas à être une monoculture de gazon, certains peuvent être aussi
riches en terme de biodiversité. Cela tient à la manière dont ces
espaces sont gérés. C’est une zone que nous pouvons en partie offrir aux
non-humains, pour expérimenter notre rapport au féral et apprendre à
faire de la *négociation contributive*. Cela nécessite que nous fassions
un pas vers elleux, d’accepter de les laisser faire pour ensuite
les soutenir. L’encouragement, c’est déjà être accueillant·es en
créant des lieux de rencontre où être ensemble, même si l’on ne se dit
rien, dans l’espoir de s’apprivoiser mutuellement.

En acceptant les autres non-humains, nous nous confrontons à l’altérité,
au fait d’accepter que les autres agissent différemment, ici, « chez
nous ». Dans un espace où en réalité nous avons seulement planté notre
drapeau avant de le déclarer nôtre. Il nous revient de le remettre en
partage. Dans le même mouvement, nous modifierons notre rapport à la
propriété privée. En temps normal, dans les territoires que nous nous
sommes appropriés, nous avons l’habitude de faire ce que nous voulons.
en stricte adéquation avec notre notion de la propriété. Dans le cadre de la
*respons(h)abilité*, ce n’est plus le cas. Il nous faut accepter les
réponses aux questions que nous posons. Si notre intervention peut
rester nécessaire, voire utile, nous devrions la mettre au service des
non-humains, comme une offrande qui leur est faite pour les remercier de
leur travail.

Il est possible de pousser le geste jusqu’à installer des mares, à transformer
les espaces que nous avons privatisés en abris collectifs pour les
vivants. Le passage de l’individuel à du petit collectif peut se diffuser
auprès du voisinage, avec la nécessité d’expliquer nos pratiques à celleux qui
vivent à côté. En ville, vu les espaces réduits, cette étape est
encore plus indispensable qu’à la campagne. Avec de la formation, les
habitant·es peuvent à la fois modifier l’environnement, l’améliorer pour
la biodiversité et évaluer elleux-même les effets de leurs actions.
Elles sont souvent simples et économiques, consistant à laisser faire, à
ne pas couper, ne pas arracher, ne pas ramasser.

Il ne faut pas oublier qu’améliorer les conditions de vie des
non-humains à l’échelle d’un quartier aura probablement un effet
gentrificateur. Néanmoins, recréer des espaces et des liens avec et pour
les non-humains est une question de survie pour elleux et pour nous.
Enfin, sans action concertée entre les différentes échelles, il n’est ni
possible de créer des corridors, connexions entre « les 
différents habitats d’une espèce (ou d’un groupe d’espèces
interdépendantes), permettant sa dispersion et sa migration », ni
possible de suffisamment diminuer les agressions contre les non-humains
venant de nos sociétés. Par la régénération de liens avec les *férals*
nous serions à même de politiser leur existence et de porter leurs voix
à travers une multitude d’expériences vécues et partagées au quotidien.

Nous devons redonner aux non-humains une position dans la société. Une
position à la fois légale, mais aussi dans nos esprits. Pour sortir du
*temps mythique* (de Baptiste Morizot) et stabiliser des relations
sociales humaines envers les non-humains, il faut de la pratique mais aussi
des imaginaires. Tout comme nous avons des conventions sociales pour les
interactions humaines, nous apprenons et transmettons comment nous
comporter en présence de tel et tel non-humain. C’est un travail énorme
et dont la valeur monétaire est nulle. Face à la puissance évocatrice
des techniques publicitaires et des relations publiques, nous allons
devoir utiliser des armes puissantes comme les imaginaires, le mythique
et le sacré.

Je suis athée, tendance anti-religieux. Je n’ai eu aucune éducation
religieuse, j’ai fait des sciences pendant le tiers de ma vie : connaître
la bibliographie, avoir une hypothèse, faire un protocole expérimental,
analyser les résultats. Tout n’est pas blanc dans la méthodologie
appliquée dans les labos, mais de ce que j’ai vu, nous ne trichons pas.
Même si des dérives demeurent possibles, il y a une éthique. Je ne crois 
pas aux « énergies », aux coupeurs de feu, au surnaturel. À l’époque où la
zététique a eu le vent en poupe, j’ai regardé des heures et des heures
de débunkage sur des croyances diverses.

Pourtant, maintenant je suis convaincu que nous avons besoin de
nouvelles croyances collectives. Nous en avons besoin pour vivre avec
les non-humains. Il en existe des bribes : on peut citer le cas des
hirondelles, pour lesquelles Stépanoff a retrouvé des croyances encore
existantes en France. Elles sont censées porter chance : « Les gens se
gardent de déranger leur nid ou de les empêcher de s’installer. Si un
couple vient nicher dans un garage, on se doit de laisser la porte
ouverte pour permettre aux oiseaux d’aller et venir nourrir leurs
petits. Un nid installé dans un endroit embarrassant peut être la cause
de véritables cas de conscience ». D’autres ont une image négative comme
le montre Alizé Berthier dans une thèse sur les oiseaux urbains : « la
corneille \[qui\] est peu appréciée des enquêtés, elle est associée aux
représentations négatives attribuées au noir et au corbeau : elle évoque
la peur, le mauvais présage ». Ou bien le pigeon qui étant rencontré
surtout en ville pâtit de l’image de celle-ci, envisagée comme antithèse
de la nature.

Travailler consciemment à créer de nouvelles croyances est une ligne de
crête. Les croyances sont liées aux trois principes élémentaires de
domination tel que défini par David Graeber et David Wengrow : elles font
partie du *contrôle de l’information*, peuvent soutenir le *pouvoir
charismatique* et justifier des *violences*. Il ne s’agit pas de
substituer la domination sur les non-humains par une nouvelle domination
exercée par un clergé humain. Mais le fait est que savoir que nos
sociétés (et nous avec) brûlons notre futur, ne semble pas suffisant
pour nous mettre réellement en action. Cet affect n’offre pas une
perspective heureuse et transformatrice.

Il nous est possible de commencer à prêter une intériorité aux
non-humains. C’est un acte de foi collectif. C’est avec une immense
attention envers les dérives (nombreuses et probables) qu’il nous faut
bâtir des institutions mettant en œuvre de nouvelles structures
relationnelles. Notre déconnexion avec les non-humains étant très
importante, cela peut en premier lieu passer par des récits aux bases
tangibles, pouvant s’intercaler dans nos habitudes culturelles et aider
le vivant. C’est une mission collective, qui nécessite que des
scientifiques travaillent au côté de créateurices, de poète·sses,
d’auteurices pour amender notre vision naturaliste du monde (c’est à
dire qui « *considère que les êtres vivants sont unis par les mécanismes
naturels régulant les corps, tandis que la culture et les institutions
sociales sont le propre de *l’humanité et la distinguent du reste de
l’univers ») avec une dose d’animisme « qui ne réserve pas le statut de
personne aux humains, mais l’accorde également à des non-humains,
esprits, animaux, plantes ». Encore une fois, je vais faire appel à
Charles Stépanoff qui au cours de ses recherches sur des chasseurs français a
publié en 2021 le récit de pratiques animistes à travers des baptêmes de
chasseur·esses.

Ce baptême consiste à maculer le jeune chasseur ou la jeune chasseresse
du sang du premier sanglier, cerf ou chevreuil qu’iel a tué, geste suivi
d’autres cérémonies. Par exemple, le novice doit revêtir la tête de
l’animal ainsi que sa *cape*, c’est-à-dire la peau du dos (cette
pratique a lieu en Picardie, Flandres, Perche, Jura suisse) : « Son
visage disparaît alors sous le mufle sanglant de la bête, donnant
naissance à une étrange figure thérianthropique que l’on force à
s’agiter au milieu des imitations de grognements et de cris animaux dans
un moment d’ensauvagement collectif ». D’autres rituels existent comme
le *botez vanatoresc* en Roumanie où lae chasseur·esses se couche sur le
cadavre de l’animal et est fessé·e avec une branche. De façon moins
violente, il semble que de nombreux chasseur·esses rêvent de chasse,
« tel rêveur était visité la nuit par un cerf d’exception, tel autre par
un chevreuil à la ramure remarquable. Mais, à défaut d’être socialisée,
cette capacité est délaissée et finit par s’estomper ». C’est un point
commun avec chasseurs boréaux, sauf qu’eux y « voient un remarquable
pouvoir d’explorer sur un mode social les mondes non humains, une
compétence qu’ils cultivent et approfondissent tout au long de la vie et
qui gagne donc en intensité avec l’âge. »

Des chasseurs bien français peuvent avoir des rites animistes et une
relation émotionnelle vis à vis des non-humains, au point qu’iels
envahissent leur rêve. Ils savent comment se comporter avec tel ou
tel individu *féral* pour l’approcher. Si elleux peuvent le faire,
pourquoi pas nous ?

Une de leur force est je pense la pratique collective d’une activité,
qui leur donne une légitimité animiste (qui n’est pas forcément nommé
comme cela par les intéressé·e). Le fait d’être ensemble et souvent hors
du regard de non-initié·es permet de se détacher pour un temps des
injonctions naturalistes (dans le sens de mode de relation collectif
dominant). Ainsi, iels peuvent avoir des pratiques qui, hors du monde
des chasseurs, seraient durement jugées. À cela, il faut ajouter que ce
sont souvent des hommes, blancs et un peu âgés, bénéficiant donc de
l’ensemble des protections liées à ce statut, sans oublier celui de
chasseur, qui aident à assumer des pratiques hors norme.

Il nous faut donc créer des collectifs qui pourront développer des
relations et apprendre à se comporter avec les non-humains en leur
prêtant des intentions et en y répondant. Pour donner de la réalité à
ces modes de vie, nous devons arriver à les inscrire dans nos
quotidiens. Piggnochi propose de créer partout des ZAD, c’est une
excellente idée mais dont la mise en œuvre peut s’avérer complexe.

Dans les territoires ruraux, il est plus facile d’avoir de l’espace et
par la puissance de la propriété privée, la possibilité d’en faire ce
que l’on veut, en particulier de l’offrir comme espace d’hébergement. Un
des espaces les plus propices à la vie sont les haies constituées
d’espèces de plantes variées. Par leur position de lisière, elles
permettent de diversifier les habitats pour les animaux et insectes. Une
telle haie, cela s’entretient. Ce serait agréable et
porteur mais insuffisant.

Réellement aider les vivants à habiter la Terre passera par la reprise de
la terre aux immenses machines et aux phytosanitaires qui tuent plus que
les voitures. Pour m’approcher de la proposition de Piggnochi, et la
lier à d’autres, il nous faut « d’une part une perspective de
subsistance d’économie communale, d’autonomie locale sans jamais se
penser extérieur à la société, d’autre part des luttes sociales au
niveau national voire européen pour stopper le déploiement machinique et
capitaliste qui viendra à bout de toutes les initiatives locales » (ou
les restreindra à des niches).

En effet, en retrouvant la capacité de subvenir à nos besoins localement
hors des systèmes machiniques et capitalistes (du moins de s’en
distancier le plus possible), nous pourrons retrouver des marges de
manœuvre dans nos vies. Nous serons moins dépendant·es d’immenses
productions sur lesquelles nous n’avons aucun contrôle et qui ont
*attaché *par le crédit* et les croyances celleux qui les possèdent.

Reprendre des terres pour les travailler, sans les exploiter, y accepter
les intrus, le faire ensemble pour apprendre et surtout partager le
produit de ce travail commun avec celleux qui ont en le plus besoin,
répondre à une partie de nos besoins en nous passant du marché, c’est
gagner en indépendance. C’est avoir moins peur du futur, gagner en
savoir-faire collectif. Ouvrir une voie pour créer des alliances entre
les campagnes et les périphéries, entre des territoires qui ont du mal à
se parler. Avoir une action régénérative pour les non-humains et créer
des alliances inter-espace pour les humains constitue un programme
politique dont peut découler les victoires. Reprendre les terres, permet
aussi de lutter contre des intérêts économiques agricoles et fonciers.
Avoir des terrains qui ne sont pas pollués par les voisins ne se fera
pas par magie, surtout quand on regarde la puissance d’une organisation
comme la FNSEA. Pour mener cette lutte, il faut trouver des points
d’accroche et, aussi étonnant que cela semble l’être, le désir
d’apprendre à relationner avec les vivants férals. Une
partie des chasseurs·esses désirent que des vivants libres
existent, pour pouvoir se confronter à elleux. Leur existence nécessite
de vastes espaces inexploités ou qui le soit avec parcimonie. Des
espaces peu pollués, des espaces connectés entre eux. Sur ces points et
tout ce qui en découlent, il y a un espace pour créer une force
politique, capable de prendre à revers celleux qui profitent de
l’exploitation de la terre.

Dans les villes où l’espace manque, c’est en reprenant des espaces au
béton, pour redonner de la place aux vivant·es partout où c’est possible
que nous pourrons recréer des lieux de vie pour les non-humains. Si dans
les campagnes, cet objectif est difficile, en ville il l’est bien
plus. Il faut supprimer 870 places de parking pour libérer un hectare. Et
les *attachements *des humains aux places de parking vire parfois au ridicule.
Pourtant il faut aussi y mener une lutte pour les non-humains. Elle
passerait à minima par l’arrêt de projets : il est presque toujours
plus facile et efficace d’éviter qu’une infrastructure soit créée plutôt
que d’en fermer une. Il semble quasi illusoire d’espérer libérer des
terres du béton sans avoir de notre côté une force politique 
qui demeure à bâtir entièrement.

Nous avons constaté les conséquences des systèmes d’oppression,
de la violence de la domination. Mais comment sortir d’eux, imaginer
d’autres rapports au monde et entre nous ? Quelles sont les actions
concrètes et les histoires que nous pouvons nous raconter pour nous
aider ? Comme le fait remarquer Andréas Malm dans *Avis de tempête*,
l’Occident vit dans un imaginaire dystopique « se nourrissant de
l’attente d’une catastrophe imminente ou qui n’en est qu’à ses
balbutiements ». S’enfermer dans la dystopie, c’est accepter que tout
est perdu. Nous devons continuer à réfléchir, mais il
nous faut aussi l’incarner au présent, dans la réalité, dans les actes 
et les actions collectives. Il nous faut des boussoles : des utopies 
capables de nous faire sortir de l’attente de la catastrophe, que nous 
pourrions opposer avec aplomb aux défenseur·euses du statut quo. 

Bibliographie

**Livres** :

BERGMAN, Carla, MONTGOMERY, Nick et ROUSSEAU, Juliette, 2021. *Joie militante: construire des luttes en prise avec leurs mondes*. Rennes : Éditions du commun. ISBN 979-10-95630-37-1.322.409 05

GALLUZZO, Anthony, 2020.* La fabrique du consommateur: une histoire de la société marchande*. Paris : Zones. ISBN 978-2-35522-142-2.HC280.C6 G35 2020

HARAWAY, Donna Jeanne et GARCÍA, Vivien, 2020. *Vivre avec le trouble*. Vaulx-en-Velin : les Éditions des Mondes à faire. ISBN 978-2-9555738-4-6.304.201

L’ATELIER PAYSAN (éd.), 2023. *Reprendre la terre aux machines: manifeste pour une autonomie paysanne et alimentaire*. [Éd. augmentée d’une] postface inédite. Paris : Éditions Points. Points, 23.ISBN 978-2-7578-9947-2.338.109 05

STÉPANOFF, Charles, 2021. *L’animal et la mort: chasses, modernité et crise du sauvage*. Paris : La Découverte. Collection Sciences sociales du vivant. ISBN 978-2-348-06896-6.QL263 .S74 2021

**Mémoires** :

BERTHIER, Alizé, 2019. *Oiseaux urbains ? Les conditions d’une cohabitation humains - animaux dans le Grand Paris* [en ligne]. phdthesis. Université Paris 1 - Panthéon Sorbonne. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://shs.hal.science/tel-02387278, version 1

**Articles** :

Agir sur les jardins particuliers. [en ligne]. [sans date]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.cdc-biodiversite.fr/agir-sur-les-jardins-particuliers-avec-hortilio/

Des néonicotinoïdes toujours autorisés dans les anti-parasitaires des animaux de compagnie. *Sciences et Avenir* [en ligne]. 29 août 2018. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.sciencesetavenir.fr/animaux/chats/des-neonicotinoides-toujours-autorises-dans-les-anti-parasitaires-des-animaux-de-compagnie_127041

« J’ai reçu un accueil glacial » : le climatologue Jean Jouzel revient sur son passage à l’université d’été du Medef et tacle le patron de Total, 2023. *Franceinfo* [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.francetvinfo.fr/meteo/climat/j-ai-recu-un-accueil-glacial-le-climatologue-jean-jouzel-revient-sur-son-passage-a-l-universite-d-ete-du-medef-et-tacle-le-patron-de-total_6042311.html

CORREIA, Mickaël, 2021. Dérèglement climatique : Total savait dès 1971. *Mediapart* [en ligne]. 20 octobre 2021. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.mediapart.fr/journal/international/201021/dereglement-climatique-total-savait-des-1971

CORREIA, Mickaël, 2023. En Antarctique, des croisières « luxe, calme et écocide ». *Mediapart* [en ligne]. 18 juin 2023. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.mediapart.fr/journal/ecologie/180623/en-antarctique-des-croisieres-luxe-calme-et-ecocide

D’ALLENS, Gaspard, 2023. Mares, nichoirs et mauvaises herbes : ces paysans accueillent le vivant. *Reporterre, le média de l’écologie* [en ligne]. 1 juillet 2023. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://reporterre.net/Mares-nichoirs-et-mauvaises-herbes-ces-paysans-accueillent-le-vivant


LAMINE, Claire, 2024. Mettre en parole les relations entre hommes et animaux d’élevage. Circulation des récits et mise en débat. *Ethnographiques.org* [en ligne]. 30 juillet 2024. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.ethnographiques.org/2006/Lamine

RIBOULOT-CHETRIT, Mathilde, 2015. Les jardins privés : de nouveaux espaces clés pour la gestion de la biodiversité dans les agglomérations ? *Articulo – revue de sciences humaines* [en ligne]. 15 mai 2015. N° Special issue 6. [Consulté le 30 juillet 2024]. DOI 10.4000/articulo.2696. Disponible à l’adresse : http://journals.openedition.org/articulo/2696

STÉPANOFF, Charles, 2020. Chapitre 25. Des chiens et des hommes : modes de vie partagés et coopération cynégétique: In : *Références* [en ligne]. Éducagri éditions. pp. 523‑541. [Consulté le 30 juillet 2024]. ISBN 979-10-275-0312-4. Disponible à l’adresse : https://www.cairn.info/comportement-et-bien-etre-du-chien-2020--9791027503124-page-523.htm?ref=doi

**Podcasts** :

Décoloniser la protection de la nature | Le monde d’après, 2023. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://shows.acast.com/le-monde-dapres/episodes/decoloniser-la-protection-de-la-nature

Élevage intensif : la carte des « fermes usines » en France | Camille passe au vert, 2023. [en ligne]. [Consulté le 19 août 2024]. Disponible à l’adresse : https://www.radiofrance.fr/franceinter/podcasts/camille-passe-au-vert/camille-passe-au-vert-du-mercredi-17-mai-2023-4401796


**Web** :

Animal de compagnie, 2024. Wikipédia [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://fr.wikipedia.org/w/index.php?title=Animal_de_compagnie&oldid=214993252

Aurélien Barrau au Parlement Européen « Au-delà de la croissance », 2023. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.youtube.com/watch?v=WcVbRA3AXJc

Interventions Aurélien Barrau  MEDEF 2022, 2022. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.youtube.com/watch?v=NfqU93KyCTc

L214, 2021. Combien d’animaux en élevage intensif en France? L214 [en ligne]. 14 janvier 2021. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.l214.com/animaux/chiffres-cles/statistiques-pourcentage-elevage-intensif-viande-lait-oeufs/

Pour une Écologie réellement Antispéciste - Conférence UniREVcité 2023 - YouTube, [sans date]. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.youtube.com/watch?v=m2c4feFxfx4&t=1s

SOLLICITUDE : Définition de SOLLICITUDE, [sans date]. [en ligne]. [Consulté le 30 juillet 2024]. Disponible à l’adresse : https://www.cnrtl.fr/definition/sollicitude

# Et maintenant ?

Parler d’amour nécessite de prendre conscience des rapports de force
dans lesquels nos relations s’inscrivent, pour éviter d’enfermer nos
tentatives de réinvention dans un prisme individuel. Pour sortir de
l’optique relationnelle consistant à « prendre soin du lien », « tester
autre chose », etc., nous pouvons prendre en compte la dimension
collective de nos oppressions. Et cela nous invite à lutter pour en
sortir.

Nous avons visité beaucoup de champs dans notre tentative de mise en
perspective de nos relations : l’amour romantique, l’amitié, la famille,
le genre, le soin, le travail, notre rapport au vivant… En cherchant,
par ces entrées, à comprendre ce qui fait commun dans les expériences
d’oppression qui traversent nos relations. Pour arriver à un constat que
nous pouvons schématiser ainsi : qu’il s’agisse de genre, de sexualité,
de race, de classe, la création d’un « nous » vient toujours exclure un
« autre ». C’est le fruit du binarisme qui nourrit le capitalisme aux
dépends de notre capacité à nous aimer correctement. En conséquence,
nous ne pensons pas qu’il soit possible ni souhaitable de lutter contre
les structures sociales de manière isolée.

Alors quelles sont les luttes qui peuvent inspirer les nôtres ? Quels
combats ont potentiellement permis d’affaiblir les structures sociales
qui nous empêchent de mieux nous aimer ? Quels sont les rapports de
force en présence ? Cette partie vise à s’appuyer sur des récits de
luttes pour donner de la matière à nos réflexions. Certains combats ont
pu, ne serait-ce qu’un temps, proposer des alternatives en luttant
contre le système capitaliste. Qu’est-ce qui a fonctionné ou pas ? En
quoi est-ce que ces expériences préfigurent un autre amour possible ?
Quelles sont les failles, les écueils ? Est-ce que les récits donnent à
voir ce qui s’est réellement passé ? Est-ce que ces luttes ont réussi à
ne pas « rendre autre » ? Comment s’appuyer sur la force narrative des
combats passés sans pour autant cesser de les ancrer dans notre réalité
matérielle ?

### Des contre-mondes pour affaiblir le système social

荣霞燕

Dans certaines luttes, dans leurs avancées, leurs failles et leurs
interprétations, résident des pistes pour penser un nouveau rapport à
l’autre, les bases d’un amour en commun. Cette partie vise à dessiner
les contours concrets d’utopies qui ont tenté de faire exister un autre
monde, affaiblissant le système capitaliste. Ici, je pars du principe
que ces luttes permettent de faire exister, de manière éphémère, des
systèmes alternatifs au capitalisme (les contre-mondes). En ce sens,
elles produisent des idées qui nourrissent nos imaginaires contemporains
et « participent à la conception générale d’une société future idéale à
construire » (définition de l’utopie, Centre National des Ressources
Textuelles et Lexicales).

D’un côté, la piraterie a tenté de s’établir en force insurrectionnelle
contre le développement capitaliste et impérialiste. J’entends
l’insurrection en tant que « action de s’insurger, de se soulever
contre un pouvoir politique établi en recourant à la violence armée »
(définition du CNRTL encore),
notamment par la mutinerie. Plus largement, le bateau pirate constituait
en lui-même un acte de rébellion vis-à-vis de l’ordre marchand et de
l’autorité des Etats. De ce fait, en tant que phénomène social et
expérience de vie, la piraterie formait un contre-monde à celui d’un
système capitaliste fondé sur le commerce international, l’esclavage et
la contrainte des travailleurs maritimes. C’est en cela que je
l’assimile à une lutte.

De l’autre, la ZAD de Notre-Dame-Des-Landes s’est montée en tant
qu’expérience de lutte collective sur un territoire donné. Elle a
cherché ses propres moyens pour se relier à un lieu, avec la volonté d’y
repenser le rapport au vivant, notamment contre l’univers du projet
d’aéroport, mais aussi celui de l’agriculture intensive. Ces analyses,
si elles sont réductrices, éclairent les axes par lesquels j’analyserai
ces tentatives de création de contre-mondes.

Je pars du principe qu’en cherchant à fonder une société en contre-point
des systèmes d’exploitation qui leur étaient contemporains, ces
expériences réunissaient certaines des conditions nécessaires pour mieux
s’aimer. Cela ne veut pas dire que ce fut le cas, ni même leur objectif.
Cela signifie simplement qu’elles touchaient pour moi à des piliers
sociétaux qui entravent l’amour aujourd’hui. À savoir, le monde du
travail et le rapport au vivant.

Je n’ai pas choisi ces deux luttes par hasard, elles portent selon moi
plusieurs points communs.

D’abord, elles ont toutes deux participé à affaiblir le système
capitaliste, le temps de leur existence. Ces expériences concrètes de
l’insurrection fragilisent les systèmes établis en montrant que d’autres
modes d’organisation sont possibles : sans État, sans hiérarchie, sans
profit voire sans argent.

Ces deux luttes ont généré des mythes dont l’ampleur a sans doute
dépassé la réalité de ce qui fut vécu. Or, dans la volonté d’écrire un
livre qui nous donne des horizons désirables, la notion même de mythe
m’interroge. D’un côté, les récits qui accompagnent et englobent les
luttes préfigurent des futurs alternatifs. Quand bien même exagérés,
quand bien même glorifiés, ils activent nos désirs et nous permettent de
nous projeter hors du système capitaliste. De l’autre, les mythes
faussent nos perceptions et nous empêchent d’apprendre de nos échecs,
puisqu’ils passent sous silence les zones d’ombres. Il semble aussi que
les mythes soient souvent le récit des vainqueurs : de celleux qui
commercialisent l’image des pirates en criminels romantiques, de celleux
qui sont resté·es sur place à la ZAD… Je me demande s’il serait
possible de valoriser le retour d’expérience pour écrire des récits plus
solides à l’épreuve des faits.

Par ailleurs, ces deux luttes peuvent se relier à la théorie des
communs, en tant que gestion des ressources par une communauté, grâce à
la mise en place de règles et de pratiques. Ce sont des expériences
collectives qui, dans leur réalisation, posent la question de la
propriété, de la répartition des ressources. Et c’est aussi cela le lien
à l’amour : dans un sens très large, ces luttes questionnent notre
rapport à l’autre. Les pirates réaffirment un « nous » autour de
l’équipage, un « nous » de la classe prolétaire internationale dans le
partage des ressources. La ZAD tente d’établir un « nous » à
Notre-Dame-Des-Landes, mais c’est finalement le mythe de l’unité qui
facilite une prise de force autoritaire de la part d’un groupe,
rétablissant rapidement un « eux » moins légitime.

Pourtant, je pense que ces luttes, en tant qu’expériences de vie,
placent les notions de lien et de liberté au centre de l’existence.
Qu’il s’agisse de fonder des rapports de solidarité (bien qu’éphémères)
pour prendre soin d’un territoire sur la ZAD, ou de préférer une
existence brève mais libre au sein de la piraterie… Cela me laisse
imaginer que dans ces contre-mondes, il serait possible de réinventer
l’amour, le rapport à l’autre, de repenser les dominations de genre.

En réalité, ce n’est pas le cas, c’est même selon moi un des points
aveugles de la piraterie comme de la ZAD. Mais je crois que ces échecs
tracent des sillons à emprunter pour faire mieux. Nous pouvons nous
dire, avec la conscience de ce qui a réussi et de ce qui a raté, que
demain nous formerons des gangs de pirates féministes, que les ZAD
seront des collectifs éphémères en mouvement perpétuel. Nous pouvons
apprendre, nous pouvons rêver.

#### La piraterie : un système social en lutte contre le capitalisme

En m’attaquant aux pirates, j’imaginais porter une comparaison entre
cette expérience insurrectionnelle et la ZAD : souligner les similitudes
dans l’opposition à l’État, les points de divergence quant au rapport à
la propriété, à l’avancée dans la subversion du travail capitaliste,
tracer d’audacieux rapprochements entre les corsaires payés par les
royaumes impérialistes et l’institutionnalisation de la ZAD avec la
signature des conventions d’occupation précaires… Ce que je ne vais
pas poursuivre au-delà de ces lignes.

Et ce, pour deux raisons : 1 – je ne suis experte d’aucun de ces deux
sujets, 2 – ils me semblent tous deux mériter une retraduction à part
entière de mes lectures et analyses. Pour autant, je vous ai livré les
traits principaux de mes volontés comparatives, et je vous encourage à
les étirer pour construire vos propres analogies lors de votre lecture !

Ce qui est intriguant avec les pirates, c’est la multiplicité des
interprétations et analyses historiques et littéraires qu’on trouve à
leur sujet. C’est ce que montre Thomas Rautureau dans son mémoire
« Voile d’encre : L’émergence de la conception européenne de la
piraterie aux 17e-18e siècles » : tantôt prémisse
révolutionnaire, tantôt stratégie individualiste de survie, lutte
sanguinaire ou symbole de liberté, l’expérience pirate nourrit des
imaginaires multiples et contradictoires.

Je prends pour ma part le parti de l’historien Américain Marcus Rediker,
choisissant la perspective révolutionnaire de la piraterie. Pour lui,
cette expérience fonde les bases de la démocratie, d’une résistance à
l’économie mondiale capitaliste en construction, d’une société
alternative face à l’oppression à laquelle les marins et prolétaires de
l’époque faisaient face. Dans *Pirates de tous les pays* (2008), Marcus
Rediker fait de la piraterie un fait social total, mouvant toute la
société : « L’histoire des pirates met en lumière les problèmes
fondamentaux de l’époque. Le cas des marins passant à la piraterie
relève de la notion de classe. Le cas des Africains ou des
Afro-Américains, précédemment esclaves, qui rejoignent le Jolly Roger,
pose la question raciale. Les femmes pirates attirent l’attention sur
les conventions de genre ».

Plus largement, nous pouvons qualifier la piraterie de révolutionnaire
en ce qu’elle a amené une « évolution des opinions, des courants de
pensée, des sciences ; découvertes, inventions entraînant un
bouleversement, une transformation profonde de l’ordre social, moral,
économique, dans un temps relativement court » (définition de la
révolution, CNRTL).

Mais revenons d’abord sur la chronologie pirate. Leur histoire est très
brève et peut se diviser en trois périodes distinctes :

Entre 1650 et 1680, certains des marins enrôlés de force dans les
bateaux des compagnies coloniales profitent des naufrages ou des
mutineries pour s’établir en communautés autonomes sur des îles des
Caraïbes. Ils entretiennent un lien étroit avec les flibustiers, qui
s’adonnent à des pillages marins autorisés par un État européen, passant
régulièrement d’une occupation à l’autre.

Dans les années 1690, de nouveaux groupes amènent la piraterie dans
l’Océan Indien. Certains établissent une base sur l’île de Madagascar,
où selon l’auteur de *Les pirates des Lumières ou la vraie histoire de
Libertalia *(2019), David Graeber, ils menèrent plusieurs expériences
sociales radicales, tout en s’intégrant progressivement à la population
locale.

Enfin, de 1716 à 1726, au lendemain de la Guerre d’Espagne, se situe
l’âge d’or de la piraterie, que nous retrouvons dans les imaginaires
contemporains : des pirates de multiples nationalités et sans soutien de
leur État « attaquent les navires de tous les pays et provoquent ainsi
une crise au sein du lucratif système commercial atlantique » (Marcus
Rediker, *Pirates de tous les pays*).

Comment ces trois périodes contribuent à constituer la piraterie en tant
qu’expérience révolutionnaire et contre-modèle de l’État capitaliste ?
Quelle forme pour l’organisation sociale de la piraterie ? Comment ces
expériences de lutte se construisent-elles autour de la notion de
collectif ?

##### Contre la hiérarchie et l’État : la société pirate

La piraterie est d’abord une réaction à l’injustice du système maritime
moderne. Dans *L’hydre aux mille têtes. L’Histoire cachée de
l’Atlantique révolutionnaire* (2008), Marcus Rediker et Peter Linebaugh,
montrent qu’au milieu du XVe siècle, les États européens, notamment
l’Angleterre, et les compagnies privées de l’entreprise coloniale
opèrent un recrutement forcé sur terre pour peupler leurs bateaux. Les
institutions ont besoin de prolétaires pour nourrir l’extension
impérialiste et capitaliste. Et la main-d’œuvre potentielle ne manque
pas : le phénomène d’enclosure des champs communaux a entraîné le
déplacement d’une grande partie de la population rurale dans les villes.
Les deux auteurs résument ainsi : « les capitalistes européens devaient
exproprier de force des masses entières de leurs terres d’origine pour
que leur force de travail puisse être redéployée dans de nouveaux
projets économiques et de nouveaux cadres géographiques ».

Or les nouveaux prolétaires délestés de leurs terres n’avaient souvent
pas le choix que de rejoindre les navires. Que ce soit pour éviter les
sanctions légales prévues contre les vagabonds et les personnes
n’entrant pas dans le cadre du travail salarié, ou qu’ils soient
contraints par des condamnations à aller travailler dans les
plantations, la plupart des marins ne choisissaient pas leur sort. Sur
le bateau, ils devaient s’accommoder de conditions de vie dégradantes :
le travail était dangereux voire mortel, les salaires n’étaient pas
toujours versés ou parfois avec des années de retard, le manque d’eau et
de vivres semblait chronique, la nourriture de piètre qualité. Surtout,
les lois navales anglaises entérinaient une discipline cruelle et
l’autorité sans limite des capitaines. De fait, les marins se
révoltaient et pouvaient ainsi entrer en piraterie. Pour le dire avec
les mots des auteurs : « face aux luttes chroniques pour leur solde et
la liberté, l’État recourait à la violence et à la terreur pour équiper
ces navires, et les équiper à bas prix, recrutant souvent les individus
les plus pauvres et des populations ethniquement très diverse ».

C’est donc face à l’érection des lois de plus en plus sévères pour
contrôler les marins que la révolte s’organisa. Le navire formait un
lieu de rencontre des membres de la classe opprimée. Du fait de ses
parcours à travers les océans, il disposait d’une dimension
internationale permettant de faire circuler les savoirs, les expériences
de lutte.

La piraterie peut donc être considérée comme un groupe social
d’opprimés : des prolétaires, dont des anciens esclaves affranchis,
décidant de se rendre maîtres de leurs conditions d’existence en prenant
la possession collective d’un bateau. Bien que la piraterie semble avoir
été un monde d’hommes, des figures féminines ont traversé le temps. On
songe à Anne Bonny et Mary Read, qui auraient été les seules à défendre
le bateau de Jack Rackam contre l’attaque de la marine, alors que le
reste des membres de l’équipage étaient encore ivres de la veille.

Ces histoires révèlent l’importance que les femmes pouvaient prendre
dans l’organisation sociale des pirates, malgré le fait que leur
présence soit rare. Au sujet des femmes, Marcus Rediker et Peter
Linebaugh dans* L’hydre aux mille têtes. L’Histoire cachée de
l’Atlantique révolutionnaire* disent que « agissant hors d’atteinte du
pouvoir de la famille, de l’État et du capital, et partageant la rude
solidarité des marins hors la loi, elles ajoutaient une autre dimension
à l’attrait subversif de la piraterie en exerçant une liberté
généralement réservée aux hommes, à une époque où la sphère de l’action
sociale des femmes allait en se rétrécissant ».

Peu à peu, une autre organisation sociale se mit en place sur les
bateaux, en réaction aux conséquences de l’impérialisme. C’est la
naissance de « l’hydrarchie, à la fois prolétaire et oppositionnelle.
\[…\] Un monde social échappant aux diktats des autorités
impérialistes et mercantiles ».

La piraterie peut aussi s’analyser comme une guerre contre la terreur
des États modernes. D’abord, parce qu’elle est le fruit des guerres
impérialistes de l’époque. Si l’on prend l’exemple de la troisième
période de la piraterie, de 1716 à 1726, celle-ci advient juste après la
fin de la guerre de succession d’Espagne, qui voit s’opposer les
puissances européennes. Comme l’explique Thomas Rautureau, ces luttes se
propagent dans les colonies, où les corsaires sont payés par les États
européens pour attaquer les flottes adverses. À la fin de la guerre,
ces derniers sont désœuvrés. Les marines militaires se
séparent aussi d’une grande partie de leurs employés en temps de paix. Et
donc, une partie d’entre eux décide de se saisir des bateaux pour
eux-mêmes, continuant la piraterie par leurs propres moyens.

Dans *Pirates de tous les pays*, Marcus Rediker se demande pourquoi la
piraterie explose en 1716. Selon lui, outre la fin de la guerre de
succession d’Espagne, le marin a désormais la certitude d’être un rouage
indispensable à la marche de l’économie mondiale et des projets
impérialistes des États. Passé du statut d’allié (les corsaires),
à celui d’ennemi de l’État à éliminer, le rapport de force change. Or
« l’Atlantique est un vaste espace que les empires ne peuvent pas
facilement contrôler. Ces circonstances créent des opportunités pour
eux ».

Dès lors, le pirate se pose en concurrent direct de l’autorité étatique.
Dans « Le drapeau pirate, contre les nations – Faire l’histoire »
(Arte, 2021), on apprend que les pirates ont pesé dans l’instauration du
droit du commerce fluvial. Au moment où les États voulaient fixer des
règles, les pirates formèrent un groupe qui s’affranchit de ces
régulations pour proposer une manière différente de naviguer et de
vivre. Là où les États tentèrent de faire des pirates des criminels,
Marcus Rediker montre que ces derniers « ne se considèrent pas comme de
« vulgaires voleurs », mais comme des hommes sans patrie. En cousant
leur drapeau noir, le symbole antinational d’un gang de prolétaires hors
la loi, ils déclarent la guerre au monde entier ».

Face à cette concurrence, les États répondent par la terreur. Après une
première tentative d’amnistie ratée, les puissances coloniales
durcissent les sanctions à l’encontre des pirates. Condamnations à mort
des hors-la-loi capturés, procès exemplaires, durcissement des lois sur
la piraterie : « Ils promettent la mort à quiconque coopérera avec les
pirates, la perte des salaires et six mois d’emprisonnement pour ceux
qui refuseront de défendre leur bateau » (Marcus Rediker, *Pirates de
tous les pays* 2008). C’est le moment où la terreur engendre la
contre-terreur. La fin de l’âge d’or de la piraterie est marquée par une
explosion de violence, où les pirates ripostent face à la politique
répressive des États.

##### De l’abolition de la propriété à celle du salariat

La piraterie est une lutte contre la concentration des pouvoirs dans les
mains du capitaine d’un navire. Dans l’expérience commune de la
servilité et de la hiérarchie, le marin apprend aussi la coopération et
la solidarité. C’est une nécessité liée au fonctionnement du navire,
mais aussi à la vie sur place : « faisant face à un capitaine de bateau
détenant un pouvoir disciplinaire presque illimité \[…\], le marin
développe une vaste capacité de résistance, qui comprend la désertion,
les mutineries et les grèves. » (Marcus Rediker, *Pirates de tous les
pays*).

Dès lors, après une mutinerie ou l’engagement volontaire dans un
équipage pirate, une autre organisation sociale prend place. Elle assure
une répartition des pouvoirs horizontale et l’égalité entre les membres
du bateau. En ceci, de nombreux auteurs comme David Graeber voit dans la
piraterie un héritage proto-démocratique, où les idées du siècle des
Lumières germèrent ailleurs que chez les penseurs occidentaux et blancs.

On peut lire dans *Pirates de tous les pays* : « Chaque
vaisseau fonctionne selon les termes d’un contrat court approuvé par
l’équipage, établi au début du voyage ou à l’occasion de l’élection d’un
nouveau capitaine. C’est en fonction de ces conventions écrites que les
équipages confient l’autorité, distribuent le butin et la nourriture et
font respecter la discipline ».

Plusieurs rôles entrent en compte dans les prises de décision sur le
navire. L’officier est élu par l’ensemble de l’équipage, et ses pouvoirs
sont limités au maximum. En fait, il n’a les plein-pouvoirs qu’en temps
de combat. Le reste du temps, l’officier est un marin comme les autres.
Il est dirigé par la majorité pour le reste des décisions collectives.

Le quartier-maître, élu lui aussi, représente l’intérêt de l’équipage
face à l’officier. Il distribue la nourriture de façon égale, répartit
le butin entre les membres du bateau, choisit ceux qui partent à
l’abordage, demande aux équipages accostés de les rejoindre dans
l’aventure pirate… Il peut aussi proposer la scission de l’équipage en
plusieurs bateaux, assurant ainsi la généalogie pirate. Il fait tout
cela, non selon son bon-vouloir, mais en tenant des comptes pour assurer
une certaine équité entre les membres de l’équipage.

Enfin, le conseil commun détient l’autorité supérieure. Il réunit tous
les membres du bateau pour prendre les décisions les plus importantes,
en termes de survie et de stratégie. Cette instance élit les officiers
et les quartier-maîtres, de la même manière qu’elle les démet lorsque
l’équipage n’est pas satisfait.

Cette organisation sociale fonctionne parce que le bateau et les prises
appartiennent désormais à tous·tes. C’est l’abolition de la propriété
qui met fin au phénomène d’exploitation. En effet, la remise en cause de
la hiérarchie au sein de l’équipage nécessite de repenser la division du
travail et la répartition des ressources.

Dans *L’hydre aux mille têtes. L’Histoire cachée de l’Atlantique
révolutionnaire*, Marcus Rediker et Peter Linebaugh montrent que la loi
des pirates se base sur « une hostilité de classe vis-à-vis des patrons
du navire », mais aussi sur des utopies paysannes visant la
redistribution de la propriété, l’abolition du travail, la suppression
des distinctions de classe et l’assurance de la santé et de
l’alimentation de tous.

Cela se traduit dans l’organisation sociale et le rapport à la propriété
à l’intérieur de navire. Marcus Rediker affirme que la distribution des
ressources était régulée par la charte du navire, afin de niveler les
différences entre les postes assurés et les parts du butin. Dans la
plupart des cas, le butin est partagé à parts égales, sans considération
pour le statut social du rôle exercé par les individus. Par exemple,
lors d’une prise, le mousse peut obtenir la même part que le·la
navigateur·ice. Une partie était également réservée pour dédommager les
personnes blessées, fondant ainsi l’ancêtre de la sécurité sociale. Pour
l’auteur, le partage équitable du butin et des ressources traduit la
valeur donnée au camarade. Il dénote de l’importance du collectif
vis-à-vis de l’individu, ainsi que de la solidarité comme règle de
cohésion sociale.

Ici, j’ai envie de faire un détour par le manga *One Piece*, d’Eiichiro
Oda. C’est une des œuvres de fiction qui témoigne de l’attirance réelle
que nous éprouvons encore pour la piraterie. Mais pas n’importe
laquelle : celle de Luffy, un capitaine qui prône la libération
vis-à-vis des systèmes oppressifs et pousse les autres à poursuivre
leurs rêves, celle de Zoro, qui jure sa loyauté à Luffy et de ne pas le
décevoir, celle de Nami, qui devient pirate pour sauver son village…
La confiance mutuelle au sein de l’équipage bigarré crée un réseau de
solidarité. Finalement, ce que *One Piece* a retenu des pirates, c’est
la recherche de la liberté, la soif d’aventures, la mise en cause du
système de justice, mais aussi un profond sens de l’amitié. En cela,
l’histoire des pirates permet aussi d’imaginer une société basée sur le
lien à l’autre.

Pour revenir à des considérations historiques, les auteurs de *L’hydre
aux mille têtes. L’Histoire cachée de l’Atlantique révolutionnaire*
soulignent aussi que les prises de navire entraînaient un enrichissement
commun, puisque les bateaux étaient considérés comme de la propriété
collective. Ce changement de perspective au sujet des bateaux, en tant
que capital, abolit le salariat : puisque le bateau appartient à
tous·tes, chacun prend des risques, non pas dans un rapport
d’exploitation et pour un patron qui en tirera profit, mais pour un
butin qui serait partagé entre tous·tes. Pour le dire avec les mots de
Marcus Rediker dans *Pirates de tous les pays*, « en expropriant un
navire marchand (après une mutinerie ou une capture), les pirates
s’approprient les moyens de production maritimes et déclarent qu’ils
sont la propriété commune de ceux qui travaillent à son bord. Ils
abolissent la relation salariale qui se trouve au cœur du processus
d’accumulation capitaliste ».

En questionnant les notions d’autorité et de salariat, les pirates se
rendent capables de vivre en collectif et de mettre la solidarité au
centre de leur existence. Au plus fort de leur âge d’or, ils ont entravé
le développement commercial et maritime des empires de l’époque. En
cela, la piraterie fonde un contre-monde au système capitaliste, basé
sur un rapport de solidarité et de camaraderie que je trouve très
inspirant. La piraterie est un fait social complexe, qui a aussi nourri
de nombreux mythes. Par l’échange de pratiques et d’idées, l’expérience
pirate a fait germer des utopies sociales et participé aux révolutions de leur
temps.

##### La place du mythe : peut-on influencer l’histoire ?

Pour moi, écrire sur les pirates aujourd’hui s’apparente à confronter
des mythes, à choisir de croire à certaines histoires qui sont racontées
à leur sujet plutôt qu’à d’autres. C’est ce que montre Thomas Ratureau
dans « Voile d’encre : L’émergence de la conception européenne de la
piraterie aux 17e-18e siècles » et que j’ai déjà souligné :
il existe de multiples interprétations de la piraterie, forcément
contradictoires. Elles entretiennent toutes un lien à la fiction, à ce
que les récits ont fait de la piraterie. C’est donc un très bon exemple
pour questionner la place des mythes dans les luttes sociales : à quel
point écrire sur une lutte participe à la glorifier, à nous inspirer des
actions futures ? Est-ce que les récits peuvent contribuer à la
Révolution ? Ou est-ce qu’ils nous aveuglent ?

Pour David Graeber dans *Les pirates des Lumières ou la vraie histoire
de Libertalia*, à l’époque de la piraterie, les légendes qui
accompagnaient les pirates ont tant influencé leurs contemporains qu’ils
ont constitué « une force matérielle dans l’histoire des hommes ». Elles
auraient eu un véritable poids, forgeant les contours de l’imaginaire
révolutionnaire de l’époque. Ainsi, pour l’auteur, elles préfigurèrent
la révolution du Siècle des Lumières, non pas en Occident, mais grâce
aux échanges d’une classe prolétarienne internationale. Pour le dire
avec ses mots : « ces expériences représentent quelques-uns des premiers
frémissements de la pensée politique des Lumières, explorant des idées
et des principes qui devaient être ensuite approfondis par des
philosophes politiques et mis en pratique par des régimes
révolutionnaires un siècle plus tard ». Et c’est précisément parce que
les légendes pirates semèrent l’idée de liberté à travers le monde
qu’elles survécurent pour nous fasciner encore aujourd’hui.

Dans son étude anthropologique, David Graeber s’intéresse à
l’établissement des pirates sur la côte Nord-Est de Madagascar, et à
l’existence d’une communauté mythique appelée Libertalia au début du
XVIIIe siècle. Dans la légende, Libertalia est une colonie de pirates où
l’esclavage aurait été aboli et les biens gérés comme des communs entre
les membres de la société. Pour David Graeber, le fait qu’il n’y ait
aucune preuve de l’existence de cette communauté est sans importance :
des utopies sociales radicales pirates ont bel et bien existé à ce
moment-là à Madagascar, moins closes sur elles-mêmes que le mythe de
Libertalia. En effet, ces expériences radicales étaient non pas le
fruit de pirates occidentaux et blancs, mais celles de populations
malgaches, ayant peu à peu incorporé les pirates en leur sein. Cela
donne une autre source de la pensée des Lumières : celle de l’histoire
non-blanche.

C’est un des points forts du livre de David Graeber : il considère les
événements du point de vue malgache. L’auteur s’appuie notamment sur
l’étude de la confédération betsimisaraka, fondée par des malgaches,
dans le sillage des pirates. À la suite de leur installation sur la
côte Nord-Est de Madagascar, des soulèvements sociaux menés par la
population locale instaurèrent les coutumes des bateaux pirates sur leur
territoire, en les mêlant aux traditions politiques égalitaires
malgaches. Pour l’auteur, cette filiation s’explique par le mélange des
pirates à la société malgache, créant des foyers mixtes et des enfants
riches d’un héritage pluriel. Avec l’arrivée des pirates, la première
révolution concerne la place des femmes dans la société. Elles affirment
leur importance dans la sphère marchande et leur influence dans le monde
public.

Un des autres éléments intéressant de l’analyse de la création de la
confédération betsimisaraka est son aspect collectif. En revanche, elle
se fait à rebours de la première révolution, par des hommes de guerre,
excluant les femmes des cercles de discussion politique. Si les écrits
occidentaux mirent l’accent sur l’autorité d’un chef, fils de pirate,
Ratsimilaho, David Graeber montre que la fondation de la société
betsimisaraka fut en fait une œuvre collective. Selon lui, l’autorité
donnée au chef lors des combats est le fruit de réflexions communes,
menées lors de grands cercles de discussion, nommés kabary. La
confédération fonctionnerait comme une démocratie décentralisée,
garantissant le pouvoir autonome des chefs locaux.

Ce détour par le mythe de Libertalia étudié par David Graeber nous
donne de la matière pour nous questionner sur le rôle du mythe dans nos
luttes contemporaines. D’un côté, la fascination pour les pirates, en
tant que force insurrectionnelle contre le système capitaliste, a
traversé le temps. Comme le montre David Graeber, elle s’imposait déjà
l’époque comme une force matérielle influençant l’histoire : « Ils \[Les
pirates\] faisaient usage de leurs récits fabuleux tout à fait comme des
armes de guerre, même si la guerre en question n’était que la lutte
désespérée, et vouée à la défaite, de petites bandes hétéroclites de
hors-la-loi contre toute la structure, alors émergente, de l’ordre
mondial ». Dans le temps, ces récits ont nourri les révolutions futures,
dont celles du siècle des Lumières.

Or raconter des histoires, c’est aussi choisir le point de vue qu’on
emprunte. Dans *Les pirates des Lumières ou la vraie histoire de
Libertalia*, David Graeber choisit de nous raconter une histoire
non-blanche, celle de l’invention d’utopies sociales malgaches, à partir
du métissage pirate. En choisissant notre point de vue, nous pourrions
demain tendre vers les récits de gangs de pirates féministes, nommer des
perspectives pour les dévoiler ou leur donner la possibilité d’advenir.

Néanmoins, raconter des histoires nécessite selon moi d’appréhender les
zones d’ombres des phénomènes que nous abordons. Ici, la mythification
des pirates empêche par exemple de penser la place des femmes au sein de
ces contre-mondes, où d’interroger le rôle des pirates dans le commerce
triangulaire qui a participé à la colonisation. Les mythes ne nous
permettent pas d’apprendre de nos erreurs.

C’est ici que se trace le parallèle avec la ZAD de
Notre-Dames-Des-Landes. La lutte de la ZAD tente de créer une société
éphémère en dehors des civilisations établies, avec ses propres codes,
au nom de la dénonciation d’un ordre mercantile. En s’ancrant sur un
territoire, elle essaie de fonder un nouveau rapport au vivant. En cela,
elle nourrit les imaginaires et nos capacités à penser, vivre et
fonctionner en dehors du système capitaliste. Pour autant, la ZAD nous
fait percevoir avec plus d’acuité encore les dangers de la mythification
d’une lutte. Qu’est-ce que l’idéalisation produit sur le réel ? À quel
point nous empêche-t-elle d’apprendre de nos échecs ?

#### La ZAD dans nos imaginaires : de l’espoir insurrectionnel à la refonte de notre rapport au vivant

Quand avec Timothé, nous avons pensé ce livre, nous voulions écrire une
partie sur les façons de vivre et de se relier au monde différemment en
prenant la ZAD pour exemple, car nous y voyions un lien avec l’amour en
commun. C’est-à-dire une possibilité dans l’îlot de la ZAD de créer un
contre-monde basé sur le lien, offrant les conditions pour s’aimer
mieux. En approfondissant le sujet et en discutant de cela autour de
moi, je me suis rendue compte que cette vision de la ZAD était
réductrice et idéalisée. Je voyais dans la ZAD un lieu d’expérimentation
de la vie collective, de création d’un autre rapport au vivant… Et je
souhaitais le valoriser comme un modèle potentiel pour le sujet de notre
livre, l’amour en commun.

Aujourd’hui, cela me semble être une erreur. Pour moi, ce qui rendrait
possible de mieux s’aimer, c’est de changer les conditions matérielles
de nos existences. Je ne crois plus à ce rapport extérieur à la société.
Je m’étais nourrie du mythe d’unité de la ZAD, sans creuser jusqu’ici
les rapports de force et les relations qui s’y jouaient réellement. De
ce fait, écrire sur la ZAD me mettait mal à l’aise. Là où j’imagine
l’amour en commun aujourd’hui, ce n’est plus à l’extérieur de la
société, en créant des liens de solidarité communautaire (ou
d’entresoi). L’objectif de « l’amour en commun » pour moi, serait plutôt
de lutter dans la société pour tenter de faire advenir les conditions
nécessaires pour mieux s’aimer (pour tous·tes). Ce qui peut passer par
la création d’une ZAD, mais pas seulement.

L’écriture de cette partie du livre se basait aussi sur l’importance de
« gagner une guerre des imaginaires » pour voir advenir d’autres façons
de vivre ensemble : si nous imaginions ces futurs désirables, nous
pourrions tendre vers eux. Derrière cette idée, je pensais aussi : s’il
existe des récits alternatifs, peut-être que « les gens » (qui ?)
finiraient par préférer aller habiter en ZAD ou dans des communautés
autonomes plutôt que d’acheter une maison de plain-pied et un écran
plasma. Aujourd’hui, je trouve ces réflexions d’un classisme
problématique.

Au-delà de ça, j’ai un rapport ambigu à la question de l’imaginaire.
D’un côté bien sûr, je suis écrivaine et j’ai envie de croire au pouvoir
de la fiction pour faire advenir sur un temps long des idées et des
désirs nouveaux. De l’autre, je ne pense pas que compter sur les mythes
soit politiquement porteur pour changer la société aujourd’hui. Encore
une fois, je pense qui si nous voulons créer les conditions pour mieux
s’aimer, il faut agir, créer un rapport de force, lutter, avant de
raconter des histoires (pourquoi te fatigues-tu, me direz-vous).

C’est l’expérience à Sainte-Soline qui m’a redonné envie d’écrire sur la
ZAD. Là encore, je pourrais observer la place ambiguë du mythe : j’y
allais pour cet imaginaire de la ZAD, pour les récits des courses à
travers champs où les policiers s’embourbent pendant que les militant·es
ricanent, pour vivre « ça ». Il y avait quelque chose d’attirant à
rejoindre des milliers de personnes prêtes à accepter un certain 
degré de conflictualité pour préserver l’eau en tant que commun. Sauf
que sur place, j’ai dessalé.

C’est à ce moment-là que s’est déployé mon questionnement, notamment
grâce à la lecture du tract *Lutter et/ou se faire manipuler au nom
d’une lutte ? Soulèvements de la terre versus État, même combat !*
(2023). Il vise à « évoquer des procédés employés par des ennemis
politiques : des habitants de l’ex ZAD de Notre dame des Landes (NDDL)
devenus des généraux des soulèvements de la terre (SDLT), et affilié-es
à l’appelisme au moins par leurs pratiques ». Pour les auteur·ices, les
appelistes ont trahi le projet de la ZAD par des pratiques autoritaires
dans le mouvement, visant à saper les tentatives d’organisation en
dehors des règles de l’État. La liste des récriminations parle
d’elle-même : destruction de cabanes et de barricades, cartographie des
lieux souhaitant négocier avec l’État (et donc de celleux à expulser par
extension)…

Selon elleux, ces habitants de l’ex ZAD utilisent les mêmes procédés
autoritaires dans les Soulèvements de la Terre : le recours à un récit
mythifié d’une horizontalité sans remise en cause des privilèges de
classe, la propagande révolutionnaire sur les « communs » par ceux-là
même qui ont accepté les expulsions, l’organisation pyramidale d’un
événement où les images spectaculaires et l’idée fausse de « l’unité »
envoient des milliers de personnes au casse-pipe… Les auteur·ices
dénoncent l’appel à manifestation du 25 mars comme une manipulation de
masse. En s’appuyant sur les mythes qui circulaient sur la ZAD et en se
réservant les décisions stratégiques, en omettant de visibiliser
l’intensité probable de la répression policière, les organisateur·ices
des Soulèvements de la Terre n’ont pas donné les moyens aux
manifestant·es de se protéger et de choisir pour elleux-mêmes.

Ce détour par Sainte-Soline permet d’amener les questions par lesquelles
j’ai voulu étudier la ZAD : comment un récit qui a fait vibrer les
militant·es de la France entière, alimenté les imaginaires
anticapitalistes, été glorifié dans la littérature occulte des réalités
moins reluisantes ? Qu’est-ce que nous avons à apprendre de ces échecs ?
Qui écrit le récit quand une partie du mouvement est forcé d’abandonner
la lutte ? C’est peut-être ça qui est intéressant pour ce livre
finalement, étudier la ZAD dans ses réalités ET dans ses mythes, le
pouvoir qu’ils ont sur nous.

Dans son passé et son présent, une des caractéristiques de la ZAD est de
nourrir de multiples imaginaires anticapitalistes. Je pourrais consacrer
une partie entière de ce livre à m’interroger sur les raisons qui font
de la ZAD un mythe pluriel et quasi universel pour les militants (ce que
je ne vais pas faire). Mais je peux donner ici quelques éléments qui ont
participé à forger le mien : l’expérimentation d’un mode de vie en
résistance au capitalisme, le succès militant de l’abandon de
l’aéroport, le rassemblement de milliers de personnes venues d’horizons
sociaux et politiques différents ainsi que les images de combats
victorieux face à la police dans les champs…

Je vais m’arrêter sur deux axes discursifs qui ont alimenté ma
réflexion : La ZAD a été le terreau de mythes basés sur les rapports de
force matériels existant entre les zadistes et l’Etat. Aujourd’hui, elle
se médiatise comme un espace pour penser un autre rapport au vivant.

Cependant, il serait peut-être judicieux de présenter la chronologie de
la ZAD de Notre-Dame-Des-Landes (NDDL) qui m’a servi d’outil de travail.

Le mouvement de lutte est né dans les années 70 face à un projet
d’aménagement du territoire. Il prévoyait la construction d’un aéroport
sur des zones humides donnant naissance à plusieurs rivières, à 25
kilomètres de Nantes. Ce projet de modernisation économique de la région
est enterré par les deux chocs pétroliers. Il revient sur la table 30
ans plus tard, dans les années 2000, sous l’impulsion du maire de
Nantes, Jean-Marc Ayrault, avec le soutien de l’État. Dès lors, des
habitants, syndicats et collectifs adoptent une stratégie de
respectabilité, ayant recours à une mobilisation légale pour empêcher le
projet d’advenir. Cela ne suffit pas : en 2009, la Déclaration d’Utilité
Publique sur la zone autorise les expulsions.

Ensuite, un appel international est lancé par des collectifs
d’habitant·es pour inviter des militant·es à venir occuper le terrain et
lutter contre le projet d’aménagement. C’est là où je marque le point de
naissance de la ZAD de Notre-Dame-Des-Landes. Se crée alors un mouvement
composite de militants zadistes, provenant de divers horizons sociaux et
politiques (milieu du squat, anarchistes, altermondialistes,
citoyennistes, paysans…) qui tentent de s’unir contre l’aéroport.

Ici, un point de définition s’impose. Le terme zadiste tel que je
l’utilise ne recouvre pas une identité politique, elle désigne seulement
des occupant·es et sympathisant·es, ayant décidé de défendre la ZAD à un
moment donné. Quand je parlerai d’habitant·es de la ZAD, il s’agira en
revanche des personnes qui sont restées sur place après l’abandon du
projet d’aéroport. Ce choix de vocabulaire vise à indiquer ce que je
vois comme la fin de la lutte et de la dimension militante de ce choix
de vie. Il est important de noter que le changement a été opéré par les
personnes sur place elles-mêmes, se présentant désormais comme
« collectif d’habitant·es ».

L’un des tournant de l’histoire de la ZAD a lieu avec l’opération César,
qui a lieu d’octobre 2012 à avril 2013. Pour expulser l’occupation
illégale du territoire, la préfecture de police de Loire-Atlantique
déploie ses forces policières afin de déloger les militants. De
nombreuses cabanes et champs de culture sont détruits, puis
reconstruits, réoccupés, à mesure que la ZAD gagne de l’ampleur et des
soutiens. Finalement, grâce à l’affluence massive de militant·es et à
l’appel des zadistes, la police et l’armée quittent le territoire.

En novembre 2013, la police se retire officiellement. Entre la
massification du nombre d’habitant·es et le retrait de l’ennemi commun,
le mouvement commence à laisser voir ses divergences. Comme l’explique
l’autrice de *Réflexions à propos de la ZAD : une autre histoire*
(2019), « la population de la ZAD a doublé ou triplé en quelques mois,
avec une division entre celleux étiqueté·es « intellectuel·les
bourgeois·es » et les autres appelé·es « schlags ». Il y eut d’autres
nouvelles arrivées, mieux organisées et avec plus de ressources, mais
qu’iels gardaient surtout pour elleux dans l’Ouest de la zone, tissant
des liens avec les paysan.nes ».

Le rapport de force continue avec l’État jusqu’en 2018, où le premier
ministre annonce début janvier l’abandon du projet d’aéroport (malgré le
référendum local donnant gagnant ses défenseurs). Dès lors, il s’agit
pour l’État de faire rentrer la zone dans la légalité : en proposant des
négociations pour régulariser l’occupation d’une main, tout en expulsant
les militant·es refusant de rentrer dans le droit de l’autre.

Cela a miné le mouvement en créant une dissociation entre la frange
légaliste, soutenant les négociations, et la frange « radicale »,
refusant de rentrer dans le jeu de l’État et subissant de fait les
premières expulsions. D’avril à mai 2018, l’entièreté de la zone Est se
trouve expulsée et détruite, dans une déferlante de violences policières
et parfois avec le soutien de certaines fractions du mouvement (on en
reparlera). C’est ici, pour moi, que la ZAD a perdu. Si elle a gagné des
baux de fermage précaire, elle a troqué un rapport de force avec l’État
contre l’image d’un lieu alternatif mais bien obligé de rester dans le
système.

Et après ? Du côté des habitant·es de la ZAD ayant joué le jeu de la
régularisation, 15 dossiers agricoles sur 29 pour bénéficier de
conventions d’occupation temporaires sont acceptés par l’État. Selon le
collectif d’habitant·es actuel de la ZAD, « Les Conventions d’Occupation
Précaire que nous avons signées sur ces terres sont pour la plupart
transformées en baux de fermage de neuf ans » (*Prise de terres, 2019*).
Un fond de dotation a été mis en place pour racheter collectivement les
terres agricoles mises en vente par l’État et le département. La plupart
des occupant·es militant·es sont parti·es. En mai 2023, on compterait
une « trentaine de lieux et habitats collectifs restaurés ou
auto-construits », menant des « activités agricoles, artisanales,
culturelles et militantes » (site zad.nadir.org, *article Portes grandes
ouvertes de la Zad de NDDL // Dimanche 4 juin de 10h à 18h*).

Si le présent de ce qu’était la ZAD n’est plus révolutionnaire, il
continue de porter des approches du lien affectif au territoire et au
biotope qui nous intéressent pour ce livre. C’est d’ailleurs ce qui fait
la particularité de la ZAD : la lutte se lie à un lieu, figeant les
personnes sur place pour un temps donné et nécessitant de réfléchir aux
règles de vie et au rapport à l’espace, aux ressources environnantes.

En un certain sens, en portant le passé de ces luttes contre l’État, le
collectif d’habitant·es de la ZAD tente de préfigurer quelque chose.
Mais quoi ? Une forme de communalisme, de retour aux communs, un autre
rapport aux besoins et au travail ? Ce sont les expériences collectives
dont nous avons besoin… à condition de reconnaître que ces projets
communalistes sont largement insuffisants : pour moi, là où le légalisme
remplace le squat, là où la propriété collective déloge l’espoir de
l’abolir, là où la négociation avec l’État dissocie, là où les
divergences politiques se transforment en rapports de classe, il ne peut
pas y avoir de futur politique désirable.

Encore une fois, pour repenser l’amour et nos relations, il est
nécessaire d’infléchir les rapports de force qui les traversent, ce en
luttant contre les structures sociales qui nous oppressent. Nous
renouons avec l’idée ici que les intentions ne suffisent pas : si notre
contre-monde ne pense pas les rapports de domination de classe, de genre
et de race, s’il se suffit au plus petit dénominateur commun et ne
contredit pas les normes du système capitaliste, alors il ne nous
permettra ni de faire la révolution, ni de nous aimer mieux.

Pourtant, la ZAD continue de vivre. Dans les imaginaires, dans ce
qu’elle a porté d’espoirs insurrectionnels pour nourrir d’autres luttes,
dans les récits qui alimentent nos envies de résister. Peut-être que
c’est ainsi que nous pouvons imaginer la victoire de la ZAD : par ce
qu’elle a été en tant qu’espace de lutte et par tous les potentiels
qu’elle a ouverts.

##### S’organiser contre l’État : la ZAD, « machine de guerre et de désir » ?

L’histoire de la ZAD, c’est d’abord celle d’une opposition à l’État par
l’occupation illégale d’un territoire, du sabotage, des manifestations
qui tournent à l’émeute… C’est l’histoire d’un rapport de force de
longue haleine contre la répression policière, la surveillance. Pour les
auteur·ices de *Zombie Walk à la ZAD, des fantômes et des
fantasmes* (2019), « avec 2012 et l’opération César, on savait que la
ZAD était aussi ce laboratoire de la résistance populaire, de
l’affrontement avec les forces de l’ordre ». 

Ce que la ZAD a de particulier, c’est qu’elle engage un territoire à
défendre. Un endroit où les occupant·es squattent, vivent, où les
soutiens viennent régulièrement donner leur aide, où les habitant·es et
paysannes cultivent la terre… C’est ce que je retrouve dans la
brochure *Réflexions à propos de la ZAD : une autre histoire*, cette
idée d’un quotidien qui s’insère dans la lutte et dans une communauté
qui essaye de la faire vivre, en même temps que le lieu.

L’autrice décrit : « Plusieurs expériences m’ont aidé à réaliser que
beaucoup des personnes qui venaient après les expulsions ne comprenaient
pas ce qui avait existé auparavant : les cabanes qui défiaient la
gravité et la logique, la solidarité collective à grande échelle, les
voitures brûlées sur la route des chicanes avec des fleurs plantées
dedans, la fabrication d’une communauté tissée par les interactions
quotidiennes, la maison des exilé·es et l’école de linguistique, une
radio pirate… Pour celleux qui n’avaient pas de comparaison, c’était
comme profiter du meilleur gâteau de tous les temps, ne réalisant pas
qu’il ne s’agissait que de miettes ».

L’image utopique de la ZAD comme un territoire avec lequel on fait corps
dans la lutte, le mythe des zones autonomes et de l’expérience
communiste, ne se comprend pas sans le rapport de force dans lequel elle
s’inscrivait. Il ne s’agit pas de dire que c’est la précarité et la
menace qui rendait l’expérience de la ZAD politique. C’est plutôt le
fait qu’elle participait d’une lutte contre l’État, ouvrant en elle des
potentialités révolutionnaires, où y étaient questionnées l’autonomie,
la propriété, le rapport à la police, la mise en commun des biens, le
rapport à l’autre, l’accueil universel, les rapports de classe…

C’est peut-être cela, que vise la brochure *Zombie walk à la ZAD, des
fantômes et des fantasmes*, en comparant ce qu’a été la ZAD à « une
machine de guerre et de désirs », en reprenant l’expression de Felix
Guattari. D’un côté, une lutte matérielle contre l’État et la police, de
l’autre une tentative de vivre une utopie hors du monde capitaliste. Et
cela viendrait comme une dialectique. Pour rappel, selon Hegel, les
contradictions sont le moteur de la dynamique. La dialectique étudie les
contradictions internes à un système comme base de la dynamique et de la
fabrication de nouvelles structures. Et ici, la guerre ne va pas sans
l’utopie (le désir), et l’utopie ne va pas sans la guerre : ils se
rendent mutuellement possibles, parce que de la lutte matérielle
émergent de nouvelles potentialités. Et à l’inverse, ces désirs de vivre
autrement ont cette force parce que la lutte les rend possible, même de
manière éphémère.

##### Refondre notre rapport au vivant : une préfiguration des communs ?

L’un des héritages de la ZAD qui perdure aujourd’hui, c’est la volonté
d’habiter et d’être avec le monde différemment. Cette partie étudie ce
qui demeure après les expulsions de 2018, dans les tentatives de
réinvention de celleux qui sont resté·es.

D’abord, les projets sur place visent encore à sortir les terres
agricoles d’une logique capitaliste grâce à la « mise en commun du
foncier par des expériences paysannes collectives » (*Prises de
terre(s)*). Selon le collectif d’habitant·es, « des collectifs
s’essayent ça et là à différentes manières de subvertir la question du
travail, de la production, de l’économie ». Le noyau nucléaire est mis
en cause par l’extension des communs dans la vie quotidienne : cuisines
collectives, lieux de vie.

Selon Alessandro Pignocchi (dans la série *Génération ZAD* de LSD,
France Culture, 2021), la ZAD fonde un nouveau rapport au monde en
reconnaissant que les usages humains s’enchevêtrent avec les usages du
biotope. C’est ce qu’affirme le collectif d’habitant·es : « La Terre
n’est ni une réserve naturelle, ni une ressource agricole, c’est un
écheveau de relations entre minéraux, végétaux, animaux et humains ».
Par exemple, des expériences d’ensauvagement dans les forêts abandonnées
par l’ONF avec le projet d’aéroport permettent de recréer une gestion
communale, pour régénérer l’écosystème.

Pour Alessandro Pignocchi, elle crée aussi un rapport affectif au
territoire. Le collectif vise ainsi à « la fabrique et l’invention de
nouvelles manières de cultiver le sol, d’élever des animaux, d’habiter
la nature, de côtoyer la vie et la mort, et d’être ensemble au monde » :

« Quiconque se livre, par exemple, à l’observation assidue des bovins
accède à une autre compréhension de la notion de symbiose. Le vanneau
huppé pond dans les empreintes de leurs pas. Le héron garde-boeufs se
nourrit des insectes qui les entourent et parfois les incommodent. Des
orchidées, des tritons, des grenouilles, des papillons, une myriade
d’espèces rares et sauvages peuplent les prairies pauvres et humides où
paissent les troupeaux. » (*Prises de Terre(s)*)

Enfin, pour Alessandro Pignocchi, la ZAD renouvelle la conception de
l’utilisation des moyens de production, définie par rapport à ce qui est
désirable et non plus par rapport à la seule productivité. Par la mise
en commun d’une partie de la production collective (maraîchage, bois
pour le logement, groupements d’achats…), les habitant·es peuvent
penser une économie visant la descente de leurs besoins.

Le collectif d’habitant·es fait le pari de la communalisation : créer
des règles et coutumes locales pour et par les habitant·es, pour réguler
un vivre ensemble, en occupant les interstices du droit national : une
convention d’occupation individuelle n’empêche pas de cultiver la terre
et de se répartir les récoltes à plusieurs par exemple. En ce sens, iels
font cohabiter stratégie légaliste (régularisation des baux) et des
formes d’illégalisme dans leurs tentatives de faire vivre la propriété
collective et des installations officieuses. Dans leur article *Prises
de Terre(s)*, iels donnent l’exemple de l’assemblée des usages, qui
continuerait à organiser la propriété commune. Enfin, le montage d’un
fond de dotation commun pour racheter les terres à l’État constituent
pour elleux une tentative de « frayer un chemin qui ne soit ni celui de
la propriété privée, ni celui de la propriété publique : une forme de
propriété commune, fruit d’une association inédite entre habitants,
paysans, usagers, naturalistes, chercheurs… ».

Sans doute sont-iels de fervent·es lecteur·ices de Silvia Federici. Dans
*Réenchanter le monde. Féminisme et politique des communs *(2022), la
politique des communs est définie comme les « nombreuses pratiques et
perspectives adoptées par les mouvements sociaux sur toute la planète
qui cherchent à renforcer la coopération sociale, à réduire le contrôle
du marché et de l’État sur nos vies, à encourager le partage de la
richesse et ainsi, à mettre des limites à l’accumulation du capital ».
Cette politique se fonde sur les associations de producteurs, visant à
dépasser la propriété privée et la conception mercantile de la valeur
des biens, sur l’équité dans la redistribution des ressources, la
revalorisation des savoirs spécifiques et locaux, la coopération dans
toutes les sphères de la vie humaine : le travail, le soin, etc.

Pour elle, l’enjeu de la politique des communs est de « s’opposer aux
divisions que le capitalisme a créées sur la base de la race, du genre,
de l’âge, réunir ce qu’il a séparé dans nos vies et reconstituer un
intérêt collectif », notamment par la création de liens de solidarité
communautaires. Et c’est là que ça pêche pour le projet paysan
d’après-ZAD. Qui est concerné·e par cette reprise en main des terres ?
Par la refonte du rapport au vivant ? Par les décisions collectives
prises dans la néo-assemblée des usages ? Le pari de la légalisation
pour maintenir les expérimentations collectives a été présenté comme un
moyen, et non pas une fin. Un moyen pour protéger la ZAD, ses
habitations, son mode de vie alternatif, le contre-monde qui s’y était
créé… aux dépends de celleux qui avaient contribué à le construire.

##### L’échec du grand soir : penser le rapport de classe

Pour moi, le choix de la légalisation entraîne une institutionnalisation
et la fin de la lutte contre l’État, soit un retour à l’ordre établi. La
ZAD devient une alternative *dans* le système capitaliste, si vertueuse
soit-elle.

Selon l’autrice de *Réflexions à propos de la ZAD : une autre histoire*,
la légalisation n’était pas le seul choix disponible. Elle provient
plutôt d’une dérive autoritaire où la « composition » de différents
groupes politiques (entretenant des buts et des moyens différents, mais
s’accordant sur le plus petit dénominateur commun pour lutter ensemble)
a poussé l’ensemble vers les décisions réformistes : « La ZAD offre
une étude intéressante sur la façon dont des tendances autoritaires
émergent, se développent, et sapent des situations potentiellement
révolutionnaires ». De fait, en visant l’unité, la composition
favorisait les actions les moins radicales, acceptable par toustes, pour
faire front commun. Cela a de facto poussé la composition vers des
positions plus réformistes (soit moins menaçantes).

Pour l’autrice, la manifestation de plus de 60 000 personnes à Nantes en
2014 ayant tourné à l’émeute a créé une scission au sein du mouvement en
effrayant les réformistes. De fait, la composition penche alors vers les
citoyennistes et ouvre une « rupture* entre pragmatisme et
idéalisme » : le pragmatisme étant « la solution la plus simple et la
plus conforme aux normes existantes », et l’idéalisme étant renvoyé à un
dogmatisme idéologique, dénigré par les tenants du premier.

L’autrice analyse deux facteurs qui ont sous-tendu la dérive autoritaire
vers la tendance pragmatique.

D’abord, la prise de pouvoir entraînée par la mise en place de
mécanismes de représentation : « Ouvrir des canaux de communication
entre leaders de groupes a court-circuité l’aspect ouvert de
l’assemblée du mouvement, pavant la voie vers l’émergence d’alliances
exclusives ». Quand quelques-un·es peuvent arriver en assemblée avec une
proposition établie, cela sape les débats collectifs et donne une
légitimité de fait à celleux qui se proclament détenteur·ices de l’avis
d’un groupe, par rapport à celleux qui le refusent et portent simplement
leurs paroles individuelles.

Ensuite, la dissociation mise en œuvre par l’État entre les « bons » et
les « mauvais manifestant·es », qui a colonisé le mouvement : « L’État
a joué un rôle important dans le renforcement des divisions
préexistantes ; la répression a souvent été dirigée vers des groupes
politiques spécifiques afin de monter les gens les un·es contre les
autres. Ces types de tactiques contre-insurectionnelles sont très
simples : se concentrer sur le groupe le plus isolé et convaincre le
reste du mouvement que tout·es les autres seront épargné·es s’iels se
dissocient de celleux-ci ».

Ainsi, en guise de bonne foi pour entrer en négociation avec l’État, une
partie du mouvement a détruit les cabanes expulsables – ouvrant ce
faisant un chemin pour la surveillance par la police qui préparait les
expulsions, comme le note l’autrice. De fait, c’est toute la zone Est,
qu’habitait la branche dite la plus « radicale » qui est détruite en
premier.

Cette dissociation prend racine au lendemain de l’occupation de
l’opération César (2012). Selon l’autrice, la disparition du front
commun ne mettait pas seulement à jour des idéaux différents qui ne
pouvaient se rencontrer, il cristallisait plutôt des rapports de classe.
Là encore, la création d’un « nous » vient créer un « autre », marquant
ainsi l’échec de faire de la ZAD un espace préfigurant une société où la
lutte contre nos oppressions permet de nous relier différemment.

Dès lors, le projet révolutionnaire est défait. La légalisation est une
éviction. De celleux qui souhaitaient défendre une zone en dehors de
l’État, du Capital, et d’une partie des occupant·es qui auraient voulu
intensifier le rapport de force pour obtenir de meilleures conditions
plutôt que des baux précaires. La « victoire » contre l’aéroport
appartient à celleux qui rentrent dans les cases des formulaires, se
dissociant de celleux pour qui devenir paysan n’était pas un projet de
vie désirable.

« Pour certain·es, « défendre la ZAD » signifiait un projet unifié et
cohérent à construire entre les occupant·es productives et certaines
parties du mouvement plus large, qui, partageant une volonté de
s’intégrer dans une structure légale, étaient capables de travailler
ensemble et trouver des buts communs. D’autres voulaient garder une ZAD
plus proche de l’endroit diversifié et désorganisé servant de foyer
pour des personnes de différents horizons sociaux et avec différentes
conceptions politiques, qui travaillaient ensemble à des niveaux
multiples mais se tenaient ensemble surtout par le fait d’être
voisin·es, et à travers les besoins et les affinités » (*Réflexions
sur la ZAD : une autre histoire*)

Les habitant·es de la ZAD ont raison quand ils affirment que le foncier
est l’enjeu qui relie la « guerre sociale pour la réappropriation des
outils de production » et « la guerre territoriale pour défendre et
prendre soin d’un monde fragile » (*Prises de Terre(s)*). Mais à la
condition que la question de la propriété (privée ou commune) ne soit
pas un péage à l’entrée. Qui se sent légitime et qui a les moyens
(physiques, psychiques, monétaires) de s’installer sur les terres de
l’ancienne ZAD ? À qui donne-t-on la voix dans les décisions qui
impactent l’ancienne ZAD ? Quelle est la composition sociologique du
collectif formé par celleux qui sont resté·es ? Quelle a été la
répartition des pouvoirs pour que la ZAD devienne un projet
d’expérimentation écologique et agricole ?

« Si c’est un kiss kiss bank bank pour permettre à quelques groupes
peut-être sympathiques d’accéder à une propriété certes collective, mais
où nous n’aurons pas notre place, et de poursuivre sereinement leurs
projets sans doute louables, mais où nous n’aurons pas notre mot à dire,
alors, non. » (*Zombie walk à la ZAD, des fantômes et des souvenirs*)

L’un des écueils de l’approche communaliste, en voulant créer un nous
(les habitant·es et les paysan.nes) ne serait-il pas de créer un « eux »
(les anciens zadistes) ?

##### « La ZAD est morte, vive la ZAD » : la place du récit pour nos futurs

Nous pourrions partir du constat des auteur·ices de « *Zombie Walk à la
ZAD* » : « Ce que la Zad n’est plus : une zone de non-droit et un refuge
absolu ; la réponse à toutes nos questions ; l’épicentre des luttes
autonomes et écologiques ; le fantasme de l’unité et de la force
insurrectionnelle ». Mais l’a-t-elle jamais été ?

La ZAD persiste dans les mythes qu’elle a forgés, dans les militant·es
qu’elle a formé·es, dans les stratégies de résistance qu’elle a créées.
Nous continuons à parler de la ZAD. Et dans un certain sens, de fait,
elle continue d’exister dans son héritage. 

Nous avons besoin de ces récits, construits sur la base tangible d’une
lutte, d’expériences collectives de résistance. Nous avons besoin de
leur traduction dans une langue littéraire, que ce soit celle d’Alain
Damasio ou de Wendy Delorme. Nous avons besoin de nous approprier les
histoires *sur* et les analyses *de*.

Et cela me questionne sur la place des mythes, comment ils peuvent à la
fois aveugler et donner des horizons d’existence réjouissants. Comment ils
ouvrent des perspectives et parfois s’écrasent face au réel. J’ai besoin
de croire à la force des histoires, à la manière dont elles fabriquent
du vrai à travers la fiction. Je me dis, si Monique Wittig nous raconte
*les Guerillères*, leurs légendes, leurs luttes, la force de ces
communautés de femmes, peut-être qu’un jour nous vivrons de manière à
nous donner ce nom. Si Alain Damasio imagine dans *les Furtifs* comment
Porquerolles pourrait passer de son statut d’île aux riches à une
arrière-base de militant·es en lutte contre l’État, alors peut-être
qu’un certain nombre de camarades auront envie de réaliser la fiction.
Si Emmanuelle Bayamack-Tam nous décrit *Arcadie*, dans sa beauté et ses
dysfonctionnements, peut-être que nous serons plus à même de penser des
communautés amoureuses libertaires, d’accueillir les migrants, de
prendre soin. Si on réécrivait *Anna Karénine* pour lui faire vivre une
relation lesbienne avec Kitty, peut-être que nous trouverions cela plus
normal que de la voir tomber dans les bras de Vronski – parce que la
norme aurait changé.

Alors à mon tour, j’ai envie d’écrire une histoire qui *me*, et j’espère
*nous*, fera du bien. C’est l’histoire de Cléo, Petite Tête, Ayo, Elio,
Amna. C’est l’histoire de pirates, de communautés autonomes, de renards
et de sauterelles. C’est une histoire d’amourS aussi. Et si nous voulons
la réaliser, peut-être que ce sera la nôtre aussi.

Bibliographie

**Livres, mémoires** :

BAYAMACK-TAM, Emmanuelle, *Arcadie*. Paris : Éditions P.O.L, 2018. ISBN 978-2-8180-6345-4.

DAMASIO, Alain, *Les Furtifs*. Paris : Éditions La Volte, 2019. ISBN 978-2-35587-077-2.

FEDERICI, Silvia, *Réenchanter le monde : féminisme et politique des communs*. Paris : Éditions Entremonde, 2022. ISBN 978-2-37381-093-1.

GRAEBER, David, *Les pirates des Lumières ou la vraie histoire de Libertalia*. Paris : Éditions Libertalia, 2019. ISBN 978-2-37356-138-0.

REDIKER, Marcus, *Pirates de tous les pays*. Paris : Éditions Libertalia, 2008. ISBN 978-2-915866-42-3.

REDIKER, Marcus, LINEBAUGH, Peter, *L’hydre aux mille têtes. L’Histoire cachée de l’Atlantique révolutionnaire*. Paris : Éditions Amsterdam, 2008. ISBN 978-2-84169-570-0.

RAUTUREAU, Thomas, *Mémoire : Voile d’encre : L’émergence de la conception européenne de la piraterie aux 17e-18e siècles*. Université de Paris, 2020. [Mémoire]

TOLSTOÏ, Léon, *Anna Karénine*. Paris : Éditions Flammarion, 1988. ISBN 978-2-08-070934-8.

WITTIG, Monique, *Les Guérillères*. Paris : Éditions de Minuit, 1969. ISBN 978-2-7073-0761-3.

**Tracts et brichures politiques disponibles en ligne** :

Lutter et/ou se faire manipuler au nom d’une lutte ? Soulèvements de la terre versus état, même combat !, 2023. [en ligne]. Disponible à l’adresse : https://iaata.info/LUTTER-ET-OU-SE-FAIRE-MANIPULER-AU-NOM-D-UNE-LUTTE-Soulevements-de-la-terre-5844.html

Prise de terre(s) - Que se passe-t-il sur la ZAD de Notre-Dame-des-Landes depuis l’abandon du projet d’aéroport ?, 2019. *Lundimatin* [en ligne]. Disponible à l’adresse : https://lundi.am/ZAD

Réflexions à propos de la ZAD : une autre histoire, 2019. *Infokiosques*.net [en ligne]. Disponible à l’adresse : https://infokiosques.net/spip.php?article1734

Zombie-walk à la ZAD - Des fantômes et des fantasmes, 2019. *Lundimatin* [en ligne]. Disponible à l’adresse : https://lundi.am/Zombie-walk-a-la-ZAD

**Autres ressources sur le web** :

INSURRECTION : Définition de INSURRECTION, [sans date]. [en ligne]. [Consulté le 4 août 2024]. Disponible à l’adresse : https://www.cnrtl.fr/definition/insurrection

Portes grandes ouvertes de la Zad de NDDL // Dimanche 4 juin de 10h à 18h - Zone A Défendre, [sans date]. [en ligne]. Disponible à l’adresse : https://zad.nadir.org/spip.php?article6887

RÉVOLUTION : Définition de RÉVOLUTION, [sans date]. [en ligne].  Disponible à l’adresse : https://www.cnrtl.fr/definition/r%C3%A9volution

UTOPIE : Définition de UTOPIE, [sans date]. [en ligne].  Disponible à l’adresse : https://www.cnrtl.fr/definition/utopie

ZAD de Notre-Dame-des-Landes, 2023. Wikipédia [en ligne]. Disponible à l’adresse : https://fr.wikipedia.org/w/index.php?title=ZAD_de_Notre-Dame-des-Landes&oldid=210814879

**Podcasts et émissions** :

Faire l’histoire - Le drapeau pirate - Contre les nations, 2021. *Educ’ARTE* [en ligne]. Disponible à l’adresse : https://educ.arte.tv/program/faire-l-histoire-saison-1-le-drapeau-pirate-contre-les-nations

Génération ZAD, 2021. *France Culture* [en ligne]. Disponible à l’adresse : https://www.radiofrance.fr/franceculture/podcasts/serie-generation-zad

Thomas et les pirates, 2022. *Passions Modernistes* [en ligne].  Disponible à l’adresse : https://passionmedievistes.fr/modernistes-ep-33-thomas-pirates/

**Films et séries** :

ODA, Eiichiro. *One Piece*. Glénat, 1997- X.

# en futur commun

## Petite tête

Cleo sentait l’anis et la fumée. Elle avait le visage plissé de larges
ouvertures vers l’extérieur. Quand le ciel bleu touche à sa fin, prend
ses teintes trop changeantes pour être nommées, les ouvertures se
ferment peu à peu. Les paupières mi-closes renvoient aux passants la
lumière neutre. Cleo s’accorde des soleils gris, des rayons blancs et
des débuts de soirée caniculaire.

Le lierre descendait sur ses joues, couvrait les balafres qui la
striaient par endroit. On avait modelé Cleo à même la chair.

Par les jours de grand vent, je l’entendais chanter. Le timbre tenait de
l’oscillation, il était difficile de distinguer sa voix d’un sifflement
long et discontinu. Elle fredonnait et je me revoyais enfant, à courir
contre le Mistral pour sentir l’air enfler dans mes oreilles. Ces
jours-là Cleo chantait avec moi, façade ouverte au vent. Nous partagions
les bourrasques.

J’ai rencontré Cleo en suivant les hirondelles. J’avais déjà marché
plusieurs jours et je manquais d’eau. À travers les plaines calcinées de
la région désaxée, j’attendais le signe des oiseaux comme promesse d’âme
vivante. Depuis des années, les hirondelles revenaient hanter les lieux
que les humains avaient abandonnés au réchauffement climatique. L’été,
elles s’accordaient des quarante-cinq degrés quotidiens, de la faible
présence de végétation touffue et des métaux lourds ingérés par les
insectes. Heureusement, il y avait la garrigue. Ni les incendies, ni la
montée des eaux n’avait eu raison de celle-là. Et grâce aux quelques
bestioles qui s’alimentaient de cette flore rugueuse, les hirondelles
survivaient.

Cette espèce migratrice revient nicher aux endroits qu’elle connaît. En
les entendant quadriller le ciel de leurs cris, je méditais sur ma
démarche. Je suivais littéralement les hirondelles. Je revenais à
l’endroit de la naissance, à la terre chérie, calcinée. Et les voir
apparaître dans mon champ de vision m’annonçait la proximité d’un
village. Ces oiseaux s’accordaient mal des parois lisses et bétonnées
des bâtiments modernes, où ils peinaient à nicher. En revanche, ils me
guidaient vers le toit à l’étanchéité douteuse, aux cavités des maisons
de pierre pillées à la hâte, aux vitres avortées, bref à la carcasse des
lieux qui m’avaient abritée.

Voilà comment j’ai rencontré Cleo. Une hirondelle s’est engouffrée en
elle par une des fenêtres béantes. Je voyais sur les tuiles la morsure
du soleil. Les rayons devenaient trop lourds à porter pour ces maisons
anciennes. Pourtant Cleo jaillissait, bien ancrée dans le sol,
centenaire, les pierres miraculeusement sauvées des pillards, la porte
en bois fermée, donc fonctionnelle. Je reconnaissais sous les écailles
le fantôme des volets bleus si familiers. Les volets repeints chaque
automne, car l’été avait volé leur lumière.

J’imaginais le jardin, l’herbe grillée qui craque sous nos pas, les
fourmis agglutinées sous le figuier dont les fruits tombent avant que
nous ayons eu le temps de les récolter. Aujourd’hui, les arbres
produisent rarement des fruits. Les réserves en eau leur permettent tout
juste de se maintenir en vie. Les arbres donnent des figues de la taille
d’un ongle, des pêches de la taille d’un pouce. Et les mains ne sont pas
grandes. Les racines puisent profondément dans la terre, survivent.
Comme Cleo.

Cela est juste pour les régions désaxées. Elles se nomment ainsi parce
qu’elles ont été radiées du spectre économique français. Pour la
plupart, ce sont des zones littorales, où la montée des eaux couplée au
réchauffement climatique a salinisé les nappes phréatiques. L’eau de mer
a progressivement empoisonné les sols, provoquant la fuite des
agriculteurs.

La mer aussi cessa de respirer. Les cartes des géographes inquiets,
pointant les zones hypoxiques où l’oxygène de l’eau disparaît, faisaient
rougir la Méditerranée. Plus de coraux, plus d’air pour les poissons.
Juste les traces d’un passé industriel écoulé dans l’estuaire du Rhône.

La chaleur caniculaire, elle, a beaucoup tué. Les vieillards et les
pauvres ne pouvaient se payer les climatiseurs indispensables pour nier
l’été. Les non-humains moururent d’arrêts cardiaques, de déshydratation.
Une année, tous les chats disparurent des villes. Une légende romantique
colporte qu’iels se réfugièrent à l’ombre des forêts. Les reportages
dénombrèrent des milliers de corps sans vie aux bords des autoroutes.

La chaleur faisait tourner les étendues d’eaux immobiles. Cocasse, les
piscines de la côte azuréenne devinrent les lieux de reproduction
favoris des grenouilles. Mais dans le même temps, la température des
lacs ne parvenait plus à se réguler. Les algues et les bactéries
proliféraient dans le peu d’eau douce restante sur le territoire. Le
bitume des routes fondait. Nos réserves de sable de construction
s’épuisant, elles ne furent pas reconstruites.

Les régions désaxées sont les espaces où les dégâts climatiques
dépassèrent de loin les possibilités matérielles de survie. Le coût de
la réhabilitation de ces lieux, de leur maintien artificiel par des
solutions technologiques ne fut pas jugé rentable. Nous aurions pu
généraliser les mégabassines en pâles remplacement des nappes
phréatiques, lancer des partenariats public privé pour assurer la
désalinisation de l’eau mer, importer les tomates des pays plus au Nord,
lancer un plan social pour la climatisation des logements, maintenir
artificiellement le béton des bâtiments grâce à des couches
réfrigérantes… Mais cela coûtait simplement trop cher. L’économie
française en déroute dû admettre son échec. Le capitalisme vert n’avait
pas triomphé.

Les habitants quittèrent les lieux.

Les départements furent supprimés des cartes officielles.

Commença l’ère des non-espaces.

L’ère des espaces vacants, des souillures historiques, des négations
sociales, des villes mortes, de la périphérisation, du hors-carte, des
friches industrielles, des trésors orduriers, des forêts de Renouée du
Japon, de la garrigue hospitalière, de la reconquête des hirondelles, de
l’ère zéro, des possibilités de reconstruire à partir de nos cendres.

Quand les arbres respirent, les gouttes de pluie se condensent dans
l’air, s’évaporent. La fumée blanche émane des feuilles et de nos
bouches entrouvertes, trop chaudes. L’eau remonte, sort de son corps.
Une âme dispersée ne peut être contenue dans un seul visage.

Il a fallu réapprendre la terre meuble, les perles de sueur coulant le
long des branches, les alvéoles des champignons, l’humidité des pierres,
le nom des plantes qui se saturaient de métaux lourds. À partager l’eau
avec les non-humains, le sol, le ciel. À reconstruire le cycle.

Quand je suis entrée chez Cleo, de petits monticules de bois de Renouée
du Japon broyé dessinaient un cercle sur la dalle. En son centre, une
bassine en cuivre recueillait les eaux de pluie mal filtrées par la
toiture. En traversant ce qui avait dû être un salon, je retrouvais la
figure géométrique dans le jardin, dans l’amoncellement de feuilles
mortes en décomposition. Au cœur, le réceptacle d’un jus de compost
malodorant. Et sur les murs de la cour intérieure, exhumées en
particules de peinture, les mêmes formes concentriques. Chaque point de
couleur ramenant au centre d’une spirale. C’était le premier signe de
vie que m’accordaient les désaxé·es.

Plus tard, iels m’offriraient la science des alambics, des paillages,
des pistages de sources, de la décontamination de l’eau de rivière, des
alliances interspécifiques. Iels avoueraient m’avoir suivi·e pendant de
longues semaines, depuis mon arrivée dans la zone désaxée, au jour de ma
rencontre avec Cleo. Iels m’avaient observé·e avec appréhension passer
la porte du premier refuge, profaner l’espace secret du lieu sûr.

Les désaxé·es sont nomades, ils suivent le chemin de l’eau le long des
zones désertées. Comme les hirondelles.

C’est Ayo qui m’a expliqué qu’iels avaient adopté la spirale, à la fois
pour symboliser le cycle, et de manière méthodique, pour quadriller le
paysage sans risquer de passer à côté d’une source. Cleo était une de
leurs hôtes, parmi d’autres. À chaque étape, iels paillaient la terre,
faisaient germer les graines pour les prochain·es arrivant·es,
épandaient le compost et récoltaient l’eau de pluie.

Les contours de la communauté restaient flous, car les désaxé·es
refusent de se sédentariser sur un territoire qu’iels devraient
défendre, intégrer. S’approprier un refuge aurait mené à devoir choisir,
faute de place et de ressources, qui avait le droit de s’y loger et de s’y
nourrir. Les désaxé·es voulaient former des rhizomes, un réseau
compost-humain à grande échelle. Le mouvement marquait l’absence de
propriété sur les terres, chacun·e pouvant se réclamer visiteur·euse. De
toute façon, l’eau exigeait de tourner.

Ce sont les yeux d’Ayo que j’ai aperçu à travers un arbuste, quand,
épuisé·e de fatigue, je me suis avachi·e dans l’herbe pour sucer la
rosée qui mouillait ses tiges. Un vert de gris sur une peau mate, une
couleur rieuse sur un visage formé pour le soleil. Et ses yeux riaient
oui, de me voir affalé·e sous l’arbre, à quelques mètres d’un puits que
les désaxé·es remettaient en route et dont j’ignorais l’existence.

Les dents lumineuses m’invitaient à rejoindre.

Ayo porte deux cicatrices remuant sa poitrine. Les vagues se meuvent
avec l’air qui gonfle, épuise son thorax.

La nuit je colle l’oreille à la mer pour l’entendre respirer : la houle
frappe, régulièrement. Alors je pense, Ayo a les côtes solides.

Le corps d’Ayo contient tout ce qu’il y a de lacs, de rivières, de cours
d’eau, de nappes phréatiques, d’océans pacifiques, de mare, de mer
Méditerranée, et nous manquons d’eau. Nous manquons d’Ayo dans notre
monde. Je pense cela et je ne peux pas lui dire, parce que j’ai lu
quelque part que la force d’une vague dépend de son éloignement depuis
la côte et de l’intensité du vent. Je ne peux pas exiger à la houle.

Six mois que nous quadrillons l’espace de nos marches concentriques,
élargissant chaque jour le périmètre, à la recherche d’un point d’eau,
d’une ancienne bergerie à consolider, d’une épicerie abandonnée pour
enrichir les réserves.

À mesure que nous avançons, nous laissons certain·es derrière nous pour
se consacrer aux ouvrages. Iels sont rejoints par d’autres hordes,
offrant un de leurs membres pour aider à la tâche, jusqu’au prochain
passage. Alors ils rejoignent leur groupe, ou adhèrent à un nouveau,
comme les nuées d’oiseaux se séparent et s’amassent pour recomposer
leurs figures. Cela fluidifie les contours d’une horde. Au moment
choisi, un·e désaxé·e peut repartir, avec le groupe de son choix.

Je participai au chantier de réhabilitation des restanques, pour
faciliter l’écoulement de la pluie. Nous revenions sur la parcellisation
des terres jadis instaurée pour y inscrire une pente amie, où l’eau
pourrait faire son chemin d’un parterre de semis à un autre. Je mis
longtemps à me faire au rythme et à la pénibilité de la tâche. Mes
paumes se couvraient régulièrement de plaies, la corne n’était pas
encore formée. Je sentais mes épaules pétrifiées par l’effort, mes
cervicales hurler lorsque je relevai la tête pour m’essuyer le front.

Eni, l’un des membres d’une horde amie, rit de la sueur qui coulait de
mon crâne sans discontinuer : « tu la sors d’où, toute cette eau ? ».

Je grimaçais et posait les deux mains sur la pelle, le menton calé sur
les jointures de mes doigts. « Ça fait longtemps que vous êtes sur le
chantier ? »  « Quelques mois. Mais personne reste autant, c’est dur.
Moi je suis passé trois fois par ici déjà. Et entres temps je fais des
pauses ». 

J’hochai la tête et remarquai que le mouvement me donnait des vertiges.
Je fermai les yeux. Eni posa une main sur mon épaule : « Tu
t’habitueras, le corps n’est plus fait pour ça, mais ça revient. Et puis
choisis ton rythme, te force pas à travailler comme les autres, on fait
pas ça ici ! »

J’entrouvrais les paupières. Un groupe de désaxé·es prenait le chemin
inverse et tournaient le dos à la colline, charriant entre leurs bras
les colonnes vertébrales de bambous. « Mais qu’est-ce qu’ils foutent ? »

« Iels ne croient pas en ce projet de réhabilitation de restanques. Iels
ont décidé d’expérimenter une irrigation goutte à goutte, en surélevant
ces bambous du sol et en y creusant des évacuations ». « Pourquoi on
fait pas ça ensemble plutôt ? » Eni rit : « Bah parce qu’iels y croient
pas je te dis ! Au moins s’iels travaillent, c’est pour un projet qui
leur plaît. Et puis c’est bien de miser sur plusieurs solutions, s’il y
en a au moins une qui fonctionne, on sera refait·es ! »

Leur système D. Cette idée première : de l’échec et des dissensions
naissaient un projet politique. Chaque soir, les voix montaient de
quelques octaves, sur la gestion des stocks, des énergies humaines. Mais
toustes repartaient renforcé·es dans leur idée fixe et leur envie
d’essayer. La seule limite posée était de toujours garder une
surcapacité, quelques jours de nourriture et d’eau d’avance, en cas de
drame. Et l’autre, implicite et non-nommée, de voyager à travers les
hordes.

Les désaxé·es tenaient parce qu’iels s’étaient configuré·es en réseau,
échangeant forces, expériences, matières premières et amour. De l’amour
pour éviter les pillages des stocks collectifs, de l’amour pour soutenir
la fatigue, de l’amour pour élever collectivement le peu d’enfants qui
naissaient, de l’amour pour ne pas se sédentariser ou renouer avec la
propriété.

Ayo a des amant·es dans plusieurs hordes de désaxé·es. À chaque fois
qu’Ayo quitte le groupe, j’ai peur que la marée se retire à jamais. Et
je compte, sans savoir, les coquillages dans sa vie. L’irisée. La
bivalve. Le nautile. La perlière. La dentale. L’ébréchée. Le papillon.
J’ai lu quelque part que la force d’une vague dépend de son
éloignement depuis la côte et de l’intensité du vent. On ne contient
pas l’eau, on ne la retient pas. On prend simplement soin des
particules de sel.

Après cinq jours aux chantiers des restanques, je rejoignais Ayo, le
corps moulu et les nerfs fatigués. Je cherchais le pli d’un
retournement amoureux, une excitation nouvelle ou une distance annonçant
le changement relationnel qu’iel allait m’annoncer. Je cherchais sur sa
peau la trace des coquillages. Et son soleil riait, comme s’il décelait
mon inspection. Je sentais la densité des mauvaises pensées dans mon
corps et j’avais honte.

J’attendais la nuit pour reposer mon visage sur son torse. Je collais
mon oreille à l’endroit des cicatrices, en retenant mon souffle. Et je
souriais de soulagement. Je pouvais encore entendre la mer.

## Féminaire de Petite Tête, jour O

Parfois je ne sais pas si je voudrais que l’on m’appelle Louie ou
Danielle

L o l l e

Parfois je voudrais que les autres se comportent avec Louie

a e i ou

avec Daniel

le

j’ai les cheveux longs

Lo lo lo lo lo ie

j’ai gardé l’habitude d’épiler l’espace entre mes sourcils

ielle

avec Louie

je fume beaucoup de cigarettes

j’entretiens l’art du vide

une certaine propension à prendre de la place

dans les débats

l l l l l l l l l l l l l l l l

ou sur les bancs en bois

D a n o u i e

en gros

tout ce qui sort de la ligne prend la forme d’un point

iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii o

tous les points sont des arguments ou des préoccupations

une forme inassouvie de la matière

ae

oe ie

ue le el

en gros

il

il n’y a rien de biologique

Dan

ce sont de simples variations

Lou

érigées en lettres capitales

A E I O U

j’invoque

tout ce qui sort de la ligne

des points

signes de vie ou de chaos

OooOoooOoOo

et la place de prénoms

D a n i o u i e l l o u i e l a n i e l a d i o u i a d i o u i a d i o
u i a d i o u i a d i o u i a d i o u i

dans une existence lisse

pour que l’on se comporte

i e l i e l i e l i el iel iel i e l l l l l lllllllllllll

non plus avec les cheveux longs

l o n lonlonlon

le vide les chromosomes ou la matière

mais avec le genre zéro

O

.

j’écris

~~lui~~elle *a*el

iel

j’écris

sur une nouvelle page

Chère Monique,

je connais les mortes les connivences les révolutions les sirènes les
spirales les génisses les nageoires les opales les nymphes les amazones
les lèvres les béances les translations les oscillations les nages les
danses initiatiques

mais reste-t-il des mots à inventer

ce que nous aimerions construire dis-moi

avec quels gestes

les contorsions les transfuges les périlleuses les pas de chat les
bourrées les anguilles les transformées les agnelles les louves les
mutations les alchimistes les rondades les latences les vides les
sourcières les bombes les cueilleuses les torsades les devenues les
solitaires les meutes les lionnes

quels onguents pour prendre soin de nous

en mémoire des saignées

## Ayo

Petite tête, je lae vois venir à mille lieues, avec son amour
hétérosexuel et sa manière de s’attendre à ce qu’un jour je ne parte
plus. Je suis l’enduit, je tiens ces groupes ensemble. En les reliant,
j’agrandis les sphères, je fais vivre l’espace vacant. Je suis ce
nomadisme, qui s’attache aux lieux en les laissant occuper celleux qui
les traversent. Et de cette liberté, je n’en reviendrai pas.

Cette histoire d’amours tristes qu’iel se raconte, iel ne la connaît
pas. Je vis depuis deux ans ici, j’ai vu des liens se rompre et
déséquilibrer les tissus de nos sphères. Je sais les relations qui se
transforment, le sang qui négocie avec le vide pour venir irriguer les
membres amputés. Mais je crois aux recompositions. Aux amant·es ami·es,
aux anciennes relations qui font grandir les hordes. C’est une manière
d’être aux autres.

Quand dans une ancienne vie, j’ai décidé d’abandonner mes seins et la
définition étriquée de la féminité, les médecins m’ont fait remarquer que
ne pourrai jamais allaiter d’enfants. C’était une manière détournée de
me demander de faire le deuil de la maternité, de laisser cela à
d’autres, aux couples fonctionnels et à l’hégémonie. Aujourd’hui je
dépends de quatre enfants. Je dépends de leur affection, de leur volonté
à me laisser apprendre à jouer avec elleux, de leur envie d’échanger
avec moi sur ce que je sais de la vie.

Deux de ces enfants viennent de mon ventre. Les autres de relations
amies. De mes compagnes et des projets de vie qu’iels ont tissés avec
leurs amant·es. Et dans tout cela, une place m’est accordée. Je ne me
suis jamais senti·e à ce point au centre de moi-même. Ces rhizomes
affectifs que nous créons, ce sont ceux de familles choisies. Contre la
norme, contre l’injonction à s’appartenir. Ils font tenir les sphères
ensemble.

Le dernier enfant à m’avoir adopté·e s’appelle Elio. Iel est issu·e de
l’union de deux amant·es d’une autre horde, avec la complicité d’un·e
géniteur·ice masculin⋅ine. Je ressens plus souvent le besoin de
m’absenter pour le voir grandir. Le rôle éducationnel que je me vois
jouer dans le futur est accepté par mes pairs, iels me laissent partir.
Je lae vois battre la terre de ses pieds nus, les cheveux ensoleillés
mêlés à la poussière. Il joue avec les sauterelles et apprend à manier
le vif. C’est une merveille de nos générations dédiées à l’amitié
inter-espèce.

Ce rapprochement entre les non-humains et les enfants n’a pas été
prémédité. Laisser des gamins hors d’une cour d’école et les livrer aux
grands espaces a créé cela : prendre les autres espèces pour mentor. À
qui le papillon, à qui l’ancien chat domestique, à qui le renard aperçu
en train de s’abreuver à la rivière quand la lumière décline. Je ne sais
pas avec quelle espèce Elio se liera finalement. Peut-être avec
plusieurs d’entre elles, comme iel nous voit le faire au quotidien entre
humains. Je vois sa peau tannée et ses cheveux sales et je décide qu’iel
me ressemble un peu.

Dans quelques années, les membres de sa horde proposeront à Elio de
devenir nomade ellui aussi. Pour l’instant, iel reste sous la protection
de ses mères. Mais l’imagination en friche et les espaces à acclimater
auront raison de ces attachements. Elio ira rencontrer d’autres mères,
d’autres enfants. Je crois qu’iel s’en doute et que cela explique les
regards discrets qu’iel me jette régulièrement. Mais ce qu’iel ne sait
pas, c’est que nous serons nombreux.ses pour veiller sur ellui.

Partir joue un rôle central dans l’éducation des enfants. C’est la
meilleure manière pour qu’iels se forment au monde par elleux-mêmes.
Aller apprendre auprès des guérisseur·euses, des troubadours, des
chamanes, des constructeur·ices, des cuisinier·es, des langagièr·es ou
des biologistes. Suivre l’instinct qui leur intime vers quels champs se
tourner. Écrire leur propre féminaire.

## Féminaire d’Ayo, non-daté

Elles disent que sans elles l’usine ne pourrait pas tourner.

Quelqu’une prend un sceau d’eau savonneuse et le répand sur le sol. Elle
frotte nettoie décape chasse le liquide d’un mouvement de bras
circulaire et ample. Les poils du balai brossent le sol, c’est une
musique, le bruit des carreaux sous le manche, un rythme mécanique.
L’eau se noircit sous le coup d’un poignet oblique. Elle avance à pas
mesurés, selon une méthode précise pour nettoyer l’ensemble de la pièce
sans repasser par le même chemin. Il ne faut pas souiller les endroits
en posant un pied maladroit sur la surface mouillée.

Quelqu’une trace des lettres noires sur un morceau de carton peint en
blanc. Ses lèvres se pincent sous le signe de la concentration. Les
lettres sont formées de bâtons qui ne signifieraient rien indépendamment
les uns des autres. Les bâtons mis bout à bout forment un slogan qui
signifie leur importance pour le reste du groupe. Leur importance est
matérialisée par le fait que l’usine ne pourrait pas tourner sans elles.
Pour aller plus loin, elles précisent en ajoutant des lettres au-dessus
d’un triangle élargissant la phrase, l’occupation de l’usine ne pourrait
pas tourner sans nous. Voilà ce que la pancarte renforcée blanche aux
lettres noires signifie.

Quelqu’une dessine les plans des futures garderies collectives. Elle
forme des schémas de pièces reliées au sein de l’usine par de minces
couloirs et relève la tête vers le groupe pour recueillir leurs
opinions. Il faudrait de grandes ouvertures, des fenêtres, des halls
pour jouer, des structures en bois et des fleurs qui tomberaient du
couloir. Elle rature le plan et chiffonne le papier, déchire une autre
feuille vierge de son cahier. Ce n’est pas encore ça, il faut
recommencer.

Quelqu’une refuse de faire à manger parce qu’aucun d’entre eux ne se
trouve dans la cuisine. Elle prend les cagettes de légumes et les empile
devant l’entrée des bureaux, des machines de manutention, des tapis
roulants, des voitures sur le parking. Elle bloque le blocage, manifeste
dans la manifestation, fait la grève des grévistes. Elles disent que
tant que les hommes ne passeront pas le tablier, elles se nourriront
entre elles, quitte à les laisser dépérir.

Quelqu’une ramasse des projectiles à jeter sur les représentants du
maintien de l’ordre quand ils tenteront de franchir les portes de
l’usine. Des pierres des bouts de bois des canettes des bouteilles de
verre des crottes de chien et des restes de viande avariée seront jetés
contre toute personne qui tentera de briser le chaos.

Quelqu’une refuse d’investir un débat sur le bien-fondé de
l’intersectionnalité. Elles disent que désormais les discussions
viseront l’amélioration matérielle de leurs modes de vie ou seront
abandonnées. Elles coupent la parole à ceux qui nient la domination
masculine au sein de la lutte des classes. Elles se moquent de ceux qui
voudraient que la révolution se fasse sans remettre en cause tous les
privilèges. Elles disent que le genre zéro doit être invoqué.

Quelqu’une déclare l’abolition du travail salarié et la collectivisation
des moyens de production. D’autres rient et dansent et crient et mettent
le feu aux ordures aux voitures aux fascistes aux institutions aux
magasins à la révolution.

Cleo se mit à accueillir les souffrant·es. Une semaine particulièrement
chaude, de nombreux lits de fortune se montèrent, saturant les espaces
sains de la maison. Y séjournaient les aîné·es, les tout·es petit·es et
les corps abîmés, ne supportant plus la chaleur. Cleo les protégeait de
l’épaisseur de ses murs, de l’humidité singulière des vieilles maisons
de pierre aux volets scellés.

J’emmenais Elio là-bas. Iel répétait, de sa voix grave, comme un chœur
récité sans encore le comprendre : veiller sur nos morts, prendre soin
des vivants. La première fois que je l’entendis prononcer ce psaume, je
me retournai vivement vers lui. Iel continua : veiller sur nos morts,
prendre soin des vivants. Iel fixait quelque chose derrière-moi, loin
des éclats de jeu qui vrillaient habituellement son regard. Et je
pensai, c’est Amna qui parle à travers lui.

Amna, la mère que nous veillions chaque nuit. Celle qui a élevé mon
esprit et celui d’Elio après le mien. Qui souffrait à l’idée que nous
prenions soin de son corps faillible, là où sa tête ne flanchait pas.
Iel aurait voulu des mots sages, profonds, des mots pour amener son âme
à bon port, là où nous lui proposions des légumes séchés et des tisanes
de thym récolté dans la garrigue. Combien de fois a-t-iel détourné les
yeux lorsque je passais un gant humide, puisé des seaux de récupération
d’eau de pluie qu’iel savait si rare, pour rafraîchir sa peau brûlante ?

Amna ne souhaitait pas prolonger la vie à tout prix. Moi, j’aurais trahi
pour rallonger les nuits passées à côté d’iel, à lae regarder dormir,
une main dans le creux de son dos. Pour prolonger ce lien physique, les
caresses sur la nuque, les cheveux démêlés, la tendresse et les ventres
noués. J’aurais voulu toucher cette peau ridée à satiété, lui procurer
du plaisir pour alléger le poids du corps.

La dernière nuit où je rejoignais sa couche, je plongeai ma tête sur son
bas-ventre, baisant le duvet, les côtes iliaques efflanquées qui
formaient l’ossature de ses hanches. Iel me fixait sans sourire,
pressant sa main sur mes cheveux. Je soutins son regard en caressant ses
cuisses, remontant mes doigts jusqu’à effleurer sa vulve. Iel ferma les
paupières et releva le menton vers le ciel. Ses ongles s’enfonçaient
dans mon cuir chevelu à mesure que je soufflai sur ses nymphes. Je
touchai mon sexe, récoltai ma cyprine pour humidifier le sien. Elle
ouvrit son bassin pour que j’entre en iel. J’embrassai son clitoris en
la pénétrant de mes doigts, mouillant davantage à l’entendre gémir.

Vers la fin, quand son corps lae rendait absente pendant de longues
minutes, cette image revint souvent brûler ma rétine. Elle s’imposait
par-dessus le réel, à mordre ma peau et tordre mon bas-ventre de savoir
que ce qui avait été partagé n’existerait plus. La fièvre avait empiré
en peu de temps et Amna perdait conscience.

Dès notre rencontre, iel m’a parlé de la mort. Iel disait que nos vies
s’étaient croisées pour qu’iel m’aide à comprendre, à tracer mon chemin
dans le dénuement. Amna voulait transmettre son expérience du mystique.
Iel s’était entiché·e d’Elio et joua le même rôle dans la construction
de ses espoirs d’enfants.

Lors de notre dernière visite, Elio enfouit sa tête blonde dans le drap
protégeant le corps d’Amna, lae suppliant de ne pas partir, de rester
avec nous et Cleo. Amna passa la main dans ses cheveux, caressant son
crâne qu’iel ne voulait plus relever. Quand les hoquets se calmèrent,
Amna saisit le menton encore mouillé : « Écoute-moi bien jeune enfant.
Je vais te raconter une histoire. »

Elio s’essuya d’une manche et ouvrit de grands yeux. Amna laissa sécher
le silence et commença : « C’est l’histoire d’un·e petit·e humain·e
haut·e comme toi, iel provenait d’une région très, très lointaine. » 
« Comme maman ? » interrogea Elio en tournant son visage vers le mien.
Amna acquiesça : « Comme maman. Figure-toi que de là où iel venait, les
humain·es ne craignaient pas la mort. Iels grandissaient dans la
pauvreté, pareille à celle que tu connais aujourd’hui. Sauf que la
société ne les éduquait pas à se battre pour leurs besoins, à lutter
pour triompher. Iels apprenaient à comprendre la Nature. Dis-moi Elio,
qu’est-ce que c’est la Nature ? »

Elio énuméra en comptant sur ses doigts : « la forêt, les arbres, la
pluie, le cycle de l’eau, les non-humains, les humains… » et Amna
hocha la tête : « La Nature, c’est tout ce qui apparaît sur Terre. Chez
lae petit·e humain·e dont nous parlons, les êtres méditaient pour
apprendre à se comprendre, à lire en elleux. Iels connaissaient aussi le
nom des plantes, des roches et des nuages. Iels connaissaient les trous
noirs, les galaxies et les exoplanètes. Et ainsi, ils accédaient à
l’empathie et à l’arcane de l’existence : nous naissons, nous vivons,
nous mourons. Lae petit·e humain·e, nous l’appelerons Elia, n’avait pas
peur de mourir. Iel savait que chaque chose était vouée à périr. »

Elio acquiesça sans parler. Amna fatiguait et avait besoin de silence
pour continuer. Elle finit par reprendre : « Tout ce qui apparaît sur
Terre doit disparaître un jour, tout. Même la Terre. Même les objets que
tu vois ici, cette table de chevet, cette chaise, cela paraît solide,
mais cela s’usera, se détériorera. Même ton corps, ton esprit,
appartiennent à la Nature. Il faut comprendre la spirale et l’accepter.
»

Amna prit la main d’Elio. Je lae sentais embarrassé·e par sa poigne, mais
iel ne pouvait quitter le visage de l’aîné·e, ses yeux, sa bouche, sa
voix qui s’animaient de l’héritage d’un·e autre, des mots à transmettre
avant de s’éclipser. « Toi Elio, tu as peur aujourd’hui parce que je
meurs. Mais si quelqu’un·e meurt à des milliers de kilomètres, tu ne
ressens rien. Pourquoi ? Toi et moi nous aimons la vie, les autres,
pareillement. Sinon, pourquoi tu t’entendrais si bien avec les
sauterelles ? C’est parce que nous sommes toustes lié·es par nos vies
d’avant, et que pour le sentir, nous avons besoin d’accepter que celleux
que nous aimons vont naître, vivre et mourir, sans se rendre
malheureux·se. Tu pourrais faire ça ? »

Elio releva le menton et acquiesça fièrement. Il enleva sa main et
l’essuya sur le drap pour la mettre dans son dos. « Et Elia, qu’est-ce
qui lui est arrivé alors ? » demanda l’enfant. Amna sourit : « Iel a
rompu le cycle des réincarnations. Si tu crois à la spirale, aux vies
d’avant, aux vies d’après, mourir, c’est un recommencement perpétuel.
Mais cela fatigue les âmes. Nous voyageons sans arrêt, d’une existence à
une autre, nous tournons, comme les hordes pour trouver de l’eau. Arrive
un moment Elio, où tu seras fatigué·e de vivre. C’est normal. Et tu
n’auras plus envie de reproduire la spirale. Elia a réussi à s’éteindre
vraiment, à calmer son âme. »

Nous repartîmes de Cleo tard dans la soirée, Elio sur mon dos, menottes
accrochées à mon cou. Nous partagions le silence que les mots d’Amna
avaient formé en nous. Je n’avais jamais vu Elio si grave. Iel ne
courait plus devant-moi dans les herbes, gardant le temps de se vautrer
dans les champs jusqu’à ce que je le rejoigne. C’est à peine si je
sentais bouger sa poitrine se plaquer contre mes épaules au rythme de sa
respiration. Avant de rejoindre la horde, il me glissa : « Je pense que
l’âme d’Amna est calme maintenant. »

J’ai renversé le seau sur le sol. Une fissure l’ébréchait depuis la
base. Elle remontait en fendillant la peinture. C’est pour cela que le
seau ne se remplissait pas, l’eau s’infiltrait dans le sol. Toutes ces
choses que l’on essaie de retenir et qui coulent, se tapissent au creux
de nous-mêmes. J’ai pensé à ce qu’Amna avait dit de la mort. Aux mots
« patience » et « déshydratation », induisant une forme de relation
entre les termes. Est-ce que l’eau craint la mort ? Est-ce que l’eau
craint le soleil quand il l’assèche, transforme son état ? J’ai pensé au
corps déshydraté d’Amna, qu’on avait brûlé sûrement. Son corps devenu
fumée. Et la fumée parcourerait l’étendue de la région désaxée, pour
donner le signal et prévenir de son abandon. Alors « patience » et
« déshydratation » devraient indiquer l’attente éprouvée par Amna avant
de pouvoir quitter son corps.

J’essaie de colmater la fissure avec de la résine. Le seau n’a pas
conscience d’être infirme. C’est de la matière inerte, le seau ne pense
pas aux différents usages que les humains lui trouvent. Lorsqu’il
recueille de l’eau, lorsqu’il se renverse pour arroser, lorsqu’il
accueille un ustensile quelconque pour mélanger ce qui réside à
l’intérieur de lui, le seau ne calcule pas. Il est insensible aux
opérations que nous réalisons avec sa chair, formant un contenant fort
utile pour stocker ce qui nous manque que trop, l’eau.

Peut-être que l’eau pense et que nous tournons autour de sa conscience.
Nous formons des spirales autour d’un Grand Tout. Nous ritualisons nos
rencontres avec le liquide. L’eau est vivante et elle ressent à sa
façon. Elle trompe nos seaux, nos soifs, nos puits et nos langues
sèches. Nous la suons, nous la pissons et elle s’enfuit sous la terre,
hors de nous. L’eau est une extension de notre poumon gauche, de notre
intestin grêle. L’eau est l’organe sans lequel notre corps ne peut
fonctionner. Sans eau, nous n’avons pas besoin de rate. Sans eau, nous
ne savons utiliser aucun hémisphère de notre cerveau. Sans eau, la roue
est une invention inutile. Sans eau, il n’importe à personne d’écrire
sur les pages blanches des féminaires.

Et quoi d’autre encore, pour les enfants ? Que peut-on leur promettre
que des seaux fendillés et de l’eau à aller chercher plus profond dans
la terre à chaque saison ? Peut-on leur livrer des symboles hors de
l’épuisette laissant filer les gouttes dans la rivière ? Et où vont les
poissons quand le lit du cours d’eau est à sec ? Peut-on imaginer
d’autres questions ? Peut-on grandir sans les rivières ? Et comment dire
ces mots ? Comment taire l’angoisse sur mes lèvres gercées lorsqu’elles
se tordent en un sourire ? Et c’est vrai, où vont-ils les poissons ?
Est-ce que quelqu’un le sait ? Est-ce que quelqu’un est déjà parvenu au
bout de cette réflexion ? Et où sont-ils les poissons sinon au bout de
cette rivière à sec ? Que vont manger les oiseaux ? Peut-on imaginer
d’autres histoires ?

Je barre la liste et j’écris

À cet instant

Tu grandiras par toi-même

Tu comprendras le vide au fond des émotions

En creusant le sol de tes doigts calleux

Tu chercheras l’eau sous tout rapport

Tu diras je cherche l’eau sous tout rapport

Et les autres répondront

Tu as quitté tes mères et ta famille

Pour venir gratter la terre à t’en déchirer la peau

Regarde le sang sous tes ongles

Tu as grandi ça y est

Alors

Tu porteras à ta bouche sans salive

La somme des épines du monde

De celles qui se logent sous ton derme

Cela te fera mal

Tu pleureras doucement

Et les autres diront

Tu as trouvé l’eau sous un rapport

Et ils riront longtemps

Tu devras repartir

Dans un chemin en spirale tu trouveras

Le bruit des ailes de papillons en tourmente

La fumée de paille soulevée par le vent

Le secret des écorces sanguinolentes

Le langage des pins murmuré aux enfants

Tu devras repartir

## Elio

J’ai joué avec les fenêtres cassées et j’ai senti le fer sortir de mes
doigts. Parfois je pense aux lunes qui se découpent et elles ne saignent
pas non. Le fer sortait rouge de ma peau et j’ai plongé les doigts dans
la terre pour arrêter la couleur de sortir, j’avais peur que le bleu et
puis le rouge et puis le jaune et le violet jusqu’à devenir
transparent·e. Amna est transparent·e je sens sa présence quand je ferme
les yeux la nuit et que maman dit que je m’endors alors que je l’entends
je sais que je ne rêve pas. Maintenant les fourmis se disputent le fer
mais comment les raisonner. Je ne crois pas qu’il faille travailler
comme dans une fourmilière et même si Kari pense mais qui serait la
reine ici, moi je veux bien être la reine seulement j’aime bien jouer au
pirate aussi alors ce serait ennuyant et qui voudrait se contenter d’un
seul rôle ici. Les fourmis ne cherchent pas l’eau de toute façon.

J’ai vu l’oizo se poser au bord de la rivière et plonger son long bec
dans la terre humide là où avant il y avait les poissons quand le lit
n’était pas encore à sec. Je m’approche en rampant comme Kari m’a appris
et je pousse un cri de guerre mais l’oizo ne veut pas jouer il décolle
en colère je m’excuse mais il est déjà parti. Il suffit de trouver les
libellules pour commencer une partie. Les libellules sont moins timides,
je fais la statue et elles viennent me chagriner les oreilles ou les
épaules et c’est celui qui bouge le premier qui a perdu. Une fois elle
s’est posée sur ma joue et c’était trop difficile de ne pas rigoler elle
a gagné voilà tout. Je ne me suis pas mis en colère. Parfois le chat
vient se frotter entre mes jambes mais on dirait que c’est toujours
quand j’ai autre chose à jouer. Quand je me cache pour l’attraper il a
chaque fois un coup d’avance alors je me dis que moi j’aimerais devenir
furtifve comme un chat et avoir le troisième œil. Kari dit que ça
n’existe pas et si c’est vrai pourquoi le chat se sauve quand je suis
dans son dos ?

La nuit quand le chat s’endort sur mon ventre de plaisir il enfonce ses
griffes dans mes vêtements, ça me chatouille un peu et j’essaie de ne
pas rire pour ne pas le réveiller. À un moment il se relève et saute
brusquement pour aller chasser, je fais semblant de ne pas m’en rendre
compte mais il y a quelque chose qui se serre au niveau de mon torse et
je rapproche mes genoux de mon ventre pour sentir une nouvelle chaleur.
Un soir quand elles dormaient je suis parti·e avec lui personne ne m’a
vu·e je jure que c’est vrai. Nous sommes allé·es chasser les sauterelles
c’était la nuit la plus drôle de ma vie, j’observais le chat se tapir
dans l’herbe sèche bondir et coincer de ses pattes l’un des insectes, le
saisir entre ses dents ça je ne l’ai pas fait. Après plusieurs
tentatives j’arrivais moi aussi à tenir les bestioles entre mes mains
alors elles se tenaient immobiles pour prétendre ne pas être vivantes ce
qui n’est pas tout à fait la même chose que d’être mortes, elles
imitaient plutôt quelque chose de minéral comme un caillou ou un bâton,
c’est Kami qui m’a appris ça, que dans la prédation une des stratégies
de survie des animaux consiste à feindre. J’ai voulu caresser les ailes
des sauterelles cette nuit-là mais alors elles ont bondi et j’avais
déjoué leur stratagème. Ce fut ma première étape pour devenir un chat.

La prédation c’est un mot compliqué. Ayo a dit c’est quand tu prends et
que tu ne rends pas. Qu’il faut essayer de rendre le plus possible, mais
que nous sommes des prédateurs féroces. J’aime bien quand iel utilise le
mot féroce, on dirait qu’iel va me raconter une histoire. Quand j’ai
pris des cerises en cachette dans le verger avant la récolte, j’étais un
prédateur. Ça m’a rendu un peu triste parce que je sais qu’elles
n’aiment pas ce mot-là, alors j’ai replanté les noyaux dans le jardin de
Cleo.

Quand j’ai demandé à Ayo pourquoi iel n’aimait pas ce mot, iel a dit
j’entretiens un rapport compliqué avec lui. Je crois que cela signifie
qu’iel aussi a déjà volé des cerises, ou peut-être même des abricots.
Dans tous les cas, le premier mot dans le féminaire d’Ayo, je lui ai
demandé un jour de me le montrer, c’est symbiose. Iel a écrit, avec les
petites lettres d’un·e adulte qui a oublié la graphie, 1. Association
durable entre deux ou plusieurs organismes et profitable à chacun
d’eux. 2. Fusion, union de plusieurs choses ; association étroite et
harmonieuse entre des personnes ou des groupes de personnes. Je l’ai
appris par cœur. Le 1., ça me fait penser au chat, quand je lui donne
les restes d’un repas et qu’il m’apprend à saisir les sauterelles en
plein vol. Le 2., ça me fait penser à mes mamans, à Ayo et Amna, à moi
et Ayo, à moi et Kami, à moi et Kari, sauf quand on se dispute pour
savoir si le troisième œil existe ou pas. J’ai marqué dans mon féminaire
que symbiose, c’est un autre mot pour dire amour.

## Féminaire d’Elio, jourarbre

Iels habitent dans les arbres, comme dans le livre qu’Amna racontait.
Certain·es ont inventé des embouts en métal qu’iels fixent dans l’écorce
pour boire la sève. C’est une boisson très sucrée. Plutôt que de la
stocker dans des bouteilles, iels se baladent avec leur ustensile. Iels
l’utilisent dès que la soif assèche leur salive. Le goût de la sève
change selon l’arbre qui la porte. La plus populaire serait celle
d’Eucalyptus, surtout l’hiver. L’été, iels préféreraient la sève
d’oranger.

Dans les arbres, les feuilles sont tour à tour des instruments de
musique des contenants des outils des becs verseurs des supports
d’écriture des toges des friandises des plats de résistance des moyens
de transports. Il est possible de voler d’une branche à l’autre en
glissant sur une feuille, si l’on saisit le sens du vent.

Les musicien·nes jouent de la feuille en bas des branches, pour que le
son remonte jusqu’aux cimes. C’est une musique simple et douce, un long
sifflement continu que les joueur·euses modulent. Parfois, une voix
s’élève pour accompagner le chant des arbres.

Pour se déplacer dans l’arbre, iels apprennent très tôt l’escalade.
Placer ses mains dans les cales de l’écorce, caler les pieds lorsque les
prises manquent, viser le minimum de mouvements. C’est très joli à
regarder, on dirait des chats qui s’élancent à la surface d’un bâtiment,
gracieux, rapides, une danse qui a pour but de se déplacer. Dans les
passages les plus difficiles, des lianes de lichen tombent pour se
hisser jusqu’aux branches peu accessibles. Le même système de poulie
existe pour transporter les provisions.

Iels se nourrissent de mousse de fruits de noix de fleurs de miel de
puces mais jamais de pucerons de feuilles de neige d’œufs et de
champignons. Iels cuisinent dans de grandes marmites collectives en
s’assurant qu’il reste de chaque met en quantité suffisante pour tout le
monde. Iels savent que les repas sont sacrés et organisent de grands
festins où iels dansent, chantent, énoncent leurs récriminations ou
célèbrent les joies procurées par la vie en hauteur. Parfois l’un·e ou
l’autre a envie de silence et se retire dans les branches solitaires.

Iels vivent dans les abris formés par les cavités de l’écorce. Une
entaille devient un couloir traversant ou un puits de lumière. L’eau de
pluie est récoltée pour alimenter les bains collectifs. Chacun·e dispose
d’une chambre à soi de coton, d’une bibliothèque en forme de château de
carte et d’un petit artichaut qui contient l’eau et le savon pour se
laver les dents ou les mains. Les salons, les jardins, les salles de
bain sont communes. Il existe aussi des dortoirs collectifs, pour
celleux qui ont besoin de chuchoter pour s’endormir.

Lorsqu’iels s’ennuient, iels jouent à colin-maillard.
Quelqu’un·e passe un bandeau sur ses yeux et doit attraper les autres
joueur·euses sans les voir, à l’oreille. Il faut être très attentif·ve
car l’étroitesse de certaines branches pourrait provoquer des chutes
mortelles. Heureusement, iels savent deviner le bruit de l’air qui
signifie le vide et le contact du sol qui s’avance vers un abysse. Si
l’un·e d’entre iels venaient à se tromper, tout le groupe se
précipiterait pour l’entraîner vers le centre. Iels formeraient un amas
de corps couchés, jetés à terre, riraient très fort des risques défiés.
Alors commencerait le jeu des roulades.

La population comporte des personnes humaines revendiquant leur identité
masculine, des personnes humaines revendiquant leur identité féminine,
mais la grande majorité déclare ne pas s’intéresser aux catégories et
vivre, simplement. La population comporte des personnes non-humaines,
des chats, des chiens, émancipés du titre de compagnons, des insectes,
des lézards, des microbes, des oiseaux. Toustes discutent mangent
chassent vivent débattent rient dansent meurent ensemble.

Quand les torrents de pluie ou les oiseaux causent des dégâts dans les
zones d’habitat, iels se réunissent pour connaître l’étendue de ce qui a
été touché. Iels écoutent les victimes parler et notent ce qui a été
emporté. Les objets, outils, cabanes restantes sont mis en commun,
redistribués entre toustes afin de rétablir l’injustice causée. Si le
risque de famine est déclaré, chacun·e abandonne ses activités
saisonnières pour aider à la reconstitution de stocks suffisants. Les
plaies de l’arbre sont pansées collectivement.

On ne peut pas vraiment dire qu’iels sont un peuple d’agriculteur·ices
ou d’éleveur·euses, même si c’est un débat souvent abordé avec les
coccinelles. Iels récoltent la sève, prennent soin des arbres et des
pucerons, répandent le pollen et dispersent les fruits. Pour elleux, il
s’agit de trouver leur place dans un cycle plus grand. Iels refusent de
s’extraire ou de jouer un rôle prépondérant dans la Vie. Iels sont
monté·es dans les arbres pour prendre de la hauteur sur les erreurs et
le mode de vie de leurs ancêtres.

Iels ont détourné un bateau d’Amazon au large de la Méditerranée

pour cela il a fallu attendre longtemps dans la cale

caché·es

entre les rats et les containers

Ayo a dit « une odeur d’urine et de pétrole cramé »

Iel a dit

« pour cela il faut la complicité des dockers »

« des gars du port des salarié·es cela va être dur de rentrer à
l’intérieur »

et d’autres voix se sont élevées

jusqu’à la fin de la nuit

je ne comprenais pas toutes leurs décisions

mais je me souviens de

« entrer la nuit par effraction »

et je ne connaissais pas encore le sens du mot effraction

Iels ont volé plusieurs kilos de café

Le café c’est ce qu’iels mélangent le matin en tournant leurs cuillères

en disant qu’il ne faudrait plus mais puisque cela existe

puisque c’est produit

acheminé

et détourné sur nos terres

Pour détourner le bateau il a fallu jeter les hommes à la mer

de gros types comme ça dans l’eau en pleine vitesse

un grand plat

on ne m’a pas tout raconté

mais j’ai demandé à Ayo si les visages qu’on voyait au fond de la mer
étaient ceux des capitaines

Ayo a répondu « quels visages »

« tu es sûr·e que ce ne sont pas des poissons ? »

Ayo ne croit pas aux fantômes que je vois dans mes rêves

Iels ont proposé aux autres membres de l’équipage de les rejoindre

de nous rejoindre dans la zone désertée

un grand nombre a dit oui

les autres

on ne m’a pas tout raconté

Le café a un goût amer que je déteste et je hais encore plus l’odeur que
cela laisse dans la bouche d’Ayo le matin

le café laisse des traces brunes sur sa langue

et une salive qui donne à son haleine le parfum du passé

mais revenons-en à mon histoire

Ayo a fait partie des pirates sur le bateau

iel aimait s’endormir sur le pont

la tête tanguant doucement

iel a dit

« parfois nous éteignions toutes les lumières pour regarder les
étoiles »

« et ne pas êtres vu·es par les autres navires »

cela iel le rajoutait en marmonnant

avant le sommeil et les étoiles il y avait le rhum

« comme autrefois »

le rhum cela se trouve encore en quantité

et iels buvaient à se tordre les boyaux

iels aiment « se retourner le crâne »

« l’ébriété par laquelle commence le chaos »

Ayo et les autres buvaient et dansaient nu·es sur le pont du bateau

parce qu’iels pensaient

« si on se fait choper on ne pourra plus vivre »

et c’était une certitude perturbante

de savoir qu’iels pourraient mourir

pour quelques crimes et kilos de café

alors iels dansaient

quand la mer avale les cadavres on n’a pas vraiment les mains sales

je ne sais pas ce qu’il est advenu du bateau

le plus sage aurait été de le couler

sans doute

je ne sais par quelle étoile

iels sont arrivé·es sauf·ves

avec ces lourds sacs en toile

remplis de cette poudre qui sent si bon

si l’on renonce à la faire bouillir

sauf·ves et en chantant

comme si les visages ne me regardaient pas sous la mer

Depuis nous avons des équipes qui sillonnent les rivages et souvent ne
nous reviennent pas

qu’iels soient saisis par le soleil

fuient vers d’autres terres

ou par la police qui les confond souvent avec les migrant·es

qu’iels ramènent à bord de leurs navires pirates dès qu’iels en ont
l’occasion

par la police qui les enferme quand iels sont prises sur le coup

ou qu’iels fuient parce que des terres plus accueillantes il y en a
beaucoup

et peut-être que cette histoire ne parle pas tant de notre zone

que de tous les exo-mondes

les possibilités à emprunter

en hors-sentier par bateau dans le regard non-humain

peut-être qu’il ne s’agit pas d’aventures de pirates

ni de détournement de café

mais d’inviter d’autres perspectives

des voies des voix

que je prête aux auteurices

aux grands enfants de leurs âges

dans ma parole non-croyable

ma voix de personnage chimérique

à laquelle on ne croit pas (ce n’est pas possiblement un enfant)

mais la conjonction

d’un récit de visions

Par exemple

je pourrai dire

imaginer la lune dans le regard d’une vache

pour y déceler l’intelligence que nous souhaitons voir

lorsque nous les regardons paître

là

sagement

lorsque le noir et le blanc se mêlent encore en un gris insondable sur
la robe des veaux

et qu’iels nous regardent

avec l’âme du monde

les vaches gardent nos morts

enfin

Après cette incursion de voix allogènes

revenons-en à mon histoire

de pirates

et d’autres choses

je disais

les équipes sillonnent les rivages et souvent

ne nous reviennent pas

qu’iels soient saisis par le soleil

fuient vers d’autres terres

ou se fassent attraper par la police

moi je crois que s’iels nous quittent

c’est pour suivre les hirondelles

# Remerciements

Un grand merci à l'équipe de Framasoft et des Livres en Commun d'avoir cru dans cette aventure un peu folle, ce projet OVNI qui partait de l'amour pour atterrir on ne savait trop où. Un merci tout spécial à Yann, Goofy et Christophe. Grâce à votre écoute, vos conseils, votre capacité d'adaptation aussi, nous avons pu relier nos pensées pour construire cet essai (en forme de tentacules avec, nous croyons, l'amour comme pieuvre). 

Un grand merci à tous·tes les personnes qui ont nourri ce livre, en se prêtant au jeu des interviews, en nous livrant un peu de leur intimité et de leur vision du genre, de l'amour, de la famille. 

Merci à celleux qui ont passé du temps à discuter avec nous de nos propositions. Merci notamment à Nathan de ne pas (souvent) être d'accord avec nous, et donc d'enrichir nos réflexions (hihi).
Un grand merci aux relecteur·ices, dont ma maman et ma mamie pour leurs avis et leur patience. 

Et merci à vous d'avoir lu !