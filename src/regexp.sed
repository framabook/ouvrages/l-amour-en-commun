# Regexp qui s’appliquent dans le répertoire deflate
# Exemple (changement de chemin d’accès pour des fichiers d’images):
s	<p>荣霞燕	<p class="mg">荣霞燕	
s	<p>(éçè)	<p class="mg">(éçè)	
s	<p>L o l l e	<p class="vb">   L        o                   l l    e	
s	<p>a e i ou	<p class="vb-c">a     e      i       ou	
s	<p>le</p>	<p class="vb-r">le</p>	
s	<p>Lo lo lo lo lo ie	<p class="vb-c">Lo    lo    lo    lo    lo    ie	
s	<p>ielle</p>	<p class="vb-r">ielle</p>	
s	<p>avec Louie</p>	<p class="vb-r">avec Louie</p>	
s	<p>j’entretiens l’art du vide</p>	<p class="vb-r">j’entretiens l’art du vide</p>	
s	<p>dans les débats</p>	<p class="vb-r">dans les débats</p>	
s	<p>l l l l l l l l l l l l l l l l</p>	<p class="vb-c">l l l l l l l l l l l l l l l l</p>	
s	<p>D a n o u i e</p>	<p class="vb-c">D   a   n   o   u   i   e</p>	
s	<p>en gros</p>	<p class="vb-c">en gros</p>	
s	<p>iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii o</p>	<p class="vb-r">iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii o</p>	
s	<p>une forme inassouvie de la matière</p>	<p class="vb-r">une forme inassouvie de la matière</p>	
s	<p>ae</p>	<p class="vb-r">ae</p>	
s	<p>oe ie</p>	<p class="vb-r">oe ie</p>	
s	<p>ue le el</p>	<p class="vb-r">ue le el</p>	
s	<p>en gros</p>	<p class="vb-c">en gros</p>	
s	<p>il</p>	<p class="vb-r">il</p>	
s	<p>Dan</p>	<p class="vb-r">Dan</p>	
s	<p>Lou</p>	<p class="vb-r">Lou</p>	
s	<p>A E I O U</p>	<p class="vb-c">A E I O U</p>	
s	<p>signes de vie ou de chaos</p>	<p class="vb-r">signes de vie ou de chaos</p>	
s	<p>dans une existence lisse</p>	<p class="vb-r">dans une existence lisse</p>	
s	<p>i e l i e l i e l i el iel iel i e l l l l l lllllllllllll</p>	<p class="vb-c">i e l i e l i e l i el iel iel i e l l l l l lllllllllllll</p>	
s	<p>non plus avec les cheveux longs</p>	<p class="vb-r">non plus avec les cheveux longs</p>	
s	<p>l o n lonlonlon</p>	<p class="vb-r">l o n lonlonlon</p>	
s	<p>mais avec le genre zéro</p>	<p class="vb-r">mais avec le genre zéro</p>	
s	<p>O</p>	<p class="vb-c">O</p>	
s	<p>.</p>	<p class="vb-c">.</p>	
s	<p><del>lui</del>elle <em>a</em>el</p>	<p class="vb-c"><del>lui</del>elle <em>a</em>el</p>	
s	<p>iel</p>	<p class="vb-r">iel</p>	
s	<p>sur une nouvelle page</p>	<p class="vb-r">sur une nouvelle page</p>	
s	<p>Chère Monique,</p>	<p class="pb">Chère Monique,</p>	
s	<section id="féminaire-de-petite-tête-jour-o" class="level2">	<section id="féminaire-de-petite-tête-jour-o" class="level2 pb">	
s	<section id="ayo" class="level2">	<section id="ayo" class="level2 pb">	
s	<section id="féminaire-dayo-non-daté" class="level2">	<section id="féminaire-dayo-non-daté" class="level2 pb">	
s	<section id="elio" class="level2">	<section id="elio" class="level2 pb">	
s	<section id="féminaire-delio-jourarbre" class="level2">	<section id="féminaire-delio-jourarbre" class="level2 pb">	
s	<p>Iels ont détourné un bateau d’Amazon au large de la Méditerranée</p>	<p class="pb">Iels ont détourné un bateau d’Amazon au large de la Méditerranée</p>	
s	<p>À tous·tes celleux que nous aimons,<br />	<p class="pb vb-r">À tous·tes celleux que nous aimons,<br />	
s	<h1 class="unnumbered">L’amour en commun</h1>	<h1 class="unnumbered spb">L’amour en commun</h1>	