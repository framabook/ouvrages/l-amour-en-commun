[Communiqué de presse, 1/10/2024]

L’amour en commun, par Margaux Lallemant et Thimothé Bodot

Comment libérer l'amour des carcans du couple et de la famille pour en faire un projet collectif ? Cet essai explore les effets du patriarcat et du capitalisme sur nos relations et montre que réinventer l'amour ne peut se faire isolément.

À travers une analyse de dynamiques interpersonnelles —amour romantique, amitié— en relation avec les structures sociales qui les façonnent — genre, soin, travail, rapport au vivant —les auteur-ices interrogent  les oppressions qui traversent l’amour au sens large. 

À la recherche de « contre-mondes » où l'affaiblissement du système capitaliste semble possible, iels s'intéressent à la piraterie et la ZAD de Notre-Dame-Des-Landes, dans leurs mythes et leurs écueils.  

Une invitation à envisager l'amour sous un jour nouveau, comme un espace d'émancipation et de création collective.

Pour en savoir plus:
 - point d’étape avec les auteurices : https://framablog.org/2023/04/25/point-detape-pour-lecriture-a-deux-mains-de-lamour-en-commun/

Vous pouvez librement télécharger le roman :

    EPUB : https://asso.framasoft.org/dolo/h/amour-en-commun

Ressources :

    Couverture : L_amour_en_commun.jpg / L_amour_en_commun.svg

Auteurices : Margaux Lallemant, Thimothé Bodo  
Titre : L’amour en commun  
Licence : CC-BY-SA  
ISBN : 979-10-92674-36-1  
Éditeur : Framasoft, Des livres en communs, 2024  
Format : électronique  










