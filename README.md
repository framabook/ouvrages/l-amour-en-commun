Ce dépôt est basé sur l’utilisation du [Système de publication Framabook](https://framagit.org/framabook/systeme-publication-framabook).

Titre :

- L’amour en commun

Auteurices : 

- Margaux Lallemant
- Thimothé Bodo

Éditeur :

- Yann Kervran

Fabricateur :

- Yann Kervran

Date de sortie :

- 01/10/2024

ISBN :

- 979-10-92674-36-1

Licence de l’ouvrage :

- [Creative Commons BY SA](https://creativecommons.org/licenses/by-sa/3.0/fr/)

Sujet:

- Lecture matérialiste et féministe de notre rapport à l'autre

Résumé (une ou deux phrases):

- Redéfinir l’amour, le penser comme un commun, en faire un moteur d’engagement qui permette de penser et construire une alternative à la société capitaliste

Version:

- 1.0
